package com.company.bloco5_testes;

import com.company.bloco5.Array;
import com.company.bloco5.TwoDimensionalArray;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class Exercise3_tests {

    @Test
    void createTwoDimensionalArrayWithNoValues() {
        TwoDimensionalArray matrix = new TwoDimensionalArray();

        int[][] expected = {};

        assertArrayEquals(matrix.toArray(), expected);
    }

    @Test
    void createSquareTwoDimensionalArrayWithSomeIntValues() {
        int[][] values = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        TwoDimensionalArray matrix = new TwoDimensionalArray(values);

        int[][] expected = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};

        assertArrayEquals(matrix.toArray(), expected);
    }

    @Test
    void createSquareTwoDimensionalArrayWithSomeArrayValues() {
        int[] values1 = {1, 2, 3};
        Array array1 = new Array(values1);
        int[] values2 = {4, 5, 6};
        Array array2 = new Array(values2);
        int[] values3 = {7, 8, 9};
        Array array3 = new Array(values3);

        Array[] arrayFinal = new Array[3];
        arrayFinal[0] = array1;
        arrayFinal[1] = array2;
        arrayFinal[2] = array3;

        TwoDimensionalArray matrix = new TwoDimensionalArray(arrayFinal);

        int[][] expected = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};

        assertArrayEquals(matrix.toArray(), expected);
    }

    @Test
    void createRectangular1TwoDimensionalArrayWithSomeIntValues() {
        int[][] values = {{1, 2, 3}, {4, 5, 6}};
        TwoDimensionalArray matrix = new TwoDimensionalArray(values);

        int[][] expected = {{1, 2, 3}, {4, 5, 6}};

        assertArrayEquals(matrix.toArray(), expected);
    }

    @Test
    void createRectangular2TwoDimensionalArrayWithSomeIntValues() {
        int[][] values = {{1, 2}, {3, 4}, {5, 6}};
        TwoDimensionalArray matrix = new TwoDimensionalArray(values);

        int[][] expected = {{1, 2}, {3, 4}, {5, 6}};

        assertArrayEquals(matrix.toArray(), expected);
    }

    @Test
    void createRectangular1TwoDimensionalArrayWithSomeArrayValues() {
        int[] values1 = {1, 2, 3};
        Array array1 = new Array(values1);
        int[] values2 = {4, 5, 6};
        Array array2 = new Array(values2);

        Array[] arrayFinal = new Array[2];
        arrayFinal[0] = array1;
        arrayFinal[1] = array2;

        TwoDimensionalArray matrix = new TwoDimensionalArray(arrayFinal);

        int[][] expected = {{1, 2, 3}, {4, 5, 6}};

        assertArrayEquals(matrix.toArray(), expected);
    }

    @Test
    void createRectangular3TwoDimensionalArrayWithSomeArrayValues() {
        int[] values1 = {1, 2};
        Array array1 = new Array(values1);
        int[] values2 = {4, 5};
        Array array2 = new Array(values2);
        int[] values3 = {7, 8};
        Array array3 = new Array(values3);

        Array[] arrayFinal = new Array[3];
        arrayFinal[0] = array1;
        arrayFinal[1] = array2;
        arrayFinal[2] = array3;

        TwoDimensionalArray matrix = new TwoDimensionalArray(arrayFinal);

        int[][] expected = {{1, 2}, {4, 5}, {7, 8}};

        assertArrayEquals(matrix.toArray(), expected);
    }

    @Test
    void createIrregularTwoDimensionalArrayWithSomeIntValues() {
        int[][] values = {{1, 2}, {4, 5, 6}, {0, 34, 56, 78}};
        TwoDimensionalArray matrix = new TwoDimensionalArray(values);

        int[][] expected = {{1, 2}, {4, 5, 6}, {0, 34, 56, 78}};

        assertArrayEquals(matrix.toArray(), expected);
    }

    @Test
    void createIrregularTwoDimensionalArrayWithSomeArrayValues() {
        int[] values1 = {1, 2};
        Array array1 = new Array(values1);
        int[] values2 = {4, 5, 6};
        Array array2 = new Array(values2);
        int[] values3 = {0, 34, 56, 78};
        Array array3 = new Array(values3);

        Array[] arrayFinal = new Array[3];
        arrayFinal[0] = array1;
        arrayFinal[1] = array2;
        arrayFinal[2] = array3;

        TwoDimensionalArray matrix = new TwoDimensionalArray(arrayFinal);

        int[][] expected = {{1, 2}, {4, 5, 6}, {0, 34, 56, 78}};

        assertArrayEquals(matrix.toArray(), expected);
    }

    @Test
    void createTwoDimensionalArrayWithSomeIntValuesAndAnEmptyLine() {
        int[][] values = {{1, 2}, {}, {0, 34, 56, 78}};
        TwoDimensionalArray matrix = new TwoDimensionalArray(values);

        int[][] expected = {{1, 2}, {}, {0, 34, 56, 78}};

        assertArrayEquals(matrix.toArray(), expected);
    }

    @Test
    void createTwoDimensionalArrayWithSomeArrayValuesAndAnEmptyLine() {
        int[] values1 = {1, 2};
        Array array1 = new Array(values1);
        int[] values2 = {};
        Array array2 = new Array(values2);
        int[] values3 = {0, 34, 56, 78};
        Array array3 = new Array(values3);

        Array[] arrayFinal = new Array[3];
        arrayFinal[0] = array1;
        arrayFinal[1] = array2;
        arrayFinal[2] = array3;

        TwoDimensionalArray matrix = new TwoDimensionalArray(arrayFinal);

        int[][] expected = {{1, 2}, {}, {0, 34, 56, 78}};

        assertArrayEquals(matrix.toArray(), expected);
    }

    @Test
    void createNullTwoDimensionalIntArray() {
        int[][] values = null;
        assertThrows(NullPointerException.class, () -> new TwoDimensionalArray(values));
    }

    @Test
    void createNullTwoDimensionalArrayOfArrays() {
        Array[] arrayFinal = null;

        assertThrows(NullPointerException.class, () -> new TwoDimensionalArray(arrayFinal));
    }

    @Test
    void createCopyOfTwoDimensionalArrayWithSomeValues() {
        int[][] values = {{1, 2}, {3, 4, 5}, {0, 34, 56, 78}};
        TwoDimensionalArray expected = new TwoDimensionalArray(values);

        TwoDimensionalArray result = expected.createCopy();

        assertEquals(expected, result);
    }

    @Test
    void createCopyOfTwoDimensionalArrayWithSomeValuesAndAnEmptyRow() {
        int[][] values = {{1, 2}, {3, 4, 5}, {}};
        TwoDimensionalArray expected = new TwoDimensionalArray(values);

        TwoDimensionalArray result = expected.createCopy();

        assertEquals(expected, result);
    }

    @Test
    void addElementToTwoDimensionalArrayRow2() {
        int[][] values = {{45, 33, 76, 45}, {34, 342, 6, 3, 234}, {32, 4}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);
        int[][] expectedValues = {{45, 33, 76, 45}, {34, 342, 6, 3, 234, 1}, {32, 4}};
        TwoDimensionalArray expected = new TwoDimensionalArray(expectedValues);
        int rowIndex = 1;
        int newValue = 1;

        TwoDimensionalArray result = original.addElementToRow(rowIndex, newValue);

        assertEquals(expected, result);
    }

    @Test
    void addElementToTwoDimensionalArrayRow1() {
        int[][] values = {{1, 2, 3}, {5, 6, 7}, {8, 9, 10}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);
        int[][] expectedValues = {{1, 2, 3, 4}, {5, 6, 7}, {8, 9, 10}};
        TwoDimensionalArray expected = new TwoDimensionalArray(expectedValues);
        int rowIndex = 0;
        int newValue = 4;

        TwoDimensionalArray result = original.addElementToRow(rowIndex, newValue);

        assertEquals(expected, result);
    }

    @Test
    void addElementToTwoDimensionalArrayRowNotExistent() {
        int[][] values = {{1, 2, 3}, {5, 6, 7}, {8, 9, 10}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);
        int rowIndex = 3;
        int newValue = 4;

        assertThrows(ArrayIndexOutOfBoundsException.class, () -> original.addElementToRow(rowIndex, newValue));
    }

    @Test
    void addElementToTwoDimensionalArrayEmptyRow() {
        int[][] values = {{1, 2, 3}, {5, 6, 7}, {}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);
        int[][] expectedValues = {{1, 2, 3}, {5, 6, 7}, {8}};
        TwoDimensionalArray expected = new TwoDimensionalArray(expectedValues);
        int rowIndex = 2;
        int newValue = 8;

        TwoDimensionalArray result = original.addElementToRow(rowIndex, newValue);

        assertEquals(expected, result);
    }

    @Test
    void addElementToTwoDimensionalEmptyArrayRowThree() {
        int[][] values = {{}, {}, {}, {}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);
        int[][] expectedValues = {{}, {}, {}, {0}};
        TwoDimensionalArray expected = new TwoDimensionalArray(expectedValues);
        int rowIndex = 3;
        int newValue = 0;

        TwoDimensionalArray result = original.addElementToRow(rowIndex, newValue);

        assertEquals(expected, result);
    }

    @Test
    void addElementToTwoDimensionalArrayNegativeIndex() {
        int[][] values = {{2, 3}, {1, 2}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);
        int rowIndex = -1;
        int newValue = 0;

        assertThrows(ArrayIndexOutOfBoundsException.class, () -> original.addElementToRow(rowIndex, newValue));
    }

    @Test
    void removeFirstOccurrenceOfValueThreeInTwoDimensionalArrayWithSomeValues() {
        int[][] values = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);
        int[][] expectedValues = {{1, 2}, {4, 5, 6}, {7, 8, 9}};
        TwoDimensionalArray expected = new TwoDimensionalArray(expectedValues);
        int valueToRemove = 3;

        TwoDimensionalArray result = original.removeFirstOccurrenceOfAValue(valueToRemove);

        assertEquals(expected, result);
    }

    @Test
    void removeFirstOccurrenceOfValueFiveInTwoDimensionalArrayWithSomeValues() {
        int[][] values = {{1, 2, 3}, {4, 5, 6}, {7, 5}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);
        int[][] expectedValues = {{1, 2, 3}, {4, 6}, {7, 5}};
        TwoDimensionalArray expected = new TwoDimensionalArray(expectedValues);
        int valueToRemove = 5;

        TwoDimensionalArray result = original.removeFirstOccurrenceOfAValue(valueToRemove);

        assertEquals(expected, result);
    }

    @Test
    void removeFirstOccurrenceOfValueZeroInTwoDimensionalArrayWhereAllValuesAreZero() {
        int[][] values = {{0, 0, 0}, {0, 0, 0}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);
        int[][] expectedValues = {{0, 0}, {0, 0, 0}};
        TwoDimensionalArray expected = new TwoDimensionalArray(expectedValues);
        int valueToRemove = 0;

        TwoDimensionalArray result = original.removeFirstOccurrenceOfAValue(valueToRemove);

        assertEquals(expected, result);
    }

    @Test
    void removeFirstOccurrenceOfValueMinusOneInTwoDimensionalArrayWithSomeNegativeValues() {
        int[][] values = {{3, -4, 5}, {5, 2, -3}, {5, 2, -1}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);
        int[][] expectedValues = {{3, -4, 5}, {5, 2, -3}, {5, 2}};
        TwoDimensionalArray expected = new TwoDimensionalArray(expectedValues);
        int valueToRemove = -1;

        TwoDimensionalArray result = original.removeFirstOccurrenceOfAValue(valueToRemove);

        assertEquals(expected, result);
    }

    @Test
    void isMatrixEmptyTrueSixRows() {
        int[][] values = {{}, {}, {}, {}, {}, {}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);

        assertTrue(original.isMatrixEmpty());
    }

    @Test
    void isMatrixEmptyTrueOneRow() {
        int[][] values = {{}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);

        assertTrue(original.isMatrixEmpty());
    }

    @Test
    void isMatrixEmptyFalseTwoRowsLastOneIsEmpty() {
        int[][] values = {{1, 2}, {}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);

        assertFalse(original.isMatrixEmpty());
    }

    @Test
    void isMatrixEmptyFalseTwoRowsFirstOneIsEmpty() {
        int[][] values = {{}, {1, 2}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);

        assertFalse(original.isMatrixEmpty());
    }

    @Test
    void getBiggestValueFromMatrixWhereBiggestValueIsPresentInAllRows() {
        int[][] values = {{3, 4, 5}, {5, 2, 3}, {5, 2, 1}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);
        int expected = 5;

        int result = original.getBiggestValueFromMatrix();

        assertEquals(expected, result);
    }

    @Test
    void getBiggestValueFromMatrixBiggestValueIsInLastRow() {
        int[][] values = {{3, 4, 5}, {5, 2, 3}, {10, 2, 1}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);
        int expected = 10;

        int result = original.getBiggestValueFromMatrix();

        assertEquals(expected, result);
    }

    @Test
    void getBiggestValueFromMatrixWithSomeNegativeValues() {
        int[][] values = {{3, -4, 0}, {5, 2, -3}, {-10, 2, -1}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);
        int expected = 5;

        int result = original.getBiggestValueFromMatrix();

        assertEquals(expected, result);
    }

    @Test
    void getBiggestValueFromMatrixWithAnEmptyRow() {
        int[][] values = {{3, -4, 0}, {}, {-10, 2, -1}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);
        int expected = 3;

        int result = original.getBiggestValueFromMatrix();

        assertEquals(expected, result);
    }

    @Test
    void getSmallestValueFromMatrixWhereSmallestValueIsPresentInAllRows() {
        int[][] values = {{3, 4, 5}, {5, 6, 3}, {5, 5, 3}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);
        int expected = 3;

        int result = original.getSmallestValueFromMatrix();

        assertEquals(expected, result);
    }

    @Test
    void getSmallestValueFromMatrixSmallestValueIsInLastRow() {
        int[][] values = {{3, 4, 5}, {5, 2, 3}, {10, 2, 1}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);
        int expected = 1;

        int result = original.getSmallestValueFromMatrix();

        assertEquals(expected, result);
    }

    @Test
    void getSmallestValueFromMatrixWithSomeNegativeValues() {
        int[][] values = {{3, -4, 0}, {5, 2, -3}, {-10, 2, -1}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);
        int expected = -10;

        int result = original.getSmallestValueFromMatrix();

        assertEquals(expected, result);
    }

    @Test
    void getSmallestValueFromMatrixWithAnEmptyRow() {
        int[][] values = {{}, {3, -4, 0}, {-10, 2, -1}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);
        int expected = -10;

        int result = original.getSmallestValueFromMatrix();

        assertEquals(expected, result);
    }

    @Test
    void getElementsMeanFromMatrixWithSomePositiveValues() {
        int[][] values = {{3, 4, 5}, {5, 2, 3}, {10, 2, 1}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);
        double expected = 3.89;

        double result = original.getMatrixElementsMean();

        assertEquals(expected, result);
    }

    @Test
    void getElementsMeanFromMatrixWithSomeNegativeValues() {
        int[][] values = {{-3, -4, 5}, {5, -2, 3}, {-10, 2, 1}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);
        double expected = -0.33;

        double result = original.getMatrixElementsMean();

        assertEquals(expected, result);
    }

    @Test
    void getElementsMeanFromIrregularMatrixWhereAllValuesAreZero() {
        int[][] values = {{0, 0, 0}, {0}, {0, 0}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);
        double expected = 0;

        double result = original.getMatrixElementsMean();

        assertEquals(expected, result);
    }

    @Test
    void getElementsMeanFromMatrixWithAnEmptyRow() {
        int[][] values = {{1, 2, 3}, {}, {4, 5}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);
        double expected = 3;

        double result = original.getMatrixElementsMean();

        assertEquals(expected, result);
    }

    @Test
    void getElementsMeanFromEmptyMatrix() {
        int[][] values = {{}, {}, {}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);

        assertThrows(IllegalArgumentException.class, original::getMatrixElementsMean);
    }

    @Test
    void getSumOfEachMatrixRowFromAquareMatrixWithSomeValues() {
        int[][] values = {{1, 2, 3}, {4, 5, 6}, {4, 5, 6}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);
        int[] expectedValues = {6, 15, 15};
        Array expected = new Array(expectedValues);

        Array result = original.getSumOfEachMatrixRow();

        assertEquals(expected, result);
    }

    @Test
    void getSumOfEachMatrixRowFromRectangularMatrixWithSomeValues() {
        int[][] values = {{1, 2, 3}, {4, 5, 6}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);
        int[] expectedValues = {6, 15};
        Array expected = new Array(expectedValues);

        Array result = original.getSumOfEachMatrixRow();

        assertEquals(expected, result);
    }

    @Test
    void getSumOfEachMatrixRowFromIrregularMatrixWithSomeValues() {
        int[][] values = {{1, 2, 3}, {4, 5}, {34, 56, 78, 95}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);
        int[] expectedValues = {6, 9, 263};
        Array expected = new Array(expectedValues);

        Array result = original.getSumOfEachMatrixRow();

        assertEquals(expected, result);
    }

    @Test
    void getSumOfEachMatrixRowFromIrregularMatrixWithAnEmptyRow() {
        int[][] values = {{1, 2, 3}, {}, {34, 56, 78, 95}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);
        int[] expectedValues = {6, 0, 263};
        Array expected = new Array(expectedValues);

        Array result = original.getSumOfEachMatrixRow();

        assertEquals(expected, result);
    }

    @Test
    void getSumOfEachMatrixRowFromEmptyMatrix() {
        int[][] values = {{}, {}, {}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);
        int[] expectedValues = {0, 0, 0};
        Array expected = new Array(expectedValues);

        Array result = original.getSumOfEachMatrixRow();

        assertEquals(expected, result);
    }

    @Test
    void getSumOfEachMatrixRowFromMatrixWithSomeNegativeNumbers() {
        int[][] values = {{0, -1, -2}, {-3}, {-1, -2, -3}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);
        int[] expectedValues = {-3, -3, -6};
        Array expected = new Array(expectedValues);

        Array result = original.getSumOfEachMatrixRow();

        assertEquals(expected, result);
    }

    @Test
    void getSumOfEachMatrixColumnFromSquareMatrixWithSomeNumbers() {
        int[][] values = {{0, 1, 2}, {3, 4, 5}, {6, 7, 8}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);
        int[] expectedValues = {9, 12, 15};
        Array expected = new Array(expectedValues);

        Array result = original.getSumOfEachMatrixColumn();

        assertEquals(expected, result);
    }

    @Test
    void getSumOfEachMatrixColumnFromRectangularMatrixWithSomePositiveNumbers() {
        int[][] values = {{0, 1, 2}, {3, 4, 5}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);
        int[] expectedValues = {3, 5, 7};
        Array expected = new Array(expectedValues);

        Array result = original.getSumOfEachMatrixColumn();

        assertEquals(expected, result);
    }

    @Test
    void getSumOfEachMatrixColumnFromRectangularMatrixWithSomeNegativeNumbers() {
        int[][] values = {{0, -1, 2}, {-3, -4, -5}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);
        int[] expectedValues = {-3, -5, -3};
        Array expected = new Array(expectedValues);

        Array result = original.getSumOfEachMatrixColumn();

        assertEquals(expected, result);
    }


    @Test
    void getSumOfEachMatrixColumnFromIrregularMatrix() {
        int[][] values = {{23, 45, 67}, {12, 34}, {67, 89, 56, 67}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);
        int[] expectedValues = {102, 168, 123, 67};
        Array expected = new Array(expectedValues);

        Array result = original.getSumOfEachMatrixColumn();

        assertEquals(expected, result);
    }

    @Test
    void getSumOfEachMatrixColumnFromEmptyMatrix() {
        int[][] values = {{}, {}, {}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);
        int[] expectedValues = {};
        Array expected = new Array(expectedValues);

        Array result = original.getSumOfEachMatrixColumn();

        assertEquals(expected, result);
    }

    @Test
    void getIndexOfRowWithHighestValueFromIrregularMatrix() {
        int[][] values = {{123, 34, 56}, {2, 500}, {44, 8, 2}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);
        int expectedIndex = 1;

        int result = original.getIndexOfRowWithHighestValue();

        assertEquals(expectedIndex, result);
    }

    @Test
    void getIndexOfRowWithHighestValueFromSquareMatrix() {
        int[][] values = {{13, 34, 56}, {2, 3, 5}, {44, 8, 62}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);
        int expectedIndex = 2;

        int result = original.getIndexOfRowWithHighestValue();

        assertEquals(expectedIndex, result);
    }

    @Test
    void getIndexOfRowWithHighestValueFromRectangularMatrix() {
        int[][] values = {{13, 34}, {2, 3}, {44, 8}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);
        int expectedIndex = 2;

        int result = original.getIndexOfRowWithHighestValue();

        assertEquals(expectedIndex, result);
    }

    @Test
    void getIndexOfRowWithHighestValueFromEMptyMatrix() {
        int[][] values = {{}, {}, {}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);
        int expectedIndex = 0;

        int result = original.getIndexOfRowWithHighestValue();

        assertEquals(expectedIndex, result);
    }

    @Test
    void getIndexOfRowWithHighestValueFromMatrixWithSomeNegativeValues() {
        int[][] values = {{-3, -2, -3}, {-7, -8, -5}, {-3, -9, -12}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);
        int expectedIndex = 0;

        int result = original.getIndexOfRowWithHighestValue();

        assertEquals(expectedIndex, result);
    }

    @Test
    void verifyIfMatrixSizeThreeIsSquareTrue() {
        int[][] values = {{3, 2, 3}, {7, 8, 5}, {3, 9, 12}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);

        boolean result = original.isMatrixSquare();

        assertTrue(result);
    }

    @Test
    void verifyIfMatrixSizeFiveIsSquareTrue() {
        int[][] values = {{3, 2, 3, 1, 2}, {7, 8, 5, 5, 6}, {3, 9, 12, 33, 2}, {7, 8, 5, 5, 6}, {7, 8, 5, 5, 6}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);

        boolean result = original.isMatrixSquare();

        assertTrue(result);
    }

    @Test
    void verifyIfRectangularMatrixIsSquareFalse() {
        int[][] values = {{3, 2, 3, 1, 2}, {7, 8, 5, 5, 6}, {3, 9, 12, 33, 2}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);

        boolean result = original.isMatrixSquare();

        assertFalse(result);
    }

    @Test
    void verifyIfIrregularMatrixIsSquareFalse() {
        int[][] values = {{3, 2, 3}, {7, 8, 5, 5, 6}, {3, 9, 12, 33}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);

        boolean result = original.isMatrixSquare();

        assertFalse(result);
    }

    @Test
    void verifyIfEmptyMatrixIsSquareFalse() {
        int[][] values = {{}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);

        boolean result = original.isMatrixSquare();

        assertFalse(result);
    }

    @Test
    void isMatrixSizeThreeSymmetricTrue() {
        int[][] values = {{1, 5, 9}, {5, 3, 8}, {9, 8, 7}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);

        boolean result = original.isMatrixSymmetric();

        assertTrue(result);
    }

    @Test
    void isMatrixSizeFiveSymmetricTrue() {
        int[][] values = {{1, 2, 3, 4, 5}, {2, 3, 4, 5, 6}, {3, 4, 5, 6, 7}, {4, 5, 6, 7, 8}, {5, 6, 7, 8, 9}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);

        boolean result = original.isMatrixSymmetric();

        assertTrue(result);
    }

    @Test
    void isMatrixSizeThreeSymmetricFalse() {
        int[][] values = {{1, 2, 3}, {2, 3, 4}, {5, 6, 7}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);

        boolean result = original.isMatrixSymmetric();

        assertFalse(result);
    }

    @Test
    void isMatrixSizeFourSymmetricFalse() {
        int[][] values = {{1, 2, 3, 6}, {2, 3, 4, 4}, {5, 6, 7, 3}, {4, 5, 6, 7}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);

        boolean result = original.isMatrixSymmetric();

        assertFalse(result);
    }

    @Test
    void isIrregularMatrixSymmetricFalse() {
        int[][] values = {{1, 2, 3, 6}, {2, 3, 4}, {5, 6}, {4, 5, 6, 7}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);

        assertThrows(IllegalArgumentException.class, () -> original.isMatrixSymmetric());

    }

    @Test
    void isEmptyMatrixSymmetricFalse() {
        int[][] values = {{}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);

        assertThrows(IllegalArgumentException.class, () -> original.isMatrixSymmetric());
    }

    @Test
    void countNrMainDiagonalElementsSquareMatrixSizeThree() {
        int[][] values = {{1, 2, 3}, {2, 3, 4}, {5, 6, 7}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);
        int expected = 3;
        int result = original.countNrMainDiagonalElements();

        assertEquals(expected, result);
    }

    @Test
    void countNrMainDiagonalElementsSquareMatrixSizeTwo() {
        int[][] values = {{1, 2}, {2, 3}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);
        int expected = 2;
        int result = original.countNrMainDiagonalElements();

        assertEquals(expected, result);
    }

    @Test
    void countNrMainDiagonalElementsMatrixNotSquare() {
        int[][] values = {{1, 2}, {2, 3}, {1, 3}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);
        int expected = -1;
        int result = original.countNrMainDiagonalElements();

        assertEquals(expected, result);
    }

    @Test
    void countNrMainDiagonalElementsEmptyMatrix() {
        int[][] values = {{}, {}, {}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);
        int expected = -1;
        int result = original.countNrMainDiagonalElements();

        assertEquals(expected, result);
    }

    @Test
    void isMainDiagonalEqualToSecondaryMatrixSizeThreeDiagonalValuesAreAllOneTrue() {
        int[][] values = {{1, 2, 1}, {2, 1, 2}, {1, 2, 1}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);
        boolean result = original.isMainDiagonalEqualToSecondary();

        assertTrue(result);
    }

    @Test
    void isMainDiagonalEqualToSecondaryMatrixSizeThreeDiagonalValuesAreAllDifferentTrue() {
        int[][] values = {{1, 2, 1}, {3, 2, 3}, {3, 2, 3}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);
        boolean result = original.isMainDiagonalEqualToSecondary();

        assertTrue(result);
    }

    @Test
    void isMainDiagonalEqualToSecondaryMatrixSizeThreeSomeDiagonalValuesAreNegativeTrue() {
        int[][] values = {{-1, 2, -1}, {3, -2, 3}, {-3, 2, -3}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);
        boolean result = original.isMainDiagonalEqualToSecondary();

        assertTrue(result);
    }

    @Test
    void isMainDiagonalEqualToSecondaryMatrixSizeTwoFalse() {
        int[][] values = {{-1, 2}, {3, -2}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);
        boolean result = original.isMainDiagonalEqualToSecondary();

        assertFalse(result);
    }

    @Test
    void isMainDiagonalEqualToSecondaryMatrixSizeThreeFalse() {
        int[][] values = {{-1, 2, 4}, {3, -2, 5}, {2, 3, 4}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);
        boolean result = original.isMainDiagonalEqualToSecondary();

        assertFalse(result);
    }

    @Test
    void isMainDiagonalEqualToSecondaryIrregularMatrixFalse() {
        int[][] values = {{-1, 2}, {3, -2, 5}, {2, 3, 4, 5}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);
        boolean result = original.isMainDiagonalEqualToSecondary();

        assertFalse(result);
    }

    @Test
    void isMainDiagonalEqualToSecondaryRectangularMatrixTrue1() {
        int[][] values = {{-1, -1}, {-2, -2}, {4, 5}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);
        boolean result = original.isMainDiagonalEqualToSecondary();

        assertTrue(result);
    }

    @Test
    void isMainDiagonalEqualToSecondaryRectangularMatrixTrue2() {
        int[][] values = {{-1, 5, -1}, {-2, -2, -1}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);
        boolean result = original.isMainDiagonalEqualToSecondary();

        assertTrue(result);
    }

    @Test
    void isMainDiagonalEqualToSecondaryEmptyMatrixTrue() {
        int[][] values = {{}, {}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);
        boolean result = original.isMainDiagonalEqualToSecondary();

        assertTrue(result);
    }

    @Test
    void getElementRowWithMoreDigitsThanAverageInMatrixWithTwoElementsWithDigitsGreaterThanAverage() {
        int[][] values = {{3, 5, 3456234, 235467}, {3, 4, 123124}, {34146, 5}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);
        int[] expectedValues = {3456234, 235467, 123124, 34146};
        Array expected = new Array(expectedValues);
        Array result = original.getElementsWithMoreDigitsThanAverage();

        assertEquals(expected, result);
    }

    @Test
    void getElementWithMoreDigitsThanAverageInMatrixWithOneElementWithDigitsGreaterThanAverage() {
        int[][] values = {{3, 5, 3, 2}, {3, 4, 4}, {34146, 5}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);
        int[] expectedValues = {34146};
        Array expected = new Array(expectedValues);
        Array result = original.getElementsWithMoreDigitsThanAverage();

        assertEquals(expected, result);
    }

    @Test
    void getElementWithMoreDigitsThanAverageInMatrixWithOneNegativeElementWithDigitsGreaterThanAverage() {
        int[][] values = {{3, 5, 3, 2}, {3, 4, 4}, {-34146, 5}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);
        int[] expectedValues = {-34146};
        Array expected = new Array(expectedValues);
        Array result = original.getElementsWithMoreDigitsThanAverage();

        assertEquals(expected, result);
    }

    @Test
    void getElementWithMoreDigitsThanAverageInEmptyMatrix() {
        int[][] values = {{}, {}, {}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);
        int[] expectedValues = {};
        Array expected = new Array(expectedValues);
        Array result = original.getElementsWithMoreDigitsThanAverage();

        assertEquals(expected, result);
    }

    @Test
    void getElementWithMoreDigitsThanAverageInMatrixWherePercentageIsTheSameInAllValues() {
        int[][] values = {{1, 2}, {3, 4}, {5, 6}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);
        int[] expectedValues = {};
        Array expected = new Array(expectedValues);
        Array result = original.getElementsWithMoreDigitsThanAverage();

        assertEquals(expected, result);
    }

    @Test
    void getElementWithMoreDigitsThanAverageInMatrixWherePercentageInEachValueIsDifferent() {
        int[][] values = {{3654, 3, 68432, -8}, {6555, 54, 98273, -8}, {32424, 545, 2345, 234}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);
        int[] expectedValues = {3654, 68432, 6555, 98273, 32424, 2345};
        Array expected = new Array(expectedValues);
        Array result = original.getElementsWithMoreDigitsThanAverage();

        assertEquals(expected, result);
    }

    @Test
    void getElementsWithAHigherPercentageOfEvenDigitsThanAverageFromMatrixWithSixElementsWithAHigherPercentageOfEvenDigits() {
        int[][] values = {{456, 3, 68432, 8}, {6555, 54, 98273, 8}, {32424, 545, 2345, 234}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);
        int[] expectedValues = {456, 68432, 8, 8, 32424, 234};
        Array expected = new Array(expectedValues);
        Array result = original.getElementsWithAHigherPercentageOfEvenDigitsThanAverage();

        assertEquals(expected, result);
    }

    @Test
    void getElementsWithAHigherPercentageOfEvenDigitsThanAverageFromMatrixWithFiveElementsWithAHigherPercentageOfEvenDigits() {
        int[][] values = {{666, 3, 5, 9}, {6555, 54, 7951, 8}, {123, 545, 2345, 34}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);
        int[] expectedValues = {666, 54, 8, 2345, 34};
        Array expected = new Array(expectedValues);
        Array result = original.getElementsWithAHigherPercentageOfEvenDigitsThanAverage();

        assertEquals(expected, result);
    }


    @Test
    void getElementsWithAHigherPercentageOfEvenDigitsThanAverageFromMatrixWithSomeNegativeValues() {
        int[][] values = {{666, -3, 5, 9}, {6555, -54, 7951, -8}, {123, 545, -2345, 34}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);
        int[] expectedValues = {666, -54, -8, -2345, 34};
        Array expected = new Array(expectedValues);
        Array result = original.getElementsWithAHigherPercentageOfEvenDigitsThanAverage();

        assertEquals(expected, result);
    }

    @Test
    void getElementsWithAHigherPercentageOfEvenDigitsThanAverageFromEmptyMatrix() {
        int[][] values = {{}, {}, {}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);
        int[] expectedValues = {};
        Array expected = new Array(expectedValues);
        Array result = original.getElementsWithAHigherPercentageOfEvenDigitsThanAverage();

        assertEquals(expected, result);
    }

    @Test
    void getElementsWithAHigherPercentageOfEvenDigitsThanAverageFromMatrixWherePercentageIsAlwaysAHundredPercent() {
        int[][] values = {{2, 4, 6, 8}, {20, 22}, {24, 26}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);
        int[] expectedValues = {};
        Array expected = new Array(expectedValues);
        Array result = original.getElementsWithAHigherPercentageOfEvenDigitsThanAverage();

        assertEquals(expected, result);
    }

    @Test
    void getElementsWithAHigherPercentageOfEvenDigitsThanAverageFromMatrixWherePercentageIsAlwaysZeroPercent() {
        int[][] values = {{3, 5, 7}, {9, 11}, {13, 15, 17}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);
        int[] expectedValues = {};
        Array expected = new Array(expectedValues);
        Array result = original.getElementsWithAHigherPercentageOfEvenDigitsThanAverage();

        assertEquals(expected, result);
    }

    @Test
    void reverseColumnElementsInAMatrixWithSomePositiveValues() {
        int[][] values = {{3, 5, 7}, {9, 11}, {13, 15, 17}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);
        int[][] expectedValues = {{7, 5, 3}, {11, 9}, {17, 15, 13}};
        TwoDimensionalArray expected = new TwoDimensionalArray(expectedValues);

        TwoDimensionalArray result = original.reverseColumnElements();

        assertEquals(expected, result);
    }

    @Test
    void reverseColumnElementsInAMatrixWithSomeNegativeValues() {
        int[][] values = {{-3, 5, -7}, {9, 0, 56}, {13, -15, -17, 55}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);
        int[][] expectedValues = {{-7, 5, -3}, {56, 0, 9}, {55, -17, -15, 13}};
        TwoDimensionalArray expected = new TwoDimensionalArray(expectedValues);

        TwoDimensionalArray result = original.reverseColumnElements();

        assertEquals(expected, result);
    }

    @Test
    void reverseColumnElementsInAnEmptyMatrix() {
        int[][] values = {{}, {}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);
        int[][] expectedValues = {{}, {}};
        TwoDimensionalArray expected = new TwoDimensionalArray(expectedValues);

        TwoDimensionalArray result = original.reverseColumnElements();

        assertEquals(expected, result);
    }

    @Test
    void reverseColumnElementsInAMatrixWhereRowsOnlyHaveOneColumn() {
        int[][] values = {{1}, {2}, {3}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);
        int[][] expectedValues = {{1}, {2}, {3}};
        TwoDimensionalArray expected = new TwoDimensionalArray(expectedValues);

        TwoDimensionalArray result = original.reverseColumnElements();

        assertEquals(expected, result);
    }

    @Test
    void reverseColumnElementsInAMatrixWhereThereIsOnlyOneRow() {
        int[][] values = {{13, 34, 22}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);
        int[][] expectedValues = {{22, 34, 13}};
        TwoDimensionalArray expected = new TwoDimensionalArray(expectedValues);

        TwoDimensionalArray result = original.reverseColumnElements();

        assertEquals(expected, result);
    }

    @Test
    void reverseRowElementsInAMatrixWithSomePositiveValues() {
        int[][] values = {{3, 5, 7}, {9, 11, 33}, {13, 15, 17}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);
        int[][] expectedValues = {{13, 15, 17}, {9, 11, 33}, {3, 5, 7}};
        TwoDimensionalArray expected = new TwoDimensionalArray(expectedValues);

        TwoDimensionalArray result = original.reverseRowElements();

        assertEquals(expected, result);
    }

    @Test
    void reverseRowElementsInAnIrregularMatrixWithSomeNegativeValues() {
        int[][] values = {{-7, -98, -76, -45}, {-9, -8, -65}, {0, -5}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);
        int[][] expectedValues = {{0, -5}, {-9, -8, -65}, {-7, -98, -76, -45}};
        TwoDimensionalArray expected = new TwoDimensionalArray(expectedValues);

        TwoDimensionalArray result = original.reverseRowElements();

        assertEquals(expected, result);
    }

    @Test
    void reverseRowElementsInAnEmptyMatrix() {
        int[][] values = {{}, {}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);
        int[][] expectedValues = {{}, {}};
        TwoDimensionalArray expected = new TwoDimensionalArray(expectedValues);

        TwoDimensionalArray result = original.reverseRowElements();

        assertEquals(expected, result);
    }

    @Test
    void reverseRowElementsInAMatrixWithOnlyOneColumn() {
        int[][] values = {{3}, {9}, {13}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);
        int[][] expectedValues = {{13}, {9}, {3}};
        TwoDimensionalArray expected = new TwoDimensionalArray(expectedValues);

        TwoDimensionalArray result = original.reverseRowElements();

        assertEquals(expected, result);
    }

    @Test
    void reverseRowElementsInAMatrixWithOnlyOneRow() {
        int[][] values = {{3, 4, 5}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);
        int[][] expectedValues = {{3, 4, 5}};
        TwoDimensionalArray expected = new TwoDimensionalArray(expectedValues);

        TwoDimensionalArray result = original.reverseRowElements();

        assertEquals(expected, result);
    }

    @Test
    void rotateSquareMatrixNinetyDegrees() {
        int[][] values = {{3, 4, 5}, {5, 6, 7}, {8, 9, 10}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);
        int[][] expectedValues = {{5, 7, 10}, {4, 6, 9}, {3, 5, 8}};
        TwoDimensionalArray expected = new TwoDimensionalArray(expectedValues);

        TwoDimensionalArray result = original.rotateMatrixNinetyDegrees();

        assertEquals(expected, result);
    }

    @Test
    void rotateRectangularMatrixNinetyDegrees() {
        int[][] values = {{3, 4, 5}, {5, 6, 7}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);
        int[][] expectedValues = {{5, 7}, {4, 6}, {3, 5}};
        TwoDimensionalArray expected = new TwoDimensionalArray(expectedValues);

        TwoDimensionalArray result = original.rotateMatrixNinetyDegrees();

        assertEquals(expected, result);
    }

    @Test
    void rotateAnotherRectangularMatrixNinetyDegrees() {
        int[][] values = {{3, 4}, {5, 6}, {8, 9}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);
        int[][] expectedValues = {{4, 6, 9}, {3, 5, 8}};
        TwoDimensionalArray expected = new TwoDimensionalArray(expectedValues);

        TwoDimensionalArray result = original.rotateMatrixNinetyDegrees();

        assertEquals(expected, result);
    }

    @Test
    void rotateIrregularMatrixNinetyDegrees() {
        int[][] values = {{3, 4, 6}, {5, 6}, {8, 9, 7, 2}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);

        assertThrows(IllegalArgumentException.class, original::rotateMatrixNinetyDegrees);
    }

    @Test
    void rotateSquareMatrixWithSomeNegativeValuesNinetyDegrees() {

        int[][] values = {{3, -4, 6}, {-5, 6, -8}, {8, -9, 4}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);
        int[][] expectedValues = {{6, -8, 4}, {-4, 6, -9}, {3, -5, 8}};
        TwoDimensionalArray expected = new TwoDimensionalArray(expectedValues);

        TwoDimensionalArray result = original.rotateMatrixNinetyDegrees();

        assertEquals(expected, result);
    }

    @Test
    void rotateSquareMatrixAHundredAndEightyDegrees() {
        int[][] values = {{3, 4, 5}, {5, 6, 7}, {8, 9, 10}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);
        int[][] expectedValues = {{10, 9, 8}, {7, 6, 5}, {5, 4, 3}};
        TwoDimensionalArray expected = new TwoDimensionalArray(expectedValues);

        TwoDimensionalArray result = original.rotateMatrixAHundredAndEightyDegrees();

        assertEquals(expected, result);
    }

    @Test
    void rotateRectangularMatrixAHundredAndEightyDegrees() {
        int[][] values = {{3, 4, 5}, {5, 6, 7}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);
        int[][] expectedValues = {{7, 6, 5}, {5, 4, 3}};
        TwoDimensionalArray expected = new TwoDimensionalArray(expectedValues);

        TwoDimensionalArray result = original.rotateMatrixAHundredAndEightyDegrees();

        assertEquals(expected, result);
    }

    @Test
    void rotateAnotherRectangularMatrixAHundredAndEightyDegrees() {
        int[][] values = {{3, 4}, {5, 6}, {8, 9}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);
        int[][] expectedValues = {{9, 8}, {6, 5}, {4, 3}};
        TwoDimensionalArray expected = new TwoDimensionalArray(expectedValues);

        TwoDimensionalArray result = original.rotateMatrixAHundredAndEightyDegrees();

        assertEquals(expected, result);
    }

    @Test
    void rotateIrregularMatrixAHundredAndEightyDegrees() {
        int[][] values = {{3, 4, 6}, {5, 6}, {8, 9, 7, 2}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);

        assertThrows(IllegalArgumentException.class, original::rotateMatrixAHundredAndEightyDegrees);
    }

    @Test
    void rotateSquareMatrixWithSomeNegativeValuesAHundredAndEightyDegrees() {

        int[][] values = {{3, -4, 6}, {-5, 6, -8}, {8, -9, 4}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);
        int[][] expectedValues = {{4, -9, 8}, {-8, 6, -5}, {6, -4, 3}};
        TwoDimensionalArray expected = new TwoDimensionalArray(expectedValues);

        TwoDimensionalArray result = original.rotateMatrixAHundredAndEightyDegrees();

        assertEquals(expected, result);
    }

    @Test
    void rotateSquareMatrixMinusNinetyDegrees() {
        int[][] values = {{3, 4, 5}, {5, 6, 7}, {8, 9, 10}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);
        int[][] expectedValues = {{8, 5, 3}, {9, 6, 4}, {10, 7, 5}};
        TwoDimensionalArray expected = new TwoDimensionalArray(expectedValues);

        TwoDimensionalArray result = original.rotateMatrixMinusNinetyDegrees();

        assertEquals(expected, result);
    }

    @Test
    void rotateRectangularMatrixMinusNinetyDegrees() {
        int[][] values = {{3, 4, 5}, {5, 6, 7}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);
        int[][] expectedValues = {{5, 3}, {6, 4}, {7, 5}};
        TwoDimensionalArray expected = new TwoDimensionalArray(expectedValues);

        TwoDimensionalArray result = original.rotateMatrixMinusNinetyDegrees();

        assertEquals(expected, result);
    }

    @Test
    void rotateAnotherRectangularMatrixMinusNinetyDegrees() {
        int[][] values = {{3, 4}, {5, 6}, {8, 9}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);
        int[][] expectedValues = {{8, 5, 3}, {9, 6, 4}};
        TwoDimensionalArray expected = new TwoDimensionalArray(expectedValues);

        TwoDimensionalArray result = original.rotateMatrixMinusNinetyDegrees();

        assertEquals(expected, result);
    }

    @Test
    void rotateIrregularMatrixMinusNinetyDegrees() {
        int[][] values = {{3, 4, 6}, {5, 6}, {8, 9, 7, 2}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);

        assertThrows(IllegalArgumentException.class, original::rotateMatrixMinusNinetyDegrees);
    }

    @Test
    void rotateSquareMatrixWithSomeNegativeValuesMinusNinetyDegrees() {

        int[][] values = {{3, -4, 6}, {-5, 6, -8}, {8, -9, 4}};
        TwoDimensionalArray original = new TwoDimensionalArray(values);
        int[][] expectedValues = {{8, -5, 3}, {-9, 6, -4}, {4, -8, 6}};
        TwoDimensionalArray expected = new TwoDimensionalArray(expectedValues);

        TwoDimensionalArray result = original.rotateMatrixMinusNinetyDegrees();

        assertEquals(expected, result);
    }


}
