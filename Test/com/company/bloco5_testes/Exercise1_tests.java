package com.company.bloco5_testes;

import com.company.bloco5.Array;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class Exercise1_tests {

    @Test
    void createEmptyArray() {
        Array array1 = new Array();

        int[] expected = {};

        assertArrayEquals(array1.toArray(), expected);
    }

    @Test
    void createArrayWithSomeValues() {
        int[] values = {4, 7, 23, 99};
        Array array1 = new Array(values);

        int[] expected = {4, 7, 23, 99};

        assertArrayEquals(array1.toArray(), expected);
    }

    @Test
    void createNullArray() {
        int[] values = null;

        assertThrows(NullPointerException.class, () -> new Array(values));
    }

    @Test
    void addNewValueToEmptyArray() {
        Array array1 = new Array();

        int[] expected = {35};
        array1.addNewValueToArray(35);

        assertArrayEquals(array1.toArray(), expected);
    }

    @Test
    void addNewValueToArrayWithSomeValues() {
        int[] values = {4, 7, 23, 99};
        Array array1 = new Array(values);

        int[] expected = {4, 7, 23, 99, 35};
        array1.addNewValueToArray(35);


        assertArrayEquals(array1.toArray(), expected);
    }

    @Test
    void addNewNegativeValueToArrayWithSomeValues() {
        int[] values = {4, 7, 23, 99};
        Array array1 = new Array(values);

        int[] expected = {4, 7, 23, 99, -5};
        array1.addNewValueToArray(-5);

        assertArrayEquals(array1.toArray(), expected);
    }

    @Test
    void addZeroAsNewValueToEmptyArray() {
        Array array1 = new Array();

        int[] expected = {0};
        array1.addNewValueToArray(0);


        assertArrayEquals(array1.toArray(), expected);
    }

    @Test
    void removeOnlyOccurrenceOfAValueFromArrayWithSomeValues() {
        int[] values = {4, 7, 23, 99};
        Array array1 = new Array(values);

        array1.removeFirstOccurrenceValueFromArray(7);

        int[] expected = {4, 23, 99};

        assertArrayEquals(array1.toArray(), expected);
    }

    @Test
    void removeFirstOccurrenceOfAValueFromArrayWithSomeValues() {
        int[] values = {4, 7, 23, 99, 23, 55, 23};
        Array array1 = new Array(values);

        array1.removeFirstOccurrenceValueFromArray(23);

        int[] expected = {4, 7, 99, 23, 55, 23};

        assertArrayEquals(array1.toArray(), expected);
    }

    @Test
    void removeValueInEmptyArray() {
        int[] values = {};
        Array array1 = new Array(values);

        assertThrows(IllegalArgumentException.class, () -> array1.removeFirstOccurrenceValueFromArray(23));
    }

    @Test
    void removeOnlyValueFromArray() {
        int[] values = {55};
        Array array1 = new Array(values);

        array1.removeFirstOccurrenceValueFromArray(55);

        int[] expected = {};

        assertArrayEquals(array1.toArray(), expected);
    }

    @Test
    void removeValueThatDoesNotExistInArray() {
        int[] values = {55, 9, 84, -5, -76};
        Array array1 = new Array(values);

        assertThrows(IllegalArgumentException.class, () -> array1.removeFirstOccurrenceValueFromArray(10));

    }

    @Test
    void returnValueInPositionThreeFromArrayWithFiveElements() {
        int[] values = {-1, 5, 2, -62, 75};
        Array array1 = new Array(values);

        int expected = -62;
        int result = array1.getValueFromPosition(3);

        assertEquals(expected, result);
    }

    @Test
    void returnValueInPositionZeroFromArrayWithThreeElements() {
        int[] values = {3, 0, 0,};
        Array array1 = new Array(values);

        int expected = 3;
        int result = array1.getValueFromPosition(0);

        assertEquals(expected, result);
    }

    @Test
    void returnValueInPositionZeroFromArrayWithOneElements() {
        int[] values = {65};
        Array array1 = new Array(values);

        int expected = 65;
        int result = array1.getValueFromPosition(0);

        assertEquals(expected, result);
    }

    @Test
    void returnValueInLastPositionFromArrayWithFiveElements() {
        int[] values = {-1, 5, 2, -62, 75};
        Array array1 = new Array(values);

        int expected = 75;
        int result = array1.getValueFromPosition(4);

        assertEquals(expected, result);
    }

    @Test
    void returnValueInInvalidPositionFromArrayWithFiveElements() {
        int[] values = {-1, 5, 2, -62, 75};
        Array array1 = new Array(values);

        assertThrows(IllegalArgumentException.class, () -> array1.getValueFromPosition(6));
    }

    @Test
    void returnValueInNegativePositionFromArrayWithFiveElements() {
        int[] values = {-1, 5, 2, -62, 75};
        Array array1 = new Array(values);

        assertThrows(IllegalArgumentException.class, () -> array1.getValueFromPosition(-1));
    }

    @Test
    void returnValueInPositionZeroFromEmptyArray() {
        int[] values = {};
        Array array1 = new Array(values);

        assertThrows(IllegalArgumentException.class, () -> array1.getValueFromPosition(2));
    }

    @Test
    void countFiveElements() {
        int[] values = {3, 4, 5, 6, 7};
        Array array1 = new Array(values);

        int expected = 5;
        int result = array1.countArrayElements();

        assertEquals(expected, result);
    }

    @Test
    void countSevenElements() {
        int[] values = {3, 4, 5, 6, 7, 8, 9};
        Array array1 = new Array(values);

        int expected = 7;
        int result = array1.countArrayElements();

        assertEquals(expected, result);
    }

    @Test
    void countThreeElements() {
        int[] values = {-3, 4, 0};
        Array array1 = new Array(values);

        int expected = 3;
        int result = array1.countArrayElements();

        assertEquals(expected, result);
    }

    @Test
    void countElementsOfEmptyArray() {
        int[] values = {};
        Array array1 = new Array(values);

        int expected = 0;
        int result = array1.countArrayElements();

        assertEquals(expected, result);
    }

    @Test
    void returnBiggestElementFromArrayWithPositiveNumbers() {
        int[] values = {9, 8, 7, 6, 5, 4, 3, 2, 1};
        Array array1 = new Array(values);

        int expected = 9;
        int result = array1.getBiggestValue();

        assertEquals(expected, result);
    }

    @Test
    void returnBiggestElementFromArrayWhereAllElementsAreEqual() {
        int[] values = {8, 8, 8};
        Array array1 = new Array(values);

        int expected = 8;
        int result = array1.getBiggestValue();

        assertEquals(expected, result);
    }

    @Test
    void returnBiggestElementFromArrayWithNegativeNumbers() {
        int[] values = {0, -1, -1, -8};
        Array array1 = new Array(values);

        int expected = 0;
        int result = array1.getBiggestValue();

        assertEquals(expected, result);
    }

    @Test
    void returnBiggestElementFromEmptyArray() {
        int[] values = {};
        Array array1 = new Array(values);

        assertThrows(IllegalArgumentException.class, () -> array1.getBiggestValue());
    }

    @Test
    void returnSmallestElementFromArrayWithPositiveNumbers() {
        int[] values = {9, 8, 7, 6, 5, 4, 3, 2, 1};
        Array array1 = new Array(values);

        int expected = 1;
        int result = array1.getSmallestValue();

        assertEquals(expected, result);
    }

    @Test
    void returnSmallestElementFromArrayWhereAllElementsAreEqual() {
        int[] values = {8, 8, 8};
        Array array1 = new Array(values);

        int expected = 8;
        int result = array1.getSmallestValue();

        assertEquals(expected, result);
    }

    @Test
    void returnSmallestElementFromArrayWithNegativeNumbers() {
        int[] values = {0, -1, 1, -8};
        Array array1 = new Array(values);

        int expected = -8;
        int result = array1.getSmallestValue();

        assertEquals(expected, result);
    }

    @Test
    void returnSmallestElementFromEmptyArray() {
        int[] values = {};
        Array array1 = new Array(values);

        assertThrows(IllegalArgumentException.class, () -> array1.getSmallestValue());
    }

    @Test
    void returnMeanOfElementsFromArrayWithPositiveNumbers() {
        int[] values = {9, 8, 7, 6, 5, 4, 3, 2, 1};
        Array array1 = new Array(values);

        double expected = 5.00;
        double result = array1.getElementsMean();

        assertEquals(expected, result);
    }

    @Test
    void returnMeanOfElementsFromArrayWithNegativeNumbers() {
        int[] values = {9, -8, 7, -86, 5, -54, 233, 342, 0};
        Array array1 = new Array(values);

        double expected = 50.00;
        double result = array1.getElementsMean();

        assertEquals(expected, result);
    }

    @Test
    void returnMeanOfElementsFromEmptyArray() {
        int[] values = {};
        Array array1 = new Array(values);

        assertThrows(IllegalArgumentException.class, () -> array1.getElementsMean());
    }

    @Test
    void returnMeanOfElementsFromArrayWhereAllElementsAreZero() {
        int[] values = {0, 0, 0, 0};
        Array array1 = new Array(values);

        double expected = 0.00;
        double result = array1.getElementsMean();

        assertEquals(expected, result);
    }

    @Test
    void returnMeanOfEvenElementsFromArrayWhereAllElementsAreZero() {
        int[] values = {0, 0, 0, 0};
        Array array1 = new Array(values);

        double expected = 0.00;
        double result = array1.getEvenElementsMean();

        assertEquals(expected, result);
    }

    @Test
    void returnMeanOfEvenElementsFromArrayWhereAllElementsAreOdd() {
        int[] values = {9, 7, 5, 3, 457, 23};
        Array array1 = new Array(values);

        assertThrows(IllegalArgumentException.class, () -> array1.getEvenElementsMean());
    }

    @Test
    void returnMeanOfEvenElementsFromArrayWhereAllElementsAreEven() {
        int[] values = {8, 6, 4, 12};
        Array array1 = new Array(values);

        double expected = 8.00;
        double result = array1.getEvenElementsMean();

        assertEquals(expected, result);
    }

    @Test
    void returnMeanOfEvenElementsFromArrayWithNegativeElements() {
        int[] values = {-46, -35, 8, -456};
        Array array1 = new Array(values);

        double expected = -165.00;
        double result = array1.getEvenElementsMean();

        assertEquals(expected, result);
    }

    @Test
    void returnMeanOfEvenElementsFromEmptyArray() {
        int[] values = {};
        Array array1 = new Array(values);

        assertThrows(IllegalArgumentException.class, () -> array1.getEvenElementsMean());
    }

    @Test
    void returnMeanOfEvenElementsFromArrayWithPositiveAndNegativeElements() {
        int[] values = {-54, 25, 98, -456, 128, 48, -5874, 0};
        Array array1 = new Array(values);

        double expected = -873.00;
        double result = array1.getEvenElementsMean();

        assertEquals(expected, result);
    }

    @Test
    void returnMeanOfOddElementsFromArrayWhereAllElementsAreZero() {
        int[] values = {0, 0, 0, 0};
        Array array1 = new Array(values);

        assertThrows(IllegalArgumentException.class, () -> array1.getOddElementsMean());
    }

    @Test
    void returnMeanOfOddElementsFromArrayWhereAllElementsAreOdd() {
        int[] values = {9, 7, 5, 3, 457, 23};
        Array array1 = new Array(values);

        double expected = 84.00;
        double result = array1.getOddElementsMean();

        assertEquals(expected, result);
    }

    @Test
    void returnMeanOfOddElementsFromArrayWhereAllElementsAreEven() {
        int[] values = {8, 6, 4, 12};
        Array array1 = new Array(values);

        assertThrows(IllegalArgumentException.class, () -> array1.getOddElementsMean());
    }

    @Test
    void returnMeanOfOddElementsFromArrayWithNegativeElements() {
        int[] values = {-47, -35, 8, -456};
        Array array1 = new Array(values);

        double expected = -41.00;
        double result = array1.getOddElementsMean();

        assertEquals(expected, result);
    }

    @Test
    void returnMeanOfOddElementsFromEmptyArray() {
        int[] values = {};
        Array array1 = new Array(values);

        assertThrows(IllegalArgumentException.class, () -> array1.getOddElementsMean());
    }

    @Test
    void returnMeanOfOddElementsFromArrayWithPositiveAndNegativeElements() {
        int[] values = {-54, 25, 97, -456, 128, 48, -5873, 0};
        Array array1 = new Array(values);

        double expected = -1917.00;
        double result = array1.getOddElementsMean();

        assertEquals(expected, result);
    }

    @Test
    void returnMultiplesOfThreeMeanFromArrayWithPositiveNumbersOnly() {
        int[] values = {30, 25, 97, 128, 48, 0};
        Array array1 = new Array(values);
        int n = 3;

        double expected = 26.0;
        double result = array1.getMultiplesOfANumberMean(n);

        assertEquals(expected, result);
    }

    @Test
    void returnMultiplesOfFiveMeanFromArrayWithPositiveNumbersOnly() {
        int[] values = {150, 1255, 96, 128, 48};
        Array array1 = new Array(values);
        int n = 5;

        double expected = 703.0;
        double result = array1.getMultiplesOfANumberMean(n);

        assertEquals(expected, result);
    }

    @Test
    void returnMultiplesOfSixMeanFromArrayWithNegativeNumbers() {
        int[] values = {-18, 46, 36, -128, -48};
        Array array1 = new Array(values);
        int n = 6;

        double expected = -10.0;
        double result = array1.getMultiplesOfANumberMean(n);

        assertEquals(expected, result);
    }

    @Test
    void returnMultiplesOfFiftyMeanFromArray() {
        int[] values = {0, 150, 100, -220, -48};
        Array array1 = new Array(values);
        int n = 50;

        double expected = 83.0;
        double result = array1.getMultiplesOfANumberMean(n);

        assertEquals(expected, result);
    }

    @Test
    void returnMultiplesOfTwelveThousandThreeHundredFortyFiveMeanFromArrayWhereAllElementsAreZero() {
        int[] values = {0, 0, 0, 0, 0};
        Array array1 = new Array(values);
        int n = 12345;

        double expected = 0.0;
        double result = array1.getMultiplesOfANumberMean(n);

        assertEquals(expected, result);
    }

    @Test
    void returnMultiplesOfTwoMeanFromEmptyArray() {
        int[] values = {};
        Array array1 = new Array(values);
        int n = 2;

        assertThrows(IllegalArgumentException.class, () -> array1.getMultiplesOfANumberMean(n));
    }

    @Test
    void returnMultiplesOfTwoMeanFromArrayWhereNoElementsAreMultipleOfTwo() {
        int[] values = {3, 5, 7, 9, 13};
        Array array1 = new Array(values);
        int n = 2;

        assertThrows(IllegalArgumentException.class, () -> array1.getMultiplesOfANumberMean(n));
    }

    @Test
    void sortArrayAscSeveralElementsIncorrectlyOrdered() {
        int[] values = {9, 78, 5, 45, 32};
        Array array1 = new Array(values);

        boolean expected = array1.sortArrayElementsAsc();
        int[] array1Expected = {5, 9, 32, 45, 78};

        assertTrue(expected);
        assertArrayEquals(array1.toArray(), array1Expected);
    }

    @Test
    void sortArrayAscSeveralNegativeElementsIncorrectlyOrdered() {
        int[] values = {-9, -78, -5, -45, -32};
        Array array1 = new Array(values);

        boolean expected = array1.sortArrayElementsAsc();
        int[] array1Expected = {-78, -45, -32, -9, -5};

        assertTrue(expected);
        assertArrayEquals(array1.toArray(), array1Expected);
    }

    @Test
    void sortArrayAscArrayWithOnlyOneElement() {
        int[] values = {1000};
        Array array1 = new Array(values);

        boolean expected = array1.sortArrayElementsAsc();
        int[] array1Expected = {1000};

        assertTrue(expected);
        assertArrayEquals(array1.toArray(), array1Expected);
    }

    @Test
    void sortArrayAscArrayAlreadySorted() {
        int[] values = {1, 2, 3, 4, 5, 6};
        Array array1 = new Array(values);

        boolean expected = array1.sortArrayElementsAsc();
        int[] array1Expected = {1, 2, 3, 4, 5, 6};

        assertTrue(expected);
        assertArrayEquals(array1.toArray(), array1Expected);
    }

    @Test
    void sortArrayAscArrayIsEmpty() {
        int[] values = {};
        Array array1 = new Array(values);

        assertThrows(IllegalArgumentException.class, () -> array1.sortArrayElementsAsc());
    }

    @Test
    void sortArrayDescSeveralElementsIncorrectlyOrdered() {
        int[] values = {9, 78, 5, 45, 32};
        Array array1 = new Array(values);

        boolean expected = array1.sortArrayElementsDesc();
        int[] array1Expected = {78, 45, 32, 9, 5};

        assertTrue(expected);
        assertArrayEquals(array1.toArray(), array1Expected);
    }

    @Test
    void sortArrayDescSeveralNegativeElementsIncorrectlyOrdered() {
        int[] values = {-9, -78, -5, -45, -32};
        Array array1 = new Array(values);

        boolean expected = array1.sortArrayElementsDesc();
        int[] array1Expected = {-5, -9, -32, -45, -78};

        assertTrue(expected);
        assertArrayEquals(array1.toArray(), array1Expected);
    }

    @Test
    void sortArrayDescArrayWithOnlyOneElement() {
        int[] values = {500};
        Array array1 = new Array(values);

        boolean expected = array1.sortArrayElementsDesc();
        int[] array1Expected = {500};

        assertTrue(expected);
        assertArrayEquals(array1.toArray(), array1Expected);
    }

    @Test
    void sortArrayDescArrayAlreadySorted() {
        int[] values = {6, 5, 4, 3, 2, 1};
        Array array1 = new Array(values);

        boolean expected = array1.sortArrayElementsDesc();
        int[] array1Expected = {6, 5, 4, 3, 2, 1};

        assertTrue(expected);
        assertArrayEquals(array1.toArray(), array1Expected);
    }

    @Test
    void sortArrayDescArrayIsEmpty() {
        int[] values = {};
        Array array1 = new Array(values);

        assertThrows(IllegalArgumentException.class, () -> array1.sortArrayElementsDesc());
    }

    @Test
    void verifyThatArrayOnlyHasOneElementSingleDigit() {
        int[] values = {5};
        Array array1 = new Array(values);

        boolean expected = array1.containsOneElementOnly();

        assertTrue(expected);
    }

    @Test
    void verifyThatArrayOnlyHasOneElementMultipleDigits() {
        int[] values = {589};
        Array array1 = new Array(values);

        boolean expected = array1.containsOneElementOnly();

        assertTrue(expected);
    }

    @Test
    void verifyThatArrayHasMoreThanOneElement() {
        int[] values = {45, 56};
        Array array1 = new Array(values);

        boolean expected = array1.containsOneElementOnly();

        assertFalse(expected);
    }

    @Test
    void verifyThatArrayHasZeroElements() {
        int[] values = {};
        Array array1 = new Array(values);

        boolean expected = array1.containsOneElementOnly();

        assertFalse(expected);
    }

    @Test
    void verifyEvenElementsFromArrayWithOnlyEvenPositiveNumbers() {
        int[] values = {4, 46, 24, 12, 2};
        Array array1 = new Array(values);

        boolean expected = array1.containsOnlyEvenElements();

        assertTrue(expected);
    }

    @Test
    void verifyEvenElementsFromArrayWithOnlyEvenNegativeNumbersOrZero() {
        int[] values = {-4, -56, 0, -14, -8};
        Array array1 = new Array(values);

        boolean expected = array1.containsOnlyEvenElements();

        assertTrue(expected);
    }

    @Test
    void verifyEvenElementsFromArrayWithOnlyOddNumbers() {
        int[] values = {5, 47, 25, 13, 3};
        Array array1 = new Array(values);

        boolean expected = array1.containsOnlyEvenElements();

        assertFalse(expected);
    }

    @Test
    void verifyEvenElementsFromArrayWithEvenAndOddNumbers() {
        int[] values = {5, 34, 25, 12, 3};
        Array array1 = new Array(values);

        boolean expected = array1.containsOnlyEvenElements();

        assertFalse(expected);
    }

    @Test
    void verifyEvenElementsFromEmptyArray() {
        int[] values = {};
        Array array1 = new Array(values);

        assertThrows(IllegalArgumentException.class, () -> array1.containsOnlyEvenElements());
    }

    @Test
    void verifyOddElementsFromArrayWithOnlyOddPositiveNumbers() {
        int[] values = {5, 47, 35, 25, 3};
        Array array1 = new Array(values);

        boolean expected = array1.containsOnlyOddElements();

        assertTrue(expected);
    }

    @Test
    void verifyOddElementsFromArrayWithOnlyEvenNegativeNumbers() {
        int[] values = {-7, -45, -5, -27, -9};
        Array array1 = new Array(values);

        boolean expected = array1.containsOnlyOddElements();

        assertTrue(expected);
    }

    @Test
    void verifyOddElementsFromArrayWithOnlyEvenNumbersOrZero() {
        int[] values = {4, 6, 34, 12, 0};
        Array array1 = new Array(values);

        boolean expected = array1.containsOnlyOddElements();

        assertFalse(expected);
    }

    @Test
    void verifyOddElementsFromArrayWithEvenAndOddNumbers() {
        int[] values = {5, 34, 25, 12, 3};
        Array array1 = new Array(values);

        boolean expected = array1.containsOnlyOddElements();

        assertFalse(expected);
    }

    @Test
    void verifyOddElementsFromEmptyArray() {
        int[] values = {};
        Array array1 = new Array(values);

        assertThrows(IllegalArgumentException.class, () -> array1.containsOnlyOddElements());
    }

    @Test
    void verifyDuplicatedElementsInArrayWithSomeDuplicatedValues() {
        int[] values = {34, 56, 12, 3, 34};
        Array array1 = new Array(values);

        boolean expected = array1.containsRepeatingElements();

        assertTrue(expected);
    }

    @Test
    void verifyDuplicatedElementsInArrayWithOnlyDuplicatedValues() {
        int[] values = {34, 56, 56, 34};
        Array array1 = new Array(values);

        boolean expected = array1.containsRepeatingElements();

        assertTrue(expected);
    }

    @Test
    void verifyDuplicatedElementsInArrayWithNoDuplicatedValues() {
        int[] values = {34, 67, 56, 45};
        Array array1 = new Array(values);

        boolean expected = array1.containsRepeatingElements();

        assertFalse(expected);
    }

    @Test
    void verifyDuplicatedElementsInArrayWithTheSameValueRepeated() {
        int[] values = {5, 5, 5, 5, 5};
        Array array1 = new Array(values);

        boolean expected = array1.containsRepeatingElements();

        assertTrue(expected);
    }

    @Test
    void getElementsWithDigitsGreaterThanAverageInArrayWithTwoElementsWithDigitsGreaterThanAverage() {
        int[] array1values = {3, 5, 3456234, 235467};
        Array array1 = new Array(array1values);
        int[] expectedValues = {3456234, 235467};
        Array expected = new Array(expectedValues);

        Array result = array1.getElementsWithMoreDigitsThanAverage();

        assertArrayEquals(expected.toArray(), result.toArray());
    }

    @Test
    void getElementsWithDigitsGreaterThanAverageInArrayWithOneElementWithDigitsGreaterThanAverage() {
        int[] values = {3, 5, 3456234, 6};
        Array array1 = new Array(values);
        int[] expectedValues = {3456234};
        Array expected = new Array(expectedValues);

        Array result = array1.getElementsWithMoreDigitsThanAverage();

        assertArrayEquals(expected.toArray(), result.toArray());
    }

    @Test
    void getElementsWithDigitsGreaterThanAverageInArrayWithZeroElementsWithDigitsGreaterThanAverage() {
        int[] values = {34, 23, 45, 43};
        Array array1 = new Array(values);
        int[] expectedValues = {};
        Array expected = new Array(expectedValues);
        Array result = array1.getElementsWithMoreDigitsThanAverage();

        assertArrayEquals(expected.toArray(), result.toArray());
    }

    @Test
    void getElementsWithDigitsGreaterThanAverageInArrayWithOneNegativeElementWithDigitsGreaterThanAverage() {
        int[] values = {-34, 23, -4546, 0};
        Array array1 = new Array(values);
        int[] expectedValues = {-4546};
        Array expected = new Array(expectedValues);

        Array result = array1.getElementsWithMoreDigitsThanAverage();

        assertArrayEquals(expected.toArray(), result.toArray());
    }

    @Test
    void getElementsWithDigitsGreaterThanAverageInEmptyArray() {
        int[] values = {};
        Array array1 = new Array(values);
        assertThrows(IllegalArgumentException.class, array1::getElementsWithMoreDigitsThanAverage);
    }

    @Test
    void getElementsWithPercentageOfEvenDigitsGreaterThanAverageInArrayWherePercentageIsTheSameInAllValues() {
        int[] values = {123, 369, 987, 147};
        Array array1 = new Array(values);
        int[] expectedValues = {};
        Array expected = new Array(expectedValues);

        Array result = array1.getElementsWithHigherPercentageOfEvenNumbersThanAverage();

        assertArrayEquals(expected.toArray(), result.toArray());
    }

    @Test
    void getElementsWithPercentageOfEvenDigitsGreaterThanAverageInArrayWhereOneElementHasAGreaterPercentage() {
        int[] values = {12, 36, 98, 240};
        Array array1 = new Array(values);
        int[] expectedValues = {240};
        Array expected = new Array(expectedValues);

        Array result = array1.getElementsWithHigherPercentageOfEvenNumbersThanAverage();

        assertArrayEquals(expected.toArray(), result.toArray());
    }

    @Test
    void getElementsWithPercentageOfEvenDigitsGreaterThanAverageInArrayWherePercentageIsAHundredPercent() {
        int[] values = {2468, 8462, 2408, 6482};
        Array array1 = new Array(values);
        int[] expectedValues = {};
        Array expected = new Array(expectedValues);

        Array result = array1.getElementsWithHigherPercentageOfEvenNumbersThanAverage();

        assertArrayEquals(expected.toArray(), result.toArray());
    }

    @Test
    void getElementsWithPercentageOfEvenDigitsGreaterThanAverageInArrayWherePercentageInEachValueIsDifferent() {
        int[] values = {3654, 3, 68432, -8};
        Array array1 = new Array(values);
        int[] expectedValues = {68432, -8};
        Array expected = new Array(expectedValues);

        Array result = array1.getElementsWithHigherPercentageOfEvenNumbersThanAverage();

        assertArrayEquals(expected.toArray(), result.toArray());
    }

    @Test
    void getElementsWithPercentageOfEvenDigitsGreaterThanAverageInArrayWhereNoValuesHaveEvenDigits() {
        int[] values = {3, 5, 7, 5, 9};
        Array array1 = new Array(values);
        int[] expectedValues = {};
        Array expected = new Array(expectedValues);

        Array result = array1.getElementsWithHigherPercentageOfEvenNumbersThanAverage();

        assertArrayEquals(expected.toArray(), result.toArray());
    }

    @Test
    void getElementsWithPercentageOfEvenDigitsGreaterThanAverageInEmptyArray() {
        int[] values = {};
        Array array1 = new Array(values);
        assertThrows(IllegalArgumentException.class, array1::getElementsWithHigherPercentageOfEvenNumbersThanAverage);

    }

    @Test
    void getElementsWithEvenDigitsFromArrayOfPositiveElementsWithEvenDigitsAndNot() {
        int[] values = {246, 34, 88, 46, 345, 75};
        Array array1 = new Array(values);
        int[] expectedValues = {246, 88, 46};
        Array expected = new Array(expectedValues);

        Array result = array1.getElementsWithOnlyEvenDigits();

        assertArrayEquals(expected.toArray(), result.toArray());
    }

    @Test
    void getElementsWithEvenDigitsFromArrayOfPositiveAndNegativeOrZeroElementsWithEvenDigitsAndNot() {
        int[] values = {246, 0, -357, -46, 345, 75};
        Array array1 = new Array(values);
        int[] expectedValues = {246, 0, -46};
        Array expected = new Array(expectedValues);

        Array result = array1.getElementsWithOnlyEvenDigits();

        assertArrayEquals(expected.toArray(), result.toArray());
    }

    @Test
    void getElementsWithEvenDigitsFromArrayOfElementsWithEvenDigits() {
        int[] values = {246, 44, 88, 46, 646, 64};
        Array array1 = new Array(values);
        int[] expectedValues = {246, 44, 88, 46, 646, 64};
        Array expected = new Array(expectedValues);

        Array result = array1.getElementsWithOnlyEvenDigits();

        assertArrayEquals(expected.toArray(), result.toArray());
    }

    @Test
    void getElementsWithEvenDigitsFromArrayOfElementsWithoutOnlyEvenDigits() {
        int[] values = {34, 23, 56, 76, 456, 643};
        Array array1 = new Array(values);
        int[] expectedValues = {};
        Array expected = new Array(expectedValues);

        Array result = array1.getElementsWithOnlyEvenDigits();

        assertArrayEquals(expected.toArray(), result.toArray());
    }

    @Test
    void getElementsWithEvenDigitsFromAnEmptyArray() {
        int[] values = {};
        Array array1 = new Array(values);
        int[] expectedValues = {};
        Array expected = new Array(expectedValues);

        Array result = array1.getElementsWithOnlyEvenDigits();

        assertArrayEquals(expected.toArray(), result.toArray());
    }

    @Test
    void getElementsWithAnAscendingSequenceOfDigitsFromArrayWithElementsOfMixedSequences() {
        int[] values = {347, 654, 23, 6, 876};
        Array array1 = new Array(values);
        int[] expectedValues = {347, 23};
        Array expected = new Array(expectedValues);

        Array result = array1.getElementsWithDigitsInAscOrder();

        assertArrayEquals(expected.toArray(), result.toArray());
    }

    @Test
    void getElementsWithAnAscendingSequenceOfDigitsFromArrayWithElementsOfMixedSequencesAndNegativeNumbers() {
        int[] values = {-347, -654, -23, 6, -876};
        Array array1 = new Array(values);
        int[] expectedValues = {-654, -876};
        Array expected = new Array(expectedValues);

        Array result = array1.getElementsWithDigitsInAscOrder();

        assertArrayEquals(expected.toArray(), result.toArray());
    }

    @Test
    void getElementsWithAnAscendingSequenceOfDigitsFromArrayWithNoElementsWithAnAscendingSequence() {
        int[] values = {9876, 43, 654, 6, -2};
        Array array1 = new Array(values);
        int[] expectedValues = {};
        Array expected = new Array(expectedValues);

        Array result = array1.getElementsWithDigitsInAscOrder();

        assertArrayEquals(expected.toArray(), result.toArray());
    }

    @Test
    void getElementsWithAnAscendingSequenceOfDigitsFromEmptyArray() {
        int[] values = {};
        Array array1 = new Array(values);
        int[] expectedValues = {};
        Array expected = new Array(expectedValues);

        Array result = array1.getElementsWithDigitsInAscOrder();

        assertArrayEquals(expected.toArray(), result.toArray());
    }

    @Test
    void getPalindromesFromArrayWithSomePalindromes() {
        int[] values = {323, 54, 45654, 7, 456};
        Array array1 = new Array(values);
        int[] expectedValues = {323, 45654, 7};
        Array expected = new Array(expectedValues);

        Array result = array1.getPalindromes();

        assertArrayEquals(expected.toArray(), result.toArray());
    }

    @Test
    void getPalindromesFromArrayWithSomePalindromesAndNegativeNumbers() {
        int[] values = {-323, -54, -45654, -7, -456};
        Array array1 = new Array(values);
        int[] expectedValues = {-323, -45654, -7};
        Array expected = new Array(expectedValues);

        Array result = array1.getPalindromes();

        assertArrayEquals(expected.toArray(), result.toArray());
    }

    @Test
    void getPalindromesFromArrayWithOnlyPalindromes() {
        int[] values = {121, 25852, 8, 6556};
        Array array1 = new Array(values);
        int[] expectedValues = {121, 25852, 8, 6556};
        Array expected = new Array(expectedValues);

        Array result = array1.getPalindromes();

        assertArrayEquals(expected.toArray(), result.toArray());
    }

    @Test
    void getPalindromesFromArrayWithNoPalindromes() {
        int[] values = {45, 69, 87};
        Array array1 = new Array(values);
        int[] expectedValues = {};
        Array expected = new Array(expectedValues);

        Array result = array1.getPalindromes();

        assertArrayEquals(expected.toArray(), result.toArray());
    }

    @Test
    void getPalindromesFromEmptyArray() {
        int[] values = {};
        Array array1 = new Array(values);
        int[] expectedValues = {};
        Array expected = new Array(expectedValues);

        Array result = array1.getPalindromes();

        assertArrayEquals(expected.toArray(), result.toArray());
    }

    @Test
    void getElementsComposedOfTheSameDigitsFromArrayWithSomeElementsComposedOfTheSameDigits1() {
        int[] values = {3, 456, 222, 534};
        Array array1 = new Array(values);
        int[] expectedValues = {3, 222};
        Array expected = new Array(expectedValues);

        Array result = array1.getElementsComposedOfTheSameDigits();

        assertArrayEquals(expected.toArray(), result.toArray());
    }

    @Test
    void getElementsComposedOfTheSameDigitsFromArrayWithSomeElementsComposedOfTheSameDigits2() {
        int[] values = {5, 6, 8, 888, 45, 12, 32, 55, 987, 0};
        Array array1 = new Array(values);
        int[] expectedValues = {5, 6, 8, 888, 55, 0};
        Array expected = new Array(expectedValues);

        Array result = array1.getElementsComposedOfTheSameDigits();

        assertArrayEquals(expected.toArray(), result.toArray());
    }

    @Test
    void getElementsComposedOfTheSameDigitsFromArrayWithNegativeElements() {
        int[] values = {-55, 456, 789, -44, 88};
        Array array1 = new Array(values);
        int[] expectedValues = {-55, -44, 88};
        Array expected = new Array(expectedValues);

        Array result = array1.getElementsComposedOfTheSameDigits();

        assertArrayEquals(expected.toArray(), result.toArray());
    }

    @Test
    void getElementsComposedOfTheSameDigitsFromArrayWithElementsComposedOfOnlyDifferentDigits() {
        int[] values = {34, 56, 23, 28, 60};
        Array array1 = new Array(values);
        int[] expectedValues = {};
        Array expected = new Array(expectedValues);

        Array result = array1.getElementsComposedOfTheSameDigits();

        assertArrayEquals(expected.toArray(), result.toArray());
    }

    @Test
    void getElementsComposedOfTheSameDigitsFromArrayWithOnlyElementsComposedOfTheSameDigits() {
        int[] values = {222, 333, 444, 555};
        Array array1 = new Array(values);
        int[] expectedValues = {222, 333, 444, 555};
        Array expected = new Array(expectedValues);

        Array result = array1.getElementsComposedOfTheSameDigits();

        assertArrayEquals(expected.toArray(), result.toArray());
    }

    @Test
    void getNonArmstrongNumbersFromArrayWithSomeArmstrongNumbers1() {
        int[] values = {153, 44, 173, 987, 0, 1, 23, 45, 371};
        Array array1 = new Array(values);
        int[] expectedValues = {44, 173, 987, 23, 45};
        Array expected = new Array(expectedValues);

        Array result = array1.getNonArmstrongElements();

        assertArrayEquals(expected.toArray(), result.toArray());
    }

    @Test
    void getNonArmstrongNumbersFromArrayWithSomeArmstrongNumbers2() {
        int[] values = {45, 66, 78, 2, 34, 407, 370};
        Array array1 = new Array(values);
        int[] expectedValues = {45, 66, 78, 34};
        Array expected = new Array(expectedValues);

        Array result = array1.getNonArmstrongElements();

        assertArrayEquals(expected.toArray(), result.toArray());
    }

    @Test
    void getNonArmstrongNumbersFromArrayWithNegativeNumbers() {
        int[] values = {45, -66, 78, -2, 34, 407, -370};
        Array array1 = new Array(values);
        int[] expectedValues = {45, -66, 78, -2, 34, -370};
        Array expected = new Array(expectedValues);

        Array result = array1.getNonArmstrongElements();

        assertArrayEquals(expected.toArray(), result.toArray());
    }

    @Test
    void getNonArmstrongNumbersFromArrayWithOnlyArmstrongNumbers() {
        int[] values = {1, 153, 370};
        Array array1 = new Array(values);
        int[] expectedValues = {};
        Array expected = new Array(expectedValues);

        Array result = array1.getNonArmstrongElements();

        assertArrayEquals(expected.toArray(), result.toArray());
    }

    @Test
    void getNonArmstrongNumbersFromArrayWithNoArmstrongNumbers() {
        int[] values = {10, 11, 12, 13, 14, 15};
        Array array1 = new Array(values);
        int[] expectedValues = {10, 11, 12, 13, 14, 15};
        Array expected = new Array(expectedValues);

        Array result = array1.getNonArmstrongElements();

        assertArrayEquals(expected.toArray(), result.toArray());
    }

    @Test
    void getNonArmstrongNumbersFromEmptyArray() {
        int[] values = {};
        Array array1 = new Array(values);
        int[] expectedValues = {};
        Array expected = new Array(expectedValues);

        Array result = array1.getNonArmstrongElements();

        assertArrayEquals(expected.toArray(), result.toArray());
    }

    @Test
    void getElementsWithNrOfItsDigitsInAscOrderFromArrayWithSomeValuesInAscOrderWhereNEqualsThree() {
        int[] values = {12345, 12, 567, 45};
        int n = 3;
        Array array1 = new Array(values);
        int[] expectedValues = {12345, 567};
        Array expected = new Array(expectedValues);

        Array result = array1.getElementsWithNOfItsDigitsInAscOrder(n);

        assertArrayEquals(expected.toArray(), result.toArray());
    }

    @Test
    void getElementsWithNrOfItsDigitsInAscOrderFromArrayWithSomeValuesInAscOrderWhereNEqualsTwo() {
        int[] values = {5678, 12, 213456, 456, 6, 0};
        int n = 1;
        Array array1 = new Array(values);
        int[] expectedValues = {5678, 12, 213456, 456};
        Array expected = new Array(expectedValues);

        Array result = array1.getElementsWithNOfItsDigitsInAscOrder(n);

        assertArrayEquals(expected.toArray(), result.toArray());
    }

    @Test
    void getElementsWithNrOfItsDigitsInAscOrderFromArrayWithSomeValuesInAscOrderWhereNEqualsFive() {
        int[] values = {5678, 8765, 123456, 12345};
        int n = 5;
        Array array1 = new Array(values);
        int[] expectedValues = {123456, 12345};
        Array expected = new Array(expectedValues);

        Array result = array1.getElementsWithNOfItsDigitsInAscOrder(n);

        assertArrayEquals(expected.toArray(), result.toArray());
    }

    @Test
    void getElementsWithNrOfItsDigitsInAscOrderFromArrayWithSomeValuesInAscOrderWhereNEqualsOne() {
        int[] values = {5678, 8765, 123456, 12345};
        int n = 1;
        Array array1 = new Array(values);
        int[] expectedValues = {5678, 8765, 123456, 12345};
        Array expected = new Array(expectedValues);

        Array result = array1.getElementsWithNOfItsDigitsInAscOrder(n);

        assertArrayEquals(expected.toArray(), result.toArray());
    }

    @Test
    void getElementsWithNrOfItsDigitsInAscOrderFromArrayWithSomeNegativeValuesWhereNEqualsThree() {
        int[] values = {-5678, -8765, -123456, 12345};
        int n = 3;
        Array array1 = new Array(values);
        int[] expectedValues = {-8765, 12345};
        Array expected = new Array(expectedValues);

        Array result = array1.getElementsWithNOfItsDigitsInAscOrder(n);

        assertArrayEquals(expected.toArray(), result.toArray());
    }

    @Test
    void getElementsWithNrOfItsDigitsInAscOrderFromArrayWithNoValuesInAscWhereNEqualsThree() {
        int[] values = {1542, 978, 76, 56};
        int n = 3;
        Array array1 = new Array(values);
        int[] expectedValues = {};
        Array expected = new Array(expectedValues);

        Array result = array1.getElementsWithNOfItsDigitsInAscOrder(n);

        assertArrayEquals(expected.toArray(), result.toArray());
    }

    @Test
    void getElementsWithNrOfItsDigitsInAscOrderFromEmptyArrayWhereNEqualsThree() {
        int[] values = {};
        int n = 3;
        Array array1 = new Array(values);
        int[] expectedValues = {};
        Array expected = new Array(expectedValues);

        Array result = array1.getElementsWithNOfItsDigitsInAscOrder(n);

        assertArrayEquals(expected.toArray(), result.toArray());
    }

    @Test
    void isArrayTheSameCompareWithArrayWithTheSameValues1() {
        int[] values = {0, 1, 2, 3, 4, 5, 6};
        Array array1 = new Array(values);
        int[] array = {0, 1, 2, 3, 4, 5, 6};

        boolean expected = array1.isArrayTheSame(array);

        assertTrue(expected);
        assertArrayEquals(array1.toArray(), array);
    }

    @Test
    void isArrayTheSameCompareWithArrayWithTheSameValues2() {
        int[] values = {98, 43, -12, 0, -546};
        Array array1 = new Array(values);
        int[] array = {98, 43, -12, 0, -546};

        boolean expected = array1.isArrayTheSame(array);

        assertTrue(expected);
        assertArrayEquals(array1.toArray(), array);
    }

    @Test
    void isArrayTheSameCompareWithArrayWithTheDifferentValues() {
        int[] values = {98, 43, -12, 0, -546};
        Array array1 = new Array(values);
        int[] array = {0, 1, 2, 3, 4};

        boolean expected = array1.isArrayTheSame(array);

        assertFalse(expected);
    }

    @Test
    void isArrayTheSameCompareWithArrayWithTheDifferentValues2() {
        int[] values = {0, 1, 2, 3, 4, 5};
        Array array1 = new Array(values);
        int[] array = {0, 1, 2, 3, 4};

        boolean expected = array1.isArrayTheSame(array);

        assertFalse(expected);
    }

    @Test
    void isArrayTheSameCompareWithEmptyArray() {
        int[] values = {0, 1, 2, 3, 4, 5};
        Array array1 = new Array(values);
        int[] array = {};

        boolean expected = array1.isArrayTheSame(array);

        assertFalse(expected);
    }

    @Test
    void isEmptyArrayTheSameCompareWithEmptyArray() {
        int[] values = {};
        Array array1 = new Array(values);
        int[] array = {};

        boolean expected = array1.isArrayTheSame(array);

        assertTrue(expected);
    }


}
