package com.company;

import com.company.bloco2.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class Bloco2_Testes {

    //Exercício 1

    @Test
    void Ex1_test1() {
        int grade1 = 4;
        int grade2 = 5;
        int grade3 = 7;
        int weight1 = 45;
        int weight2 = 23;
        int weight3 = 32;

        String expected = "O aluno não cumpre a nota mínima exigida, pois teve uma nota de 5";
        String result = Bloco2_Exercicio1.getAverageGrade(grade1, grade2, grade3, weight1, weight2, weight3);

        assertEquals(expected, result);
    }

    @Test
    void Ex1_test2() {
        int grade1 = 4;
        int grade2 = 5;
        int grade3 = 7;
        int weight1 = 45;
        int weight2 = 23;
        int weight3 = 32;

        String expected = "O aluno não cumpre a nota mínima exigida, pois teve uma nota de 5";
        String result = Bloco2_Exercicio1.getAverageGrade(grade1, grade2, grade3, weight1, weight2, weight3);

        assertEquals(expected, result);
    }

      @Test
    void Ex1_test3() {
        int grade1 = 16;
        int grade2 = 12;
        int grade3 = 14;
        int weight1 = 23;
        int weight2 = 56;
        int weight3 = 21;

        String expected = "O aluno cumpre a nota mínima exigida, pois teve uma nota de 13";
        String result = Bloco2_Exercicio1.getAverageGrade(grade1, grade2, grade3, weight1, weight2, weight3);

        assertEquals(expected, result);
    }

    @Test
    void Ex1_test4() {
        int grade1 = 15;
        int grade2 = 15;
        int grade3 = 16;
        int weight1 = 25;
        int weight2 = 35;
        int weight3 = 50;

        String expected = "Erro: A soma dos pesos das notas é diferente de 100 ou uma das notas toma valores inválidos.";
        String result = Bloco2_Exercicio1.getAverageGrade(grade1, grade2, grade3, weight1, weight2, weight3);

        assertEquals(expected, result);
    }

    @Test
    void Ex1_test5() {
        int grade1 = 26;
        int grade2 = 14;
        int grade3 = 32;
        int weight1 = 25;
        int weight2 = 25;
        int weight3 = 50;

        String expected = "Erro: A soma dos pesos das notas é diferente de 100 ou uma das notas toma valores inválidos.";
        String result = Bloco2_Exercicio1.getAverageGrade(grade1, grade2, grade3, weight1, weight2, weight3);

        assertEquals(expected, result);
    }

    @Test
    void Ex1_test6() {
        int grade1 = -1;
        int grade2 = 14;
        int grade3 = 0;
        int weight1 = 25;
        int weight2 = 25;
        int weight3 = 50;

        String expected = "Erro: A soma dos pesos das notas é diferente de 100 ou uma das notas toma valores inválidos.";
        String result = Bloco2_Exercicio1.getAverageGrade(grade1, grade2, grade3, weight1, weight2, weight3);

        assertEquals(expected, result);
    }

    //Exercício 2

    @Test
    void Ex2_test1() {
        int number = 346;

        String expected = "O dígito 1 é 6, o dígito 2 é 4, o dígito 3 é 3 e o número é par.";
        String result = Bloco2_Exercicio2.getDigits(number);

        assertEquals(expected, result);
    }

    @Test
    void Ex2_test2() {
        int number = 678;

        String expected = "O dígito 1 é 8, o dígito 2 é 7, o dígito 3 é 6 e o número é par.";
        String result = Bloco2_Exercicio2.getDigits(number);

        assertEquals(expected, result);
    }

    @Test
    void Ex2_test3() {
        int number = 98;

        String expected = "O número não tem três dígitos e é par.";
        String result = Bloco2_Exercicio2.getDigits(number);

        assertEquals(expected, result);
    }
    @Test
    void Ex2_test4() {
        int number = 567;

        String expected = "O dígito 1 é 7, o dígito 2 é 6, o dígito 3 é 5 e o número é ímpar.";
        String result = Bloco2_Exercicio2.getDigits(number);

        assertEquals(expected, result);
    }

    @Test
    void Ex2_test5() {
        int number = 4679;

        String expected = "O número não tem três dígitos e é ímpar.";
        String result = Bloco2_Exercicio2.getDigits(number);

        assertEquals(expected, result);
    }

   //Exercício 3

    @Test
    void Ex3_test1() {
        double x1 = -1;
        double x2 = -9;
        double y1 = 3;
        double y2 = 6;

        double expected = 8.544003745;
        double result = Bloco2_Exercicio3.getDistanceBetweenTwoPoints(x1, x2, y1, y2);

        assertEquals(expected, result, 0.00001);
    }

    @Test
    void Ex3_test2() {
        double x1 = 12;
        double x2 = 2354.34;
        double y1 = 354;
        double y2 = 967.4523;

        double expected = 2421.338555;
        double result = Bloco2_Exercicio3.getDistanceBetweenTwoPoints(x1, x2, y1, y2);

        assertEquals(expected, result, 0.00001);
    }

    @Test
    void Ex3_test3() {
        double x1 = 0;
        double x2 = 0;
        double y1 = 0;
        double y2 = 0;

        double expected = 0;
        double result = Bloco2_Exercicio3.getDistanceBetweenTwoPoints(x1, x2, y1, y2);

        assertEquals(expected, result, 0.00001);
    }

    //Exercício 4

    @Test
    void Ex4_test1() {
        double x = 234;

        double expected = 54288;
        double result = Bloco2_Exercicio4.piecewiseFunction(x);

        assertEquals(expected, result, 0.00001);
    }

    @Test
    void Ex4_test2() {
        double x = 3529;

        double expected = 12446783;
        double result = Bloco2_Exercicio4.piecewiseFunction(x);

        assertEquals(expected, result, 0.00001);
    }

    @Test
    void Ex4_test3() {
        double x = - 234;

        double expected = -234;
        double result = Bloco2_Exercicio4.piecewiseFunction(x);

        assertEquals(expected, result, 0.00001);
    }

    @Test
    void Ex4_test4() {
        double x = - 13;

        double expected = -13;
        double result = Bloco2_Exercicio4.piecewiseFunction(x);

        assertEquals(expected, result, 0.00001);
    }

    @Test
    void Ex4_test5() {
        double x = 0;

        double expected = 0;
        double result = Bloco2_Exercicio4.piecewiseFunction(x);

        assertEquals(expected, result, 0.00001);
    }

    //Exercício 5


   @Test
    void Ex5_AreaNegativa() {
        double area = -4;

        String expected = "Valor da área incorreto.";
        String result = Bloco2_Exercicio5.getCubeVolume(area);

        assertEquals(expected, result);
    }

    @Test
    void Ex5_VolumeCuboGrande() {
        double area = 68786;

        String expected = "O cubo é grande, o seu volume é de 1227,5048 dm3.";
        String result = Bloco2_Exercicio5.getCubeVolume(area);

        assertEquals(expected, result);
    }

    @Test
    void Ex5_VolumeCuboPequeno() {
        double area = 234;

        String expected = "O cubo é pequeno, o seu volume é de 0,2436 dm3.";
        String result = Bloco2_Exercicio5.getCubeVolume(area);

        assertEquals(expected, result);
    }

    @Test
    void Ex5_VolumeCuboMedio() {
        double area = 629;

        String expected = "O cubo é médio, o seu volume é de 1,0734 dm3.";
        String result = Bloco2_Exercicio5.getCubeVolume(area);

        assertEquals(expected, result);
    }

    @Test
    void Ex5_AreaIgualZero() {

        double area = 0;

        String expected = "Valor da área incorreto.";
        String result = Bloco2_Exercicio5.getCubeVolume(area);

        assertEquals(expected, result);
    }

    //Exercício 6


    @Test
    void Ex6_TotalSecondsIsFiveThousand() {

        int totalSeconds = 5000;

        String expected = "1:23:20";
        String result = Bloco2_Exercicio6.getHoursMinutesSeconds(totalSeconds);

        assertEquals(expected, result);
    }

    @Test
    void Ex6_TotalSecondsIsThreeHundred() {

        int totalSeconds = 300;

        String expected = "0:5:0";
        String result = Bloco2_Exercicio6.getHoursMinutesSeconds(totalSeconds);

        assertEquals(expected, result);
    }

    @Test
    void Ex6_TotalSecondsIsForty() {

        int totalSeconds = 40;

        String expected = "0:0:40";
        String result = Bloco2_Exercicio6.getHoursMinutesSeconds(totalSeconds);

        assertEquals(expected, result);
    }

    @Test
    void Ex6_TotalSecondsIsTwoHundredThirtyEight() {

        int totalSeconds = 238;

        String expected = "0:3:58";
        String result = Bloco2_Exercicio6.getHoursMinutesSeconds(totalSeconds);

        assertEquals(expected, result);
    }

    @Test
    void Ex6_TotalSecondsIsThreeThousandandSixHundredSixty() {

        int totalSeconds = 3660;

        String expected = "1:1:0";
        String result = Bloco2_Exercicio6.getHoursMinutesSeconds(totalSeconds);

        assertEquals(expected, result);
    }

    @Test
    void Ex6_TotalSecondsIsSevenThousandTwoHundred() {

        int totalSeconds = 7200;

        String expected = "2:0:0";
        String result = Bloco2_Exercicio6.getHoursMinutesSeconds(totalSeconds);

        assertEquals(expected, result);
    }

    @Test
    void Ex6_TotalSecondsIsTenThousandEightHundredAndTwo() {

        int totalSeconds = 10802;

        String expected = "3:0:2";
        String result = Bloco2_Exercicio6.getHoursMinutesSeconds(totalSeconds);

        assertEquals(expected, result);
    }
    
    //Exercício 7


    @Test
    void Ex7_getWorkerCost_BuildingAreaUnderOneHundred() {
        double buildingArea = 45;
        double dailyPainterRate = 356;

        double expected = 1001.25;
        Double result = Bloco2_Exercicio7.getWorkerCost(buildingArea, dailyPainterRate);

        assertEquals(expected, result);
    }

    @Test
    void Ex7_getWorkerCost_BuildingAreaBetweenOneHundredAndThreeHundred() {
        double buildingArea = 274;
        double dailyPainterRate = 9865;

        double expected = 337876.25;
        Double result = Bloco2_Exercicio7.getWorkerCost(buildingArea, dailyPainterRate);

        assertEquals(expected, result);
    }

    @Test
    void Ex7_getWorkerCost_BuildingAreaBetweenThreeHundredAndOneThousand() {
        double buildingArea = 999;
        double dailyPainterRate = 54;

        double expected = 10114.875;
        Double result = Bloco2_Exercicio7.getWorkerCost(buildingArea, dailyPainterRate);

        assertEquals(expected, result);
    }

    @Test
    void Ex7_getWorkerCost_BuildingAreaOverOneThousand() {
        double buildingArea = 1234;
        double dailyPainterRate = 66;

        double expected = 20361;
        Double result = Bloco2_Exercicio7.getWorkerCost(buildingArea, dailyPainterRate);

        assertEquals(expected, result);
    }
    @Test
    void Ex7_getPaintCostPositiveValues() {
        double buildingArea = 1234;
        double costLiterPaint = 15;   //buildingArea / literPaintYeld * costLiterPaint
        double literPaintYeld = 5;

        double expected = 3702;
        double result = Bloco2_Exercicio7.getPaintCost(buildingArea, costLiterPaint, literPaintYeld);

        assertEquals(expected, result);
    }

    @Test
    void Ex7_getTotalCost() {
        double paintCost = 35;
        double workerCost = 756;   //workerCost + paintCost

        double expected = 791;
        double result = Bloco2_Exercicio7.getTotalCost (paintCost, workerCost);

        assertEquals(expected, result);
    }

    //Exercício 8


    @Test
    void Ex8_YIsMultipleOfX() {
        int X = 3;
        int Y = 27;

        String expected = "Y é múltiplo de X.";
        String result = Bloco2_Exercicio8.multiplicityXY(X, Y);

        assertEquals(expected, result);
    }

    @Test
    void Ex8_XIsMultipleOfY() {
        int X = 72;
        int Y = 8;

        String expected = "X é múltiplo de Y.";
        String result = Bloco2_Exercicio8.multiplicityXY(X, Y);

        assertEquals(expected, result);
    }

    @Test
    void Ex8_NoTMultipleNorDivisible() {
        int X = 4537;
        int Y = 9;

        String expected = "X não é múltiplo nem divisor de Y.";
        String result = Bloco2_Exercicio8.multiplicityXY(X, Y);

        assertEquals(expected, result);
    }

    //Exercício 9


    @Test
    void Ex9_ASequenciaECrescente() {
        int numero = 259;

        String expected = "A sequência dos algarismos é crescente";
        String result = Bloco2_Exercicio9.obterSequenciaNumero(numero);

        assertEquals(expected, result);
    }

    @Test
    void Ex9_ASequenciaNaoECrescente() {
        int numero = 563;

        String expected = "A sequência dos algarismos não é crescente";
        String result = Bloco2_Exercicio9.obterSequenciaNumero(numero);

        assertEquals(expected, result);
    }

    //Exercício 10


    @Test
    void Ex10_PrecoMaiorQueDuzentos() {
        double precoNormal = 236.534;

        double expected = 94.6136;
        double result = Bloco2_Exercicio10.obterPrecoSaldo(precoNormal);

        assertEquals(expected, result);
    }

    @Test
    void Ex10_PrecoEntreCemEDuzentos() {
        double precoNormal = 165;

        double expected = 99;
        double result = Bloco2_Exercicio10.obterPrecoSaldo(precoNormal);

        assertEquals(expected, result);
    }

    @Test
    void Ex10_PrecoEntreCinquentaECem() {
        double precoNormal = 67;

        double expected = 46.9;
        double result = Bloco2_Exercicio10.obterPrecoSaldo(precoNormal);

        assertEquals(expected, result);
    }

    @Test
    void Ex10_PrecoMenorQueCinquenta() {
        double precoNormal = 23;

        double expected = 18.4;
        double result = Bloco2_Exercicio10.obterPrecoSaldo(precoNormal);

        assertEquals(expected, result, 0.001);
    }

    //Exercício 11


    @Test
    void Ex11_PercentagemInferiorAZero() {
        double percentagemAprovados = -2;
        double limiteValidacaoTurmaMa = 0.2;
        double limiteValidacaoTurmaFraca = 0.5;
        double limiteValidacaoTurmaRazoavel = 0.7;
        double limiteValidacaoTurmaBoa = 0.9;

        String expected = "Valor inválido";
        String result = Bloco2_Exercicio11.obterClassificacaoTurma(percentagemAprovados, limiteValidacaoTurmaMa, limiteValidacaoTurmaFraca, limiteValidacaoTurmaRazoavel, limiteValidacaoTurmaBoa);

        assertEquals(expected, result);
    }

    @Test
    void Ex11_PercentagemMaiorQueUm() {
        double percentagemAprovados = 1.6;
        double limiteValidacaoTurmaMa = 0.2;
        double limiteValidacaoTurmaFraca = 0.5;
        double limiteValidacaoTurmaRazoavel = 0.7;
        double limiteValidacaoTurmaBoa = 0.9;

        String expected = "Valor inválido";
        String result = Bloco2_Exercicio11.obterClassificacaoTurma(percentagemAprovados, limiteValidacaoTurmaMa, limiteValidacaoTurmaFraca, limiteValidacaoTurmaRazoavel, limiteValidacaoTurmaBoa);

        assertEquals(expected, result);
    }

    @Test
    void Ex11_PercentagemMenorQueZeroPontoDois() {
        double percentagemAprovados = 0.1;
        double limiteValidacaoTurmaMa = 0.2;
        double limiteValidacaoTurmaFraca = 0.5;
        double limiteValidacaoTurmaRazoavel = 0.7;
        double limiteValidacaoTurmaBoa = 0.9;

        String expected = "Turma má";
        String result = Bloco2_Exercicio11.obterClassificacaoTurma(percentagemAprovados, limiteValidacaoTurmaMa, limiteValidacaoTurmaFraca, limiteValidacaoTurmaRazoavel, limiteValidacaoTurmaBoa);

        assertEquals(expected, result);
    }

    @Test
    void Ex11_PercentagemMenorQueZeroPontoCinco() {
        double percentagemAprovados = 0.3;
        double limiteValidacaoTurmaMa = 0.2;
        double limiteValidacaoTurmaFraca = 0.5;
        double limiteValidacaoTurmaRazoavel = 0.7;
        double limiteValidacaoTurmaBoa = 0.9;

        String expected = "Turma fraca";
        String result = Bloco2_Exercicio11.obterClassificacaoTurma(percentagemAprovados, limiteValidacaoTurmaMa, limiteValidacaoTurmaFraca, limiteValidacaoTurmaRazoavel, limiteValidacaoTurmaBoa);

        assertEquals(expected, result);
    }

    @Test
    void Ex11_PercentagemMenorQueZeroPontoSete() {
        double percentagemAprovados = 0.5;
        double limiteValidacaoTurmaMa = 0.2;
        double limiteValidacaoTurmaFraca = 0.5;
        double limiteValidacaoTurmaRazoavel = 0.7;
        double limiteValidacaoTurmaBoa = 0.9;

        String expected = "Turma razoável";
        String result = Bloco2_Exercicio11.obterClassificacaoTurma(percentagemAprovados, limiteValidacaoTurmaMa, limiteValidacaoTurmaFraca, limiteValidacaoTurmaRazoavel, limiteValidacaoTurmaBoa);

        assertEquals(expected, result);
    }

    @Test
    void Ex11_PercentagemMenorQueZeroPontoNove() {
        double percentagemAprovados = 0.89;
        double limiteValidacaoTurmaMa = 0.2;
        double limiteValidacaoTurmaFraca = 0.5;
        double limiteValidacaoTurmaRazoavel = 0.7;
        double limiteValidacaoTurmaBoa = 0.9;

        String expected = "Turma boa";
        String result = Bloco2_Exercicio11.obterClassificacaoTurma(percentagemAprovados, limiteValidacaoTurmaMa, limiteValidacaoTurmaFraca, limiteValidacaoTurmaRazoavel, limiteValidacaoTurmaBoa);

        assertEquals(expected, result);
    }

    @Test
    void Ex11_PercentagemMaiorQueZeroPontoNove() {
        double percentagemAprovados = 0.95;
        double limiteValidacaoTurmaMa = 0.2;
        double limiteValidacaoTurmaFraca = 0.5;
        double limiteValidacaoTurmaRazoavel = 0.7;
        double limiteValidacaoTurmaBoa = 0.9;

        String expected = "Turma excelente";
        String result = Bloco2_Exercicio11.obterClassificacaoTurma(percentagemAprovados, limiteValidacaoTurmaMa, limiteValidacaoTurmaFraca, limiteValidacaoTurmaRazoavel, limiteValidacaoTurmaBoa);

        assertEquals(expected, result);
    }

    @Test
    void Ex12_NívelMaiorQueZeroPontoCinco() {
        double nivelPoluicao = 0.8;

        String expected = "Todos os grupo são intimados a suspender as suas atividades.";
        String result = Bloco2_Exercicio12.intimacaoIndustriasPoluentes(nivelPoluicao);

        assertEquals(expected, result);
    }

    @Test
    void Ex12_NívelMaiorQueZeroPontoQuatro() {
        double nivelPoluicao = 0.5;

        String expected = "O 1º e o 2º grupo são intimados a suspender as suas atividades.";
        String result = Bloco2_Exercicio12.intimacaoIndustriasPoluentes(nivelPoluicao);

        assertEquals(expected, result);
    }

    @Test
    void Ex12_NívelMaiorQueZeroPontoTres() {
        double nivelPoluicao = 0.4;

        String expected = "O 1º grupo é intimado a suspender as suas atividades.";
        String result = Bloco2_Exercicio12.intimacaoIndustriasPoluentes(nivelPoluicao);

        assertEquals(expected, result);
    }

    @Test
    void Ex12_NívelMenorQueZeroPontoTres() {
        double nivelPoluicao = 0.2;

        String expected = "Os níveis de poluição estão dentro dos limites aceitáveis.";
        String result = Bloco2_Exercicio12.intimacaoIndustriasPoluentes(nivelPoluicao);

        assertEquals(expected, result);
    }

    //Exercício 13


    @Test
    void Ex13_CalculoCusto1() {
        double areaGrama = 100; //(areaGrama * 300 + nrArvores * 600 + nrArbustos * 400) / 3600 = 11
        double nrArvores = 12; //10 * areaGrama + 20 * nrArvores + 15 * nrArbustos + 10 * tempoEstimadoHoras
        double nrArbustos = 11;

        String expected = "O tempo estimado é de 12 horas e o custo estimado é de 1520,56";
        String result = Bloco2_Exercicio13.calcularTempoEPrecoEstimado(areaGrama, nrArvores, nrArbustos);

        assertEquals(expected, result);
    }

    @Test
    void Ex13_CalculoCusto2() {
        double areaGrama = 87;
        double nrArvores = 4;
        double nrArbustos = 1;

        String expected = "O tempo estimado é de 8 horas e o custo estimado é de 1045,28";
        String result = Bloco2_Exercicio13.calcularTempoEPrecoEstimado(areaGrama, nrArvores, nrArbustos);
    }

    @Test
    void Ex14_CalculoMediaDistancias1() {
        double distanciaDia1 = 23;
        double distanciaDia2 = 56;
        double distanciaDia3 = 94;
        double distanciaDia4 = 43;
        double distanciaDia5 = 55;

        double expected = 87.2078;
        double result = Bloco2_Exercicio14.obterMediaDistancias(distanciaDia1, distanciaDia2, distanciaDia3, distanciaDia4, distanciaDia5);

        assertEquals(expected, result);
    }

    @Test
    void Ex14_CalculoMediaDistancias2() {
        double distanciaDia1 = 6545;
        double distanciaDia2 = 841;
        double distanciaDia3 = 3214;
        double distanciaDia4 = 7412;
        double distanciaDia5 = 9853;

        double expected = 8966.957;
        double result = Bloco2_Exercicio14.obterMediaDistancias(distanciaDia1, distanciaDia2, distanciaDia3, distanciaDia4, distanciaDia5);

        assertEquals(expected, result);
    }

    //Exercício 15


    @Test
    void Ex15_ClassificacaoTrianguloEquilatero() {

        double a = 56.12;
        double b = 56.12;
        double c = 56.12;

        String expected = "O triângulo é equilátero";
        String result = Bloco2_Exercicio15.obterClassificacaoTriangulo(a, b, c);

        assertEquals(expected, result);
    }

    @Test
    void Ex15_ClassificacaoTrianguloEscaleno() {

        double a = 56.12;
        double b = 56.18;
        double c = 56.15;

        String expected = "O triângulo é escaleno";
        String result = Bloco2_Exercicio15.obterClassificacaoTriangulo(a, b, c);

        assertEquals(expected, result);
    }

    @Test
    void Ex15_ClassificacaoTrianguloIsosceles() {

        double a = 56.12;
        double b = 56.12;
        double c = 56.15;

        String expected = "O triângulo é isósceles";
        String result = Bloco2_Exercicio15.obterClassificacaoTriangulo(a, b, c);

        assertEquals(expected, result);
    }

    @Test
    void Ex15_ClassificacaoTrianguloImpossivel() {

        double a = -2;
        double b = 56.12;
        double c = 56.15;

        String expected = "O triângulo não é possível.";
        String result = Bloco2_Exercicio15.obterClassificacaoTriangulo(a, b, c);

        assertEquals(expected, result);
    }

    //Exercício 16

    @Test
    void Ex16_ClassificacaoAngulosTrianguloImpossivel1() {

        double a = -2;
        double b = 56.12;
        double c = 56.15;

        String expected = "O triângulo não é possível.";
        String result = Bloco2_Exercicio16.obterClassificacaoAngulosTriangulo(a, b, c);

        assertEquals(expected, result);
    }

    @Test
    void Ex16_ClassificacaoAngulosTrianguloImpossivel2() {

        double a = 90;
        double b = 0;
        double c = 90;

        String expected = "O triângulo não é possível.";
        String result = Bloco2_Exercicio16.obterClassificacaoAngulosTriangulo(a, b, c);

        assertEquals(expected, result);
    }

    @Test
    void Ex16_ClassificacaoAngulosTrianguloImpossivel3() {

        double a = 45;
        double b = 45;
        double c = 89;

        String expected = "O triângulo não é possível.";
        String result = Bloco2_Exercicio16.obterClassificacaoAngulosTriangulo(a, b, c);

        assertEquals(expected, result);
    }

    @Test
    void Ex16_ClassificacaoAngulosTrianguloRetangulo() {

        double a = 90;
        double b = 40;
        double c = 50;

        String expected = "O triângulo é retângulo";
        String result = Bloco2_Exercicio16.obterClassificacaoAngulosTriangulo(a, b, c);

        assertEquals(expected, result);
    }

    @Test
    void Ex16_ClassificacaoAngulosTrianguloAcutangulo() {

        double a = 80;
        double b = 50;
        double c = 50;

        String expected = "O triângulo é acutângulo";
        String result = Bloco2_Exercicio16.obterClassificacaoAngulosTriangulo(a, b, c);

        assertEquals(expected, result);
    }

    @Test
    void Ex16_ClassificacaoAngulosTrianguloObtusangulo() {

        double a = 100;
        double b = 30;
        double c = 50;

        String expected = "O triângulo é obtusângulo";
        String result = Bloco2_Exercicio16.obterClassificacaoAngulosTriangulo(a, b, c);

        assertEquals(expected, result);
    }

    //Exercício 17


    @Test
    void Ex17_InfoChegadaNoMesmoDia() {
        int horaPartidaSegundos = 14576;
        int duracaoViagemSegundos = 5400;

        String expected = "O comboio chegou no mesmo dia, às 5:32:56";
        String result = Bloco2_Exercicio17.obterInformacaoChegada(horaPartidaSegundos, duracaoViagemSegundos);

        assertEquals(expected, result);
    }

    @Test
    void Ex17_InfoChegadaNoDiaASeguir() {
        int horaPartidaSegundos = 98676;
        int duracaoViagemSegundos = 10800; //30h ou 6h, 24m, 36s

        String expected = "O comboio chegou no dia seguinte, às 6:24:36";
        String result = Bloco2_Exercicio17.obterInformacaoChegada(horaPartidaSegundos, duracaoViagemSegundos);

        assertEquals(expected, result);
    }

    //Exercício 18

    @Test
    void Ex18_FimProcessamentoMenosVinteEQuatroHoras() {

        int inicioProcessamentoHoras = 18;
        int inicioProcessamentoMinutos = 12;
        int inicioProcessamentoSegundos = 33;
        int duracaoProcessamento = 6856; //1h54m16s

        String expected = "A hora de fim de processamento será às 20:6:49";
        String result = Bloco2_Exercicio18.processamentoTarefa(inicioProcessamentoHoras, inicioProcessamentoMinutos, inicioProcessamentoSegundos, duracaoProcessamento);

        assertEquals(expected, result);
    }

    @Test
    void Ex18_FimProcessamentoMaisVinteEQuatroHorasMaisDeSessentaSegundos() {

        int inicioProcessamentoHoras = 22;
        int inicioProcessamentoMinutos = 45;
        int inicioProcessamentoSegundos = 30;
        int duracaoProcessamento = 9000; //2h30m00s

        String expected = "A hora de fim de processamento será às 1:15:30 do dia seguinte";
        String result = Bloco2_Exercicio18.processamentoTarefa(inicioProcessamentoHoras, inicioProcessamentoMinutos, inicioProcessamentoSegundos, duracaoProcessamento);

        assertEquals(expected, result);
    }

    @Test
    void Ex18_FimProcessamentoMaisSessentaSegundos() {

        int inicioProcessamentoHoras = 9;
        int inicioProcessamentoMinutos = 0;
        int inicioProcessamentoSegundos = 45;
        int duracaoProcessamento = 9020; //2h30m20s

        String expected = "A hora de fim de processamento será às 11:31:5";
        String result = Bloco2_Exercicio18.processamentoTarefa(inicioProcessamentoHoras, inicioProcessamentoMinutos, inicioProcessamentoSegundos, duracaoProcessamento);

        assertEquals(expected, result);
    }

    //Exercício 19


    @Test
    void Ex19_CalculoSalarioSemanalSemHorasExtra() {

        double horasExtra = 0;

        String expected = "O salário semanal do empregado é 270,00";
        String result = Bloco2_Exercicio19.salarioEmpregado(horasExtra);

        assertEquals(expected, result);
    }

    @Test
    void Ex19_CalculoSalarioSemanalComMenosDeCincoHorasExtra() {

        double horasExtra = 3;

        String expected = "O salário semanal do empregado é 300,00";
        String result = Bloco2_Exercicio19.salarioEmpregado(horasExtra);

        assertEquals(expected, result);
    }

    @Test
    void Ex19_CalculoSalarioSemanalComMaisDeCincoHorasExtra() {

        double horasExtra = 6;

        String expected = "O salário semanal do empregado é 360,00";
        String result = Bloco2_Exercicio19.salarioEmpregado(horasExtra);

        assertEquals(expected, result);
    }

    @Test
    void Ex19_CalculoSalarioSemanalComHorasExtraNegativas() {

        double horasExtra = -2;

        String expected = "Informação inválida.";
        String result = Bloco2_Exercicio19.salarioEmpregado(horasExtra);

        assertEquals(expected, result);
    }

    //Exercicio 20


    @Test
    void Ex20_CalculoPrecoTipoA() {
        int nrDiasSemana = 3;
        int nrDiasFimSemana = 1;
        String tipoKit = "A";
        double nrKms = 10;

        String expected = "O preço do Kit A será de 150,00";
        String result = Bloco2_Exercicio20.obterPrecoAluguerUtensilios(nrDiasSemana, nrDiasFimSemana, tipoKit, nrKms);

        assertEquals(expected, result);
    }

    @Test
    void Ex20_CalculoPrecoTipoB() {
        int nrDiasSemana = 2;
        int nrDiasFimSemana = 2;
        String tipoKit = "B";
        double nrKms = 46.4;

        String expected = "O preço do Kit B será de 332,80";
        String result = Bloco2_Exercicio20.obterPrecoAluguerUtensilios(nrDiasSemana, nrDiasFimSemana, tipoKit, nrKms);

        assertEquals(expected, result);
    }

    @Test
    void Ex20_CalculoPrecoTipoC() {
        int nrDiasSemana = 5;
        int nrDiasFimSemana = 0;
        String tipoKit = "C";
        double nrKms = 19.52;

        String expected = "O preço do Kit C será de 539,04";
        String result = Bloco2_Exercicio20.obterPrecoAluguerUtensilios(nrDiasSemana, nrDiasFimSemana, tipoKit, nrKms);

        assertEquals(expected, result);
    }
}

