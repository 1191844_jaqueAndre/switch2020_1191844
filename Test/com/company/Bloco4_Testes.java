package com.company;

import com.company.bloco4.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class Bloco4_Testes {

//---------Exercise 1---------------------------------------------------------------------------------------------------

    @Test
    void Exercise1_NumberIsOneTwoThreeFourFiveSixSevenEightNineZero() {

        int number = 1234567890;

        int expected = 10;
        int result = Exercise1.returnNumberOfDigits(number);

        assertEquals(expected, result);

    }

    @Test
    void Exercise1_NumberIsZeroZeroZeroThreeTwoOne() {

        int number = 000321;

        int expected = 3;
        int result = Exercise1.returnNumberOfDigits(number);

        assertEquals(expected, result);

    }

    @Test
    void Exercise1_NumberIsMinusTwo() {

        int number = -2;

        Integer result = Exercise1.returnNumberOfDigits(number);

        assertNull(result);

    }

    @Test
    void Exercise1_NumberIsThreeSixSevenEightOne() {

        int number = 36781;

        int expected = 5;
        int result = Exercise1.returnNumberOfDigits(number);

        assertEquals(expected, result);

    }


//---------Exercise 2---------------------------------------------------------------------------------------------------

    @Test
    void Exercise2_NumberIsThreeSixSevenEightOne() {

        int number = 36781;

        int[] expected = {3, 6, 7, 8, 1};
        int[] result = Exercise2.getNumberDigits(number);

        assertArrayEquals(expected, result);

    }

    @Test
    void Exercise2_NumberIsZero() {

        int number = 0;

        int[] result = Exercise2.getNumberDigits(number);

        assertNull(result);

    }

    @Test
    void Exercise2_NumberIsMinusThree() {

        int number = -3;

        int[] result = Exercise2.getNumberDigits(number);

        assertNull(result);

    }

    @Test
    void Exercise2_NumberIsOneTwoThreeFourFiveSixSevenEightNineZero() {

        int number = 1234567890;

        int[] expected = {1, 2, 3, 4, 5, 6, 7, 8, 9, 0};
        int[] result = Exercise2.getNumberDigits(number);

        assertArrayEquals(expected, result);

    }

    @Test
    void Exercise2_NumberIsOne() {

        int number = 1;

        int[] expected = {1};
        int[] result = Exercise2.getNumberDigits(number);

        assertArrayEquals(expected, result);

    }

//---------Exercise 3---------------------------------------------------------------------------------------------------

    @Test
    void Exercise3_NumberArrayIsThreeSixSevenEightOne() {

        int[] numberArray = {3, 6, 7, 8, 1};

        Integer expected = 25;
        Integer result = Exercise3.addVectorElements(numberArray);

        assertEquals(expected, result);

    }

    @Test
    void Exercise3_NumberArrayIsOneTwoThreeFourFiftySixSevenEightNineZero() {

        int[] numberArray = {1, 2, 3, 4, 50, 6, 7, 8, 9, 0};

        Integer expected = 90;
        Integer result = Exercise3.addVectorElements(numberArray);

        assertEquals(expected, result);

    }

    @Test
    void Exercise3_NumberArrayIsOneMinusOneThreeMinusFour() {

        int[] numberArray = {1, -1, 3, -4};

        Integer expected = -1;
        Integer result = Exercise3.addVectorElements(numberArray);

        assertEquals(expected, result);

    }

    @Test
    void Exercise3_NumberArrayIsZero() {

        int[] numberArray = {0};

        Integer expected = 0;
        Integer result = Exercise3.addVectorElements(numberArray);

        assertEquals(expected, result);

    }

    @Test
    void Exercise3_NumberArrayIsEmpty() {

        int[] numberArray = {};

        Integer result = Exercise3.addVectorElements(numberArray);

        assertNull(result);

    }

//---------Exercise 4 (return even numbers)-----------------------------------------------------------------------------

    @Test
    void Exercise4A_NumberIsThreeSixSevenEightOne() {

        int[] numberArray = {3, 6, 7, 8, 1};

        int[] expected = {6, 8};
        int[] result = Exercise4.getEvenNumbersFromArray(numberArray);

        assertArrayEquals(expected, result);

    }

    @Test
    void Exercise4A_NumberIsOneTwoThreeFourFiveSixSevenEightNineZero() {

        int[] numberArray = {1, 2, 3, 4, 5, 6, 7, 8, 9, 0};

        int[] expected = {2, 4, 6, 8, 0};
        int[] result = Exercise4.getEvenNumbersFromArray(numberArray);

        assertArrayEquals(expected, result);

    }

    @Test
    void Exercise4A_NumberIsOneMinusOneThreeMinusFour() {

        int[] numberArray = {1, -1, 3, -4};

        int[] expected = {-4};
        int[] result = Exercise4.getEvenNumbersFromArray(numberArray);

        assertArrayEquals(expected, result);

    }

    @Test
    void Exercise4A_NumberIsZero() {

        int[] numberArray = {0};

        int[] expected = {0};
        int[] result = Exercise4.getEvenNumbersFromArray(numberArray);

        assertArrayEquals(expected, result);

    }

    @Test
    void Exercise4A_NumberIsMinusThree() {

        int[] numberArray = {-3};

        int[] expected = {};
        int[] result = Exercise4.getEvenNumbersFromArray(numberArray);

        assertArrayEquals(expected, result);

    }

    @Test
    void Exercise4A_NumberArrayIsEmpty() {

        int[] numberArray = {};

        int[] result = Exercise4.getEvenNumbersFromArray(numberArray);

        assertNull(result);

    }

//---------Exercise 4 (return odd numbers)------------------------------------------------------------------------------

    @Test
    void Exercise4B_NumberIsThreeSixSevenEightOne() {

        int[] numberArray = {3, 6, 7, 8, 1};

        int[] expected = {3, 7, 1};
        int[] result = Exercise4.getOddNumbersFromArray(numberArray);

        assertArrayEquals(expected, result);

    }

    @Test
    void Exercise4B_NumberIsOneTwoThreeFourFiveSixSevenEightNineZero() {

        int[] numberArray = {1, 2, 3, 4, 5, 6, 7, 8, 9, 0};

        int[] expected = {1, 3, 5, 7, 9};
        int[] result = Exercise4.getOddNumbersFromArray(numberArray);

        assertArrayEquals(expected, result);

    }

    @Test
    void Exercise4B_NumberIsOneMinusOneThreeMinusFour() {

        int[] numberArray = {1, -1, 3, -4};

        int[] expected = {1, -1, 3};
        int[] result = Exercise4.getOddNumbersFromArray(numberArray);

        assertArrayEquals(expected, result);

    }


    @Test
    void Exercise4B_NumberIsMinusThree() {

        int[] numberArray = {-3};

        int[] expected = {-3};
        int[] result = Exercise4.getOddNumbersFromArray(numberArray);

        assertArrayEquals(expected, result);

    }

    @Test
    void Exercise4B_NumberArrayIsEmpty() {

        int[] numberArray = {};

        int[] result = Exercise4.getOddNumbersFromArray(numberArray);

        assertNull(result);

    }

//---------Exercise 5 (sum even numbers)--------------------------------------------------------------------------------

    @Test
    void Exercise5A_NumberIsThreeSixSevenEightOne() {

        int number = 36781;

        Integer expected = 14;
        Integer result = Exercise5.addEvenNumbers(number);

        assertEquals(expected, result);

    }

    @Test
    void Exercise5A_NumberIsOneTwoThreeFourFiveSixSevenEightNineZero() {

        int number = 1234567890;

        Integer expected = 20;
        Integer result = Exercise5.addEvenNumbers(number);

        assertEquals(expected, result);

    }

    @Test
    void Exercise5A_NumberIsThreeFiveSeven() {

        int number = 357;

        Integer expected = 0;
        Integer result = Exercise5.addEvenNumbers(number);

        assertEquals(expected, result);

    }

    @Test
    void Exercise5A_NumberMinusFive() {

        int number = -5;

        Integer result = Exercise5.addEvenNumbers(number);

        assertNull(result);

    }

//---------Exercise 5 (sum odd numbers)--------------------------------------------------------------------------------

    @Test
    void Exercise5B_NumberIsThreeSixSevenEightOne() {

        int number = 36781;

        Integer expected = 11;
        Integer result = Exercise5.addOddNumbers(number);

        assertEquals(expected, result);

    }

    @Test
    void Exercise5B_NumberIsOneTwoThreeFourFiveSixSevenEightNineZero() {

        int number = 1234567890;

        Integer expected = 25;
        Integer result = Exercise5.addOddNumbers(number);

        assertEquals(expected, result);

    }

    @Test
    void Exercise5B_NumberMinusFive() {

        int number = -5;

        Integer result = Exercise5.addOddNumbers(number);

        assertNull(result);

    }

//---------Exercise 6---------------------------------------------------------------------------------------------------

    @Test
    void Exercise6_NumberIsThreeFourSixEightOneTwentyThreeEightyNineTwelveAndNEqualsFour() {

        int[] numberArray = {3, 4, 6, 8, 1, 23, 89, 12};
        int size = 4;

        int[] expected = {3, 4, 6, 8};
        int[] result = Exercise6.getArrayWithNElements(numberArray, size);

        assertArrayEquals(expected, result);

    }

    @Test
    void Exercise6_NumberIsOneTwoThreeFourFiveSixSevenEightNineZeroAndNEqualsSix() {

        int[] numberArray = {1, 2, 3, 4, 5, 6, 7, 8, 9, 0};
        int size = 6;

        int[] expected = {1, 2, 3, 4, 5, 6};
        int[] result = Exercise6.getArrayWithNElements(numberArray, size);

        assertArrayEquals(expected, result);
        assertNotSame(expected, result);

    }

    @Test
    void Exercise6_NumberIsOneTwoThreeAndNEqualsSix() {

        int[] numberArray = {1, 2, 3};
        int size = 6;

        int[] result = Exercise6.getArrayWithNElements(numberArray, size);

        assertNull(result);

    }

    @Test
    void Exercise6_NumberIsOneTwoThreeAndNEqualsMinusThree() {

        int[] numberArray = {1, 2, 3};
        int size = -3;

        int[] result = Exercise6.getArrayWithNElements(numberArray, size);

        assertNull(result);

    }

    @Test
    void Exercise6_NumberIsNullAndNEqualsZero() {

        int[] numberArray = {};
        int size = 0;

        int[] expected = {};
        int[] result = Exercise6.getArrayWithNElements(numberArray, size);

        assertArrayEquals(expected, result);

    }

    @Test
    void Exercise6_NumberIsMinusFiveAndNEqualsOne() {

        int[] numberArray = {-5};
        int size = 1;

        int[] expected = {-5};
        int[] result = Exercise6.getArrayWithNElements(numberArray, size);

        assertArrayEquals(expected, result);

    }

//---------Exercise 7---------------------------------------------------------------------------------------------------

    @Test
    void Exercise7_RangeBetweenFourAndTenNIsThree() {

        int rangeStart = 4;
        int rangeEnd = 10;
        int n = 3;

        int[] expected = {6, 9};
        int[] result = Exercise7.getMultiplesOfN(rangeStart, rangeEnd, n);

        assertArrayEquals(expected, result);

    }

    @Test
    void Exercise7_RangeBetweenTenAndTwentyNIsFive() {

        int rangeStart = 10;
        int rangeEnd = 20;
        int n = 5;

        int[] expected = {10, 15, 20};
        int[] result = Exercise7.getMultiplesOfN(rangeStart, rangeEnd, n);

        assertArrayEquals(expected, result);

    }

    @Test
    void Exercise7_RangeBetweenMinusFourAndFourteenNIsTwo() {

        int rangeStart = -4;
        int rangeEnd = 14;
        int n = 2;

        int[] expected = {-4, -2, 0, 2, 4, 6, 8, 10, 12, 14};
        int[] result = Exercise7.getMultiplesOfN(rangeStart, rangeEnd, n);

        assertArrayEquals(expected, result);

    }

    @Test
    void Exercise7_RangeBetweenOneAndSixNIsNine() {

        int rangeStart = 1;
        int rangeEnd = 6;
        int n = 9;

        int[] expected = {};
        int[] result = Exercise7.getMultiplesOfN(rangeStart, rangeEnd, n);

        assertArrayEquals(expected, result);

    }

    @Test
    void Exercise7_RangeBetweenSixAndOneNIsThree() {

        int rangeStart = 6;
        int rangeEnd = 1;
        int n = 3;

        int[] expected = {3, 6};
        int[] result = Exercise7.getMultiplesOfN(rangeStart, rangeEnd, n);

        assertArrayEquals(expected, result);

    }

//---------Exercise 8---------------------------------------------------------------------------------------------------


    @Test
    void Exercise8_RangeBetweenFourAndTwelveDivisorsAreTwoAndThree() {

        int rangeStart = 4;
        int rangeEnd = 12;
        int[] divisorsArray = {2, 3};

        int[] expected = {6, 12};
        int[] result = Exercise8.getCommonMultiples(rangeStart, rangeEnd, divisorsArray);

        assertArrayEquals(expected, result);

    }

    @Test
    void Exercise8_RangeBetweenTwelveAndFourDivisorsAreTwoAndThree() {

        int rangeStart = 12;
        int rangeEnd = 4;
        int[] divisorsArray = {2, 3};

        int[] expected = {6, 12};
        int[] result = Exercise8.getCommonMultiples(rangeStart, rangeEnd, divisorsArray);

        assertArrayEquals(expected, result);

    }

    @Test
    void Exercise8_RangeBetweenZeroAndZeroDivisorsAreTwoAndThree() {

        int rangeStart = 0;
        int rangeEnd = 0;
        int[] divisorsArray = {2, 3};

        int[] expected = {0};
        int[] result = Exercise8.getCommonMultiples(rangeStart, rangeEnd, divisorsArray);

        assertArrayEquals(expected, result);

    }

    @Test
    void Exercise8_RangeBetweenFourAndTwelveDivisorIsZero() {

        int rangeStart = 4;
        int rangeEnd = 12;
        int[] divisorsArray = {0};

        int[] result = Exercise8.getCommonMultiples(rangeStart, rangeEnd, divisorsArray);

        assertArrayEquals(null, result);

    }

    @Test
    void Exercise8_RangeBetweenMinusFourAndTwelveDivisorsAreTwoThreeFive() {

        int rangeStart = -4;
        int rangeEnd = 12;
        int[] divisorsArray = {2, 3, 5};

        int[] expected = {0};
        int[] result = Exercise8.getCommonMultiples(rangeStart, rangeEnd, divisorsArray);

        assertArrayEquals(expected, result);

    }

    @Test
    void Exercise8_RangeBetweenThreeAndFourDivisorIsFive() {

        int rangeStart = 3;
        int rangeEnd = 4;
        int[] divisorsArray = {5};

        int[] result = Exercise8.getCommonMultiples(rangeStart, rangeEnd, divisorsArray);

        assertArrayEquals(null, result);

    }

    @Test
    void Exercise8_RangeBetweenThreeAndTenDivisorIsFiveDivisorsArrayEmpty() {

        int rangeStart = 3;
        int rangeEnd = 10;
        int[] divisorsArray = {};

        int[] result = Exercise8.getCommonMultiples(rangeStart, rangeEnd, divisorsArray);

        assertArrayEquals(null, result);

    }

//---------Exercise 9---------------------------------------------------------------------------------------------------


    @Test
    void Exercise9_NumberIsOneTwoThreeFourThreeTwoOneSuccess() {

        int number = 1234321;

        Boolean result = Exercise9.validateAndVerifyIfPalindrome(number);

        assertTrue(result);
    }

    @Test
    void Exercise9_NumberIsOneTwoThreeFourFiveFailure() {

        int number = 12345;

        Boolean result = Exercise9.validateAndVerifyIfPalindrome(number);

        assertFalse(result);
    }

    @Test
    void Exercise9_NumberIsMinusThreeTwoTwoThreeReturnErrorMessage() {

        int number = -3223;

        Boolean result = Exercise9.validateAndVerifyIfPalindrome(number);

        assertNull(result);
    }

    @Test
    void Exercise9_NumberIsZeroReturnErrorMessage() {

        int number = 0;

        Boolean result = Exercise9.validateAndVerifyIfPalindrome(number);

        assertNull(result);
    }

//---------Exercise 10 a) ----------------------------------------------------------------------------------------------

    @Test
    void Exercise10A_NumberArrayIsNineEightSevenSixFiveFourThreeTwoOneZero() {

        int[] numberArray = {9, 8, 7, 6, 5, 4, 3, 2, 1, 0};

        Integer expected = 0;
        Integer result = Exercise10.getSmallestElement(numberArray);

        assertEquals(expected, result);
    }

    @Test
    void Exercise10A_NumberArrayIsNineEightNine() {

        int[] numberArray = {9, 8, 9};

        Integer expected = 8;
        Integer result = Exercise10.getSmallestElement(numberArray);

        assertEquals(expected, result);
    }

    @Test
    void Exercise10A_NumberArrayIsMinusOneFiveZero() {

        int[] numberArray = {-1, 5, 0};

        Integer expected = -1;
        Integer result = Exercise10.getSmallestElement(numberArray);

        assertEquals(expected, result);
    }

    @Test
    void Exercise10A_NumberArrayIsEmpty() {

        int[] numberArray = {};

        Integer expected = null;
        Integer result = Exercise10.getSmallestElement(numberArray);

        assertEquals(expected, result);
    }

//---------Exercise 10 b) ----------------------------------------------------------------------------------------------

    @Test
    void Exercise10B_NumberArrayIsNineEightSevenSixFiveFourThreeTwoOneZero() {

        int[] numberArray = {9, 8, 7, 6, 5, 4, 3, 2, 1, 0};

        Integer expected = 9;
        Integer result = Exercise10.getBiggestElement(numberArray);

        assertEquals(expected, result);
    }

    @Test
    void Exercise10B_NumberArrayIsSevenEightSeven() {

        int[] numberArray = {7, 8, 7};

        Integer expected = 8;
        Integer result = Exercise10.getBiggestElement(numberArray);

        assertEquals(expected, result);
    }

    @Test
    void Exercise10B_NumberArrayIsMinusOneFiveZero() {

        int[] numberArray = {-1, 5, 0};

        Integer expected = 5;
        Integer result = Exercise10.getBiggestElement(numberArray);

        assertEquals(expected, result);
    }

    @Test
    void Exercise10B_NumberArrayIsMinusOneMinusThreeMinusTwo() {

        int[] numberArray = {-1, -3, -2};

        Integer expected = -1;
        Integer result = Exercise10.getBiggestElement(numberArray);

        assertEquals(expected, result);
    }

    @Test
    void Exercise10B_NumberArrayIsEmpty() {

        int[] numberArray = {};

        Integer expected = null;
        Integer result = Exercise10.getBiggestElement(numberArray);

        assertEquals(expected, result);
    }

//---------Exercise 10 c) ----------------------------------------------------------------------------------------------

    @Test
    void Exercise10C_NumberArrayIsThirtyOneThirtyThreeTwentyFourSixZero() {

        int[] numberArray = {31, 33, 24, 6, 0};

        String expected = "18,80";
        String result = Exercise10.getElementsMean(numberArray);

        assertEquals(expected, result);
    }

    @Test
    void Exercise10C_NumberArrayIsMinusTwentyMinusFifteenFortyMinusFifteen() {

        int[] numberArray = {-20, -15, 40, -5};

        String expected = "0,00";
        String result = Exercise10.getElementsMean(numberArray);

        assertEquals(expected, result);
    }

    @Test
    void Exercise10C_NumberArrayIsEightThreeTwoSevenNinety() {

        int[] numberArray = {8, 3, 2, 7, 90};

        String expected = "22,00";
        String result = Exercise10.getElementsMean(numberArray);

        assertEquals(expected, result);
    }

    @Test
    void Exercise10C_NumberArrayIsEightMinusThreeTwoSevenMinusNinety() {

        int[] numberArray = {8, -3, 2, 7, -90};

        String expected = "-15,20";
        String result = Exercise10.getElementsMean(numberArray);

        assertEquals(expected, result);
    }

    @Test
    void Exercise10C_NumberArrayIsEmpty() {

        int[] numberArray = {};

        String expected = null;
        String result = Exercise10.getElementsMean(numberArray);

        assertNull(result);
    }

//---------Exercise 10 d) ----------------------------------------------------------------------------------------------

    @Test
    void Exercise10D_NumberArrayIsThirtyOneThirtyThreeTwentyFourSixZero() {

        int[] numberArray = {31, 33, 24, 6, 0};

        Integer expected = 0;
        Integer result = Exercise10.getElementsProduct(numberArray);

        assertEquals(expected, result);
    }

    @Test
    void Exercise10D_NumberArrayIsThirtyOneThirtyThreeTwentyFourSixTwo() {

        int[] numberArray = {31, 33, 24, 6, 2};

        Integer expected = 294624;
        Integer result = Exercise10.getElementsProduct(numberArray);

        assertEquals(expected, result);
    }

    @Test
    void Exercise10D_NumberArrayIsMinusTwoTwoTwoThreeTwo() {

        int[] numberArray = {-2, 2, 2, 3, 2};

        Integer expected = -48;
        Integer result = Exercise10.getElementsProduct(numberArray);

        assertEquals(expected, result);
    }

    @Test
    void Exercise10D_NumberArrayIsMinusTwoTwoTwoMinusThreeTwo() {

        int[] numberArray = {-2, 2, 2, -3, 2};

        Integer expected = 48;
        Integer result = Exercise10.getElementsProduct(numberArray);

        assertEquals(expected, result);
    }

    @Test
    void Exercise10D_NumberArrayIsEmpty() {

        int[] numberArray = {};

        String expected = null;
        String result = Exercise10.getElementsMean(numberArray);

        assertNull(result);
    }

//---------Exercise 10 e) ----------------------------------------------------------------------------------------------

    @Test
    void Exercise10E_NumberArrayIsMinusTwoTwoTwoMinusThreeTwo() {

        int[] numberArray = {-2, 2, 2, -3, 2};

        int[] expected = {-3, -2, 2};
        int[] result = Exercise10.getUniqueElements(numberArray);

        assertArrayEquals(expected, result);
    }

    @Test
    void Exercise10E_NumberArrayIsTwoFiveSevenThreeTwoTwoFive() {

        int[] numberArray = {2, 5, 7, 3, 2, 2, 5};

        int[] expected = {2, 3, 5, 7};
        int[] result = Exercise10.getUniqueElements(numberArray);

        assertArrayEquals(expected, result);
    }

    @Test
    void Exercise10E_NumberArrayIsFortyFiftyThreeThirtyTwelveTwelveFourteen() {

        int[] numberArray = {40, 50, 3, 30, 12, 12, 14};

        int[] expected = {3, 12, 14, 30, 40, 50};
        int[] result = Exercise10.getUniqueElements(numberArray);

        assertArrayEquals(expected, result);
    }

    @Test
    void Exercise10E_NumberArrayIsEmpty() {

        int[] numberArray = {};

        String expected = null;
        String result = Exercise10.getElementsMean(numberArray);

        assertNull(result);
    }


//---------Exercise 10 f) ----------------------------------------------------------------------------------------------


    @Test
    void Exercise10F_OriginalArrayIsTwoThreeFour() {

        int[] numberArray = {2, 3, 4};

        int[] expected = {4, 3, 2};
        int[] result = Exercise10.getReversed(numberArray);

        assertArrayEquals(expected, result);
    }

    @Test
    void Exercise10F_OriginalArrayIsMinusFour() {

        int[] numberArray = {-4};

        int[] expected = {-4};
        int[] result = Exercise10.getReversed(numberArray);

        assertArrayEquals(expected, result);
    }

    @Test
    void Exercise10F_OriginalArrayIsEmpty() {

        int[] numberArray = {};

        int[] expected = null;
        int[] result = Exercise10.getReversed(numberArray);

        assertArrayEquals(expected, result);
    }

    @Test
    void Exercise10F_OriginalArrayIsTwentyThirtyFortyFifty() {

        int[] numberArray = {20, 30, 40, 50};

        int[] expected = {50, 40, 30, 20};
        int[] result = Exercise10.getReversed(numberArray);

        assertArrayEquals(expected, result);
    }

//---------Exercise 10 g) ----------------------------------------------------------------------------------------------

    @Test
    void Exercise10G_ArrayIsTwoNineteenEightSixSeven() {

        int[] numberArray = {2, 19, 8, 6, 7};

        int[] expected = {2, 19, 7};
        int[] result = Exercise10.getPrimeElements(numberArray);

        assertArrayEquals(expected, result);
    }

    @Test
    void Exercise10G_ArrayIsFourSixEightTen() {

        int[] numberArray = {4, 6, 8, 10};

        int[] expected = {};
        int[] result = Exercise10.getPrimeElements(numberArray);

        assertArrayEquals(expected, result);
    }

    @Test
    void Exercise10G_ArrayIsEmpty() {

        int[] numberArray = {};

        int[] expected = null;
        int[] result = Exercise10.getPrimeElements(numberArray);

        assertArrayEquals(expected, result);
    }

    @Test
    void Exercise10G_ArrayIsFiveSevenElevenNineteenThirteen() {

        int[] numberArray = {5, 7, 11, 19, 13};
        int[] primeElements = {5, 7, 11, 19, 13};

        int[] expected = {5, 7, 11, 19, 13};
        int[] result = Exercise10.getPrimeElements(numberArray);

        assertArrayEquals(expected, result);
        assertNotEquals(primeElements, numberArray);
    }

//---------Exercise 11 -------------------------------------------------------------------------------------------------


    @Test
    void Exercise11_ArrayOneIsOneTwoThreeFourArrayTwoIsOneTwoThreeFourFive() {

        int[] vector1 = {1, 2, 3, 4};
        int[] vector2 = {1, 2, 3, 4, 5};

        Integer expected = 30;
        Integer result = Exercise11.getDotProductOfTwoVectors(vector1, vector2);

        assertEquals(expected, result);
    }

    @Test
    void Exercise11_ArrayOneIsFiveFourThreeFourArrayTwoIsOneTwo() {

        int[] vector1 = {5, 4, 3, 4};
        int[] vector2 = {1, 2};

        Integer expected = 13;
        Integer result = Exercise11.getDotProductOfTwoVectors(vector1, vector2);

        assertEquals(expected, result);
    }

    @Test
    void Exercise11_ArrayOneEmptyArrayTwoIsOneTwo() {

        int[] vector1 = {};
        int[] vector2 = {1, 2};

        Integer result = Exercise11.getDotProductOfTwoVectors(vector1, vector2);

        assertNull(result);
    }

    @Test
    void Exercise11_ArrayOneIsOneTwoFiveArrayTwoIsEmpty() {

        int[] vector1 = {1, 2, 5};
        int[] vector2 = {};

        Integer result = Exercise11.getDotProductOfTwoVectors(vector1, vector2);

        assertNull(result);
    }

    @Test
    void Exercise11_ArrayOneIsFiftyFiveNinetyOneThirtyFourArrayTwoSixtySixFiftyEighty() {

        int[] vector1 = {55, 91, 34};
        int[] vector2 = {66, 50, 80};

        Integer expected = 10900;
        Integer result = Exercise11.getDotProductOfTwoVectors(vector1, vector2);

        assertEquals(expected, result);
    }

//---------Exercise 12 -------------------------------------------------------------------------------------------------

    @Test
    void Exercise12_MatrixLineOneHasFourColumnsLineTwoHasThreeLineFourHasFour() {
        int[][] numberMatrix = {{1, 3, 4, 5}, {3, 4, 5}, {7, 8, 6, 5}};

        Integer expected = -1;
        Integer result = Exercise12.verifyIfSameNrColumns(numberMatrix);

        assertEquals(expected, result);
    }

    @Test
    void Exercise12_MatrixLinesHaveFourColumnsEach() {
        int[][] numberMatrix = {{1, 3, 4, 5}, {3, 4, 5, 3}, {7, 8, 6, 5}};

        Integer expected = 4;
        Integer result = Exercise12.verifyIfSameNrColumns(numberMatrix);

        assertEquals(expected, result);
    }

    @Test
    void Exercise12_MatrixLineOneHasFourColumnsLineTwoIsEmptyLineThreeHasFourColumns() {
        int[][] numberMatrix = {{1, 3, 4, 5}, {}, {7, 8, 6, 5}};

        Integer result = Exercise12.verifyIfSameNrColumns(numberMatrix);

        assertNull(result);
    }

    @Test
    void Exercise12_MatrixLinesHaveFiveColumns() {
        int[][] numberMatrix = {{1, 3, 4, 5, 6}, {9, 8, 5, 6, 7}, {7, 8, 6, 5, 2}};

        Integer expected = 5;
        Integer result = Exercise12.verifyIfSameNrColumns(numberMatrix);

        assertEquals(expected, result);
    }

    @Test
    void Exercise12_MatrixFirstLineIsEmpty() {
        int[][] numberMatrix = {{}, {9, 8, 5, 6, 7}, {7, 8, 6, 5, 2}};

        Integer result = Exercise12.verifyIfSameNrColumns(numberMatrix);

        assertNull(result);
    }

    @Test
    void Exercise12_MatrixFirstSecondLineIsEmpty() {
        int[][] numberMatrix = {{1, 3, 4, 5, 6}, {}, {7, 8, 6, 5, 2}};

        Integer result = Exercise12.verifyIfSameNrColumns(numberMatrix);

        assertNull(result);
    }

    @Test
    void Exercise12_MatrixFirstThirdLineIsEmpty() {
        int[][] numberMatrix = {{1, 3, 4, 5, 6}, {9, 8, 5, 6, 7}, {}};

        Integer result = Exercise12.verifyIfSameNrColumns(numberMatrix);

        assertNull(result);
    }

//---------Exercise 13 -------------------------------------------------------------------------------------------------

    @Test
    void Exercise13_MatrixLinesHaveThreeColumnsAndThreeLines() {
        int[][] numberMatrix = {{1, 3, 4}, {9, 8, 5}, {7, 8, 6}};

        Boolean result = Exercise13.verifyIfMatrixIsSquare(numberMatrix);

        assertTrue(result);
    }

    @Test
    void Exercise13_MatrixLinesHaveTwoColumnsAndTwoLines() {
        int[][] numberMatrix = {{3, 4}, {8, 5}};

        Boolean result = Exercise13.verifyIfMatrixIsSquare(numberMatrix);

        assertTrue(result);
    }

    @Test
    void Exercise13_MatrixLinesHaveThreeColumnsAndTwoLines() {
        int[][] numberMatrix = {{1, 3, 4}, {9, 8, 5}};

        Boolean result = Exercise13.verifyIfMatrixIsSquare(numberMatrix);

        assertFalse(result);
    }


    @Test
    void Exercise13_MatrixIsSquareOneLineValuesAreZero() {
        int[][] numberMatrix = {{0, 0, 0}, {9, 8, 5}, {8, 5, 2}};

        Boolean result = Exercise13.verifyIfMatrixIsSquare(numberMatrix);

        assertTrue(result);
    }

    @Test
    void Exercise13_MatrixIsSquareNegativeValues() {
        int[][] numberMatrix = {{0, 0, 0}, {9, -8, -5}, {-8, 5, 2}};

        Boolean result = Exercise13.verifyIfMatrixIsSquare(numberMatrix);

        assertTrue(result);
    }

    @Test
    void Exercise13_FirstLineIsEmpty() {
        int[][] numberMatrix = {{}, {9, 8, 5}};

        Boolean result = Exercise13.verifyIfMatrixIsSquare(numberMatrix);

        assertFalse(result);
    }

    @Test
    void Exercise13_SecondLineIsEmpty() {
        int[][] numberMatrix = {{9, 8, 5}, {}};

        Boolean result = Exercise13.verifyIfMatrixIsSquare(numberMatrix);

        assertFalse(result);
    }

    @Test
    void Exercise13_ThirdLineIsEmpty() {
        int[][] numberMatrix = {{9, 8, 5}, {7, 2, 6}, {}};

        Boolean result = Exercise13.verifyIfMatrixIsSquare(numberMatrix);

        assertFalse(result);
    }

//---------Exercise 14 -------------------------------------------------------------------------------------------------

    @Test
    void Exercise14_MatrixLinesHaveThreeColumnsAndThreeLines() {
        int[][] numberMatrix = {{1, 3, 4}, {9, 8, 5}, {7, 8, 6}};

        Boolean result = Exercise14.verifyIfMatrixIsRectangular(numberMatrix);

        assertFalse(result);
    }

    @Test
    void Exercise14_MatrixLinesHaveThreeColumnsAndTwoLines() {
        int[][] numberMatrix = {{1, 3, 4}, {9, 8, 5}};

        Boolean result = Exercise14.verifyIfMatrixIsRectangular(numberMatrix);

        assertTrue(result);
    }


    @Test
    void Exercise14_MatrixIsSquare() {
        int[][] numberMatrix = {{3, 4}, {8, 5}};

        Boolean result = Exercise14.verifyIfMatrixIsRectangular(numberMatrix);

        assertFalse(result);
    }

    @Test
    void Exercise14_MatrixIsRectangularOneLineValuesAreZero() {
        int[][] numberMatrix = {{0, 0, 0}, {9, 8, 5}};

        Boolean result = Exercise14.verifyIfMatrixIsRectangular(numberMatrix);

        assertTrue(result);
    }

    @Test
    void Exercise14_MatrixLinesHaveADifferentNrOfColumns() {
        int[][] numberMatrix = {{0, 0, 0}, {9, -8}};

        Boolean result = Exercise14.verifyIfMatrixIsRectangular(numberMatrix);

        assertFalse(result);
    }

    @Test
    void Exercise14_FirstLineIsEmpty() {
        int[][] numberMatrix = {{}, {9, 8, 5}, {7, 2, 6}};

        Boolean result = Exercise14.verifyIfMatrixIsRectangular(numberMatrix);

        assertNull(result);
    }

    @Test
    void Exercise14_SecondLineIsEmpty() {
        int[][] numberMatrix = {{9, 8, 5}, {}, {7, 2, 6}};

        Boolean result = Exercise14.verifyIfMatrixIsRectangular(numberMatrix);

        assertNull(result);
    }

    @Test
    void Exercise14_ThirdLineIsEmpty() {
        int[][] numberMatrix = {{9, 8, 5}, {7, 2, 6}, {}};

        Boolean result = Exercise14.verifyIfMatrixIsRectangular(numberMatrix);

        assertNull(result);
    }

//---------Exercise 15 A-------------------------------------------------------------------------------------------------

    @Test
    void Exercise15A_SmallestValueIsZeroOnPositionZeroZero() {
        int[][] numberMatrix = {{0, 3, 4}, {9, 8, 5}};

        Integer expected = 0;
        Integer result = Exercise15.getSmallestValueElement(numberMatrix);

        assertEquals(expected, result);
    }

    @Test
    void Exercise15A_SmallestValueIsFiveOnPositionTwoThree() {
        int[][] numberMatrix = {{7, 8, 6}, {9, 8, 5}, {7, 8, 6}};

        Integer expected = 5;
        Integer result = Exercise15.getSmallestValueElement(numberMatrix);

        assertEquals(expected, result);
    }

    @Test
    void Exercise15A_SmallestValueIsMinusTwoPositionThreeTwo() {
        int[][] numberMatrix = {{7, 8, 6}, {9, 8, 5}, {7, -2, 6}};

        Integer expected = -2;
        Integer result = Exercise15.getSmallestValueElement(numberMatrix);

        assertEquals(expected, result);
    }

    @Test
    void Exercise15A_SmallestValueIsTwoPositionThreeTwoFirstLineEmpty() {
        int[][] numberMatrix = {{}, {9, 8, 5}, {7, 2, 6}};

        Integer result = Exercise15.getSmallestValueElement(numberMatrix);

        assertNull(result);
    }

    @Test
    void Exercise15A_SmallestValueIsTwoPositionThreeTwoSecondLineEmpty() {
        int[][] numberMatrix = {{9, 8, 5}, {}, {7, 2, 6}};

        Integer result = Exercise15.getSmallestValueElement(numberMatrix);

        assertNull(result);
    }

    @Test
    void Exercise15A_SmallestValueIsTwoPositionThreeTwoThirdLineEmpty() {
        int[][] numberMatrix = {{9, 8, 5}, {7, 2, 6}, {}};

        Integer result = Exercise15.getSmallestValueElement(numberMatrix);

        assertNull(result);
    }

//---------Exercise 15 B-------------------------------------------------------------------------------------------------

    @Test
    void Exercise15B_BiggestValueIsNineOnPositionTwoZero() {
        int[][] numberMatrix = {{0, 3, 4}, {9, 8, 5}};

        Integer expected = 9;
        Integer result = Exercise15.getBiggestValueElement(numberMatrix);

        assertEquals(expected, result);
    }

    @Test
    void Exercise15B_BiggestValueIsEightOnSeveralPositions() {
        int[][] numberMatrix = {{7, 8, 6}, {0, 8, 5}, {7, 8, 6}};

        Integer expected = 8;
        Integer result = Exercise15.getBiggestValueElement(numberMatrix);

        assertEquals(expected, result);
    }

    @Test
    void Exercise15B_BiggestValueIsMinusTwoPositionThreeTwo() {
        int[][] numberMatrix = {{-7, -8, -6}, {-9, -8, -5}, {-7, -2, -6}};

        Integer expected = -2;
        Integer result = Exercise15.getBiggestValueElement(numberMatrix);

        assertEquals(expected, result);
    }

    @Test
    void Exercise15B_BiggestValueFirstLineEmpty() {
        int[][] numberMatrix = {{}, {9, 8, 5}, {7, 2, 6}};

        Integer result = Exercise15.getBiggestValueElement(numberMatrix);

        assertNull(result);
    }

    @Test
    void Exercise15B_BiggestValueSecondLineEmpty() {
        int[][] numberMatrix = {{9, 8, 5}, {}, {7, 2, 6}};

        Integer result = Exercise15.getBiggestValueElement(numberMatrix);

        assertNull(result);
    }

    @Test
    void Exercise15B_BiggestValueIsTwoPositionThreeTwoThirdLineEmpty() {
        int[][] numberMatrix = {{9, 8, 5}, {7, 2, 6}, {}};

        Integer result = Exercise15.getBiggestValueElement(numberMatrix);

        assertNull(result);
    }

//---------Exercise 15 C-------------------------------------------------------------------------------------------------


    @Test
    void Exercise15C_MeanIsSixPointOneOneOneMatrixHasThreeLinesAndThreeColumns() {
        int[][] numberMatrix = {{7, 8, 6}, {0, 8, 5}, {7, 8, 6}};

        Double expected = 6.111;
        Double result = Exercise15.getMeanValue(numberMatrix);

        assertEquals(expected, result, 0.001);

    }

    @Test
    void Exercise15C_MeanIsSevenMatrixHasOneLineThreeColumns() {
        int[][] numberMatrix = {{7, 8, 6}};

        Double expected = 7.00;
        Double result = Exercise15.getMeanValue(numberMatrix);

        assertEquals(expected, result, 0.001);
    }

    @Test
    void Exercise15C_MeanIsTwentyOnePointSeventyFiveMatrixHasThreeLinesThreeColumnsOrTwoColumns() {
        int[][] numberMatrix = {{7, 8, 6}, {5, 7, 32}, {44, 65}};

        Double expected = 21.75;
        Double result = Exercise15.getMeanValue(numberMatrix);

        assertEquals(expected, result, 0.001);
    }

    @Test
    void Exercise15C_MeanIsTwentyOnePointSeventyFiveMatrixHasThreeLinesFourColumnsTwoColumnsThreeColumns() {
        int[][] numberMatrix = {{7, 8, 6, 5}, {5, 7}, {44, 65, 34}};

        Double expected = 20.111;
        Double result = Exercise15.getMeanValue(numberMatrix);

        assertEquals(expected, result, 0.001);
    }

    @Test
    void Exercise15C_SecondLineIsEmpty() {
        int[][] numberMatrix = {{7, 8, 6, 5}, {}, {44, 65, 34}};

        Double result = Exercise15.getMeanValue(numberMatrix);

        assertNull(result);
    }

    @Test
    void Exercise15C_MeanIsOnePointTwoTwoTwoMatrixHasThreeLinesFourColumnsTwoColumnsThreeColumns() {
        int[][] numberMatrix = {{-7, 8, -6, 5}, {5, -7}, {44, -65, 34}};

        Double expected = 1.222;
        Double result = Exercise15.getMeanValue(numberMatrix);

        assertEquals(expected, result, 0.001);
    }

//---------Exercise 15 D-------------------------------------------------------------------------------------------------

    @Test
    void Exercise15D_ProductIsZeroMatrixHasThreeLinesAndThreeColumns() {
        int[][] numberMatrix = {{7, 8, 6}, {0, 8, 5}, {7, 8, 6}};

        Integer expected = 0;
        Integer result = Exercise15.getElementsProduct(numberMatrix);

        assertEquals(expected, result);

    }

    @Test
    void Exercise15D_ProductIsThreeHundredThirtySixMatrixHasOneLineThreeColumns() {
        int[][] numberMatrix = {{7, 8, 6}};

        Integer expected = 336;
        Integer result = Exercise15.getElementsProduct(numberMatrix);

        assertEquals(expected, result);
    }

    @Test
    void Exercise15D_ProductIsOneZeroSevenSixTwoSevenFiveTwoZeroZeroMatrixHasThreeLinesThreeColumnsOrTwoColumns() {
        int[][] numberMatrix = {{7, 8, 6}, {5, 7, 32}, {44, 65}};

        Integer expected = 1076275200;
        Integer result = Exercise15.getElementsProduct(numberMatrix);

        assertEquals(expected, result);
    }

    @Test
    void Exercise15D_SecondLineIsEmpty() {
        int[][] numberMatrix = {{7, 8, 6, 5}, {}, {44, 65, 34}};

        Integer result = Exercise15.getElementsProduct(numberMatrix);

        assertNull(result);
    }

    @Test
    void Exercise15D_ProductIsMinusFiveEightEightZeroZeroMatrixHasTwoLinesFourColumnsTwoColumns() {
        int[][] numberMatrix = {{-7, 8, -6, 5}, {5, -7}};

        Integer expected = -58800;
        Integer result = Exercise15.getElementsProduct(numberMatrix);

        assertEquals(expected, result);
    }

//---------Exercise 15 E-------------------------------------------------------------------------------------------------


    @Test
    void Exercise15E_UniqueElementsThreeRepeated() {

        int[][] numberMatrix = {{5, 3, 8, 9}, {3, 1, 5}, {6, 7, 4, 3, 9}};

        int[] expected = {1, 3, 4, 5, 6, 7, 8, 9};
        int[] result = Exercise15.getNonRepeatingElements(numberMatrix);

        assertArrayEquals(expected, result);
    }

    @Test
    void Exercise15E_UniqueElementsSixRepeated() {

        int[][] numberMatrix = {{5, 0, 0, 9}, {3, 0, 5}, {6, 0, 4, 0, 0}};

        int[] expected = {0, 3, 4, 5, 6, 9};
        int[] result = Exercise15.getNonRepeatingElements(numberMatrix);

        assertArrayEquals(expected, result);
    }

    @Test
    void Exercise15E_UniqueElementsOneLineEmpty() {

        int[][] numberMatrix = {{}, {3, 0, 5}, {6, 0, 4, 0, 0}};

        int[] expected = {0, 3, 4, 5, 6};
        int[] result = Exercise15.getNonRepeatingElements(numberMatrix);

        assertArrayEquals(expected, result);
    }

    @Test
    void Exercise15E_UniqueElementsDoubleOrTripleDigitElements() {

        int[][] numberMatrix = {{25, 40, 40, 39}, {23, 450, 65}, {655, 40, 884, 7650, 60}};

        int[] expected = {23, 25, 39, 40, 60, 65, 450, 655, 884, 7650,};
        int[] result = Exercise15.getNonRepeatingElements(numberMatrix);

        assertArrayEquals(expected, result);
    }

    @Test
    void Exercise15E_UniqueElementsNegativeElements() {

        int[][] numberMatrix = {{-25, 40, -40, 39}, {23, 450, -65}, {655, -40, 884, 7650, -60}};

        int[] expected = {-65, -60, -40, -25, 23, 39, 40, 450, 655, 884, 7650};
        int[] result = Exercise15.getNonRepeatingElements(numberMatrix);

        assertArrayEquals(expected, result);
    }

    @Test
    void Exercise15E_AllElementsAreUnique() {

        int[][] numberMatrix = {{10, 11, 12, 13}, {14, 15, 16}, {17, 18, 19, 20, 21}};

        int[] expected = {10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21};
        int[] result = Exercise15.getNonRepeatingElements(numberMatrix);

        assertArrayEquals(expected, result);
    }

    @Test
    void Exercise15E_MatrixIsNull() {

        int[][] numberMatrix = null;

        int[] result = Exercise15.getNonRepeatingElements(numberMatrix);

        assertNull(result);
    }

//---------Exercise 15 F-------------------------------------------------------------------------------------------------

    @Test
    void Exercise15F_AllElementsArePrime() {

        int[][] numberMatrix = {{2, 7, 11,}, {11, 13, 17, 19}, {23, 29, 31}};

        int[] expected = {2, 7, 11, 11, 13, 17, 19, 23, 29, 31};
        int[] result = Exercise15.getPrimeElements(numberMatrix);

        assertArrayEquals(expected, result);
    }

    @Test
    void Exercise15F_ZeroPrimeElements() {

        int[][] numberMatrix = {{102, 33, 10,}, {15, 20, 4, 9}};

        int[] expected = {};
        int[] result = Exercise15.getPrimeElements(numberMatrix);

        assertArrayEquals(expected, result);
    }

    @Test
    void Exercise15F_TwoPrimeElements() {

        int[][] numberMatrix = {{102, 11, 2,}, {15, 20, 4, 9}};

        int[] expected = {11, 2};
        int[] result = Exercise15.getPrimeElements(numberMatrix);

        assertArrayEquals(expected, result);
    }

    @Test
    void Exercise15F_FivePrimeElements() {

        int[][] numberMatrix = {{102, 20, 8}, {181, 15, 20, 4, 97, 9}, {3, 19, 131}};

        int[] expected = {181, 97, 3, 19, 131};
        int[] result = Exercise15.getPrimeElements(numberMatrix);

        assertArrayEquals(expected, result);
    }

    @Test
    void Exercise15F_TwoPrimeElementsOneEmptyLine() {

        int[][] numberMatrix = {{}, {15, 20, 4, 9}, {3, 19, 60}};

        int[] expected = {3, 19};
        int[] result = Exercise15.getPrimeElements(numberMatrix);

        assertArrayEquals(expected, result);
    }

    @Test
    void Exercise15F_MatrixIsNull() {

        int[][] numberMatrix = null;

        int[] result = Exercise15.getPrimeElements(numberMatrix);

        assertArrayEquals(null, result);
    }

    @Test
    void Exercise15F_NegativeElements() {

        int[][] numberMatrix = {{-102, -20, 8, -181}, {15, 20, 4, -97, 9}, {-3, -19, -131}};

        int[] expected = {};
        int[] result = Exercise15.getPrimeElements(numberMatrix);

        assertArrayEquals(expected, result);
    }

//---------Exercise 15 G-------------------------------------------------------------------------------------------------

    @Test
    void Exercise15G_DiagonalFromSquareMatrix() {

        int[][] numberMatrix = {{2, 7, 11}, {13, 17, 19}, {23, 29, 31}};

        int[] expected = {2, 17, 31};
        int[] result = Exercise15.getMatrixMainDiagonal(numberMatrix);

        assertArrayEquals(expected, result);
    }

    @Test
    void Exercise15G_DiagonalFromRectangularMatrixWithMoreColumnsThanRows() {

        int[][] numberMatrix = {{3, 7, 11, 879, 5432}, {13, 21, 19, 75, 345}, {23, 29, 41, 342, 654}};

        int[] expected = {3, 21, 41};
        int[] result = Exercise15.getMatrixMainDiagonal(numberMatrix);

        assertArrayEquals(expected, result);
    }

    @Test
    void Exercise15G_DiagonalFromRectangularMatrixWithMoreRowsThanColumns() {

        int[][] numberMatrix = {{3, 7, 11, 879}, {13, 21, 19, 75}, {23, 29, 41, 342}, {23, 46, 76, 88}, {66, 7, 53, 634}};

        int[] expected = {3, 21, 41, 88};
        int[] result = Exercise15.getMatrixMainDiagonal(numberMatrix);

        assertArrayEquals(expected, result);
    }

    @Test
    void Exercise15G_DiagonalFromIrregularMatrix() {

        int[][] numberMatrix = {{3, 7, 11, 879}, {13, 21, 19}, {23, 29}, {23, 46, 76, 88}};

        int[] result = Exercise15.getMatrixMainDiagonal(numberMatrix);

        assertArrayEquals(null, result);
    }

    @Test
    void Exercise15G_DiagonalFromSquareMatrixNegativeNumbers() {

        int[][] numberMatrix = {{-3, -7, -11, -879}, {-13, -21, -19, -55}, {23, -29, 55, -32}, {23, 46, 76, 88}};

        int[] expected = {-3, -21, 55, 88};
        int[] result = Exercise15.getMatrixMainDiagonal(numberMatrix);

        assertArrayEquals(expected, result);
    }

    @Test
    void Exercise15G_DiagonalFromSquareMatrixEmptyLine() {

        int[][] numberMatrix = {{-3, -7, -11, -879}, {}, {23, -29, 55, -32}, {23, 46, 76, 88}};

        int[] result = Exercise15.getMatrixMainDiagonal(numberMatrix);

        assertArrayEquals(null, result);
    }

//---------Exercise 15 H-------------------------------------------------------------------------------------------------


    @Test
    void Exercise15H_DiagonalFromSquareMatrix() {

        int[][] numberMatrix = {{2, 7, 11}, {13, 17, 19}, {23, 29, 31}};

        int[] expected = {11, 17, 23};
        int[] result = Exercise15.getMatrixSecondaryDiagonal(numberMatrix);

        assertArrayEquals(expected, result);
    }

    @Test
    void Exercise15H_DiagonalFromRectangularMatrixWithMoreColumnsThanRows() {

        int[][] numberMatrix = {{3, 7, 11, 879, 5432}, {13, 21, 19, 75, 345}, {23, 29, 41, 342, 654}};

        int[] expected = {5432, 75, 41};
        int[] result = Exercise15.getMatrixSecondaryDiagonal(numberMatrix);

        assertArrayEquals(expected, result);
    }

    @Test
    void Exercise15H_DiagonalFromRectangularMatrixWithMoreRowsThanColumns() {

        int[][] numberMatrix = {{3, 7, 11, 879}, {13, 21, 19, 75}, {23, 29, 41, 342}, {23, 46, 76, 88}, {66, 7, 53, 634}};

        int[] expected = {879, 19, 29, 23};
        int[] result = Exercise15.getMatrixSecondaryDiagonal(numberMatrix);

        assertArrayEquals(expected, result);
    }

    @Test
    void Exercise15H_DiagonalFromIrregularMatrix() {

        int[][] numberMatrix = {{3, 7, 11, 879}, {13, 21, 19}, {23, 29}, {23, 46, 76, 88}};

        int[] result = Exercise15.getMatrixSecondaryDiagonal(numberMatrix);

        assertArrayEquals(null, result);
    }

    @Test
    void Exercise15H_DiagonalFromSquareMatrixNegativeNumbers() {

        int[][] numberMatrix = {{-3, -7, -11, -879}, {-13, -21, -19, -55}, {23, -29, 55, -32}, {23, 46, 76, 88}};

        int[] expected = {-879, -19, -29, 23};
        int[] result = Exercise15.getMatrixSecondaryDiagonal(numberMatrix);

        assertArrayEquals(expected, result);
    }

    @Test
    void Exercise15H_DiagonalFromSquareMatrixEmptyLine() {

        int[][] numberMatrix = {{-3, -7, -11, -879}, {}, {23, -29, 55, -32}, {23, 46, 76, 88}};

        int[] result = Exercise15.getMatrixSecondaryDiagonal(numberMatrix);

        assertArrayEquals(null, result);
    }

//---------Exercise 15 I-------------------------------------------------------------------------------------------------


    @Test
    void Exercise15I_NotIdentityMatrixButMainDiagonalValuesAreAllOne() {

        int[][] numberMatrix = {{1, 7, 11}, {13, 1, 19}, {23, 29, 1}};

        Boolean result = Exercise15.verifyIfIdentityMatrix(numberMatrix);

        assertFalse(result);
    }

    @Test
    void Exercise15I_IdentityMatrixSuccess() {

        int[][] numberMatrix = {{1, 0, 0, 0}, {0, 1, 0, 0}, {0, 0, 1, 0}, {0, 0, 0, 1}};

        Boolean result = Exercise15.verifyIfIdentityMatrix(numberMatrix);

        assertTrue(result);
    }

    @Test
    void Exercise15I_MatrixIsntSquare() {

        int[][] numberMatrix = {{1, 0, 0, 0}, {0, 1, 0, 0}, {0, 0, 1, 0}};

        Boolean result = Exercise15.verifyIfIdentityMatrix(numberMatrix);

        assertEquals(null, result);
    }

    @Test
    void Exercise15I_MatrixIsntSquare2() {

        int[][] numberMatrix = {{1, 0, 0}, {0, 1, 0}, {0, 0, 1}, {0, 0, 0}};

        Boolean result = Exercise15.verifyIfIdentityMatrix(numberMatrix);

        assertEquals(null, result);
    }

    @Test
    void Exercise15I_NegativeNumbers() {

        int[][] numberMatrix = {{1, -1, -1}, {-2, 1, -2}, {-2, -2, 1}};

        Boolean result = Exercise15.verifyIfIdentityMatrix(numberMatrix);

        assertFalse(result);
    }

    @Test
    void Exercise15I_MatrixNumbersWithMultipleDigits() {

        int[][] numberMatrix = {{11, 0, 0}, {0, 111, 0}, {0, 0, 1111}};

        Boolean result = Exercise15.verifyIfIdentityMatrix(numberMatrix);

        assertFalse(result);
    }

    @Test
    void Exercise15I_IdentityMatrixFiveByFive() {

        int[][] numberMatrix = {{1, 0, 0, 0, 0}, {0, 1, 0, 0, 0}, {0, 0, 1, 0, 0}, {0, 0, 0, 1, 0}, {0, 0, 0, 0, 1}};

        Boolean result = Exercise15.verifyIfIdentityMatrix(numberMatrix);

        assertTrue(result);
    }


//---------Exercise 15 J-------------------------------------------------------------------------------------------------


    @Test
    void Exercise15J_DeterminantIsZeroResultIsNull1() {

        int[][] numberMatrix = {{1, 2, 3, 4}, {5, 6, 7, 8}, {9, 10, 11, 12}, {13, 14, 15, 16}};

        double[][] result = Exercise15.getInverseMatrix(numberMatrix);

        assertArrayEquals(null, result);
    }

    @Test
    void Exercise15J_DeterminantIsZeroResultIsNull2() {

        int[][] numberMatrix = {{1, 2, 3}, {5, 6, 7}, {9, 10, 11}};

        double[][] result = Exercise15.getInverseMatrix(numberMatrix);

        assertArrayEquals(null, result);
    }

    @Test
    void Exercise15J_MatrixIsntSquare() {

        int[][] numberMatrix = {{1, 2, 3}, {5, 6, 7}};

        double[][] result = Exercise15.getInverseMatrix(numberMatrix);

        assertArrayEquals(null, result);
    }

    @Test
    void Exercise15J_MatrixThreeByThreeSuccess1() {

        int[][] numberMatrix = {{3, 0, 2}, {9, 1, 7}, {1, 0, 1}};

        double[][] expected = {{1.0, 0.0, -2.0}, {-2.0, 1.0, -3.0}, {-1.0, 0.0, 3.0}};
        double[][] result = Exercise15.getInverseMatrix(numberMatrix);

        assertArrayEquals(expected, result);
    }

    @Test
    void Exercise15J_MatrixThreeByThreeSuccess2() {

        int[][] numberMatrix = {{1, 2, 3}, {0, 1, 4}, {0, 0, 1}};

        double[][] expected = {{1, -2, 5}, {0, 1.0, -4.0}, {0, 0.0, 1.0}};
        double[][] result = Exercise15.getInverseMatrix(numberMatrix);

        assertArrayEquals(expected, result);
    }

    @Test
    void Exercise15J_MatrixFourByFourSuccess() {

        int[][] numberMatrix = {{1, 2, 3, 4}, {0, 5, 6, 0}, {7, 0, 8, 0}, {9, 0, 11, 0}};

        double[][] expected = {{0, 0, 2.2, -1.6}, {0, 0.2, 2.16, -1.68}, {0, 0, -1.8, 1.4}, {0.25, -0.1, -0.28, 0.19}};
        double[][] result = Exercise15.getInverseMatrix(numberMatrix);

        assertArrayEquals(expected, result);
    }


//---------Exercise 15 K-------------------------------------------------------------------------------------------------

    @Test
    void Exercise15K_TransposedMatrixRectangularMoreColumnsThanRows() {

        double[][] numberMatrix = {{1, 7, 11, 12}, {13, 5, 19, 67}, {23, 29, 7, 4}};

        double[][] expected = {{1, 13, 23}, {7, 5, 29}, {11, 19, 7}, {12, 67, 4}};
        double[][] result = Exercise15.getTransposedMatrix(numberMatrix);

        assertArrayEquals(expected, result);
    }

    @Test
    void Exercise15K_TransposedMatrixRectangularMoreRowsThanColumns() {

        double[][] numberMatrix = {{1, 13, 23}, {7, 5, 29}, {11, 19, 7}, {12, 67, 4}};

        double[][] expected = {{1, 7, 11, 12}, {13, 5, 19, 67}, {23, 29, 7, 4}};
        double[][] result = Exercise15.getTransposedMatrix(numberMatrix);

        assertArrayEquals(expected, result);
    }

    @Test
    void Exercise15K_TransposedMatrixSquare() {

        double[][] numberMatrix = {{1, 13, 23}, {7, 5, 29}, {11, 19, 7}};

        double[][] expected = {{1, 7, 11}, {13, 5, 19}, {23, 29, 7}};
        double[][] result = Exercise15.getTransposedMatrix(numberMatrix);

        assertArrayEquals(expected, result);
    }

    @Test
    void Exercise15K_IrregularMatrix() {

        double[][] numberMatrix = {{}, {7, 5, 29}, {11, 19, 7}};

        double[][] result = Exercise15.getTransposedMatrix(numberMatrix);

        assertArrayEquals(null, result);
    }

    @Test
    void Exercise15K_TransposedMatrixNegativeNumbers() {

        double[][] numberMatrix = {{-1, -13, -23}, {-7, -5, -29}, {-11, -19, -7}};

        double[][] expected = {{-1, -7, -11}, {-13, -5, -19}, {-23, -29, -7}};
        double[][] result = Exercise15.getTransposedMatrix(numberMatrix);

        assertArrayEquals(expected, result);
    }

//---------Exercise 16-------------------------------------------------------------------------------------------------

    @Test
    void GetNMinusOneMatrix1() {

        int[][] numberMatrix = {{1, 13, 23}, {7, 5, 29}, {11, 19, 7}};
        int horizontalPosition = 0;
        int verticalPosition = 0;
        int[][] expected = {{5, 29}, {19, 7}};
        int[][] result = MethodsLibrary.getNMinusOneMatrix(numberMatrix, horizontalPosition, verticalPosition);
        assertArrayEquals(expected, result);
    }

    @Test
    void GetNMinusOneMatrix2() {

        int[][] numberMatrix = {{1, 13, 23, 56}, {7, 5, 29, 76}, {11, 19, 7, 45}, {56, 34, 87, 12}};
        int horizontalPosition = 1;
        int verticalPosition = 1;
        int[][] expected = {{1, 23, 56}, {11, 7, 45}, {56, 87, 12}};
        int[][] result = MethodsLibrary.getNMinusOneMatrix(numberMatrix, horizontalPosition, verticalPosition);
        assertArrayEquals(expected, result);
    }

    @Test
    void GetNMinusOneMatrix3() {

        int[][] numberMatrix = {{1, 13, 23, 56}, {7, 5, 29, 76}, {11, 19, 7, 45}, {56, 34, 87, 12}};
        int horizontalPosition = 2;
        int verticalPosition = 3;
        int[][] expected = {{1, 13, 56}, {7, 5, 76}, {11, 19, 45}};
        int[][] result = MethodsLibrary.getNMinusOneMatrix(numberMatrix, horizontalPosition, verticalPosition);
        assertArrayEquals(expected, result);
    }

    @Test
    void GetNMinusOneMatrix4() {

        int[][] numberMatrix = {{1, 13, 23, 56, 980}, {7, 5, 29, 76, 234}, {11, 19, 7, 45, 56}, {56, 34, 87, 12, 67}, {534, 75, 234, 241, 1234}};
        int horizontalPosition = 4;
        int verticalPosition = 2;
        int[][] expected = {{1, 13, 23, 56}, {7, 5, 29, 76}, {56, 34, 87, 12}, {534, 75, 234, 241}};
        int[][] result = MethodsLibrary.getNMinusOneMatrix(numberMatrix, horizontalPosition, verticalPosition);
        assertArrayEquals(expected, result);
    }

    @Test
    void Exercise16_MatrixThreePerThree() {

        int[][] numberMatrix = {{1, 13, 23}, {7, 5, 29}, {11, 19, 7}};

        Double expected = 4788.00;
        Double result = Exercise16.getMatrixDeterminant(numberMatrix);

        assertEquals(expected, result);
    }

    @Test
    void Exercise16_MatrixFourPerFour() {

        int[][] numberMatrix = {{34, 54, 34, 51}, {345, 345, 13, 46}, {12, 67, 567, 87}, {67, 56, 3, 1}};

        Double expected = -63353183.00;
        Double result = Exercise16.getMatrixDeterminant(numberMatrix);

        assertEquals(expected, result, 0.1);
    }

    @Test
    void Exercise16_MatrixFivePerFive() {

        int[][] numberMatrix = {{1, 2, 3, 4, 5}, {6, 7, 8, 9, 10}, {11, 12, 13, 14, 15}, {16, 17, 18, 19, 20}, {21, 22, 23, 24, 25}};

        Double expected = 0.00;
        Double result = Exercise16.getMatrixDeterminant(numberMatrix);

        assertEquals(expected, result, 0.1);
    }

    @Test
    void Exercise16_MatrixTwoPerTwo() {

        int[][] numberMatrix = {{4, 5}, {7, 8}};

        Double expected = -3.00;
        Double result = Exercise16.getMatrixDeterminant(numberMatrix);

        assertEquals(expected, result, 0.01);
    }

    @Test
    void Exercise16_MatrixTwoPerTwoNegativeNumbers() {

        int[][] numberMatrix = {{-4, -5}, {7, 8}};

        Double expected = 3.00;
        Double result = Exercise16.getMatrixDeterminant(numberMatrix);

        assertEquals(expected, result, 0.01);
    }

    @Test
    void Exercise16_MatrixThreePerThreeDeterminantIsZero() {

        int[][] numberMatrix = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};

        Double expected = 0.00;
        Double result = Exercise16.getMatrixDeterminant(numberMatrix);

        assertEquals(expected, result, 0.01);
    }

    @Test
    void Exercise16_MatrixOneElement() {

        int[][] numberMatrix = {{1}};

        Double expected = 1.00;
        Double result = Exercise16.getMatrixDeterminant(numberMatrix);

        assertEquals(expected, result, 0.01);
    }

    @Test
    void Exercise16_MatrixNull() {

        int[][] numberMatrix = null;

        Double result = Exercise16.getMatrixDeterminant(numberMatrix);

        assertNull(result);
    }

    @Test
    void Exercise16_MatrixIsntSquare() {

        int[][] numberMatrix = {{-4, -5}, {7, 8}, {5, 4}};

        Double result = Exercise16.getMatrixDeterminant(numberMatrix);

        assertNull(result);
    }

//---------Exercise 17A-------------------------------------------------------------------------------------------------


    @Test
    void Exercise17A_SquarerMatrixConstantIsTwo() {
        int[][] numberMatrix = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        int constant = 2;

        int[][] expected = {{2, 4, 6}, {8, 10, 12}, {14, 16, 18}};
        int[][] result = Exercise17.getProductMatrixByConstant(numberMatrix, constant);

        assertArrayEquals(expected, result);
    }

    @Test
    void Exercise17A_RectangularMatrixConstantIsThree() {
        int[][] numberMatrix = {{1, 2, 3}, {4, 5, 6},};
        int constant = 3;

        int[][] expected = {{3, 6, 9}, {12, 15, 18}};
        int[][] result = Exercise17.getProductMatrixByConstant(numberMatrix, constant);

        assertArrayEquals(expected, result);
    }

    @Test
    void Exercise17A_RectangularMatrix2ConstantIsThree() {
        int[][] numberMatrix = {{1, 2}, {4, 5}, {7, 8}};
        int constant = 3;

        int[][] expected = {{3, 6}, {12, 15}, {21, 24}};
        int[][] result = Exercise17.getProductMatrixByConstant(numberMatrix, constant);

        assertArrayEquals(expected, result);
    }

    @Test
    void Exercise17A_IrregularMatrixConstantIsThree() {
        int[][] numberMatrix = {{1, 2, 3}, {4, 5}, {7, 8, 9, 10}};
        int constant = 3;

        int[][] expected = {{3, 6, 9}, {12, 15}, {21, 24, 27, 30}};
        int[][] result = Exercise17.getProductMatrixByConstant(numberMatrix, constant);

        assertArrayEquals(expected, result);
    }

    @Test
    void Exercise17A_IrregularMatrix2ConstantIsTen() {
        int[][] numberMatrix = {{}, {4, 5}, {7, 8, 9, 10}};
        int constant = 10;

        int[][] expected = {{}, {40, 50}, {70, 80, 90, 100}};
        int[][] result = Exercise17.getProductMatrixByConstant(numberMatrix, constant);

        assertArrayEquals(expected, result);
    }

    @Test
    void Exercise17A_SquarerMatrixConstantIsFiveNegativeNumbers() {
        int[][] numberMatrix = {{-3, -7, -5}, {-4, -5, -3}, {-7, -8, -9, -10}};
        int constant = 5;

        int[][] expected = {{-15, -35, -25}, {-20, -25, -15}, {-35, -40, -45, -50}};
        int[][] result = Exercise17.getProductMatrixByConstant(numberMatrix, constant);

        assertArrayEquals(expected, result);
    }

//---------Exercise 17B-------------------------------------------------------------------------------------------------

    @Test
    void Exercise17B_SquareMatrix() {
        int[][] numberMatrix1 = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        int[][] numberMatrix2 = {{9, 8, 7}, {6, 5, 4}, {3, 2, 1}};

        int[][] expected = {{10, 10, 10}, {10, 10, 10}, {10, 10, 10}};
        int[][] result = Exercise17.sumTwoMatrixes(numberMatrix1, numberMatrix2);

        assertArrayEquals(expected, result);
    }

    @Test
    void Exercise17B_RectangularMatrix() {
        int[][] numberMatrix1 = {{1, 2, 3}, {4, 5, 6},};
        int[][] numberMatrix2 = {{5, 6, 7}, {8, 9, 0},};

        int[][] expected = {{6, 8, 10}, {12, 14, 6}};
        int[][] result = Exercise17.sumTwoMatrixes(numberMatrix1, numberMatrix2);

        assertArrayEquals(expected, result);
    }

    @Test
    void Exercise17B_RectangularMatrix2() {
        int[][] numberMatrix1 = {{1, 2}, {4, 5}, {7, 8}};
        int[][] numberMatrix2 = {{5, 4}, {2, 7}, {4, 2}};

        int[][] expected = {{6, 6}, {6, 12}, {11, 10}};
        int[][] result = Exercise17.sumTwoMatrixes(numberMatrix1, numberMatrix2);

        assertArrayEquals(expected, result);
    }

    @Test
    void Exercise17B_IrregularMatrix() {
        int[][] numberMatrix1 = {{1, 2, 3}, {4, 5}, {7, 8, 9, 10}};
        int[][] numberMatrix2 = {{11, 12, 13}, {2, 7}, {14, 15, 16, 17}};

        int[][] expected = {{12, 14, 16}, {6, 12}, {21, 23, 25, 27}};
        int[][] result = Exercise17.sumTwoMatrixes(numberMatrix1, numberMatrix2);

        assertArrayEquals(expected, result);
    }

    @Test
    void Exercise17B_IrregularMatrixEmptyLine() {
        int[][] numberMatrix1 = {{}, {4, 5}, {7, 8, 9, 10}};
        int[][] numberMatrix2 = {{}, {2, 7}, {14, 15, 16, 17}};

        int[][] expected = {{}, {6, 12}, {21, 23, 25, 27}};
        int[][] result = Exercise17.sumTwoMatrixes(numberMatrix1, numberMatrix2);

        assertArrayEquals(expected, result);
    }

    @Test
    void Exercise17B_MatrixDifferentNrColumns() {
        int[][] numberMatrix1 = {{3, 6}, {4, 5}, {7, 8, 9, 10}};
        int[][] numberMatrix2 = {{5, 6, 8}, {2, 7}, {14, 15, 16, 17}};

        int[][] result = Exercise17.sumTwoMatrixes(numberMatrix1, numberMatrix2);

        assertArrayEquals(null, result);
    }

    @Test
    void Exercise17B_MatrixDifferentNrColumns2() {
        int[][] numberMatrix1 = {{3, 6, 23}, {4, 5, 5}, {7, 8,}};
        int[][] numberMatrix2 = {{5, 6, 8}, {2, 7}, {14, 15, 16, 17}};

        int[][] result = Exercise17.sumTwoMatrixes(numberMatrix1, numberMatrix2);

        assertArrayEquals(null, result);
    }

    @Test
    void Exercise17B_MatrixDifferentNrLines() {
        int[][] numberMatrix1 = {{3, 6, 23}, {4, 5, 5}};
        int[][] numberMatrix2 = {{5, 6, 8}, {2, 7, 45}, {14, 15, 16, 17}};

        int[][] result = Exercise17.sumTwoMatrixes(numberMatrix1, numberMatrix2);

        assertArrayEquals(null, result);
    }

    @Test
    void Exercise17B_SquareMatrixNegativeNrs() {
        int[][] numberMatrix1 = {{-3, -7, -5}, {-4, -5, -3}, {-7, -8, -9}};
        int[][] numberMatrix2 = {{5, 6, 8}, {2, 7, 45}, {14, 15, 16}};

        int[][] expected = {{2, -1, 3}, {-2, 2, 42}, {7, 7, 7}};
        int[][] result = Exercise17.sumTwoMatrixes(numberMatrix1, numberMatrix2);

        assertArrayEquals(expected, result);
    }

//---------Exercise 17C-------------------------------------------------------------------------------------------------

    @Test
    void Exercise17C_SquareMatrix() {
        int[][] numberMatrix1 = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        int[][] numberMatrix2 = {{9, 8, 7}, {6, 5, 4}, {3, 2, 1}};

        int[][] expected = {{9, 16, 21}, {24, 25, 24}, {21, 16, 9}};
        int[][] result = Exercise17.multiplyTwoMatrixes(numberMatrix1, numberMatrix2);

        assertArrayEquals(expected, result);
    }

    @Test
    void Exercise17C_RectangularMatrix() {
        int[][] numberMatrix1 = {{1, 2, 3}, {4, 5, 6},};
        int[][] numberMatrix2 = {{5, 6, 7}, {8, 9, 0},};

        int[][] expected = {{5, 12, 21}, {32, 45, 0}};
        int[][] result = Exercise17.multiplyTwoMatrixes(numberMatrix1, numberMatrix2);

        assertArrayEquals(expected, result);
    }

    @Test
    void Exercise17C_RectangularMatrix2() {
        int[][] numberMatrix1 = {{1, 2}, {4, 5}, {7, 8}};
        int[][] numberMatrix2 = {{5, 4}, {2, 7}, {4, 2}};

        int[][] expected = {{5, 8}, {8, 35}, {28, 16}};
        int[][] result = Exercise17.multiplyTwoMatrixes(numberMatrix1, numberMatrix2);

        assertArrayEquals(expected, result);
    }

    @Test
    void Exercise17C_IrregularMatrix() {
        int[][] numberMatrix1 = {{1, 2, 3}, {4, 5}, {7, 8, 9, 10}};
        int[][] numberMatrix2 = {{11, 12, 13}, {2, 7}, {14, 15, 16, 17}};

        int[][] expected = {{11, 24, 39}, {8, 35}, {98, 120, 144, 170}};
        int[][] result = Exercise17.multiplyTwoMatrixes(numberMatrix1, numberMatrix2);

        assertArrayEquals(expected, result);
    }

    @Test
    void Exercise17C_IrregularMatrixEmptyLine() {
        int[][] numberMatrix1 = {{}, {4, 5}, {7, 8, 9, 10}};
        int[][] numberMatrix2 = {{}, {2, 7}, {14, 15, 16, 17}};

        int[][] expected = {{}, {8, 35}, {98, 120, 144, 170}};
        int[][] result = Exercise17.multiplyTwoMatrixes(numberMatrix1, numberMatrix2);

        assertArrayEquals(expected, result);
    }

    @Test
    void Exercise17C_MatrixDifferentNrColumns() {
        int[][] numberMatrix1 = {{3, 6}, {4, 5}, {7, 8, 9, 10}};
        int[][] numberMatrix2 = {{5, 6, 8}, {2, 7}, {14, 15, 16, 17}};

        int[][] result = Exercise17.multiplyTwoMatrixes(numberMatrix1, numberMatrix2);

        assertArrayEquals(null, result);
    }

    @Test
    void Exercise17C_MatrixDifferentNrColumns2() {
        int[][] numberMatrix1 = {{3, 6, 23}, {4, 5, 5}, {7, 8,}};
        int[][] numberMatrix2 = {{5, 6, 8}, {2, 7}, {14, 15, 16, 17}};

        int[][] result = Exercise17.multiplyTwoMatrixes(numberMatrix1, numberMatrix2);

        assertArrayEquals(null, result);
    }

    @Test
    void Exercise17C_MatrixDifferentNrLines() {
        int[][] numberMatrix1 = {{3, 6, 23}, {4, 5, 5}};
        int[][] numberMatrix2 = {{5, 6, 8}, {2, 7, 45}, {14, 15, 16, 17}};

        int[][] result = Exercise17.multiplyTwoMatrixes(numberMatrix1, numberMatrix2);

        assertArrayEquals(null, result);
    }

    @Test
    void Exercise17C_SquareMatrixNegativeNrs() {
        int[][] numberMatrix1 = {{-3, -7, -5}, {-4, -5, -3}, {-7, -8, -9}};
        int[][] numberMatrix2 = {{5, 6, 8}, {2, 7, 45}, {14, 15, 16}};

        int[][] expected = {{-15, -42, -40}, {-8, -35, -135}, {-98, -120, -144}};
        int[][] result = Exercise17.multiplyTwoMatrixes(numberMatrix1, numberMatrix2);

        assertArrayEquals(expected, result);
    }

//---------Exercise 18-------------------------------------------------------------------------------------------------


}



























