package com.company;

import com.company.bloco1.Bloco1_V2;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class Bloco1_V2_Testes {
    
    @Test
    void Exercicio1_teste1() {

        int numRaparigas = 17;
        int numRapazes = 14;
        double expected = 54.83870968;
        double result = Bloco1_V2.obterPercentagemRaparigas(numRaparigas, numRapazes);
        assertEquals(expected, result, 0.0001);
    }

    @Test
    void Exercicio1_teste2() {

        int numRaparigas = 17;
        int numRapazes = 14;
        double expected = 45.1612903;
        double result = Bloco1_V2.obterPercentagemRapazes(numRapazes, numRaparigas);
        assertEquals(expected, result, 0.0001);
    }

    @Test
    void Exercicio1_teste3() {

        int numRaparigas= 976;
        int numRapazes= 237;
        double expected = 100;
        double result = Bloco1_V2.obterPercentagemRapazes(numRapazes, numRaparigas) + Bloco1_V2.obterPercentagemRaparigas(numRaparigas, numRapazes);
        assertEquals(expected, result, 0.001);
    }

    @Test
    void Exercicio2_teste() {

        //arrange
        int numTulipas = 3;
        int numRosas = 4;
        double precoTulipa = 1.5;
        double precoRosa = 2.3;
        double expected = 13.7;

        //act
        double result = Bloco1_V2.obterPrecoTotal(numTulipas, numRosas, precoTulipa, precoRosa);

        //assert
        assertEquals(expected, result, 0.00001);
    }

    @Test
    void Exercicio3_teste() {

        //arrange
        double raioBase = 3;
        double alturaCilindro = 5;
        double expected = 141371.6694115407;

        //act
        double result = Bloco1_V2.obterVolumecilindro(raioBase, alturaCilindro);

        //assert
        assertEquals(expected, result);

    }

    @Test
    void Exercicio4_teste1() {

        //arrange
        double instanteRelampago = 50;
        double instanteTrovao = 40;
        double expected = 3400;

        //act
        double result = Bloco1_V2.obterDistanciaTrovoada(instanteRelampago, instanteTrovao);

        //assert
        assertEquals(expected, result);
    }

    @Test
    void Exercicio4_teste2() {

        //arrange
        double instanteRelampago = 45;
        double instanteTrovao = 30.3;
        double expected = 5000;

        //act
        double result = Bloco1_V2.obterDistanciaTrovoada(instanteRelampago, instanteTrovao);

        //assert
        assertEquals(expected, result, 2);
    }

    @Test
    void Exercicio5_teste() {

        double tempo = 2;
        double expected= 19.6;

        double result = Bloco1_V2.obterAlturaPredio(tempo);

        assertEquals(expected, result, 0.00001);
    }

    @Test
    void Exercicio6_teste() {

        double sombraEdificio = 40;
        double sombraPessoa = 4;
        double alturaPessoa = 2;
        double expected = 20;

        double result = Bloco1_V2.obterAlturaEdificio(sombraEdificio, sombraPessoa, alturaPessoa);

        assertEquals(expected, result, 0.00001);
    }

    @Test
    void Exercicio7_teste() {

        double expected = 11.32556779;

        double result = Bloco1_V2.obterDistanciaZe();

        assertEquals(expected, result, 0.0001);
    }

    @Test
    void Exercicio8_teste() {

        double expected = 52.91502622;

        double result = Bloco1_V2.obterLadoC();

        assertEquals(expected, result, 0.00001);
    }

    @Test
    void Exercicio9_teste() {

        double ladoA = 56.765;
        double ladoB = 34.9768;

        double expected = 183.4836;

        double result = Bloco1_V2.obterPerimetro(ladoA, ladoB);

        assertEquals(expected, result);
    }

    @Test
    void Exercicio10_teste() {

        double cateto1 = 579.87;
        double cateto2 = 24.76576;

        double expected = 580.3986214;

        double result = Bloco1_V2.obterHipotenusa(cateto1, cateto2);

        assertEquals(expected, result, 0.00001);
    }

    @Test
    void Exercicio11_teste() {

        double x = 5436.765;

        double expected = 29542104.37;

        double result = Bloco1_V2.obterValorExpressao(x);
    }

    @Test
    void Exercicio12_teste() {

        double celsius = 32;

        double expected = 89.6;

        double result = Bloco1_V2.obterTemperaturaFahrenheit(celsius);

        assertEquals(expected, result, 0.00001);
    }

    @Test
    void Exercicio13_teste() {

        int H = 13;
        int M = 27;

        int expected = 807;

        int result = Bloco1_V2.obterHorasMinutos(H, M);

        assertEquals(expected, result);
    }
}
