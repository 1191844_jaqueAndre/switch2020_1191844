package com.company;

import com.company.OOPExercicios.Student;
import com.company.OOPExercicios.StudentList;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class OOP_Tests {

    @Test
    public void sortStudentsByNumberAscWithSeveralElementsIncorrectlyOrdered() {
        //Arrange
        Student student1 = new Student(1200145, "Sampaio");
        Student student2 = new Student(1200054, "Moreira");
        Student student3 = new Student(1200086, "Silva");

        Student[] students = {student1, student2, student3};    //Unordered
        Student[] expected = {student2, student3, student1};    //Ordered

        StudentList studentList = new StudentList(students);

        //Act
        studentList.sortByNumberAsc();

        //Assert
        assertArrayEquals(expected, studentList.toArray());                  //Checks that students is sorted
    }

    @Test
    public void sortStudentsByGradeDescWithSeveralElementsIncorrectlyOrdered() {
        //Arrange
        Student student1 = new Student(1200145, "Sampaio");
        student1.setGrade(11);
        Student student2 = new Student(1200054, "Moreira");
        student2.setGrade(12);
        Student student3 = new Student(1200086, "Silva");
        student3.setGrade(20);

        Student[] students = {student1, student2, student3};    //Unordered
        Student[] expected = {student3, student2, student1};    //Ordered

        StudentList studentList = new StudentList(students);

        //Act
        studentList.sortByGradeDesc();

        //Assert
        assertArrayEquals(expected, studentList.toArray());                  //Checks that students is sorted
    }

    @Test
    public void createValidStudent() {
        Student student = new Student(1190001, "Paulo");

        assertNotNull(student);
    }

    @Test
    void createStudentNegativeNumberWithSixDigits() {
        assertThrows(IllegalArgumentException.class, () -> new Student(-190001, "Paulo Maio"));
    }

    @Test
    void createStudentNegativeNumberWithSevenDigits() {
        assertThrows(IllegalArgumentException.class, () -> new Student(-1190001, "Paulo Maio"));
    }

    @Test
    void createStudentLongerNumberDigits() {
        assertThrows(IllegalArgumentException.class, () -> new Student(-11900013, "Paulo Maio"));
    }

    @Test
    void createStudentNameIsEmpty() {
        assertThrows(IllegalArgumentException.class, () -> new Student(1190001, ""));
    }

    @Test
    void createStudentNameIsFullOfSpaces() {
        assertThrows(IllegalArgumentException.class, () -> new Student(1190001, "      "));
    }

    @Test
    void createStudentShortNameLength() {
        assertThrows(IllegalArgumentException.class, () -> new Student(1190001, "Rui"));
    }

    @Test
    void createStudentValidNumberButInvalidName() {
        assertThrows(IllegalArgumentException.class, () -> new Student(1980398, "Bia"));
    }

    @Test
    void createStudentsList() {
        StudentList stList = new StudentList(); //Empty list
        Student[] result = stList.toArray();

        assertEquals(0, result.length); //checks array
    }

    @Test
    void createStudentListWithSomeElements() {
        Student student1 = new Student(1200145, "Sampaio");
        Student student2 = new Student(1200054, "Moreira");
        Student student3 = new Student(1200086, "Silva");
        Student[] students = {student1, student2, student3};
        Student[] expected = {student1, student2, student3};    //A copy of students

        StudentList stList = new StudentList(students);
        students[2] = student1;
        Student[] result = stList.toArray();

        assertArrayEquals(expected, result);
        assertNotSame(students, result);
    }

    @Test
    void equalsTo() {
        Student student1 = new Student(1980398, "Beatriz");
        Student student2 = new Student(1980398, "Beatriz Costa");

        boolean result = student1.equals(student2);

        assertTrue(result);
    }

    @Test
    void equalsTrueToItself() {
        Student student1 = new Student(1980398, "Beatriz");
        assertTrue(student1.equals(student1));
    }

    @Test
    void equalsFalseDueToNull() {
        Student student1 = new Student(1980398, "Beatriz");
        boolean result = student1.equals(null);
        assertFalse(result);
    }

    @Test
    void equalsFalseDueToDifferentType() {
        Student student1 = new Student(1980398, "Beatriz");
        boolean result = student1.equals(new String("1980398"));
        assertFalse(result);
    }

    @Test
    void equalsFalseDueToDifferentNumbers() {
        Student student1 = new Student(1980398, "Beatriz");
        Student student2 = new Student(1980399, "Beatriz");

        boolean result = student1.equals(student2);
        assertFalse(result);
    }

    @Test
    void addWhenNotEmptyButDifferent() {
        Student student1 = new Student(1200054, "Moreira");
        Student student2 = new Student(1200145, "Sampaio");
        StudentList stList = new StudentList();
        stList.add(student1);

        boolean result = stList.add(student2);
        Student[] content = stList.toArray();

        assertTrue(result);
        assertEquals(2, content.length);
    }

    @Test
    void addingSameStudentTwice() {
        Student student1 = new Student(1200054, "Moreira");
        StudentList stList = new StudentList();
        stList.add(student1);

        boolean result = stList.add(student1);
        Student[] content = stList.toArray();

        assertFalse(result);
        assertEquals(1, content.length);
    }

    @Test
    void addingStudentWithSameNumber() {
        Student student1 = new Student(1200054, "Moreira");
        Student student2 = new Student(1200054, "Moreira");
        StudentList stList = new StudentList();
        stList.add(student1);

        boolean result = stList.add(student2);
        Student[] content = stList.toArray();

        assertFalse(result);
        assertEquals(1, content.length);
    }

    @Test
    void addingNull() {
        Student student1 = new Student(1200054, "Moreira");
        StudentList stList = new StudentList();
        stList.add(student1);

        boolean result = stList.add(null);
        Student[] content = stList.toArray();

        assertFalse(result);
        assertEquals(1, content.length);
    }

    @Test
    void removingMiddleStudentInSeveral() {
        Student student1 = new Student(1200054, "Moreira");
        Student student2 = new Student(1200154, "Sampaio");
        Student student3 = new Student(1201154, "Costa");
        Student student4 = new Student(1201354, "Lídia");
        Student student5 = new Student(1101354, "Maria");

        Student[] all = {student1, student2, student3, student4, student5};
        Student[] expected = {student1, student2, student3, student5};
        StudentList stList = new StudentList(all);

        boolean result = stList.remove(student4);
        Student[] content = stList.toArray();

        assertTrue(result);
        assertArrayEquals(expected, content);
    }

    @Test
    void removingSameStudentTwice() {
        Student student1 = new Student(1200054, "Moreira");
        StudentList stList = new StudentList();
        stList.add(student1);

        boolean result1 = stList.remove(student1);
        boolean result2 = stList.remove(student1);
        Student[] content = stList.toArray();

        assertTrue(result1);
        assertFalse(result2);
        assertEquals(0, content.length);
    }
}
