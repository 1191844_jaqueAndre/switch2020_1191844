package com.company;

import com.company.bloco3.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class Bloco3_Testes {

    //Exercício 1 ----------------------------------------------------------------------------------------------------

    @Test
    void Exercicio1_NumENegativo() {

        int num = -1;

        int esperado = 1;
        int resultado = Exercicio1.obterValorRes(num);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio1_NumIgualAUm() {

        int num = 1;

        int esperado = 1;
        int resultado = Exercicio1.obterValorRes(num);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio1_NumIgualADois() {

        int num = 2;

        int esperado = 2;
        int resultado = Exercicio1.obterValorRes(num);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio1_NumIgualATres() {

        int num = 3;

        int esperado = 6;
        int resultado = Exercicio1.obterValorRes(num);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio1_NumIgualASeis() {

        int num = 6;

        int esperado = 720;
        int resultado = Exercicio1.obterValorRes(num);

        assertEquals(esperado, resultado);
    }

    //Exercício 3 ----------------------------------------------------------------------------------------------------

    @Test
    void Exercicio3_NrsPositivosNegativosEPositivos1() {

        int[] arrayDeNumeros = {1, 2, 5, 9, 5, 324, -1, 87, 345, 23};

        String esperado = "Para a sequência de números [1, 2, 5, 9, 5, 324] temos a seguinte média dos números ímpares: 5,00, e a seguinte porcentagem de números pares: 33,33%.";
        String resultado = Exercicio3.obterSequenciaNumerosPositivos(arrayDeNumeros);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio3_NrsNegativosEPositivos() {

        int[] arrayDeNumeros = {-1, 2, 5, 9, 5, 324};

        String esperado = "O primeiro número da sequência não é positivo.";
        String resultado = Exercicio3.obterSequenciaNumerosPositivos(arrayDeNumeros);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio3_NrsPositivosNegativosEPositivos2() {

        int[] arrayDeNumeros = {1234567, 2345678, 3456789, 4567890, -9876, -8765, 456321};

        String esperado = "Para a sequência de números [1234567, 2345678, 3456789, 4567890] temos a seguinte média dos números ímpares: 2345678,00, e a seguinte porcentagem de números pares: 50,00%.";
        String resultado = Exercicio3.obterSequenciaNumerosPositivos(arrayDeNumeros);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio3_ZeroNrsPares() {

        int[] arrayDeNumeros = {1, 3, 5, 7, 9};

        String esperado = "Para a sequência de números [1, 3, 5, 7, 9] temos a seguinte média dos números ímpares: 5,00, e a seguinte porcentagem de números pares: 0,00%.";
        String resultado = Exercicio3.obterSequenciaNumerosPositivos(arrayDeNumeros);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio3_ZeroNrsImpares() {

        int[] arrayDeNumeros = {2, 4, 6, 8, 10};

        String esperado = "Para a sequência de números [2, 4, 6, 8, 10] temos a seguinte média dos números ímpares: 0,00, e a seguinte porcentagem de números pares: 100,00%.";
        String resultado = Exercicio3.obterSequenciaNumerosPositivos(arrayDeNumeros);

        assertEquals(esperado, resultado);
    }

    //Exercício 4 a) ----------------------------------------------------------------------------------------------------

    @Test
    void Exercicio4A_TesteComDoisMultiplos() {
        int inicioIntervalo = 5;
        int fimIntervalo = 10;

        String esperado = "No intervalo dado há 2 múltiplo(s) de 3";
        String resultado = Exercicio4.obterNrsMultiplosDeTres(inicioIntervalo, fimIntervalo);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio4A_TesteSemMultiplos() {
        int inicioIntervalo = 7;
        int fimIntervalo = 8;

        String esperado = "Nenhum dos números da sequência é múltiplo de três";
        String resultado = Exercicio4.obterNrsMultiplosDeTres(inicioIntervalo, fimIntervalo);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio4A_TesteComDezMultiplos() {
        int inicioIntervalo = 0;
        int fimIntervalo = 30;

        String esperado = "No intervalo dado há 10 múltiplo(s) de 3";
        String resultado = Exercicio4.obterNrsMultiplosDeTres(inicioIntervalo, fimIntervalo);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio4A_TesteComFimIntervaloMaiorQueOInicio() {
        int inicioIntervalo = 30;
        int fimIntervalo = 0;

        String esperado = "O fim do intervalo tem de ser maior do que o início.";
        String resultado = Exercicio4.obterNrsMultiplosDeTres(inicioIntervalo, fimIntervalo);

        assertEquals(esperado, resultado);
    }
    //Exercício 4 b) ----------------------------------------------------------------------------------------------------

    @Test
    void Exercicio4B_TesteSemMultiplos() {
        int inicioIntervalo = 1;
        int fimIntervalo = 4;
        int nrInteiro = 5;

        String esperado = "Nenhum dos números da sequência é múltiplo de 5";
        String resultado = Exercicio4.obterMultiplosDeUmDadoNrInteiro(inicioIntervalo, fimIntervalo, nrInteiro);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio4B_TesteComTresMultiplos() {
        int inicioIntervalo = 2;
        int fimIntervalo = 31;
        int nrInteiro = 10;

        String esperado = "No intervalo dado há 3 múltiplo(s) de 10";
        String resultado = Exercicio4.obterMultiplosDeUmDadoNrInteiro(inicioIntervalo, fimIntervalo, nrInteiro);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio4B_FimIntervaloMenorQueInicio() {
        int inicioIntervalo = 89;
        int fimIntervalo = 6;
        int nrInteiro = 5;

        String esperado = "O fim do intervalo tem de ser maior do que o início.";
        String resultado = Exercicio4.obterMultiplosDeUmDadoNrInteiro(inicioIntervalo, fimIntervalo, nrInteiro);

        assertEquals(esperado, resultado);
    }

    //Exercício 4 c) ----------------------------------------------------------------------------------------------------

    @Test
    void Exercicio4C_TesteSemMultiplosDeCincoEUmMultiploDeTres() {
        int inicioIntervalo = 7;
        int fimIntervalo = 8;

        String esperado = "Nenhum dos números da sequência é múltiplo de três e cinco";
        String resultado = Exercicio4.obterNrsMultiplosDeTresECinco(inicioIntervalo, fimIntervalo);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio4C_TesteSemMultiplosDeTresEUmMultiploDeCinco() {
        int inicioIntervalo = 4;
        int fimIntervalo = 5;

        String esperado = "Nenhum dos números da sequência é múltiplo de três e cinco";
        String resultado = Exercicio4.obterNrsMultiplosDeTresECinco(inicioIntervalo, fimIntervalo);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio4C_TesteSoComMultiplosDeTresEDeCinco1() {
        int inicioIntervalo = 0;
        int fimIntervalo = 30;

        String esperado = "No intervalo dado há 2 múltiplo(s) de três e cinco";
        String resultado = Exercicio4.obterNrsMultiplosDeTresECinco(inicioIntervalo, fimIntervalo);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio4C_TesteSoComMultiplosDeTresEDeCinco2() {
        int inicioIntervalo = 20;
        int fimIntervalo = 100;

        String esperado = "No intervalo dado há 5 múltiplo(s) de três e cinco";
        String resultado = Exercicio4.obterNrsMultiplosDeTresECinco(inicioIntervalo, fimIntervalo);

        assertEquals(esperado, resultado);
    }

    //Exercício 4 d) ----------------------------------------------------------------------------------------------------

    @Test
    void Exercicio4D_TesteSemMultiplos() {
        int inicioIntervalo = 2;
        int fimIntervalo = 4;
        int nrInteiro1 = 6;
        int nrInteiro2 = 9;

        String esperado = "Nenhum dos números da sequência é múltiplo de 6 e de 9";
        String resultado = Exercicio4.obterMultiplosDeDoisNrsInteiro(inicioIntervalo, fimIntervalo, nrInteiro1, nrInteiro2);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio4D_TesteComQuatroMultiplos() {
        int inicioIntervalo = 0;
        int fimIntervalo = 20;
        int nrInteiro1 = 3;
        int nrInteiro2 = 6;

        String esperado = "No intervalo dado há 4 múltiplo(s) de 3 e de 6";
        String resultado = Exercicio4.obterMultiplosDeDoisNrsInteiro(inicioIntervalo, fimIntervalo, nrInteiro1, nrInteiro2);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio4D_TesteComTresMultiplosEDoisNaoMultiplos() {
        int inicioIntervalo = 10;
        int fimIntervalo = 50;
        int nrInteiro1 = 10;
        int nrInteiro2 = 5;

        String esperado = "No intervalo dado há 4 múltiplo(s) de 10 e de 5";
        String resultado = Exercicio4.obterMultiplosDeDoisNrsInteiro(inicioIntervalo, fimIntervalo, nrInteiro1, nrInteiro2);

        assertEquals(esperado, resultado);
    }

    //Exercício 4 e) ----------------------------------------------------------------------------------------------------

    @Test
    void Exercicio4E_TesteComQuatroMultiplos() {
        int inicioIntervalo = 2;
        int fimIntervalo = 20;
        int nrInteiro1 = 2;
        int nrInteiro2 = 4;

        String esperado = "A soma dos múltiplos de 2 e 4 é 40";
        String resultado = Exercicio4.obterSomaDosMultiplosDeDoisNrsInteiro(inicioIntervalo, fimIntervalo, nrInteiro1, nrInteiro2);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio4E_TesteComDoisMultiplos() {
        int inicioIntervalo = 5;
        int fimIntervalo = 30;
        int nrInteiro1 = 5;
        int nrInteiro2 = 10;

        String esperado = "A soma dos múltiplos de 5 e 10 é 30";
        String resultado = Exercicio4.obterSomaDosMultiplosDeDoisNrsInteiro(inicioIntervalo, fimIntervalo, nrInteiro1, nrInteiro2);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio4E_TesteSemMultiplos() {
        int inicioIntervalo = 5;
        int fimIntervalo = 30;
        int nrInteiro1 = 6;
        int nrInteiro2 = 45;

        String esperado = "Nenhum dos números da sequência é múltiplo de 6 e de 45";
        String resultado = Exercicio4.obterSomaDosMultiplosDeDoisNrsInteiro(inicioIntervalo, fimIntervalo, nrInteiro1, nrInteiro2);

        assertEquals(esperado, resultado);
    }

    //Exercício 5 a) ----------------------------------------------------------------------------------------------------

    @Test
    void Exercicio5A_TesteComTresNrsPares() {
        int inicioIntervalo = 5;
        int fimIntervalo = 12;

        int esperado = 24;
        int resultado = Exercicio5.obterSomaDeNrsPares(inicioIntervalo, fimIntervalo);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio5A_ComInicioMaiorQueFim() {
        int inicioIntervalo = 30;
        int fimIntervalo = 12;

        int esperado = -1;
        int resultado = Exercicio5.obterSomaDeNrsPares(inicioIntervalo, fimIntervalo);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio5A_TesteSemNrsPares() {
        int inicioIntervalo = 11;
        int fimIntervalo = 12;

        int esperado = 0;
        int resultado = Exercicio5.obterSomaDeNrsPares(inicioIntervalo, fimIntervalo);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio5A_TesteComNrsPares() {
        int inicioIntervalo = 51;
        int fimIntervalo = 60;

        int esperado = 220;
        int resultado = Exercicio5.obterSomaDeNrsPares(inicioIntervalo, fimIntervalo);

        assertEquals(esperado, resultado);
    }

    //Exercício 5 b) ----------------------------------------------------------------------------------------------------

    @Test
    void Exercicio5B_TesteComZeroNrsPares() {
        int inicioIntervalo = 51;
        int fimIntervalo = 52;

        int esperado = 0;
        int resultado = Exercicio5.obterQuantidadeDeNrsPares(inicioIntervalo, fimIntervalo);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio5B_TesteComQuinzeNrsPares() {
        int inicioIntervalo = 22;
        int fimIntervalo = 50;

        int esperado = 14;
        int resultado = Exercicio5.obterQuantidadeDeNrsPares(inicioIntervalo, fimIntervalo);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio5B_TesteComNoveNrsPares() {
        int inicioIntervalo = 53;
        int fimIntervalo = 71;

        int esperado = 9;
        int resultado = Exercicio5.obterQuantidadeDeNrsPares(inicioIntervalo, fimIntervalo);

        assertEquals(esperado, resultado);
    }

    //Exercício 5 c) ----------------------------------------------------------------------------------------------------

    @Test
    void Exercicio5C_TesteComquatroNrsImpares() {
        int inicioIntervalo = 5;
        int fimIntervalo = 12;

        int esperado = 32;
        int resultado = Exercicio5.obterSomaDeNrsImpares(inicioIntervalo, fimIntervalo);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio5C_ComInicioMaiorQueFim() {
        int inicioIntervalo = 30;
        int fimIntervalo = 12;

        int esperado = -1;
        int resultado = Exercicio5.obterSomaDeNrsImpares(inicioIntervalo, fimIntervalo);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio5C_TesteSemNrsImpares() {
        int inicioIntervalo = 10;
        int fimIntervalo = 11;

        int esperado = 0;
        int resultado = Exercicio5.obterSomaDeNrsImpares(inicioIntervalo, fimIntervalo);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio5C_TesteComNrsPares() {
        int inicioIntervalo = 51;
        int fimIntervalo = 60;

        int esperado = 275;
        int resultado = Exercicio5.obterSomaDeNrsImpares(inicioIntervalo, fimIntervalo);

        assertEquals(esperado, resultado);
    }

    //Exercício 5 d) ----------------------------------------------------------------------------------------------------

    @Test
    void Exercicio5D_TesteComZeroNrsImpares() {
        int inicioIntervalo = 52;
        int fimIntervalo = 53;

        int esperado = 0;
        int resultado = Exercicio5.obterQuantidadeDeNrsImpares(inicioIntervalo, fimIntervalo);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio5D_TesteComQuinzeNrsImpares() {
        int inicioIntervalo = 23;
        int fimIntervalo = 51;

        int esperado = 14;
        int resultado = Exercicio5.obterQuantidadeDeNrsImpares(inicioIntervalo, fimIntervalo);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio5D_TesteComNoveNrsImpares() {
        int inicioIntervalo = 53;
        int fimIntervalo = 71;

        int esperado = 9;
        int resultado = Exercicio5.obterQuantidadeDeNrsImpares(inicioIntervalo, fimIntervalo);

        assertEquals(esperado, resultado);
    }

    //Exercício 5 e) ----------------------------------------------------------------------------------------------------

    @Test
    void Exercicio5E_TesteComDoisMultiplos() {
        int inicioIntervalo = 20;
        int fimIntervalo = 30;
        int num = 7;

        int esperado = 49;
        int resultado = Exercicio5.obterSomaDeNrsMultiplos(inicioIntervalo, fimIntervalo, num);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio5E_TesteComCincoMultiplos() {
        int inicioIntervalo = 26;
        int fimIntervalo = 58;
        int num = 6;

        int esperado = 210;
        int resultado = Exercicio5.obterSomaDeNrsMultiplos(inicioIntervalo, fimIntervalo, num);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio5E_TesteSemMultiplos() {
        int inicioIntervalo = 27;
        int fimIntervalo = 29;
        int num = 6;

        int esperado = 0;
        int resultado = Exercicio5.obterSomaDeNrsMultiplos(inicioIntervalo, fimIntervalo, num);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio5E_TesteComIntervalosTrocados1() {
        int inicioIntervalo = 29;
        int fimIntervalo = 27;
        int num = 6;

        int esperado = 0;
        int resultado = Exercicio5.obterSomaDeNrsMultiplos(inicioIntervalo, fimIntervalo, num);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio5E_TesteComIntervalosTrocados2() {
        int inicioIntervalo = 50;
        int fimIntervalo = 26;
        int num = 5;

        int esperado = 150;
        int resultado = Exercicio5.obterSomaDeNrsMultiplos(inicioIntervalo, fimIntervalo, num);

        assertEquals(esperado, resultado);
    }

    //Exercício 5 f) ----------------------------------------------------------------------------------------------------

    @Test
    void Exercicio5F_TesteSemMultiplos() {
        int inicioIntervalo = 22;
        int fimIntervalo = 25;
        int num = 9;

        int esperado = 0;
        int resultado = Exercicio5.obterProdutoDeNrsMultiplos(inicioIntervalo, fimIntervalo, num);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio5F_TesteComNumNegativo() {
        int inicioIntervalo = 1;
        int fimIntervalo = 10;
        int num = -2;

        int esperado = 384;
        int resultado = Exercicio5.obterProdutoDeNrsMultiplos(inicioIntervalo, fimIntervalo, num);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio5F_TesteComTresMultiplos() {
        int inicioIntervalo = 6;
        int fimIntervalo = 13;
        int num = 3;

        int esperado = 648;
        int resultado = Exercicio5.obterProdutoDeNrsMultiplos(inicioIntervalo, fimIntervalo, num);

        assertEquals(esperado, resultado);
    }

    //Exercício 5 g) ----------------------------------------------------------------------------------------------------

    @Test
    void Exercicio5G_TesteComDoisMultiplos() {
        int inicioIntervalo = 20;
        int fimIntervalo = 30;
        int num = 7;

        double esperado = 24.5;
        double resultado = Exercicio5.obterMediaDeNrsMultiplos(inicioIntervalo, fimIntervalo, num);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio5G_TesteComCincoMultiplos() {
        int inicioIntervalo = 26;
        int fimIntervalo = 58;
        int num = 6;

        double esperado = 42;
        double resultado = Exercicio5.obterMediaDeNrsMultiplos(inicioIntervalo, fimIntervalo, num);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio5G_TesteSemMultiplos() {
        int inicioIntervalo = 27;
        int fimIntervalo = 29;
        int num = 6;

        double esperado = -1;
        double resultado = Exercicio5.obterMediaDeNrsMultiplos(inicioIntervalo, fimIntervalo, num);

        assertEquals(esperado, resultado);
    }

    //Exercício 5 h) ----------------------------------------------------------------------------------------------------

    @Test
    void Exercicio5H_TesteComCincoMultiplos() {
        int inicioIntervalo = 0;
        int fimIntervalo = 22;
        int x = 6; //3 múltiplos : 6, 12, 18
        int y = 9; //2 múltiplos : 9, 18

        double esperado = 9;
        double resultado = Exercicio5.obterMediaDeNrsMultiplosXY(inicioIntervalo, fimIntervalo, x, y);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio5H_TesteSemMultiplos() {
        int inicioIntervalo = 0;
        int fimIntervalo = 22;
        int x = 25; //0 múltiplos : 0
        int y = 30; //0 múltiplos : 0

        double esperado = 0.0;
        double resultado = Exercicio5.obterMediaDeNrsMultiplosXY(inicioIntervalo, fimIntervalo, x, y);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio5H_TesteComMultiplos() {
        int inicioIntervalo = 3;
        int fimIntervalo = 32;
        int x = 10; //3 múltiplos : 10, 20, 30
        int y = 5; //6 múltiplos : 5, 10, 15, 20, 25, 30

        double esperado = 17.5;
        double resultado = Exercicio5.obterMediaDeNrsMultiplosXY(inicioIntervalo, fimIntervalo, x, y);

        assertEquals(esperado, resultado);
    }

//Exercício 6 a) -------------------------------------------------------------------------------------------------------


    @Test
    void Exercicio6A_TestNrWithSixDigits() {
        long num = 123456L;

        int expected = 6;
        int result = Exercicio6.NrDigitsLongNr(num);

        assertEquals(expected, result);
    }

    @Test
    void Exercicio6A_TestNrWithTenDigits() {
        long num = 1234567891L;

        int expected = 10;
        int result = Exercicio6.NrDigitsLongNr(num);

        assertEquals(expected, result);
    }

//Exercise 6 b) -------------------------------------------------------------------------------------------------------

    @Test
    void Exercise6B_TestNrWithSixDigitsAndThreeAreEven() {
        long num = 123456L;

        int expected = 3;
        int result = Exercicio6.NrEvenDigitsLongNr(num);

        assertEquals(expected, result);
    }

    @Test
    void Exercise6B_TestNrWithTenDigitsAndFourAreEven() {
        long num = 1234567891L;

        int expected = 4;
        int result = Exercicio6.NrEvenDigitsLongNr(num);

        assertEquals(expected, result);
    }

    @Test
    void Exercise6B_TestNrWithEighteenDigitsAndSixAreEven() {

        long num = 963852147753951852L;

        int expected = 6;
        int result = Exercicio6.NrEvenDigitsLongNr(num);

        assertEquals(expected, result);
    }

    @Test
    void Exercise6B_TestNrWithFiveDigitsAndZeroAreEven() {

        long num = 59317L;

        int expected = 0;
        int result = Exercicio6.NrEvenDigitsLongNr(num);

        assertEquals(expected, result);
    }

//Exercise 6 c) --------------------------------------------------------------------------------------------------------

    @Test
    void Exercise6C_TestNrWithSevenDigitsAndFourAreOdd() {
        long num = 1234567L;

        int expected = 4;
        int result = Exercicio6.NrOddDigitsLongNr(num);

        assertEquals(expected, result);
    }

    @Test
    void Exercise6C_TestNrWithTenDigitsAndFiveAreOdd() {
        long num = 1234567891L;

        int expected = 6;
        int result = Exercicio6.NrOddDigitsLongNr(num);

        assertEquals(expected, result);
    }

    @Test
    void Exercise6C_TestNrWithEighteenDigitsAndElevenAreOdd() {

        long num = 963852147753951852L;

        int expected = 12;
        int result = Exercicio6.NrOddDigitsLongNr(num);

        assertEquals(expected, result);
    }

    @Test
    void Exercise6C_TestNrWithFiveDigitsAndZeroAreOdd() {

        long num = 24680L;

        int expected = 0;
        int result = Exercicio6.NrOddDigitsLongNr(num);

        assertEquals(expected, result);
    }

//Exercise 6 d) --------------------------------------------------------------------------------------------------------


    @Test
    void Exercise6D_TestAddTenDigits() {

        long num = 123456786723L;

        long expected = 54;
        int result = Exercicio6.AddDigitsLongNr(num);

        assertEquals(expected, result);

    }

    @Test
    void Exercise6D_TestAddNegativeLongNumber() {

        long num = -123456L;

        long expected = -21;
        int result = Exercicio6.AddDigitsLongNr(num);

        assertEquals(expected, result);
    }

    @Test
    void Exercise6D_TestAddNumberWithOneDigit() {

        long num = 9L;

        long expected = 9;
        int result = Exercicio6.AddDigitsLongNr(num);

        assertEquals(expected, result);
    }

    @Test
    void Exercise6D_TestWithZero() {

        long num = 0L;

        long expected = 0;
        int result = Exercicio6.AddDigitsLongNr(num);

        assertEquals(expected, result);
    }

//Exercise 6 e) --------------------------------------------------------------------------------------------------------

    @Test
    void Exercise6E_TestAddSixEvenDigits() {

        long num = 123456786723L;

        long expected = 28;
        int result = Exercicio6.AddEvenDigitsLongNr(num);

        assertEquals(expected, result);

    }

    @Test
    void Exercise6E_TestAddNegativeLongNumber() {

        long num = -123456L;

        long expected = -12;
        int result = Exercicio6.AddEvenDigitsLongNr(num);

        assertEquals(expected, result);
    }

    @Test
    void Exercise6E_TestAddNumberWithOneDigit() {

        long num = 9L;

        long expected = 0;
        int result = Exercicio6.AddEvenDigitsLongNr(num);

        assertEquals(expected, result);
    }

    @Test
    void Exercise6E_TestWithZero() {

        long num = 0L;

        long expected = 0;
        int result = Exercicio6.AddEvenDigitsLongNr(num);

        assertEquals(expected, result);
    }

//Exercise 6 f) --------------------------------------------------------------------------------------------------------

    @Test
    void ExerciseF_TestAddSixEvenDigits() {

        long num = 123456786723L;

        long expected = 26;
        long result = Exercicio6.AddOddDigitsLongNr(num);

        assertEquals(expected, result);

    }

    @Test
    void Exercise6F_TestAddNegativeLongNumber() {

        long num = -123456L;

        long expected = -9L;
        long result = Exercicio6.AddOddDigitsLongNr(num);

        assertEquals(expected, result);
    }

    @Test
    void Exercise6F_TestAddNumberWithOneEvenDigit() {

        long num = 8L;

        long expected = 0;
        int result = Exercicio6.AddOddDigitsLongNr(num);

        assertEquals(expected, result);
    }

    @Test
    void Exercise6F_TestWithZero() {

        long num = 0L;

        long expected = 0;
        int result = Exercicio6.AddOddDigitsLongNr(num);

        assertEquals(expected, result);
    }
//Exercise 6 g) --------------------------------------------------------------------------------------------------------

    @Test
    void Exercise6G_TestMeanOfTenDigits() {

        long num = 75432856992L;

        String expected = "5,45";
        String result = Exercicio6.getMeanDigitsLongNr(num);

        assertEquals(expected, result);

    }

    @Test
    void Exercise6G_TestMeanWithNegativeLongNumber() {

        long num = -123456L;

        String expected = "-3,50";
        String result = Exercicio6.getMeanDigitsLongNr(num);

        assertEquals(expected, result);
    }

    @Test
    void Exercise6G_TestMeanWithOneDigit() {

        long num = 9L;

        String expected = "9,00";
        String result = Exercicio6.getMeanDigitsLongNr(num);

        assertEquals(expected, result);
    }

    @Test
    void Exercise6G_TestMeanZero() {

        long num = 0L;

        String expected = "0,00";
        String result = Exercicio6.getMeanDigitsLongNr(num);

        assertEquals(expected, result);
    }

//Exercise 6 h) --------------------------------------------------------------------------------------------------------

    @Test
    void Exercise6H_TestMeanTenDigits() {

        long num = 75432856992L;

        String expected = "4,40";
        String result = Exercicio6.meanEvenDigitsLongNr(num);

        assertEquals(expected, result);

    }

    @Test
    void Exercise6H_TestAddNegativeLongNumber() {

        long num = -123456L;

        String expected = "-4,00";
        String result = Exercicio6.meanEvenDigitsLongNr(num);

        assertEquals(expected, result);
    }

    @Test
    void Exercise6H_TestAddNumberWithOneDigit() {

        long num = 9L;

        String expected = "O número tem zero dígitos ou não tem algarismos pares";
        String result = Exercicio6.meanEvenDigitsLongNr(num);

        assertEquals(expected, result);
    }

    @Test
    void Exercise6H_TestWithZero() {

        long num = 0L;

        String expected = "O número tem zero dígitos ou não tem algarismos pares";
        String result = Exercicio6.meanEvenDigitsLongNr(num);

        assertEquals(expected, result);
    }

//Exercise 6 i) --------------------------------------------------------------------------------------------------------

    @Test
    void Exercise6I_TestMeanTenDigits() {

        long num = 75432856992L;

        String expected = "6,33";
        String result = Exercicio6.meanOddDigitsLongNr(num);

        assertEquals(expected, result);

    }

    @Test
    void Exercise6I_TestAddNegativeLongNumber() {

        long num = -123456L;

        String expected = "-3,00";
        String result = Exercicio6.meanOddDigitsLongNr(num);

        assertEquals(expected, result);
    }

    @Test
    void Exercise6I_TestAddNumberWithOneDigit() {

        long num = 9L;

        String expected = "9,00";
        String result = Exercicio6.meanOddDigitsLongNr(num);

        assertEquals(expected, result);
    }

    @Test
    void Exercise6I_TestWithZero() {

        long num = 0L;

        String expected = "O número tem zero dígitos ou não tem algarismos ímpares";
        String result = Exercicio6.meanOddDigitsLongNr(num);

        assertEquals(expected, result);
    }

//Exercise 6 j) --------------------------------------------------------------------------------------------------------


    @Test
    void Exercise6J_ReverseNumberWithFifteenDigits() {

        long number = 123456789123456L;

        long expected = 654321987654321L;
        long result = Exercicio6.getReversedNumber(number);

        assertEquals(expected, result);
    }

    @Test
    void Exercise6J_ReverseNumberWithTenDigits() {

        long number = 9573162485L;

        long expected = 5842613759L;
        long result = Exercicio6.getReversedNumber(number);

        assertEquals(expected, result);
    }

    @Test
    void Exercise6J_ReverseNumberWithOneDigit() {

        long number = 1L;

        long expected = 1L;
        long result = Exercicio6.getReversedNumber(number);

        assertEquals(expected, result);
    }

    @Test
    void Exercise6J_ReverseNumberWithZeroDigits() {

        long number = 0L;

        long expected = 0L;
        long result = Exercicio6.getReversedNumber(number);

        assertEquals(expected, result);
    }

//Exercise 7 a) --------------------------------------------------------------------------------------------------------

    @Test
    void Exercise7A_PalindromeWithFifteenDigits() {

        long number = 123456787654321L;

        boolean result = Exercicio7.verifyIfNumberIsPalindrome(number);

        assertEquals(true, result);
    }

    @Test
    void Exercise7A_NotAPalindrome() {

        long number = 9573162485L;

        boolean result = Exercicio7.verifyIfNumberIsPalindrome(number);

        assertEquals(false, result);
    }

    @Test
    void Exercise7A_PalindromeWithOneDigit() {

        long number = 1L;

        boolean result = Exercicio7.verifyIfNumberIsPalindrome(number);

        assertEquals(true, result);
    }

    @Test
    void Exercise7A_PalindromeWithSixDigits() {

        long number = 123321L;

        boolean result = Exercicio7.verifyIfNumberIsPalindrome(number);

        assertEquals(true, result);
    }

//Exercise 7 b) --------------------------------------------------------------------------------------------------------

    @Test
    void Exercise7B_ArmstrongNrWithThreeDigits() {

        int originalNumber = 153;

        boolean result = Exercicio7.verifyArmstrongNumber(originalNumber);

        assertEquals(true, result);
    }

    @Test
    void Exercise7B_NotAnArmstrongNr() {

        int originalNumber = 1537;

        boolean result = Exercicio7.verifyArmstrongNumber(originalNumber);

        assertEquals(false, result);
    }

    @Test
    void Exercise7B_ArmstrongNrWithFourDigits() {

        int originalNumber = 8208;

        boolean result = Exercicio7.verifyArmstrongNumber(originalNumber);

        assertEquals(true, result);
    }

    @Test
    void Exercise7B_NegativeArmstrongNrWithOneDigit() {

        int originalNumber = -1;

        boolean result = Exercicio7.verifyArmstrongNumber(originalNumber);

        assertFalse(result);
        //assertEquals(true, result);
    }

    @Test
    void Exercise7B_ArmstrongNrWithOneDigit() {

        int originalNumber = 1;

        boolean result = Exercicio7.verifyArmstrongNumber(originalNumber);

        assertTrue(result);
    }
//Exercise 7 c) --------------------------------------------------------------------------------------------------------

    @Test
    void Exercise7C_GetFirstPalindromeRangeOneToTwelve() {

        int rangeStart = 1;
        int rangeEnd = 12;

        String expected = "A primeira capicua é: 11";
        String result = Exercicio7.getFirstPalindrome(rangeStart, rangeEnd);

        assertEquals(expected, result);
    }

    @Test
    void Exercise7C_GetFirstPalindromeRangeFiveThousandToTenThousand() {

        int rangeStart = 5000;
        int rangeEnd = 10000;

        String expected = "A primeira capicua é: 5005";
        String result = Exercicio7.getFirstPalindrome(rangeStart, rangeEnd);

        assertEquals(expected, result);
    }

    @Test
    void Exercise7C_NoPalindromeInTheRange() {

        int rangeStart = 13;
        int rangeEnd = 15;

        String expected = "Não há capicuas no intervalo dado.";
        String result = Exercicio7.getFirstPalindrome(rangeStart, rangeEnd);

        assertEquals(expected, result);
    }

//Exercise 7 d) --------------------------------------------------------------------------------------------------------

    @Test
    void Exercise7D_GetBiggestPalindromeRangeFiveThousandToSixThousand() {

        int rangeStart = 5000;
        int rangeEnd = 6000;

        String expected = "A maior capicua é: 5995";
        String result = Exercicio7.getBiggestPalindrome(rangeStart, rangeEnd);

        assertEquals(expected, result);
    }

    @Test
    void Exercise7D_GetBiggestPalindromeRangeTenToThreeHundred() {

        int rangeStart = 10;
        int rangeEnd = 300;

        String expected = "A maior capicua é: 5995";
        String result = Exercicio7.getBiggestPalindrome(rangeStart, rangeEnd);

        assertEquals(expected, result);
    }

    @Test
    void Exercise7D_GetBiggestPalindromeRangeOneToNine() {

        int rangeStart = 1;
        int rangeEnd = 9;

        String expected = "Não há capicuas no intervalo dado.";
        String result = Exercicio7.getBiggestPalindrome(rangeStart, rangeEnd);

        assertEquals(expected, result);
    }

    @Test
    void Exercise7D_GetBiggestPalindromeRangeOneToFifteen() {

        int rangeStart = 1;
        int rangeEnd = 15;

        String expected = "A maior capicua é: 11";
        String result = Exercicio7.getBiggestPalindrome(rangeStart, rangeEnd);

        assertEquals(expected, result);
    }

//Exercise 7 e) --------------------------------------------------------------------------------------------------------

    @Test
    void Exercise7E_GetNrPalindromesRangeOneToFifteen() {

        int rangeStart = 1;
        int rangeEnd = 15;

        String expected = "O número de capicuas é: 10";
        String result = Exercicio7.getNrPalindromes(rangeStart, rangeEnd);

        assertEquals(expected, result);
    }

    @Test
    void Exercise7E_GetNrPalindromesRangeOneHundredToTwoHundredAndTwo() {

        int rangeStart = 100;
        int rangeEnd = 202;

        String expected = "O número de capicuas é: 11";
        String result = Exercicio7.getNrPalindromes(rangeStart, rangeEnd);

        assertEquals(expected, result);
    }

    @Test
    void Exercise7E_GetNrPalindromesRangeTwelveToTwenty() {

        int rangeStart = 12;
        int rangeEnd = 20;

        String expected = "Não há capicuas no intervalo dado.";
        String result = Exercicio7.getNrPalindromes(rangeStart, rangeEnd);

        assertEquals(expected, result);
    }

//Exercise 7 f) --------------------------------------------------------------------------------------------------------

    @Test
    void Exercise7F_GetFirstArmstrongNrRangeTwoToTwoHundred() {

        int rangeStart = 2;
        int rangeEnd = 200;

        String expected = "O primeiro número de Armstrong é: 2";
        String result = Exercicio7.getFirstArmstrongNumber(rangeStart, rangeEnd);

        assertEquals(expected, result);
    }

    @Test
    void Exercise7F_GetFirstArmstrongNrRangeZeroToTwo() {

        int rangeStart = 0;
        int rangeEnd = 2;

        String expected = "O primeiro número de Armstrong é: 1";
        String result = Exercicio7.getFirstArmstrongNumber(rangeStart, rangeEnd);

        assertEquals(expected, result);
    }

    @Test
    void Exercise7F_GetFirstArmstrongNrRangeOneHundredToTwoHundred() {

        int rangeStart = 152;
        int rangeEnd = 200;

        String expected = "O primeiro número de Armstrong é: 153";
        String result = Exercicio7.getFirstArmstrongNumber(rangeStart, rangeEnd);

        assertEquals(expected, result);
    }

    @Test
    void Exercise7F_GetFirstArmstrongNrRangeThreeHundredToFiveHundred() {

        int rangeStart = 300;
        int rangeEnd = 500;

        String expected = "O primeiro número de Armstrong é: 370";
        String result = Exercicio7.getFirstArmstrongNumber(rangeStart, rangeEnd);

        assertEquals(expected, result);
    }

//Exercise 7 g) --------------------------------------------------------------------------------------------------------

    @Test
    void Exercise7F_CountTwoArmstrongNrs() {

        int rangeStart = 369;
        int rangeEnd = 372;

        int expected = 2;
        int result = Exercicio7.getAmountOfArmstrongNumbers(rangeStart, rangeEnd);

        assertEquals(expected, result);
    }

    @Test
    void Exercise7F_CountOneArmstrongNrs1() {

        int rangeStart = 400;
        int rangeEnd = 410;

        int expected = 1;
        int result = Exercicio7.getAmountOfArmstrongNumbers(rangeStart, rangeEnd);

        assertEquals(expected, result);
    }

    @Test
    void Exercise7F_CountOneArmstrongNrs2() {

        int rangeStart = 140;
        int rangeEnd = 160;

        int expected = 1;
        int result = Exercicio7.getAmountOfArmstrongNumbers(rangeStart, rangeEnd);

        assertEquals(expected, result);
    }

    @Test
    void Exercise7F_CountZeroArmstrongNrs() {

        int rangeStart = 500;
        int rangeEnd = 600;

        int expected = -1;
        int result = Exercicio7.getAmountOfArmstrongNumbers(rangeStart, rangeEnd);

        assertEquals(expected, result);
    }

//Exercise 11 --------------------------------------------------------------------------------------------------------


    @Test
    void Exercise11_TestNumberTwenty() {

        int number = 20;

        String expected = "10+10;O número de combinações é 1";
        String result = Exercicio11.getCombinations(number);

        assertEquals(expected, result);
    }

    @Test
    void Exercise11_TestNumberZero() {

        int number = 0;

        String expected = "O número tem de pertencer ao intervalo de 1 a 20";
        String result = Exercicio11.getCombinations(number);

        assertEquals(expected, result);
    }

    @Test
    void Exercise11_TestNumberTwentyOne() {

        int number = 21;

        String expected = "O número tem de pertencer ao intervalo de 1 a 20";
        String result = Exercicio11.getCombinations(number);

        assertEquals(expected, result);
    }

    @Test
    void Exercise11_TestNumberTwelve() {

        int number = 12;

        String expected = "2+10;3+9;4+8;5+7;6+6;O número de combinações é 5";
        String result = Exercicio11.getCombinations(number);

        assertEquals(expected, result);
    }

//Exercise 12 --------------------------------------------------------------------------------------------------------


    @Test
    void Exercise12_TestWithAEqualsToTwoBEqualsToFourAndCToTen() {
        double a = 2;
        double b = 4;
        double c = 10;

        String expected = "A equação não tem raízes reais";
        String result = Exercicio12.getEquationSolution(a, b, c);

        assertEquals(expected, result);
    }

    @Test
    void Exercise12_TestWithAEqualsToThreeBEqualsToSixAndCToTwo() {
        double a = 3;
        double b = 6;
        double c = 2;

        String expected = "A equação tem duas raízes reais: x = -0,4226, y = -1,5774";
        String result = Exercicio12.getEquationSolution(a, b, c);

        assertEquals(expected, result);
    }

    @Test
    void Exercise12_TestWithAEqualsToThirtyBEqualsToSeventyFiveAndCToFifteen() {
        double a = 30;
        double b = 75;
        double c = 15;

        String expected = "A equação tem duas raízes reais: x = -0,2192, y = -2,2808";
        String result = Exercicio12.getEquationSolution(a, b, c);

        assertEquals(expected, result);
    }

    @Test
    void Exercise12_TestWithAEqualsToOneBEqualsToMinusSixAndCToNine() {
        double a = 1;
        double b = -6;
        double c = 9;

        String expected = "A equação tem uma raíz dupla: x = 3.0";
        String result = Exercicio12.getEquationSolution(a, b, c);

        assertEquals(expected, result);
    }

//Exercise 16 --------------------------------------------------------------------------------------------------------


    @Test
    void Exercise16_SalaryUnderFiveHundred() {

        double salaryBeforeTax = 450;

        double expected = 405;
        double result = Exercicio16.getSalaryAfterTax(salaryBeforeTax);

        assertEquals(expected, result);
    }

    @Test
    void Exercise16_SalaryBetweenFiveHundredAndAThousand() {

        double salaryBeforeTax = 798;

        double expected = 678.3;
        double result = Exercicio16.getSalaryAfterTax(salaryBeforeTax);

        assertEquals(expected, result);
    }

    @Test
    void Exercise16_SalaryOverAThousand() {

        double salaryBeforeTax = 234325;

        double expected = 187460;
        double result = Exercicio16.getSalaryAfterTax(salaryBeforeTax);

        assertEquals(expected, result);
    }

    @Test
    void Exercise16_NegativeSalary() {

        double salaryBeforeTax = -4;

        double expected = -1;
        double result = Exercicio16.getSalaryAfterTax(salaryBeforeTax);

        assertEquals(expected, result);
    }

//Exercise 17 --------------------------------------------------------------------------------------------------------


    @Test
    void Exercise17A_SmallBreedTooMuchFood() {

        double dogWeight = 5;
        double foodWeight = 130;

        String expected = "A quantidade de comida é demasiada para um cão de raça pequena";
        String result = Exercicio17.verifyDogFoodWeight(dogWeight, foodWeight);

        assertEquals(expected, result);
    }

    @Test
    void Exercise17A_SmallBreedEnoughFood() {

        double dogWeight = 5;
        double foodWeight = 100;

        String expected = "A quantidade de comida é adequada para um cão de raça pequena";
        String result = Exercicio17.verifyDogFoodWeight(dogWeight, foodWeight);

        assertEquals(expected, result);
    }

    @Test
    void Exercise17A_SmallBreedTooLittleFood() {

        double dogWeight = 5;
        double foodWeight = 90;

        String expected = "A quantidade de comida não é suficiente para um cão de raça pequena";
        String result = Exercicio17.verifyDogFoodWeight(dogWeight, foodWeight);

        assertEquals(expected, result);
    }

    @Test
    void Exercise17A_MediumBreedTooMuchFood() {

        double dogWeight = 25;
        double foodWeight = 300;

        String expected = "A quantidade de comida é demasiada para um cão de raça média";
        String result = Exercicio17.verifyDogFoodWeight(dogWeight, foodWeight);

        assertEquals(expected, result);
    }

    @Test
    void Exercise17A_BigBreedEnoughFood() {

        double dogWeight = 45;
        double foodWeight = 300;

        String expected = "A quantidade de comida é adequada para um cão de raça grande";
        String result = Exercicio17.verifyDogFoodWeight(dogWeight, foodWeight);

        assertEquals(expected, result);
    }

    @Test
    void Exercise17A_GiantBreedTooLittleFood() {

        double dogWeight = 55;
        double foodWeight = 300;

        String expected = "A quantidade de comida não é suficiente para um cão de raça gigante";
        String result = Exercicio17.verifyDogFoodWeight(dogWeight, foodWeight);

        assertEquals(expected, result);
    }

//Exercise 18 --------------------------------------------------------------------------------------------------------

    @Test
    void Exercise18_CardNumberWithThreeDigits() {

        int idCardNumber = 123;
        int idCardControlNumber = 9;

        String expected = "Por favor, introduza um número válido";
        String result = Exercicio18.verifyIdCardNumber(idCardNumber, idCardControlNumber);

        assertEquals(expected, result);
    }

    @Test
    void Exercise18_CardNumberWithTenDigits() {

        int idCardNumber = 1234567891;
        int idCardControlNumber = 9;

        String expected = "Por favor, introduza um número válido";
        String result = Exercicio18.verifyIdCardNumber(idCardNumber, idCardControlNumber);

        assertEquals(expected, result);
    }

    @Test
    void Exercise18_CardControlNumberWithTwoDigits() {

        int idCardNumber = 12345678;
        int idCardControlNumber = 12;

        String expected = "Por favor, introduza um número válido";
        String result = Exercicio18.verifyIdCardNumber(idCardNumber, idCardControlNumber);

        assertEquals(expected, result);
    }

    @Test
    void Exercise18_CardControlNumberIsZero() {

        int idCardNumber = 12345678;
        int idCardControlNumber = 0;

        String expected = "Por favor, introduza um número válido";
        String result = Exercicio18.verifyIdCardNumber(idCardNumber, idCardControlNumber);

        assertEquals(expected, result);
    }

    @Test
    void Exercise18_CardControlIsAMultipleOfEleven1() {

        int idCardNumber = 13734526;
        int idCardControlNumber = 7;

        String expected = "Verificação: true";
        String result = Exercicio18.verifyIdCardNumber(idCardNumber, idCardControlNumber);

        assertEquals(expected, result);
    }

    @Test
    void Exercise18_CardControlIsAMultipleOfEleven2() {

        int idCardNumber = 13106482;
        int idCardControlNumber = 7;

        String expected = "Verificação: true";
        String result = Exercicio18.verifyIdCardNumber(idCardNumber, idCardControlNumber);

        assertEquals(expected, result);
    }

//Exercise 20 --------------------------------------------------------------------------------------------------------


    @Test
    void Exercise20_TestWithNegativeNumber() {

        int number = -2;

        String expected = "O número não pode ser negativo";
        String result = Exercicio20.classifyNumbers(number);

        assertEquals(expected, result);
    }

    @Test
    void Exercise20_TestWithPerfectNumber() {

        int number = 6;

        String expected = "O número é perfeito";
        String result = Exercicio20.classifyNumbers(number);

        assertEquals(expected, result);
    }

    @Test
    void Exercise20_TestWithAbundantNumber() {

        int number = 12;

        String expected = "O número é abundante";
        String result = Exercicio20.classifyNumbers(number);

        assertEquals(expected, result);
    }

    @Test
    void Exercise20_TestWithReducedNumber() {

        int number = 9;

        String expected = "O número é reduzido";
        String result = Exercicio20.classifyNumbers(number);

        assertEquals(expected, result);
    }

}