package com.company.bloco2;

public class Bloco2_Exercicio20 {
    public static void main(String[] args) {

    }

    public static String obterPrecoAluguerUtensilios(int nrDiasSemana, int nrDiasFimSemana, String tipoKit, double nrKms) {
        double precoKitASemana = 30;
        double precoKitAFimSemana = 40;

        double precoKitBSemana = 50;
        double precoKitBFimSemana = 70;

        double precoKitCSemana = 100;
        double precoKitCFimSemana = 140;

        double precoKitATotal;
        double precoKitBTotal;
        double precoKitCTotal;

        double precoDeslocacao = 2 * nrKms;


        String mensagem = "";


        switch (tipoKit) {
            case "A":
                precoKitATotal = precoKitASemana * nrDiasSemana + precoKitAFimSemana * nrDiasFimSemana + precoDeslocacao;
                mensagem = "O preço do Kit A será de " + String.format("%.2f",precoKitATotal);
                break;
            case "B":
                precoKitBTotal = precoKitBSemana * nrDiasSemana + precoKitBFimSemana * nrDiasFimSemana + precoDeslocacao;
                mensagem = "O preço do Kit B será de " + String.format("%.2f",precoKitBTotal);
                break;
            case "C":
                precoKitCTotal = precoKitCSemana * nrDiasSemana + precoKitCFimSemana * nrDiasFimSemana + precoDeslocacao;
                mensagem = "O preço do Kit C será de " + String.format("%.2f",precoKitCTotal);
                break;

        }

        return mensagem;
    }
}

