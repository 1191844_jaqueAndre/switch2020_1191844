package com.company.bloco2;

public class Bloco2_Exercicio3 {

    public static void main(String[] args) {
        getDistanceBetweenTwoPoints(234, -235, 98, 34.324);
    }

    public static double getDistanceBetweenTwoPoints ( double x1, double x2, double y1, double y2){
            return Math.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
        }
    }

