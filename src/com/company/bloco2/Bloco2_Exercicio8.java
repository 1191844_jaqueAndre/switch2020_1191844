package com.company.bloco2;

import java.io.Serializable;

public class Bloco2_Exercicio8 {

    public static void main(String[] args) {
        multiplicityXY(5, 10);
    }

    public static String multiplicityXY(int X, int Y) {
        if (X % Y == 0) {
            System.out.println("X é múltiplo de Y.");
            return "X é múltiplo de Y.";
        } else if (Y % X == 0) {
            System.out.println("Y é múltiplo de X.");
            return "Y é múltiplo de X.";
        } else {
            System.out.println("X não é múltiplo nem divisor de Y.");
            return "X não é múltiplo nem divisor de Y.";
        }
    }
}
