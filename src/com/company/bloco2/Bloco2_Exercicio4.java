package com.company.bloco2;

public class Bloco2_Exercicio4 {

    public static void main (String[] args){

        piecewiseFunction(45.4673);
    }

    public static double piecewiseFunction(double x) {
        double y;
        if (x < 0) {
            y = x;
            return y;
        } else if (x == 0) {
            y = 0;
            return y;
        } else if (x > 0) {
            y = x * x - 2 * x;
            return y;
        }
        return piecewiseFunction(x);
    }
}
