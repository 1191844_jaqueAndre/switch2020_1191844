package com.company.bloco2;

import java.util.Scanner;

public class Bloco2_Exercicio5 {

    //Exercício 5 processamento

    public static String getCubeVolume(double area) {

        if (area > 0) {
            double side = Math.sqrt(area / 6);
            double volume = Math.pow(side, 3) / 1000;

            if (volume <= 1) {
                //Saída de dados
                return "O cubo é pequeno, o seu volume é de " + String.format("%.4f",volume) + " dm3.";
            } else if (volume > 1 && volume <= 2) {
                //Saída de dados
                return "O cubo é médio, o seu volume é de " + String.format("%.4f",volume) + " dm3.";
            }
                //Saída de dados
                return "O cubo é grande, o seu volume é de " + String.format("%.4f",volume) + " dm3.";
             }
        //Saída de dados
            return "Valor da área incorreto.";
}

 /*   //Exercício 5 - versão modularizada

    public static void exercicio5() {
        //Entrada de dados
        Scanner ler = new Scanner(System.in);
        System.out.println("Introduza a área do cubo");
        double area = ler.nextDouble();
        //Invocar processamento
        double volume = obterVolumeCubo(area);
        String classificacao = obterClassificacaoCubo(volume);
        //Saída de dados
        if (volume > 0) {
            System.out.println("O volume é: " + volume);
            System.out.println("É um cubo " + classificacao);
        } else {
            System.out.println("Valor de área incorreto");
        }
    }

    public static double obterVolumeCubo(double area) {
        if (area > 0) {
            double aresta = Math.sqrt(area / 6);
            double volume = Math.pow(aresta, 3);
            return volume;
        } else {
            return -1;
        }
    }

    public static String obterClassificacaoCubo(double volume) {
        if (volume <= 1) {
            return "O cubo é pequeno";
        } else if (volume > 1 && volume <= 2) {
            return "O cubo é médio";
        } else if (volume > 2) {
            return "O cubo é grande";
        }
        return obterClassificacaoCubo(volume);
    }

  */
}
