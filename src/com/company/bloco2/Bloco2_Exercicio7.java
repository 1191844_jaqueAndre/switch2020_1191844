package com.company.bloco2;

public class Bloco2_Exercicio7 {

    public static void main (String[] args){

    }

    public static double getWorkerCost(double buildingArea, double dailyPainterRate){
        int numberOfPainters;
        if (buildingArea > 0 && buildingArea < 100){
           numberOfPainters = 1;
        } else if (buildingArea >= 100 && buildingArea < 300){
           numberOfPainters = 2;
        } else if (buildingArea >= 300 && buildingArea < 1000){
           numberOfPainters = 3;
        } else{
           numberOfPainters = 4;
        }
        double workerCost = dailyPainterRate * numberOfPainters * (buildingArea / 16); //Cada pintor tem um rendimento de 2m2/h, por 8h
        return workerCost;
    }

    public static double getPaintCost(double buildingArea, double costLiterPaint, double literPaintYeld){
        double paintCost = buildingArea / literPaintYeld * costLiterPaint;
        return paintCost;
    }
    public static double getTotalCost (double paintCost, double workerCost){
        double totalCost = workerCost + paintCost;
        return totalCost;
    }
}
