package com.company.bloco2;

public class Bloco2_Exercicio13 {

    public static void main(String[] args) {

    }
    public static String calcularTempoEPrecoEstimado (double areaGrama, double nrArvores, double nrArbustos){
        double tempoEstimadoHoras = (areaGrama * 300 + nrArvores * 600 + nrArbustos * 400) / 3600;
        double custoEstimado = 10 * areaGrama + 20 * nrArvores + 15 * nrArbustos + 10 * tempoEstimadoHoras;

        return "O tempo estimado é de " + String.format("%.0f",tempoEstimadoHoras) + " horas e o custo estimado é de " + String.format("%.2f",custoEstimado);
    }




}
