package com.company.bloco2;

public class Bloco2_Exercicio10 {

    public static void main(String[] args) {

    }

    public static double obterPrecoSaldo(double precoNormal){
        double precoSaldo;
        if (precoNormal > 200){
            precoSaldo = 0.4 * precoNormal;
        }else if(precoNormal > 100){
            precoSaldo = 0.6 * precoNormal;
        }else if(precoNormal > 50){
            precoSaldo = 0.7 * precoNormal;
        }else{
            precoSaldo = 0.8 * precoNormal;
        }
        return precoSaldo;
    }
}
