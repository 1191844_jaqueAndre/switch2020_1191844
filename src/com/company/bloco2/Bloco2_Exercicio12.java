package com.company.bloco2;

import java.util.Scanner;

public class Bloco2_Exercicio12 {

    public static void main(String[] args) {

        Scanner ler = new Scanner(System.in);
        System.out.println("Introduza o nível de poluição");
        double nivelPoluicao = ler.nextDouble();

        intimacaoIndustriasPoluentes(nivelPoluicao);

    }

    public static String intimacaoIndustriasPoluentes(double nivelPoluicao){
        String resultado = "vazio";
        if(nivelPoluicao > 0.5){
            resultado = "Todos os grupo são intimados a suspender as suas atividades.";
        }else if(nivelPoluicao > 0.4){
            resultado = "O 1º e o 2º grupo são intimados a suspender as suas atividades.";
        }else if(nivelPoluicao > 0.3){
            resultado = "O 1º grupo é intimado a suspender as suas atividades.";
        }else{
            resultado = "Os níveis de poluição estão dentro dos limites aceitáveis.";
        }
        System.out.println(resultado);
        return resultado;
    }
}
