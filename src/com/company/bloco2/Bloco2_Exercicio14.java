package com.company.bloco2;

public class Bloco2_Exercicio14 {

    public static void main(String[] args) {
    }

    public static double obterMediaDistancias(double distanciaDia1, double distanciaDia2, double distanciaDia3, double distanciaDia4, double distanciaDia5){
        double mediaDistanciasMilhas = (distanciaDia1 + distanciaDia2 + distanciaDia3 + distanciaDia4 + distanciaDia5) / 5;
        double mediaDistanciaKm = mediaDistanciasMilhas * 1.609;

        return mediaDistanciaKm;
    }
}
