package com.company.bloco2;

public class Bloco2_Exercicio15 {

    public static void main(String[] args) {

    }

    public static String obterClassificacaoTriangulo(double a, double b, double c) {
        String resultado = "";
        if (a <= 0 || b <= 0 || c <= 0 || a > b + c || b > c + a || c > a + b){
            resultado = "O triângulo não é possível.";
        }else if (a == b && a == c) {
            resultado = "O triângulo é equilátero";
        } else if (a != b && a != c && b != c) {
            resultado = "O triângulo é escaleno";
        } else {
            resultado = "O triângulo é isósceles";
        }

        return resultado;
    }

}