package com.company.bloco2;

import java.util.Scanner;

public class Bloco2_Exercicio11 {

    public static void main(String[] args) {

        //Entrada de dados
        Scanner ler = new Scanner(System.in);
        System.out.println("Introduza a percentagem de alunos aprovados");
        double percentagemAprovados = ler.nextDouble();
        System.out.println("Introduza o limite de validação da turma má");
        double limiteValidacaoTurmaMa = ler.nextDouble();
        System.out.println("Introduza o limite de validação da turma fraca");
        double limiteValidacaoTurmaFraca = ler.nextDouble();
        System.out.println("Introduza o limite de validação da turma razoável");
        double limiteValidacaoTurmaRazoavel = ler.nextDouble();
        System.out.println("Introduza o limite de validação da turma boa");
        double limiteValidacaoTurmaBoa = ler.nextDouble();

        //Processamento
        obterClassificacaoTurma(percentagemAprovados, limiteValidacaoTurmaMa, limiteValidacaoTurmaFraca, limiteValidacaoTurmaRazoavel, limiteValidacaoTurmaBoa);
    }
        //Saída de dados
    public static String obterClassificacaoTurma(double percentagemAprovados, double limiteValidacaoTurmaMa, double limiteValidacaoTurmaFraca, double limiteValidacaoTurmaRazoavel, double limiteValidacaoTurmaBoa){
        String resultado = "vazio";
        if (percentagemAprovados < 0 || percentagemAprovados > 1) {
            resultado = "Valor inválido";
        }else if(percentagemAprovados < limiteValidacaoTurmaMa){
            resultado = "Turma má";
        } else if(percentagemAprovados < limiteValidacaoTurmaFraca){
            resultado = "Turma fraca";
         }else if(percentagemAprovados < limiteValidacaoTurmaRazoavel){
            resultado = "Turma razoável";
        }else if(percentagemAprovados < limiteValidacaoTurmaBoa){
            resultado = "Turma boa";
        }else if(percentagemAprovados >= limiteValidacaoTurmaBoa) {
            resultado = "Turma excelente";
        }
        System.out.println(resultado);
        return resultado;
    }
}
