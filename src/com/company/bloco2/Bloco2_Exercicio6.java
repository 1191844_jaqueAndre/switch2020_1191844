package com.company.bloco2;

public class Bloco2_Exercicio6 {

    public static void main (String[]args){
      getHoursMinutesSeconds(5000);
    }

    public static String getHoursMinutesSeconds (int totalSeconds) {
        int hours = totalSeconds / 3600;
        int minutes = (totalSeconds % 3600) / 60;
        int seconds = (totalSeconds % 3600) % 60;
        return hours + ":" + minutes + ":" + seconds;
    }
}


