package com.company.bloco2;

public class Bloco2_Exercicio19 {
    public static void main(String[] args) {

    }

    public static String salarioEmpregado(double horasExtra){
        final int HORAS_SEMANAIS = 36;
        final double PRECO_HORA = 7.5;
        double salarioSemanalTotal;

        double precoHoraExtra;
        if (horasExtra > 5){
            precoHoraExtra = 15;
        }else if (horasExtra < 0){
            return "Informação inválida.";
        }else {
            precoHoraExtra = 10;
        }

        salarioSemanalTotal = HORAS_SEMANAIS * PRECO_HORA + horasExtra * precoHoraExtra;

        return "O salário semanal do empregado é " + String.format("%.2f", salarioSemanalTotal);

    }
}
