package com.company.bloco2;

public class Bloco2_Exercicio17 {

    public static void main(String[] args) {

    }

    public static String obterInformacaoChegada(int horaPartidaSegundos, int duracaoViagemSegundos){
        int horaChegadaSegundos;
        String chegada = "";

        horaChegadaSegundos = horaPartidaSegundos + duracaoViagemSegundos;

        int horaChegadaHoras = horaChegadaSegundos / 3600;
        int horaChegadaMinutos = (horaChegadaSegundos % 3600) / 60;
        int horaChegadaSegundosFinal = (horaChegadaSegundos % 3600) % 60;


        if (horaChegadaSegundos < 86400){
            chegada = "O comboio chegou no mesmo dia, ";
        }else {
            chegada = "O comboio chegou no dia seguinte, ";
            horaChegadaHoras = horaChegadaHoras - 24;
        }

        String resultado = "às " + horaChegadaHoras + ":" + horaChegadaMinutos + ":" + horaChegadaSegundosFinal;

        return chegada + resultado;
    }

}

