package com.company.bloco2;

import java.util.Scanner;

public class Bloco2_Exercicio2 {

    public static void main(String[] args) {

        //Entrada de dados
        Scanner ler = new Scanner(System.in);
        System.out.println("Introduza o número");
        int number = ler.nextInt();
    }
    //Processamento e Saída de dados
    public static String getDigits(int number) {
        if (number < 100 || number > 999) {
            if (number % 2 != 0) {
                return "O número não tem três dígitos e é ímpar.";
            } else {
                return "O número não tem três dígitos e é par.";
            }
        } else {
            int digit1 = number % 10;
            int digit2 = (number / 10) % 10;
            int digit3 = (number / 100) % 10;

            if (number % 2 != 0) {
                return "O dígito 1 é " + digit1 + ", o dígito 2 é " + digit2 + ", o dígito 3 é " + digit3 + " e o número é ímpar.";
            } else {
                return "O dígito 1 é " + digit1 + ", o dígito 2 é " + digit2 + ", o dígito 3 é " + digit3 + " e o número é par.";
            }
        }
    }
}
