package com.company.bloco2;

public class Bloco2_Exercicio9 {

    public static void main(String[] args) {

    }

    public static String obterSequenciaNumero(int numero){
        int unidades = numero % 10;
        int dezenas = (numero / 10) % 10;
        int centenas = (numero / 100) % 10;
        if (centenas < dezenas && dezenas < unidades) {
            return "A sequência dos algarismos é crescente";
        }else{
            return "A sequência dos algarismos não é crescente";
        }
    }
}
