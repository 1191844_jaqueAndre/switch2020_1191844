package com.company.bloco2;

public class Bloco2_Exercicio18 {

    public static void main(String[] args) {

    }

    public static String processamentoTarefa(int inicioProcessamentoHoras, int inicioProcessamentoMinutos, int inicioProcessamentoSegundos, int duracaoProcessamento){

        String mensagemResposta = "";

        int duracaoProcessamentoHoras = duracaoProcessamento / 3600;
        int duracaoProcessamentoMinutos = (duracaoProcessamento % 3600) / 60;
        int duracaoProcessamentoSegundos = (duracaoProcessamento % 3600) % 60;

        int fimProcessamentoSegundos = inicioProcessamentoSegundos + duracaoProcessamentoSegundos;

        if (fimProcessamentoSegundos >59) {
            fimProcessamentoSegundos = fimProcessamentoSegundos - 60;
            duracaoProcessamentoMinutos = duracaoProcessamentoMinutos + 1;
        }

        int fimProcessamentoMinutos = inicioProcessamentoMinutos + duracaoProcessamentoMinutos;

        if (fimProcessamentoMinutos >59) {
            fimProcessamentoMinutos = fimProcessamentoMinutos - 60;
            duracaoProcessamentoHoras = duracaoProcessamentoHoras + 1;
        }

        int fimProcessamentoHoras = inicioProcessamentoHoras + duracaoProcessamentoHoras;

        if (fimProcessamentoHoras > 24){
            fimProcessamentoHoras = fimProcessamentoHoras - 24;
            mensagemResposta = "A hora de fim de processamento será às " + fimProcessamentoHoras + ":" + fimProcessamentoMinutos + ":" + fimProcessamentoSegundos + " do dia seguinte";
        }else {
            mensagemResposta = "A hora de fim de processamento será às " + fimProcessamentoHoras + ":" + fimProcessamentoMinutos + ":" + fimProcessamentoSegundos;
        }

        return mensagemResposta;

    }
}
