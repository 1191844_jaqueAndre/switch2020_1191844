package com.company.bloco2;

import java.util.Scanner;

public class Bloco2_Exercicio1 {

    public static void main(String[] args) {

        //Entrada de dados
        Scanner ler = new Scanner(System.in);

        System.out.println("Introduza a nota 1");
        int grade1 = ler.nextInt();
        System.out.println("Introduza a nota 2");
        int grade2 = ler.nextInt();
        System.out.println("Introduza a nota 3");
        int grade3 = ler.nextInt();
        System.out.println("Introduza o peso da nota 1");
        int weight1 = ler.nextInt();
        System.out.println("Introduza o peso da nota 2");
        int weight2 = ler.nextInt();
        System.out.println("Introduza o peso da nota 3");
        int weight3 = ler.nextInt();
        String averageGrade;


        //Processamento e Saída de dados
        getAverageGrade(grade1, grade2, grade3, weight1, weight2, weight3);
    }

    public static String getAverageGrade(int grade1, int grade2, int grade3, int weight1, int weight2, int weight3) {
        int averageGrade = (grade1 * weight1 + grade2 * weight2 + grade3 * weight3) / (weight1 + weight2 + weight3);
        if (weight1 + weight2 + weight3 != 100 || grade1 > 20 || grade2 > 20 || grade3 > 20 || grade1 < 0 || grade2 < 0 || grade3 < 0) {
            return "Erro: A soma dos pesos das notas é diferente de 100 ou uma das notas toma valores inválidos.";
        } else {
            if (averageGrade >= 8) {
                return "O aluno cumpre a nota mínima exigida, pois teve uma nota de " + averageGrade;
            } else {
                return "O aluno não cumpre a nota mínima exigida, pois teve uma nota de " + averageGrade;
            }
        }
    }


}