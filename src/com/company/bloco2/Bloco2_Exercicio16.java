package com.company.bloco2;

public class Bloco2_Exercicio16 {

    public static void main(String[] args) {

    }

    public static String obterClassificacaoAngulosTriangulo(double a, double b, double c) {
        String resultado = "";
        if (a <= 0 || b <= 0 || c <= 0 || a + b + c != 180){
            resultado = "O triângulo não é possível.";
        }else if (a == 90 || b == 90 || c == 90) {
            resultado = "O triângulo é retângulo";
        } else if (a > 90 || b > 90 || c > 90) {
            resultado = "O triângulo é obtusângulo";
        } else {
            resultado = "O triângulo é acutângulo";
        }

        return resultado;
    }
}
