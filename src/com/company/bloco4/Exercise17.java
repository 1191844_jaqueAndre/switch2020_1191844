package com.company.bloco4;

public class Exercise17 {
    public static void main(String[] args) {

    }

//-------A--------------------------------------------------------------------------------------------------------------

    /**
     * O método multiplica cada elemento de uma amtriz por uma constante
     *
     * @param numberMatrix matriz de números inteiros
     * @param constant     constante
     * @return matriz original multiplicada plea constante
     */
    public static int[][] getProductMatrixByConstant(int[][] numberMatrix, int constant) {
        //verifica se a matriz é nula
        if (numberMatrix == null) {
            return null;
        }
        //multiplica-se cada valor da matriz com a constante
        for (int i = 0; i < numberMatrix.length; i++) {
            for (int j = 0; j < numberMatrix[i].length; j++) {
                numberMatrix[i][j] = numberMatrix[i][j] * constant;
            }
        }
        return numberMatrix;
    }

//-------B--------------------------------------------------------------------------------------------------------------

    /**
     * O método soma duas matrizes, verificando se têm o mesmo número de linhas e colunas
     *
     * @param numberMatrix1 matriz de números inteiros 1
     * @param numberMatrix2 matriz de números inteiros 2
     * @return matriz resultante da soma
     */
    public static int[][] sumTwoMatrixes(int[][] numberMatrix1, int[][] numberMatrix2) {
        //Verifica que as matrizes a somar têm o mesmo número de linhas. Se não tiverem, returna null
        if (numberMatrix1.length != numberMatrix2.length) {
            return null;
        }
        //O ciclo percorre as linhas das matrizes
        for (int i = 0; i < numberMatrix1.length; i++) {
            //Se o número de colunas da linha i da matriz um não for igual ao da matriz dois, retorna null, pois não se podem somar
            if (numberMatrix1[i].length != numberMatrix2[i].length) {
                return null;
            }
            //O ciclo percorre cada coluna e soma os elementos da matriz dois à matriz um
            for (int j = 0; j < numberMatrix1[i].length; j++) {
                numberMatrix1[i][j] = numberMatrix1[i][j] + numberMatrix2[i][j];
            }
        }
        return numberMatrix1;
    }

//-------C--------------------------------------------------------------------------------------------------------------

    /**
     * O método multiplica duaas matrizes, verificando se têm o mesmo número de linhas e colunas
     *
     * @param numberMatrix1 matriz de números inteiros 1
     * @param numberMatrix2 matriz de números inteiros 2
     * @return matriz resultante do produto
     */
    public static int[][] multiplyTwoMatrixes(int[][] numberMatrix1, int[][] numberMatrix2) {
        if (numberMatrix1.length != numberMatrix2.length) {
            return null;
        }
        //O ciclo percorre as linhas das matrizes
        for (int i = 0; i < numberMatrix1.length; i++) {
            //Se o número de colunas da linha i da matriz um não for igual ao da matriz dois, retorna null, pois não se podem multiplicar
            if (numberMatrix1[i].length != numberMatrix2[i].length) {
                return null;
            }
            //O ciclo percorre cada coluna e multiplica os elementos da matriz dois aos da matriz um
            for (int j = 0; j < numberMatrix1[i].length; j++) {
                numberMatrix1[i][j] = numberMatrix1[i][j] * numberMatrix2[i][j];
            }
        }
        return numberMatrix1;
    }

}
