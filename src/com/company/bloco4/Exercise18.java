package com.company.bloco4;

import java.util.Scanner;

import static com.company.bloco4.MethodsLibrary.generateMaskMatrix;
import static com.company.bloco4.MethodsLibrary.printMatrixWordSearchGame;

//Exercício testado apenas com a consola
public class Exercise18 {

    public static void main(String[] args) {

        //Criar matriz
        char[][] letterMatrix = {
                {'Q', 'V', 'N', 'X', 'L', 'Z', 'A', 'F', 'A', 'I'},
                {'F', 'B', 'V', 'A', 'H', 'G', 'N', 'H', 'N', 'F'},
                {'B', 'D', 'Y', 'Q', 'I', 'Y', 'U', 'G', 'G', 'Z'},
                {'Q', 'M', 'L', 'A', 'J', 'S', 'Q', 'X', 'O', 'K'},
                {'I', 'E', 'A', 'D', 'A', 'B', 'S', 'T', 'L', 'R'},
                {'Z', 'T', 'T', 'A', 'P', 'I', 'U', 'U', 'A', 'P'},
                {'L', 'A', 'I', 'N', 'A', 'X', 'R', 'X', 'R', 'Z'},
                {'Q', 'X', 'J', 'A', 'N', 'L', 'E', 'X', 'C', 'V'},
                {'R', 'I', 'F', 'C', 'G', 'Q', 'P', 'G', 'C', 'C'},
                {'U', 'N', 'O', 'N', 'A', 'B', 'E', 'L', 'M', 'M'}};

        int gameSize = 10;
        //Imprimir a matriz de letras
        printMatrixWordSearchGame(gameSize, letterMatrix);

        //Pedir ao utilizador para introduzir a letra que procura
        Scanner read = new Scanner(System.in);
        System.out.println("Please insert a letter from the game. If more than a letter is written, we will only consider the first one");
        char letter = read.next().charAt(0);
        letter = Character.toUpperCase(letter);

        //Criar matriz máscara
        char[][] maskMatrix = generateMaskMatrix(letterMatrix, letter);

        //Obter a(s) posição(ões) da letra desejada e imprimi-la(s) a negrito, itálico e azul na matriz
        printMatrixWithHighlightedLetter(letterMatrix, gameSize, maskMatrix);

        //Pedir ao utilizador para inserir uma palavra
        System.out.println("Please insert a word");
        String word = read.next();
        word = word.toUpperCase();

        String[] existentWords = {"PERU", "ITALY", "JAPAN", "ANGOLA", "CANADA", "RUSSIA", "LEBANON"};

        String[] foundWords = new String[existentWords.length];

        int foundWordsIndex = 0;
        int foundWordsCounter = 0;

        //Se a palavra existir na sopa de letras, imprime-se uma mensagem de sucesso na consola e pede-se a próxima palavra ao utilizador
        while (foundWordsCounter < existentWords.length) {

            if (doesWordMatch(word, letterMatrix) && wordExistsInArray(existentWords, word) && !wordExistsInArray(foundWords, word)) {
                foundWords[foundWordsIndex] = word;
                foundWordsIndex++;
                foundWordsCounter++;

                System.out.println("Encontrou a palavra " + word + "! Insira a próxima palavra");

                if (foundWordsCounter == existentWords.length) {
                    System.out.println("Parabéns! Encontrou todas as palavras!");
                }

            } else {
                System.out.println("A palavra não existe ou já foi encontrada, tente outra vez");
            }

            word = read.next();
            word = word.toUpperCase();
        }
    }

    public static void printMatrixWithHighlightedLetter(char[][] letterMatrix, int gameSize, char[][] maskMatrix) {
        for (int i = 0; i < gameSize; i++) {
            for (int j = 0; j < gameSize; j++) {
                if (maskMatrix[i][j] == 1) {
                    System.out.print("| \033[0;1m\033[3m\u001B[34m" + letterMatrix[i][j] + "\033[0m\033[0;0m |");
                } else {
                    System.out.print("| " + letterMatrix[i][j] + " |");
                }
            }
            System.out.println();
        }
    }

    public static boolean doesWordMatch(String word, char[][] letterMatrix) {
        char[] wordAsArray = word.toCharArray();
        int wordLength = wordAsArray.length;
        char firstLetter = wordAsArray[0];

        //O método devolve uma matriz com a lista de possíveis coordenadas para a primeira letra da palavra
        int[][] firstLetterPosition = findWhereFirstLetterAppears(firstLetter, letterMatrix, -1, -1);

        //Enquanto a matriz de coordenadas da primeira letra não for nulo e as coordenadas da letra forem maiores ou iguais a zero
        while (firstLetterPosition != null && firstLetterPosition[0][0] >= 0 && firstLetterPosition[0][1] >= 0) {
            //O método calcula as coordenadas possíveis da última letra da palavra, tendo em conta as direções
            int[][] lastLetterPosition = calculatePossibleLastLetterCoordinates(letterMatrix, firstLetterPosition[0][0], firstLetterPosition[0][1], wordLength);

            for (int i = 0; i < lastLetterPosition.length; i++) {
                //Se as coordenadas da última letra forem maiores que -1 (ou seja, a última letra é válida e corresponde à última
                // letra da palavra inserida pelo utilizador) e se a palavra do utilizador corresponder a uma palavra da lista, retorna true
                if (lastLetterPosition[i][0] > -1 && lastLetterPosition[i][1] > -1) {
                    if (checkWordBetweenFirstAndLastLetters(word, letterMatrix, firstLetterPosition[0][1], firstLetterPosition[0][0], lastLetterPosition[i][1], lastLetterPosition[i][0])) {
                        return true;
                    }
                }
            }
            //Se a última letra da palavra não corresponder, ou se a palavra não corresponder, procura-se a próxima posição da primeira letra
            firstLetterPosition = findWhereFirstLetterAppears(firstLetter, letterMatrix, firstLetterPosition[0][0], firstLetterPosition[0][1] + 1);
        }
        return false;
    }

    //O método retorna uma matriz que contém as coordenadas de todas as posições onde se encontra a primeira letra da palavra inserida pelo utilizador
    public static int[][] findWhereFirstLetterAppears(char firstLetter, char[][] letterMatrix, int rowInitialIndex, int columnInitialIndex) {
        //Se o index da coluna da possível primeira letra da palavra for maior ou igual que o número de colunas da matriz, torna-se zero
        if (columnInitialIndex >= letterMatrix[0].length) {
            columnInitialIndex = 0;
            rowInitialIndex++;
        }
        //Se o index da linha ou o index da coluna onde está posicionada a possível primeira letra for inferior a zero, tornam-se zero
        if (rowInitialIndex < 0) {
            rowInitialIndex = 0;
        }
        if (columnInitialIndex < 0) {
            columnInitialIndex = 0;
        }
        //Se a primeira letra da palavra corresponder a uma letra da matriz, guardam-se essas coordenadas
        for (int row = rowInitialIndex; row < letterMatrix.length; row++) {
            for (int column = columnInitialIndex; column < letterMatrix[0].length; column++) {
                if (letterMatrix[row][column] == firstLetter) {
                    return new int[][]{{row, column}};
                }
            }
            columnInitialIndex = 0;
        }
        return null;
    }

    public static int[][] calculatePossibleLastLetterCoordinates(char[][] letterMatrix, int rowInitial, int columnInitial, int wordLength) {
        int[][] result = {{-1, -1}, {-1, -1}, {-1, -1}, {-1, -1}, {-1, -1}, {-1, -1}, {-1, -1}, {-1, -1}};

        wordLength = wordLength - 1;

        int matrixLength = letterMatrix.length;
        int matrixHeight = letterMatrix.length;

        //Validações para que a posição da primeira letra da palavra mais o comprimento da mesma não seja maior que o tamanho da matriz
        boolean rowMinusWordLengthInside = rowInitial - wordLength >= 0;
        boolean rowPlusWordLengthInside = rowInitial + wordLength < matrixLength;
        boolean columnMinusWordLengthInside = columnInitial - wordLength >= 0;
        boolean columnPlusWordLengthInside = columnInitial + wordLength < matrixHeight;

        //Esquerda para a direita
        if (columnPlusWordLengthInside) {
            result[0][0] = rowInitial;
            result[0][1] = columnInitial + wordLength;
        }

        //Direita para a esquerda
        if (columnMinusWordLengthInside) {
            result[1][0] = rowInitial;
            result[1][1] = columnInitial - wordLength;
        }

        //Cima para baixo
        if (rowPlusWordLengthInside) {
            result[2][0] = rowInitial + wordLength;
            result[2][1] = columnInitial;
        }

        //Baixo para cima
        if (rowMinusWordLengthInside) {
            result[3][0] = rowInitial - wordLength;
            result[3][1] = columnInitial;
        }

        //Diagonal 1º quadrante
        if (columnPlusWordLengthInside && rowMinusWordLengthInside) {
            result[4][0] = rowInitial - wordLength;
            result[4][1] = columnInitial + wordLength;
        }

        //Diagonal 2º quadrante
        if (columnMinusWordLengthInside && rowMinusWordLengthInside) {
            result[5][0] = rowInitial - wordLength;
            result[5][1] = columnInitial - wordLength;
        }
        //Diagonal 3º quadrante
        if (columnMinusWordLengthInside && rowPlusWordLengthInside) {
            result[6][0] = rowInitial + wordLength;
            result[6][1] = columnInitial - wordLength;
        }

        //Diagonal 4º quadrante
        if (columnPlusWordLengthInside && rowPlusWordLengthInside) {
            result[7][0] = rowInitial + wordLength;
            result[7][1] = columnInitial + wordLength;
        }

        return result;
    }

    private static boolean checkWordBetweenFirstAndLastLetters(String word, char[][] letterMatrix, int columnInitialPosition, int rowInitialPosition, int columnFinalPosition, int rowFinalPosition) {
        //Condições do posicionamento da primeira e última letras da palavra, de acordo com as direções
        boolean horizontalReverse = rowInitialPosition > rowFinalPosition;
        boolean verticalReverse = columnInitialPosition > columnFinalPosition;
        boolean horizontal = columnInitialPosition == columnFinalPosition;
        boolean vertical = rowInitialPosition == rowFinalPosition;

        String finalWord = "";

        //O ciclo vai construindo uma palavra com o mesmo tamanho da palavra inserida pelo utilizador, usando as letras nas
        // diferentes posições, e ver se corresponde
        for (int i = 0; i < word.length(); i++) {
            if (horizontal) {
                if (!horizontalReverse) {
                    finalWord += letterMatrix[rowInitialPosition + i][columnInitialPosition];
                } else {
                    finalWord = letterMatrix[rowFinalPosition + i][columnInitialPosition] + finalWord;
                }
            } else if (vertical) {
                if (!verticalReverse) {
                    finalWord += letterMatrix[rowInitialPosition][columnInitialPosition + i];
                } else {
                    finalWord = letterMatrix[rowInitialPosition][columnFinalPosition + i] + finalWord;
                }
            } else {
                if (!horizontalReverse) {
                    if (!verticalReverse) {
                        finalWord += letterMatrix[rowInitialPosition + i][columnInitialPosition + i];
                    } else {
                        finalWord += letterMatrix[rowInitialPosition + i][columnInitialPosition - i];
                    }
                } else {
                    if (!verticalReverse) {
                        finalWord = letterMatrix[rowFinalPosition + i][columnFinalPosition - i] + finalWord;
                    } else {
                        finalWord = letterMatrix[rowFinalPosition + i][columnFinalPosition + i] + finalWord;
                    }
                }
            }
        }

        return finalWord.equals(word);
    }

    private static Boolean wordExistsInArray(String[] wordsArray, String word) {
        //Verifica se a palavra já foi encontrada
        for (int i = 0; i < wordsArray.length; i++) {
            if (wordsArray[i] != null && wordsArray[i].equals(word)) {
                return true;
            }
        }
        return false;
    }
}










