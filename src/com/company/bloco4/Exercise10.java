package com.company.bloco4;

import java.util.Arrays;

import static com.company.bloco4.MethodsLibrary.*;

public class Exercise10 {

    public static void main(String[] args) {

        //10 a)
        int[] numberArray = {1234567880};
        getSmallestElement(numberArray);
        //10 b)
        getBiggestElement(numberArray);
        //10 c)
        getElementsMean(numberArray);
        //10 d)
        getElementsProduct(numberArray);
        //10 e)
        getUniqueElements(numberArray);
        //10 f)
        getReversed(numberArray);
    }

//-----Exercise 10 a) --------------------------------------------------------------------------------------------------

    /**
     * A função obtém o valor mais baixo de um array
     *
     * @param numberArray
     * @return número correspondente ao valor mais baixo do array
     */
    public static Integer getSmallestElement(int[] numberArray) {
        //Verifica se o array é vazio e, se for, retorna um null
        if (verifyIfArrayIsEmpty(numberArray)) {
            return null;
        }

        //Iguala o elemento de menor valor ao primeiro elemento do array
        int smallestElement = numberArray[0];

        //O ciclo percorre o array para encontrar o menor valor. Sempre que encontra um valor menor que o anterior, guarda-o em "smallestElement"
        for (int i = 0; i < numberArray.length; i++) {
            if (numberArray[i] < smallestElement) {
                smallestElement = numberArray[i];
            }
        }
        return smallestElement;
    }

//-----Exercise 10 b) --------------------------------------------------------------------------------------------------

    /**
     * A função obtém o valor mais alto de um array
     *
     * @param numberArray
     * @return número correspondente ao valor mais alto do array
     */
    public static Integer getBiggestElement(int[] numberArray) {
        //Verifica se o array é vazio e, se for, retorna um null
        if (verifyIfArrayIsEmpty(numberArray)) {
            return null;
        }
        //Iguala o elemento de maior valor ao primeiro elemento do array
        int biggestElement = numberArray[0];

        //O ciclo percorre o array para encontrar o maior valor. Sempre que encontra um valor maior que o anterior, guarda-o em "biggestElement"
        for (int i = 0; i < numberArray.length; i++) {
            if (numberArray[i] > biggestElement) {
                biggestElement = numberArray[i];
            }
        }
        return biggestElement;
    }

//-----Exercise 10 c) --------------------------------------------------------------------------------------------------

    /**
     * A função obtém a média dos valores de um array
     *
     * @param numberArray
     * @return média dos valores do array, arredondado às centésimas, em formato String
     */
    public static String getElementsMean(int[] numberArray) {
        //Verifica se o array é vazio e, se for, retorna um null
        if (verifyIfArrayIsEmpty(numberArray)) {
            return null;
        }
        //O método obtém a soma de todos os elementos do array; guarda-se esse valor numa variável
        double elementsSum = addArrayElements(numberArray);

        //Cálculo da média
        double elementsMean = elementsSum / numberArray.length;

        return String.format("%.2f", elementsMean);
    }

//-----Exercise 10 d) --------------------------------------------------------------------------------------------------

    /**
     * A função obtém o produto entre os elementos de um array
     *
     * @param numberArray
     * @return número correspondente ao produto de todos os elementos do array
     */
    public static Integer getElementsProduct(int[] numberArray) {
        //Verifica se o array é vazio e, se for, retorna null
        if (verifyIfArrayIsEmpty(numberArray)) {
            return null;
        }

        int elementsProduct = 1;
        //O ciclo percorre o array, multiplicando cada valor ao anterior
        for (int i = 0; i < numberArray.length; i++) {
            elementsProduct = elementsProduct * numberArray[i];
        }

        return elementsProduct;
    }

//-----Exercise 10 e) --------------------------------------------------------------------------------------------------

    /**
     * O método obtém os números de um array, retirando os repetidos
     *
     * @param numberArray Array com a sequência de números original
     * @return Array com números únicos
     */

    public static int[] getUniqueElements(int[] numberArray) {
        //Verifica se o array é vazio e, se for, retorna null
        if (verifyIfArrayIsEmpty(numberArray)) {
            return null;
        }
        //O método organiza os valores do array por ordem crescente
        Arrays.sort((numberArray));

        //O ciclo percorre o array e faz a contagem dos elementos que não estão repetidos, verificando se cada elemento é diferente do anterior
        int uniqueElementsCounter = countNonRepeatingElementsFromSortedArray(numberArray);

        int[] uniqueElementsArray = new int[uniqueElementsCounter];
        uniqueElementsArray[0] = numberArray[0];
        int k = 1;

        //O ciclo percorre o array e copia os elementos únicos para um novo array
        for (int i = 1; i < numberArray.length; i++) {
            if (numberArray[i] != numberArray[i - 1]) {
                uniqueElementsArray[k] = numberArray[i];
                k++;
            }
        }

        return uniqueElementsArray;

    }


//-----Exercise 10 f) --------------------------------------------------------------------------------------------------

    /**
     * Este método obtém os elementos de um array e ordena-os numa ordem inversa
     *
     * @param numberArray Array com a sequência de números original
     * @return Array com números com ordem inversa
     */
    public static int[] getReversed(int[] numberArray) {
        //Verifica se o array é vazio e, se for, retorna null
        if (verifyIfArrayIsEmpty(numberArray)) {
            return null;
        }

        int[] reversedArray = new int[numberArray.length];
        //O ciclo percorre o array original e copia os seus elementos para outro array, numa ordem inversa
        for (int i = 0, j = (numberArray.length - 1); i < numberArray.length; i++) {
            reversedArray[j] = numberArray[i];
            j--;
        }

        return reversedArray;
    }

//-----Exercise 10 g) --------------------------------------------------------------------------------------------------

    /**
     * Este método obtém os elementos primos de um array
     *
     * @param numberArray Array com a sequência de números original
     * @return Array com apenas números primos
     */
    public static int[] getPrimeElements(int[] numberArray) {
        //Verifica se o array é vazio e, se for, retorna null
        if (verifyIfArrayIsEmpty(numberArray)) {
            return null;
        }

        int primeElementsCounter = 0;

        //O ciclo percorre cada linha da matriz
        for (int i = 0; i < numberArray.length; i++) {

            if (isNumberPrime(numberArray[i])) {
                primeElementsCounter++;
            }
        }
        int[] primeElements = new int[primeElementsCounter];
        int k = 0;

        //O ciclo percorre o array de números inicial
        for (int i = 0; i < numberArray.length; i++) {

            if (isNumberPrime(numberArray[i])) {
                primeElements[k] = numberArray[i];
                k++;
            }
        }
        return primeElements;
    }
}














