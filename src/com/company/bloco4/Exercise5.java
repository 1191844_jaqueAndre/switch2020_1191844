package com.company.bloco4;

import static com.company.bloco4.MethodsLibrary.*;

public class Exercise5 {

    public static void main(String[] args) {

        int number = 36781;
        addEvenNumbers(number);
        addOddNumbers(number);
    }

    /**
     * A função obtém a soma dos dígitos pares de um número positivo
     * @param number número inteiro positivo
     * @return soma dos dígitos pares de um número positivo
     */
    public static Integer addEvenNumbers(int number) {

        //Verifica se o número é positivo e, se não for, retorna null
        if (!MethodsLibrary.verifyIfIntIsPositive(number)){
            return null;
        }

        int digit = 0;
        int sumEvenNumberElements = 0;

        //O ciclo obtém cada dígito do número, somando-o ao anterior se for par
        while (number != 0) {
            digit = number % 10;
            number = number / 10;

            if (isDivisibleByNumber(digit, 2)) {
                sumEvenNumberElements = sumEvenNumberElements + digit;
            }
        }
        return sumEvenNumberElements;
    }

    /**
     * A função obtém a soma dos dígitos ímpares de um número positivo
     * @param number número inteiro positivo
     * @return soma dos dígitos ímpares de um número positivo
     */
    public static Integer addOddNumbers(int number) {

        //Verifica se o número é positivo e, se não for, retorna null
        if (!MethodsLibrary.verifyIfIntIsPositive(number)){
            return null;
        }

        int digit;
        int sumOddNumberElements = 0;

        //O ciclo obtém cada dígito do número, somando-o ao anterior se for ímpar
        while (number != 0) {
            digit = number % 10;
            number = number / 10;

            if (!isDivisibleByNumber(digit, 2)) {
                sumOddNumberElements = sumOddNumberElements + digit;
            }
        }
        return sumOddNumberElements;
    }
}
