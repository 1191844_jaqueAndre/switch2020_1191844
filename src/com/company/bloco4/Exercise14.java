package com.company.bloco4;

import static com.company.bloco4.MethodsLibrary.verifyIfAllLinesHaveSameColumns;
import static com.company.bloco4.MethodsLibrary.verifyIfMatrixLineIsEmpty;

public class Exercise14 {
    public static void main(String[] args) {

    }

    /**
     * A função verifica se a matriz é retangular
     * @param numberMatrix matriz de números inteiros
     * @return booleano true se a matriz for retangular
     */
    public static Boolean verifyIfMatrixIsRectangular (int[][] numberMatrix){

        //Verifica se alguma das linhas da matriz está vazia
        if(verifyIfMatrixLineIsEmpty(numberMatrix)){
            return null;
        }

        //Conta o número de linhas da matriz
        int numberOfLines = numberMatrix.length;
        //Verifica se todas as linhas têm o mesmo número de colunas e, se sim, conta o número de colunas
        int numberOfColumns = verifyIfAllLinesHaveSameColumns(numberMatrix);

        //Se não tiver o mesmo número de colunas, a matriz não é retangular
        if(verifyIfAllLinesHaveSameColumns(numberMatrix) == -1){
            return false;
        }
        //Se o número de colunas for superior ao de linhas ou vice versa, a matriz é retangular
        if (numberOfColumns > numberOfLines || numberOfColumns < numberOfLines) {
            return true;
        }
        //Se a matriz for quadrada, retorna falso
        return false;
    }
}
