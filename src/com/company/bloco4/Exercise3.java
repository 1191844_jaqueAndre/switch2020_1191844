package com.company.bloco4;

import static com.company.bloco4.MethodsLibrary.addArrayElements;
import static com.company.bloco4.MethodsLibrary.verifyIfArrayIsEmpty;

public class Exercise3 {

    public static void main(String[] args) {

        int[] numberArray = new int[]{3, 6, 7, 8, 1};
        addArrayElements(numberArray);
    }

    /**
     * A função obtém a soma dos valores de um array de números inteiros
     * @param numberArray vetor de números inteiros
     * @return número inteiro que corresponde à soma dos valores do array
     */
    public static Integer addVectorElements (int[] numberArray){

        //Verifica se o array não está vazio e retorna um null se estiver
        if(verifyIfArrayIsEmpty(numberArray)){
            return null;
        }

        //O método addArrayElements vai somar todos os elementos de um array "numberArray"
        return addArrayElements (numberArray);
    }


}
