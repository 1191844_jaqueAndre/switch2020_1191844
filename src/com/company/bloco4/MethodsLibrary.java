package com.company.bloco4;

import java.util.Random;

public class MethodsLibrary {

    /**
     * Esta condição verifica se a variável "number" é positiva ou não
     *
     * @param number número inteiro
     * @return booleano true se o número for positivo ou false se for negativo ou zero
     */
    public static boolean verifyIfIntIsPositive(int number) {
        return number > 0;
    }
//----------------------------------------------------------------------------------------------------------------------

    /**
     * Obtém o número de dígitos da variável "number"
     *
     * @param number número inteiro
     * @return número de dígitos que compõe o número inteiro
     */
    public static int getDigitsCountFromNumber(int number) {
        int digitCounter = 0;

        if(number == 0){
            return 1;
        }
        while (number != 0) {
            number = number / 10;
            digitCounter++;
        }
        return digitCounter;
    }
//----------------------------------------------------------------------------------------------------------------------

    /**
     * O método vai colocar os dígitos de um int "number" num array, ordenados da mesma forma
     *
     * @param number       número inteiro
     * @param digitCounter corresponde ao tamanho do array final
     * @return array digits
     */
    public static int[] placeNumberDigitsInArray(int number, int digitCounter) {
        int[] digits = new int[digitCounter];
        int digit;
        for (int i = (digitCounter - 1); i >= 0; i--) {
            digit = number % 10;
            digits[i] = digit;
            number = number / 10;
        }
        return digits;
    }
//----------------------------------------------------------------------------------------------------------------------

    /**
     * O método addArrayElements vai somar todos os elementos de um array "numberArray"
     *
     * @param numberArray vetor de números inteiros
     * @return soma de todos os elementos do vetor
     */
    public static int addArrayElements(int[] numberArray) {

        int addArrayNumbers = 0;

        for (int i = 0; i < numberArray.length; i++) {
            addArrayNumbers = addArrayNumbers + numberArray[i];
        }

        return addArrayNumbers;
    }
//----------------------------------------------------------------------------------------------------------------------

    /**
     * O método verifica se o array está vazio
     *
     * @param numberArray vetor de números inteiros
     * @return true, se o vetor estiver vazio, false se não for vazio
     */
    public static boolean verifyIfArrayIsEmpty(int[] numberArray) {
        return numberArray.length == 0;
    }
//----------------------------------------------------------------------------------------------------------------------

    /**
     * O método verifica se o valor "value" é divisível por um número "n"
     *
     * @param value número que será dividido por "n"
     * @param n     possível divisor de "value"
     * @return booleano true, se "value" for divisível por "n", ou falso se não for
     */
    public static boolean isDivisibleByNumber(int value, int n) {
        return value % n == 0;
    }

//----------------------------------------------------------------------------------------------------------------------

    /**
     * O método faz a contagem dos múltiplos de um número inteiro "n" num dado vetor
     *
     * @param numberArray vetor que contém ou não os múltiplos de n
     * @param n           número inteiro que corresponde ao divisor
     * @return número inteiro que corresponde à contagem dos múltiplos de n
     */
    public static int countMultiplesOfN(int[] numberArray, int n) {
        int multiplesOfNCounter = 0;

        for (int i = 0; i < numberArray.length; i++) {
            if (isDivisibleByNumber(numberArray[i], n)) {
                multiplesOfNCounter++;
            }
        }
        return multiplesOfNCounter;
    }
//----------------------------------------------------------------------------------------------------------------------

    /**
     * O método copia os múltiplos de um número "n" de um dado vetor "number array" e retorna-os noutro vetor
     *
     * @param n                   número divisor
     * @param numberArray         vetor que contém os valores de onde serão copiados os múltiplos de "n"
     * @param multiplesOfNCounter contagem dos números múltiplos de "n", que será o tamanho do vetor de destino
     * @return array com os valores múltiplos de "n"
     */
    public static int[] getMultiplesOfNFromAnArray(int n, int[] numberArray, int multiplesOfNCounter) {
        int[] multiplesOfN = new int[multiplesOfNCounter];
        for (int i = 0, j = 0; i < numberArray.length; i++) {
            if (isDivisibleByNumber(numberArray[i], n)) {
                multiplesOfN[j] = numberArray[i];
                j++;
            }
        }
        return multiplesOfN;
    }
//----------------------------------------------------------------------------------------------------------------------

    /**
     * O método verifica se o valor de fim é maior que o valor de início do intervalo
     *
     * @param rangeStart valor de início do intervalo
     * @param rangeEnd   valor de fim do intervalo
     * @return booleano true, se o valor de fim for maior ou false, se for menor
     */
    public static boolean isRangeEndBiggerThanRangeStart(int rangeStart, int rangeEnd) {
        return rangeEnd > rangeStart;
    }
//----------------------------------------------------------------------------------------------------------------------

    /**
     * O método verifica se dois arrays são do mesmo tamanho. Se não forem, o array maior ficará com o tamanho do mais
     * pequeno
     *
     * @param array1
     * @param array2
     * @return array truncado
     */
    public static int[] getSameLengthArraysTruncate(int[] array1, int[] array2) {
        if (array2.length < array1.length) {
            int[] vector1Copy = new int[array2.length];
            System.arraycopy(array1, 0, vector1Copy, 0, array2.length);
            array1 = vector1Copy;
        }
        return array1;
    }
//----------------------------------------------------------------------------------------------------------------------

    /**
     * A função verifica se todas as linhas da matriz têm o mesmo número de colunas
     *
     * @param numberMatrix matriz de números inteiros
     * @return -1 se existir uma ou mais linhas com número de colunas diferente, ou o número de colunas, se as linhas
     * tiverem todas o mesmo número
     */
    public static int verifyIfAllLinesHaveSameColumns(int[][] numberMatrix) {

        int columnsNumber = numberMatrix[0].length;

        //O ciclo percorre cada linha da matriz e procura o número de colunas em cada uma, comparando-a à anterior.
        // Se o número de colunas da linha atual for diferente da anterior, retorna -1
        for (int i = 1; i < numberMatrix.length; i++) {
            columnsNumber = numberMatrix[i].length;
            if (columnsNumber != numberMatrix[i - 1].length) {
                return -1;
            }
        }
        return numberMatrix[0].length;
    }
//----------------------------------------------------------------------------------------------------------------------

    /**
     * O método verifica se alguma linha de uma matriz está vazia
     *
     * @param numberMatrix matriz de números inteiros
     * @return true, se alguma linha da matriz está vazia, false se nenhuma for vazia
     */
    public static boolean verifyIfMatrixLineIsEmpty(int[][] numberMatrix) {
        //O ciclo percorre cada linha da matriz e, se for vazia, retorna true
        for (int i = 0; i < numberMatrix.length; i++) {
            if (numberMatrix[i].length == 0) {
                return true;
            }
        }
        return false;
    }

//----------------------------------------------------------------------------------------------------------------------

    /**
     * O método obtém o número de elementos de uma matriz
     *
     * @param numberMatrix matriz de números
     * @return número de elementos de uma matriz
     */
    public static int countNrElements(int[][] numberMatrix) {
        int elementCounter = 0;
        for (int i = 0; i < numberMatrix.length; i++) {
            //O ciclo percorre cada coluna para contar o número de elementos
            for (int j = 0; j < numberMatrix[i].length; j++) {
                elementCounter++;
            }
        }
        return elementCounter;
    }
//----------------------------------------------------------------------------------------------------------------------

    /**
     * O método converte uma matriz para um array
     *
     * @param numberMatrix matriz de números
     * @param matrixSize   número de elementos da matriz
     * @return vetor com todos os elementos da matriz
     */
    public static int[] convertMatrixToArray(int[][] numberMatrix, int matrixSize) {

        int[] matrixConvertedToArray = new int[matrixSize];
        //O ciclo percorre cada linha
        for (int i = 0, k = 0; i < numberMatrix.length; i++) {
            //O ciclo percorre cada coluna e copia cada elemento para o array
            for (int j = 0; j < numberMatrix[i].length; j++) {
                matrixConvertedToArray[k] = numberMatrix[i][j];
                k++;
            }
        }
        return matrixConvertedToArray;
    }
//----------------------------------------------------------------------------------------------------------------------

    /**
     * O método conta os elementos diferentes entre si existentes num array ordenado crescentemente
     *
     * @param matrixConvertedToArray vetor de números
     * @return quantidade de números diferentes entre si
     */
    public static int countNonRepeatingElementsFromSortedArray(int[] matrixConvertedToArray) {
        //O contador é inicializado a um, para contar com o primeiro elemento do array
        int nonRepeatingElementsCounter = 1;
        //O ciclo percorre o array, comparando um elemento com o anterior. Se forem diferentes, o contador incrementa
        for (int i = 1; i < matrixConvertedToArray.length; i++) {
            if (matrixConvertedToArray[i] != matrixConvertedToArray[i - 1]) {
                nonRepeatingElementsCounter++;
            }
        }
        return nonRepeatingElementsCounter;
    }

//----------------------------------------------------------------------------------------------------------------------

    /**
     * O método verifica se um número é primo
     *
     * @param number número a verificar
     * @return booleano true, se for número primo
     */
    public static boolean isNumberPrime(int number) {
        int divisorsCounter = 0;
        boolean primeNumber = false;
        for (int k = 1; k <= (number / 2); k++) {
            if (number % k == 0) {
                divisorsCounter++;
            }
        }
        //O número 1 será sempre o divisor de um número primo. Se o número de divisores do valor for um, então esse valor
        // é um número primo
        if (divisorsCounter == 1) {
            primeNumber = true;
        }
        return primeNumber;
    }

//----------------------------------------------------------------------------------------------------------------------

    /**
     * O método conta os números primos de uma matriz
     *
     * @param numberMatrix matriz de números
     * @return contagem dos números primos
     */
    public static Integer countPrimeElementsFromMatrix(int[][] numberMatrix) {
        int primeElementsCounter = 0;
        //O ciclo percorre cada linha da matrix
        for (int i = 0; i < numberMatrix.length; i++) {
            //O ciclo percorre cada valor da linha
            for (int j = 0; j < numberMatrix[i].length; j++) {
                if (isNumberPrime(numberMatrix[i][j])) {
                    primeElementsCounter++;
                }
            }
        }
        return primeElementsCounter;
    }

//----------------------------------------------------------------------------------------------------------------------

    /**
     * O método conta o número de elementos da diagonal principal de uma matriz
     *
     * @param numberMatrix matriz de números
     * @return número de elementos da diagonal principal
     */
    public static int countNumberOfMainDiagonalElements(int[][] numberMatrix) {
        int countNumberElements = 0, verticalPosition = 0, horizontalPosition = 0;
        while (horizontalPosition < numberMatrix[0].length && verticalPosition < numberMatrix.length) {
            verticalPosition++;
            horizontalPosition++;
            countNumberElements++;
        }
        return countNumberElements;
    }

//----------------------------------------------------------------------------------------------------------------------

    /**
     * O método verifica se uma matriz quadrada é matriz identidade
     *
     * @param numberMatrix matrix de números
     * @return booleano true, se for matriz identidade ou false, se não for
     */
    public static boolean isIdentityMatrix(int[][] numberMatrix) {

        int horizontalPosition = 0, verticalPosition = 0, identityElementCounter = 0, zeroElementCounter = 0;
        boolean identityMatrix = false;

        //O ciclo conta o número de elementos da diagonal principal
        int countNumberMainDiagonalElements = countNumberOfMainDiagonalElements(numberMatrix);

        //O ciclo conta o número de valores igual a um da diagonal principal e os elementos iguais a zero fora da diagonal
        for (verticalPosition = 0; verticalPosition < numberMatrix.length; verticalPosition++) {
            for (horizontalPosition = 0; horizontalPosition < numberMatrix[0].length; horizontalPosition++) {
                if (numberMatrix[verticalPosition][horizontalPosition] == 1 && verticalPosition == horizontalPosition) {
                    identityElementCounter++;
                } else if (numberMatrix[verticalPosition][horizontalPosition] == 0 && verticalPosition != horizontalPosition) {
                    zeroElementCounter++;
                }
            }
        }
        //Calcula-se o número de elementos da matriz
        int totalNrElements = countNrElements(numberMatrix);

        //Se o número total de elementos da diagonal for igual ao número de elementos da diagonal que são iguais a um e se
        // o número de elementos iguais a zero for igual ao número de elementos da matriz menos os elementos da diagonal principal, então a matriz é uma matriz identidade
        if (identityElementCounter == countNumberMainDiagonalElements && zeroElementCounter == (totalNrElements - countNumberMainDiagonalElements)) {
            identityMatrix = true;
        }
        return identityMatrix;
    }

//----------------------------------------------------------------------------------------------------------------------

    /**
     * O método cria uma cópia da matriz quadrada original, sem a coluna especificada por horizontalPosition e a linha
     * especificada por verticalPosition
     *
     * @param numberMatrix       matriz de números
     * @param horizontalPosition coordenada que indica a coluna a remover
     * @param verticalPosition   coordenada que indica a linha a remover
     * @return matriz n-1
     */
    public static int[][] getNMinusOneMatrix(int[][] numberMatrix, int horizontalPosition, int verticalPosition) {
        //Cálculo do número de linhas e colunas da nova matriz
        int nrColumns = (numberMatrix[0].length - 1);
        int nrRows = (numberMatrix.length - 1);
        int[][] temporaryMatrix = new int[nrRows][nrColumns];
        //O ciclo percorre cada linha e coluna da matriz original
        for (int i = 0, k = 0; i < numberMatrix.length; i++) {
            for (int j = 0, l = 0; j < numberMatrix[0].length; j++) {
                //Copia-se os elementos da matriz original para uma nova matriz, ignorando os elementos que pertencem à linha i e coluna j
                if (j != horizontalPosition && i != verticalPosition) {
                    temporaryMatrix[k][l] = numberMatrix[i][j];
                    l++;
                    if (l == (nrColumns)) {
                        l = 0;
                        k++;
                    }
                }
            }
        }
        numberMatrix = temporaryMatrix;
        return numberMatrix;

    }
//----------------------------------------------------------------------------------------------------------------------

    /**
     * O método obtém o determinante de uma matriz quadrada, usando a lei de Laplace
     *
     * @param numberMatrix matriz de números
     * @return número correspondente ao determinante da matriz
     */
    public static int getDeterminantLaplace(int[][] numberMatrix) {

        int determinant = 0;
        //Se a matriz só tiver um elemento, o próprio é o determinante
        if (numberMatrix.length == 1) {
            determinant = numberMatrix[0][0];
        }
        //Fórmula do determinante de uma matriz, usando sempre a primeira linha para o cálculo e usando recursividade para calcular as matrizes menores
        for (int j = 0; j < numberMatrix.length; j++) {
            determinant = (int) (determinant + Math.pow(-1, j) * numberMatrix[0][j] * getDeterminantLaplace(getNMinusOneMatrix(numberMatrix, j, 0)));
        }
        return determinant;
    }

//----------------------------------------------------------------------------------------------------------------------

    /**
     * O método obtém o cofator de um determinado elemento de uma matriz
     *
     * @param numberMatrix       matriz de números inteiros
     * @param horizontalPosition posição horizontal do elemento
     * @param verticalPosition   posição vertical do elemento
     * @return cofator do elemento
     */
    public static double getElementCofactor(int[][] numberMatrix, int horizontalPosition, int verticalPosition) {

        //Cálculo do determinante da matriz menor (matriz original menos a linha e coluna representados por vertical e horizontal positions)
        int numberDeterminant = getDeterminantLaplace(getNMinusOneMatrix(numberMatrix, horizontalPosition, verticalPosition));
        //Cálculo do cofator
        double numberCofactor = Math.pow(-1, (verticalPosition + horizontalPosition)) * numberDeterminant;

        return (numberCofactor + 0.0);

    }

//----------------------------------------------------------------------------------------------------------------------

    /**
     * A função verifica se todas as linhas da matriz têm o mesmo número de colunas
     *
     * @param numberMatrix matriz de números reais
     * @return -1 se existir uma ou mais linhas com número de colunas diferente, ou o número de colunas, se as linhas
     * tiverem todas o mesmo número
     */
    public static int verifyIfAllLinesHaveSameColumnsDouble(double[][] numberMatrix) {

        int columnsNumber = numberMatrix[0].length;

        //O ciclo percorre cada linha da matriz e procura o número de colunas em cada uma, comparando-a à anterior.
        // Se o número de colunas da linha atual for diferente da anterior, retorna -1
        for (int i = 1; i < numberMatrix.length; i++) {
            columnsNumber = numberMatrix[i].length;
            if (columnsNumber != numberMatrix[i - 1].length) {
                return -1;
            }
        }
        return numberMatrix[0].length;
    }

//----------------------------------------------------------------------------------------------------------------------

    /**
     * A função determina de a matriz é ou não quadrada
     *
     * @param numberMatrix matriz de números reais
     * @return booleano verdadeiro se a matriz for quadrada
     */
    public static Boolean verifyIfMatrixIsSquareDouble(double[][] numberMatrix) {

        //Conta o número de linhas da matriz
        int numberOfLines = numberMatrix.length;
        //Verifica se todas as linhas têm o mesmo número de colunas e, se tiverem, retorna o número de colunas
        int numberOfColumns = verifyIfAllLinesHaveSameColumnsDouble(numberMatrix);
        //Se o número de colunas for igual ao número de linhas, a matriz é quadrada
        return numberOfColumns == numberOfLines;
    }
//----------------------------------------------------------------------------------------------------------------------

    /**
     * O método gera uma matriz quadrada de tamanho "size", com letras aleatórias
     *
     * @param gameSize tamanho da matriz quadrada
     * @return matriz de letras aleatórias
     */
    public static char[][] getRandomLetterMatrix(int gameSize) {
        Random randomNumber = new Random();
        //Guarda-se o alfabeto numa string para cada letra ser usada
        String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        char[][] letterMatrix = new char[gameSize][gameSize];
        //Gera uma matriz de letras aleatórias
        for (int i = 0; i < gameSize; i++) {
            for (int j = 0; j < gameSize; j++) {
                letterMatrix[i][j] = (alphabet.toCharArray()[randomNumber.nextInt(alphabet.toCharArray().length)]);
            }
        }
        return letterMatrix;
    }

//----------------------------------------------------------------------------------------------------------------------

    /**
     * Gera uma matriz máscara a partir de uma matriz de letras. A posição da letra selecionada corresponderá ao valor um na matriz máscara
     *
     * @param letterMatrix matriz de letras
     * @param letter       letra selecionada
     * @return matriz máscara
     */
    public static char[][] generateMaskMatrix(char[][] letterMatrix, char letter) {
        char[][] maskMatrix = new char[letterMatrix.length][letterMatrix[0].length];
        for (int i = 0; i < letterMatrix.length; i++) {
            for (int j = 0; j < letterMatrix[i].length; j++) {
                //Se os valores da matriz de letras forem iguais à letra selecionada, corresponderão a 1 na matriz máscara
                if (letterMatrix[i][j] == letter) {
                    maskMatrix[i][j] = 1;
                    //Senão, os valores corresponderão a 0
                } else {
                    maskMatrix[i][j] = 0;
                }
            }
        }
        return maskMatrix;
    }

//----------------------------------------------------------------------------------------------------------------------

    /**
     * O método imprime na consola uma matriz quadrada com o formato do jogo sopa de letras
     *
     * @param gameSize     Tamanho da matriz quadrada a imprimir
     * @param letterMatrix Matriz a imprimir
     */
    public static void printMatrixWordSearchGame(int gameSize, char[][] letterMatrix) {
        for (int i = 0; i < gameSize; i++) {
            for (int j = 0; j < gameSize; j++) {
                System.out.print("| " + letterMatrix[i][j] + " |");
            }
            System.out.println();
        }
    }

//----------------------------------------------------------------------------------------------------------------------

}

