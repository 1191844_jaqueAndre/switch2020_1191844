package com.company.bloco4;

import static com.company.bloco4.MethodsLibrary.verifyIfAllLinesHaveSameColumns;
import static com.company.bloco4.MethodsLibrary.verifyIfMatrixLineIsEmpty;

public class Exercise13 {
    public static void main(String[] args) {
        int[][] numberMatrix = { {1, 3, 4, 5}, {3, 4, 5, 5},{7, 8, 6, 5} };
        verifyIfMatrixIsSquare(numberMatrix);
    }

    /**
     * A função determina de a matriz é ou não quadrada
     * @param numberMatrix matriz de números inteiros
     * @return booleano verdadeiro se a matriz for quadrada
     */
    public static Boolean verifyIfMatrixIsSquare (int[][] numberMatrix){

        //Conta o número de linhas da matriz
        int numberOfLines = numberMatrix.length;
        //Verifica se todas as linhas têm o mesmo número de colunas e, se tiverem, retorna o número de colunas
        int numberOfColumns = verifyIfAllLinesHaveSameColumns(numberMatrix);
        //Se o número de colunas for igual ao número de linhas, a matriz é quadrada
        if (numberOfColumns == numberOfLines){
            return true;
        }
        return false;
    }
}
