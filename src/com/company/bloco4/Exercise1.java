package com.company.bloco4;

public class Exercise1 {

    public static void main(String[] args) {
        int number = 1234567890;
        returnNumberOfDigits(number);
    }

    /**
     * A função obtém o número de dígitos de um número inteiro positivo
     * @param number Número inteiro positivo
     * @return número de dígitos de um número inteiro positivo
     */
    public static Integer returnNumberOfDigits(int number){

        //Esta condição diz que se a variável "number" não for positiva, o return será null
        if (!MethodsLibrary.verifyIfIntIsPositive(number)){
            return null;
        }

        //Obtém o número de dígitos da variável "number"
        return MethodsLibrary.getDigitsCountFromNumber(number);
    }
    
}
