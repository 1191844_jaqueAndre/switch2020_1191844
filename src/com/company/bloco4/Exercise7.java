package com.company.bloco4;

import static com.company.bloco4.MethodsLibrary.*;

public class Exercise7 {

    public static void main(String[] args) {

        int rangeStart = 4;
        int rangeEnd = 10;
        int n = 3;
        getMultiplesOfN(rangeStart, rangeEnd, n);
    }

    /**
     * a função obtém todos os múltiplos de um número "n", num dado intervalo
     * @param rangeStart valor de início do intervalo
     * @param rangeEnd valor de fim do intervalo
     * @param n número divisor
     * @return array com os múltiplos de "n"
     */
    public static int[] getMultiplesOfN(int rangeStart, int rangeEnd, int n) {

        //verifica se o fim do intervalo é maior que o início e, se não for, troca os valores para que seja
        if (!isRangeEndBiggerThanRangeStart(rangeStart, rangeEnd)) {
            int temp = rangeStart;
            rangeStart = rangeEnd;
            rangeEnd = temp;
        }

        //Transformação do intervalo num array
        int[] numberArray = new int[(rangeEnd - rangeStart) + 1];

        for (int i = rangeStart, j = 0; i <= rangeEnd; i++){
            numberArray[j] = i;
            j++;
        }

        //A variável guarda a contagem do número de múltiplos de "n" existentes no array
        int multiplesOfNCounter = countMultiplesOfN(numberArray, n);

        //Retorna um array com os múltiplos de "n"
        return getMultiplesOfNFromAnArray(n, numberArray, multiplesOfNCounter);
    }



}
