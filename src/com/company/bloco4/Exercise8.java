package com.company.bloco4;

import static com.company.bloco4.MethodsLibrary.verifyIfArrayIsEmpty;

public class Exercise8 {

    public static void main(String[] args) {
        int rangeStart = 12;
        int rangeEnd = 4;
        int[] divisorsArray = new int[]{2, 3};
        getCommonMultiples(rangeStart, rangeEnd, divisorsArray);
    }
    /**
     * A função vai determinar os múltiplos comuns de vários números inteiros num dado intervalo
     *
     * @param tRangeStart   valor de início do intervalo
     * @param tRangeEnd     valor de fim do intervalo
     * @param divisorsArray conjunto dos divisores
     * @return array com os múltiplos comuns
     */
    public static int[] getCommonMultiples(int tRangeStart, int tRangeEnd, int[] divisorsArray) {

        //verifica se o fim do intervalo é maior que o início e, se não for, troca os valores para que seja
        int rangeStart = Math.min(tRangeStart, tRangeEnd);
        int rangeEnd = Math.max(tRangeStart, tRangeEnd);

        if (verifyIfArrayIsEmpty(divisorsArray)) {      //Verifica se o array é vazio e, se for, retorna um null
            return null;
        }

        int[] numberArray = new int[(rangeEnd - rangeStart) + 1];   //Transformação do intervalo num array
        for (int i = rangeStart, j = 0; i <= rangeEnd; i++) {
            numberArray[j] = i;
            j++;
        }

        int commonMultiplesCounter = 0;

        for (int i = 0; i < numberArray.length; i++) {      //O ciclo percorre o array dos números no intervalo

            int counterOfDivisorsPerRangeValue = 0;

            for (int j = 0; j < divisorsArray.length; j++) {        //O ciclo percorre o array dos divisores
                if (divisorsArray[j] == 0) {        //Se o valor do array dos divisores for zero, retorna null
                    return null;
                }
                if (numberArray[i] % divisorsArray[j] == 0) {   //Se o valor do intervalo dado é divisível pelo valor do array dos divisores,
                    counterOfDivisorsPerRangeValue++;           //o contador de números divisores do valor do intervalo dado incrementa
                }
            }
            if (counterOfDivisorsPerRangeValue == divisorsArray.length) {   //Se o número de números divisores desse valor do intervalo for
                commonMultiplesCounter++;                                   //igual ao número de valores do array de divisores (o seu
            }                                                               //comprimento), então é porque o valor do intervalo é um múltiplo comum
        }
        if (commonMultiplesCounter == 0) {       //Se não existirem múltiplos comuns para o valor do intervalo, retorna null
            return null;
        }

        int[] commonMultiples = new int[commonMultiplesCounter];    //Inicialização do array que vai guardar os múltiplos comuns
        int k = 0;

        for (int i = 0; i < numberArray.length; i++) {      //O ciclo vai percorrer o array dos valores do intervalo dado

            int counterOfMultiplesPerRangeValue = 0;

            for (int j = 0; j < divisorsArray.length; j++) {    //O ciclo vai percorrer o array dos divisores
                if (numberArray[i] % divisorsArray[j] == 0) {   //Se o valor do intervalo dado é divisível pelo valor do array dos divisores,
                    counterOfMultiplesPerRangeValue++;          //o contador de números divisores do valor do intervalo dado incrementa
                }
            }
            if (counterOfMultiplesPerRangeValue == divisorsArray.length) {  //Se o número de números divisores desse valor do intervalo for
                commonMultiples[k] = numberArray[i];                        //igual ao número de valores do array de divisores (o seu
                k++;                                                        //comprimento), então guarda-se esse valor no array de múltiplos comuns
            }
        }
        return commonMultiples;
    }
}

