package com.company.bloco4;

import java.util.Arrays;

public class Exercise2 {

    public static void main(String[] args) {
        int number = 36781;
        getNumberDigits(number);
    }

    /**
     * Esta função obtém os dígitos de um número inteiro positivo, guardando-os num array, com a mesma ordem.
     * @param number número inteiro positivo
     * @return array com os os dígitos que compõem o número inteiro
     */
    public static int[] getNumberDigits(int number) {


        //Esta condição diz que se a variável "number" não for positiva, o return será null
        if (!MethodsLibrary.verifyIfIntIsPositive(number)){
            return null;
        }

        //A variável digitCounter vai guardar a contagem de dígitos da variável "number"
        int digitCounter = MethodsLibrary.getDigitsCountFromNumber(number);

        int[] digits = new int[digitCounter];

        //O método vai colocar os dígitos do int "number" num array, ordenados da mesma forma
        return MethodsLibrary.placeNumberDigitsInArray(number, digitCounter);
    }


}
