package com.company.bloco4;

import com.sun.image.codec.jpeg.TruncatedFileException;

import static com.company.bloco4.MethodsLibrary.getSameLengthArraysTruncate;
import static com.company.bloco4.MethodsLibrary.verifyIfArrayIsEmpty;

public class Exercise11 {

    public static void main(String[] args) {

        int[] vector1 = new int[]{1, 2, 3, 4};
        int[] vector2 = new int[]{1, 2, 3, 4, 5};

    }

    /**
     * A função obtém o produto escalar de dois vetores, verificando que são do mesmo tamanho e não vazios
     * @param vector1
     * @param vector2
     * @return produto escalar dos dois vetores
     */
    public static Integer getDotProductOfTwoVectors(int[] vector1, int[] vector2) {

        //Verifica se o comprimento do vetor 1 é maior que o do vetor 2 e, se for, copia o vetor 1 para um novo array
        // do mesmo tamanho que o vetor 2.
        vector1 = getSameLengthArraysTruncate(vector1, vector2);
        //Verifica se o comprimento do vetor 2 é maior que o do vetor 1 e, se for, copia o vetor 2 para um novo array
        // do mesmo tamanho que o vetor 1.
        vector2 = getSameLengthArraysTruncate(vector2, vector1);

        //Verifica se algum dos arrays é vazio e, se sim, retorna null
        if (verifyIfArrayIsEmpty(vector1)||verifyIfArrayIsEmpty(vector2)) {
            return null;
        }

        int dotProduct = 0;
        int vectorPositionProduct = 1;

        //O cliclo percorre os dois vetores e multiplica os valores na mesma posição. A cada iteração soma-se esse
        // produto ao produto dos valores anteriores
        for (int i = 0; i < vector1.length; i++){
            vectorPositionProduct = vector1[i] * vector2[i];
            dotProduct = dotProduct + vectorPositionProduct;
        }
        return dotProduct;
    }


}
