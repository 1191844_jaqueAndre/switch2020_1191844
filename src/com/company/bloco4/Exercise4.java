package com.company.bloco4;

import static com.company.bloco4.MethodsLibrary.*;

public class Exercise4 {

    public static void main(String[] args) {
        int[] numberArray = {3, 6, 7, 8, 1};
        getEvenNumbersFromArray(numberArray);
        getOddNumbersFromArray(numberArray);
    }

    /**
     * A função verifica que o array não é vazio e copia os valores pares do mesmo para um novo array
     * @param numberArray array de origem
     * @return array com apenas os números pares do array original
     */
    public static int[] getEvenNumbersFromArray(int[] numberArray) {

        //Verifica se o array é vazio e, se for, retorna um null
        if (verifyIfArrayIsEmpty(numberArray)) {
            return null;
        }

        //O método faz a contagem de números divisíveis por dois
        int evenNumbersCounter = countMultiplesOfN(numberArray, 2);

        int[] evenNumbers = new int[evenNumbersCounter];

        int j = 0;

        //O ciclo for percorre os valores do array e copia os números pares para um novo array
        for (int i = 0; i < numberArray.length; i++) {
            if (isDivisibleByNumber(numberArray[i], 2)) {
                evenNumbers[j] = numberArray[i];
                j++;
            }
        }
        return evenNumbers;
    }


    /**
     * A função verifica que o array não é vazio e copia os valores ímpares do mesmo para um novo array
     * @param numberArray array de origem
     * @return array com apenas os números ímpares do array original
     */
    public static int[] getOddNumbersFromArray(int[] numberArray) {

        //Verifica se o array é vazio e, se for, retorna um null
        if (verifyIfArrayIsEmpty(numberArray)) {
            return null;
        }

        int oddNumbersCounter = 0;

        //O cliclo faz a contagem de números ímpares
        for (int i = 0; i < numberArray.length; i++) {
            if (!isDivisibleByNumber(numberArray[i], 2)) {
                oddNumbersCounter++;
            }
        }

        int[] oddNumbers = new int[oddNumbersCounter];
        int j = 0;

        //O ciclo for percorre os valores do array e copia os números ímpares para um novo array
        for (int i = 0; i < numberArray.length; i++) {
            if (!isDivisibleByNumber(numberArray[i], 2)) {
                oddNumbers[j] = numberArray[i];
                j++;
            }
        }
        return oddNumbers;
    }
}
