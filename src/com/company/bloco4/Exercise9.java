package com.company.bloco4;

import java.util.Arrays;

import static com.company.bloco4.MethodsLibrary.getDigitsCountFromNumber;
import static com.company.bloco4.MethodsLibrary.placeNumberDigitsInArray;

public class Exercise9 {

    public static void main(String[] args) {

    }

    /**
     * A função vai transformar um número positivo num array e comparar se esse array é igual am um array invertido
     * @param number número inteiro positivo
     * @return booleano true, se os arrays forem iguais, ou false, se não o forem
     */
    public static Boolean validateAndVerifyIfPalindrome(int number){

        //Verifica se o número é positivo e, se não for, retorna null
        if (!MethodsLibrary.verifyIfIntIsPositive(number)){
            return null;
        }

        return verifyIfPalindrome(number);

    }

    public static boolean verifyIfPalindrome (int number){
        //O método obtém a contagem de dígitos em "number" e o valor é guardado numa variável
        int digitCounter = getDigitsCountFromNumber(number);

        //O método cria um array com os dígitos de "number", ordenados da mesma forma
        int[] digits = placeNumberDigitsInArray(number, digitCounter);

        int[] reverseNumberArray = new int[digitCounter];
        int k = (digitCounter-1);

        //O ciclo vai atribuir os valores do array dos dígitos do número, de forma inversa
        for (int i = 0; i < digitCounter ; i++){
            reverseNumberArray[i] = digits[k];
            k--;
        }
        return Arrays.equals(digits, reverseNumberArray);
    }
}
