package com.company.bloco4;

import static com.company.bloco4.MethodsLibrary.verifyIfAllLinesHaveSameColumns;
import static com.company.bloco4.MethodsLibrary.verifyIfMatrixLineIsEmpty;

public class Exercise12 {

    public static void main(String[] args) {
        int[][] numberMatrix = { {1, 3, 4, 5}, {3, 4, 5},{7, 8, 6, 5} };
        verifyIfAllLinesHaveSameColumns(numberMatrix);
    }

    /**
     * A função verifica se todas as linhas da matriz têm o mesmo número de colunas
     * @param numberMatrix matriz de números inteiros
     * @return -1 se existir uma ou mais linhas com número de colunas diferente, ou o número de colunas, se as linhas
     * tiverem todas o mesmo número
     */

    public static Integer verifyIfSameNrColumns(int[][] numberMatrix){

        //Verifica se alguma das linhas da matriz está vazia
        if(verifyIfMatrixLineIsEmpty(numberMatrix)){
            return null;
        }
        //Verifica se todas as linhas da matriz têm o mesmo número de colunas
        return verifyIfAllLinesHaveSameColumns(numberMatrix);

    }

}
