package com.company.bloco4;

public class Exercise6 {

    public static void main(String[] args) {

        int[] numberArray = {3, 4, 6, 8, 1, 23, 89, 12};
        int size = 4;
        getArrayWithNElements(numberArray, size);

    }

    /**
     * Esta função copia os valores de um array "numberArray" para outro "nElementsArray" com um tamanho "size",
     * inferior ou igual ao tamanho de numberArray
     * @param numberArray array que pretendemos copiar
     * @param size tamanho do novo array, que terá de ser igual ou inferior ao tamanho do array original
     * @return array com tamanho "size"
     */
    public static int[] getArrayWithNElements(int[] numberArray, int size) {

        //verifica se o tamanho do novo array é igual ou inferior ao array original, retornando null se não for
        if (size > numberArray.length || size < 0) {
            return null;
        }

        int[] nElementsArray = new int[size];

        //copia os valores do array original para o novo array de tamanho "size"
        for (int i = 0; i < size; i++) {
            nElementsArray[i] = numberArray[i];
        }

        return nElementsArray;
    }

}
