package com.company.bloco4;

import static com.company.bloco4.Exercise13.verifyIfMatrixIsSquare;
import static com.company.bloco4.MethodsLibrary.getDeterminantLaplace;

public class Exercise16 {

    public static void main(String[] args) {

        int[][] numberMatrix = {{1, 13, 23}, {7, 5, 29}, {11, 19, 7}};
        getMatrixDeterminant(numberMatrix);
    }

    /**
     * O método obtém o determinante de uma matriz, se ela não for nula e for quadrada
     *
     * @param numberMatrix matriz de números
     * @return determinante da matriz
     */
    public static Double getMatrixDeterminant(int[][] numberMatrix) {

        //Verifica se a matriz é nula ou não quadrada
        if (numberMatrix == null || !verifyIfMatrixIsSquare(numberMatrix)) {
            return null;
        }
        //Se a matriz for só de um elemento, o próprio é o determinante
        if (numberMatrix.length == 1) {
            int determinant = numberMatrix[0][0];
            //Fórmula para o cálculo da matriz 2x2
        } else if (numberMatrix.length == 2) {
            int determinant = numberMatrix[0][0] * numberMatrix[1][1] - numberMatrix[0][1] * numberMatrix[1][0];
        }
        //Se a matriz for de ordem superior a 2, usa-se a lei de Laplace
        double determinant = getDeterminantLaplace(numberMatrix);

        return determinant;

    }
}
