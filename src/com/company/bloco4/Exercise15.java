package com.company.bloco4;

import java.util.Arrays;

import static com.company.bloco4.Exercise13.verifyIfMatrixIsSquare;
import static com.company.bloco4.MethodsLibrary.*;

public class Exercise15 {

    public static void main(String[] args) {

        int[][] numberMatrix = {{3, 0, 2}, {9, 1, 7}, {1, 0, 1}};
        //getSmallestValueElement(numberMatrix);
        //getBiggestValueElement(numberMatrix);
        //getMeanValue(numberMatrix);
        //getElementsProduct(numberMatrix);
        //getNonRepeatingElements(numberMatrix);
        //getPrimeElements(numberMatrix);
        //getMatrixMainDiagonal(numberMatrix);
        //getMatrixSecondaryDiagonal(numberMatrix);
        //verifyIfIdentityMatrix(numberMatrix);
        //getInverseMatrix(numberMatrix);
        //double[][] numberMatrix1 = {{1, 13, 23}, {7, 5, 29}, {11, 19, 7}};
        //getTransposedMatrix(numberMatrix1);


    }

//------A---------------------------------------------------------------------------------------------------------------

    /**
     * A função obtém o valor mais baixo do elemento de uma matriz
     *
     * @param numberMatrix matriz de números
     * @return número correspondente ao valor mais baixo de um elemento da matriz
     */
    public static Integer getSmallestValueElement(int[][] numberMatrix) {

        //Verifica se alguma das linhas da matriz está vazia
        if (verifyIfMatrixLineIsEmpty(numberMatrix)) {
            return null;
        }

        //Iguala o elemento de menor valor ao primeiro elemento da matriz
        int smallestElement = numberMatrix[0][0];

        //O ciclo percorre cada linha
        for (int i = 0; i < numberMatrix.length; i++) {
            //O ciclo percorre cada coluna para encontrar o menor valor. Sempre que encontra um valor menor que o anterior, guarda-o em "smallestElement"
            for (int j = 0; j < numberMatrix[i].length; j++) {
                if (numberMatrix[i][j] < smallestElement) {
                    smallestElement = numberMatrix[i][j];
                }
            }
        }
        return smallestElement;
    }

//------B---------------------------------------------------------------------------------------------------------------

    /**
     * A função obtém o valor mais alto do elemento de uma matriz
     *
     * @param numberMatrix matriz de números
     * @return número correspondente ao valor mais alto de um elemento da matriz
     */
    public static Integer getBiggestValueElement(int[][] numberMatrix) {

        //Verifica se alguma das linhas da matriz está vazia
        if (verifyIfMatrixLineIsEmpty(numberMatrix)) {
            return null;
        }

        //Iguala o elemento de maior valor ao primeiro elemento da matriz
        int biggestElement = numberMatrix[0][0];

        //O ciclo percorre cada linha
        for (int i = 0; i < numberMatrix.length; i++) {
            //O ciclo percorre cada coluna para encontrar o maior valor. Sempre que encontra um valor maior que o anterior,
            // guarda-o em "biggestElement"
            for (int j = 0; j < numberMatrix[i].length; j++) {
                if (numberMatrix[i][j] > biggestElement) {
                    biggestElement = numberMatrix[i][j];
                }
            }
        }
        return biggestElement;
    }

//------C---------------------------------------------------------------------------------------------------------------

    /**
     * A função obtém a média dos elemento de uma matriz
     *
     * @param numberMatrix matriz de números
     * @return número correspondente à média dos elemento da matriz
     */
    public static Double getMeanValue(int[][] numberMatrix) {

        //Verifica se alguma das linhas da matriz está vazia
        if (verifyIfMatrixLineIsEmpty(numberMatrix)) {
            return null;
        }

        //O método conta o número de elementos da matriz e guarda-se essa informação numa variável
        int elementCounter = countNrElements(numberMatrix);

        double sumValues = 0;

        //O ciclo percorre cada linha
        for (int i = 0; i < numberMatrix.length; i++) {
            //O ciclo percorre cada coluna para somar os elementos
            for (int j = 0; j < numberMatrix[i].length; j++) {
                sumValues = sumValues + numberMatrix[i][j];
            }

        }
        //Retorna a média
        return (sumValues / elementCounter);
    }

//------D---------------------------------------------------------------------------------------------------------------

    /**
     * A função obtém o produto de todos os elementos de uma matriz
     *
     * @param numberMatrix matriz de números
     * @return produto dos elementos da matriz
     */
    public static Integer getElementsProduct(int[][] numberMatrix) {
        //Verifica se alguma das linhas da matriz está vazia
        if (verifyIfMatrixLineIsEmpty(numberMatrix)) {
            return null;
        }
        int productValue = 1;

        //O ciclo percorre cada linha
        for (int i = 0; i < numberMatrix.length; i++) {
            //O ciclo percorre cada coluna para multiplicar os elementos
            for (int j = 0; j < numberMatrix[i].length; j++) {
                productValue = productValue * numberMatrix[i][j];
            }
        }
        //Retorna o produto dos elementos
        return productValue;
    }

//------E---------------------------------------------------------------------------------------------------------------

    /**
     * O método obtém um vetor com os elementos não repetidos da matriz
     *
     * @param numberMatrix matriz de números
     * @return vetor com os números não repetidos
     */

    public static int[] getNonRepeatingElements(int[][] numberMatrix) {
        //Verifica se a matriz é nula
        if (numberMatrix == null) {
            return null;
        }

        //O método conta o número de elementos numa matriz
        int matrixSize = countNrElements(numberMatrix);
        //O método converte a matriz num array
        int[] matrixConvertedToArray = convertMatrixToArray(numberMatrix, matrixSize);

        //O método organiza os valores do array por ordem crescente
        Arrays.sort((matrixConvertedToArray));

        //Faz-se a contagem dos elementos não repetidos
        int nonRepeatingElementsCounter = countNonRepeatingElementsFromSortedArray(matrixConvertedToArray);

        //Cria-se o array com os elementos não repetidos
        int[] nonRepeatingElementsArray = new int[nonRepeatingElementsCounter];
        nonRepeatingElementsArray[0] = matrixConvertedToArray[0];

        int k = 1;
        //O ciclo percorre o array e copia os elementos únicos para um novo array
        for (int i = 1; i < matrixConvertedToArray.length; i++) {
            if (matrixConvertedToArray[i] != matrixConvertedToArray[i - 1]) {
                nonRepeatingElementsArray[k] = matrixConvertedToArray[i];
                k++;
            }
        }

        return nonRepeatingElementsArray;

    }

//------F---------------------------------------------------------------------------------------------------------------

    /**
     * O método obtém um vetor com os números primos de uma matriz
     *
     * @param numberMatrix matriz de números
     * @return vetor de números primos
     */
    public static int[] getPrimeElements(int[][] numberMatrix) {
        //Verifica se a matriz é nula
        if (numberMatrix == null) {
            return null;
        }
        //Contagem dos números primos
        int primeElementsCounter = countPrimeElementsFromMatrix(numberMatrix);

        int[] primeElements = new int[primeElementsCounter];
        int l = 0;

        //Copia os números primos da matriz para um array
        for (int i = 0; i < numberMatrix.length; i++) {
            for (int j = 0; j < numberMatrix[i].length; j++) {
                if (isNumberPrime(numberMatrix[i][j])) {
                    primeElements[l] = numberMatrix[i][j];
                    l++;
                }
            }
        }
        return primeElements;
    }

//------G---------------------------------------------------------------------------------------------------------------

    /**
     * O método obtém a diagonal principal de uma matriz, verificando se esta é quadrada ou retangular
     *
     * @param numberMatrix matriz de números
     * @return array com os valores da diagonal principal
     */
    public static int[] getMatrixMainDiagonal(int[][] numberMatrix) {
        //Verifica se todas as linhas da matriz têm o mesmo número de colunas
        if (verifyIfAllLinesHaveSameColumns(numberMatrix) == -1) {
            return null;
        }
        int verticalPosition = 0, horizontalPosition = 0, indexArray = 0;

        //O ciclo vai contar o número de elementos da diagonal
        int countNumberElements = countNumberOfMainDiagonalElements(numberMatrix);

        int[] mainDiagonalArray = new int[countNumberElements];

        //O ciclo vai copiar os elementos da diagonal principal da matriz para o novo array
        while (horizontalPosition < numberMatrix[0].length && verticalPosition < numberMatrix.length) {
            mainDiagonalArray[indexArray] = numberMatrix[horizontalPosition][verticalPosition];
            indexArray++;
            verticalPosition++;
            horizontalPosition++;
        }

        return mainDiagonalArray;
    }

//------H---------------------------------------------------------------------------------------------------------------

    /**
     * O método obtém a diagonal secundária de uma matriz, verificando se esta é quadrada ou retangular primeiro
     *
     * @param numberMatrix matriz de números
     * @return array com os valores da diagonal secundária
     */
    public static int[] getMatrixSecondaryDiagonal(int[][] numberMatrix) {
        //Verifica se todas as linhas da matriz têm o mesmo número de colunas
        if (verifyIfAllLinesHaveSameColumns(numberMatrix) == -1) {
            return null;
        }

        int countNumberElements = 0, verticalPosition = 0, horizontalPosition = (numberMatrix[0].length - 1), indexArray = 0;
        //O ciclo vai contar o número de elementos da diagonal secundária
        while (horizontalPosition >= 0 && verticalPosition < numberMatrix.length) {
            verticalPosition++;
            horizontalPosition--;
            countNumberElements++;
        }

        int[] secondaryDiagonalArray = new int[countNumberElements];
        verticalPosition = 0;
        horizontalPosition = (numberMatrix[0].length - 1);

        //O ciclo vai copiar os elementos da diagonal secundária da matriz para o novo array
        while (horizontalPosition >= 0 && verticalPosition < numberMatrix.length) {
            secondaryDiagonalArray[indexArray] = numberMatrix[verticalPosition][horizontalPosition];
            indexArray++;
            verticalPosition++;
            horizontalPosition--;
        }

        return secondaryDiagonalArray;
    }

//------I---------------------------------------------------------------------------------------------------------------

    /**
     * O método verifica se a matriz é quadrada e se é identidade
     *
     * @param numberMatrix matriz de números
     * @return booleano true, se for matriz identidade, false se não for
     */
    public static Boolean verifyIfIdentityMatrix(int[][] numberMatrix) {

        //Verifica se a matriz é quadrada
        if (!verifyIfMatrixIsSquare(numberMatrix)) {
            return null;
        }

        return isIdentityMatrix(numberMatrix);
    }

//------J---------------------------------------------------------------------------------------------------------------

    /**
     * O método obtém a matriz inversa
     *
     * @param numberMatrix matriz de números inteiros
     * @return matriz inversa da original
     */
    public static double[][] getInverseMatrix(int[][] numberMatrix) {

        double numberMatrixDeterminant = getDeterminantLaplace(numberMatrix);

        //Se a matriz não for quadrada ou se o determinante for igual a zero (não é inversível), retorna null
        if (!verifyIfMatrixIsSquare(numberMatrix) || numberMatrixDeterminant == 0.00) {
            return null;
        }
        //Inicialização da matriz dos cofatores
        double[][] cofactorMatrix = new double[numberMatrix.length][numberMatrix.length];

        //Cálculo do cofator de cada elemento da matriz original, atribuição dos cofatores à matriz dos cofatores
        for (int i = 0; i < numberMatrix.length; i++) {
            for (int j = 0; j < numberMatrix[0].length; j++) {
                double value = getElementCofactor(numberMatrix, j, i);

                cofactorMatrix[i][j] = value;
            }
        }
        //Inicialização da matriz adjunta, calculando-a através da matriz dos cofatores
        double[][] adjunctMatrix = getTransposedMatrix(cofactorMatrix);

        //Inicialização da matriz inversa
        double[][] inverseMatrix = new double[numberMatrix.length][numberMatrix.length];

        //Cálculo de cada elemento da matriz inversa, através da divisão de cada elemento da matriz adjunta pelo determinante da matriz original
        for (int i = 0; i < numberMatrix.length; i++) {
            for (int j = 0; j < numberMatrix[0].length; j++) {
                inverseMatrix[i][j] = adjunctMatrix[i][j] / numberMatrixDeterminant;
            }
        }
        return inverseMatrix;
    }
//------K---------------------------------------------------------------------------------------------------------------

    /**
     * O método verifica se todas as linhas da matriz têm o mesmo número de colunas e, se tiver, retorna a matriz transposta
     *
     * @param numberMatrix matriz de números
     * @return matriz transposta
     */
    public static double[][] getTransposedMatrix(double[][] numberMatrix) {

        //Verifica se todas as linhas da matriz têm o mesmo número de colunas
        if (verifyIfAllLinesHaveSameColumnsDouble(numberMatrix) == -1) {
            return null;
        }

        double[][] transposedMatrix = new double[numberMatrix[0].length][numberMatrix.length];

        //São copiados os valores da matriz original para a matriz transposta, invertendo as colunas e as linhas
        for (int i = 0; i < numberMatrix[0].length; i++) {
            for (int j = 0; j < numberMatrix.length; j++) {
                transposedMatrix[i][j] = numberMatrix[j][i];
            }
        }
        return transposedMatrix;
    }

}
