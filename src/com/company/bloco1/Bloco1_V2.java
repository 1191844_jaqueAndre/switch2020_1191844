package com.company.bloco1;

import static java.lang.Math.*;

public class Bloco1_V2 {

    public static void main (String[] args){

    }

    //Exercício 1

    public static double obterPercentagemRaparigas(int numRaparigas, int numRapazes) {
        double numTotal = numRaparigas + numRapazes;
        double percentagemRaparigas = numRaparigas / numTotal * 100;
        return percentagemRaparigas;
    }

    public static double obterPercentagemRapazes(int numRapazes, int numRaparigas) {
        double numTotal = numRaparigas + numRapazes;
        double percentagemRapazes = numRapazes / numTotal * 100;
        return percentagemRapazes;
    }

    //Exercício 2

    public static double obterPrecoTotal(int numTulipas, int numRosas, double precoTulipa, double precoRosa) {
        double precoTotal;
        precoTotal = (numTulipas * precoTulipa) + (numRosas * precoRosa);
        return precoTotal;
    }

    //Exercício 3

    public static double obterVolumecilindro(double raioBase, double alturaCilindro) {
        double volumeCilindro;
        volumeCilindro = PI * raioBase * raioBase * alturaCilindro * 1000;
        return volumeCilindro;
    }

    //Exercício 4

    public static double obterDistanciaTrovoada(double instanteRelampago, double instanteTrovao) {
        final int VELOCIDADE_SOM = 1224 * 1000 / 3600;
        double distanciaTrovoada;
        distanciaTrovoada = VELOCIDADE_SOM * (instanteRelampago - instanteTrovao);
        return distanciaTrovoada;
    }

    //Exercício 5

    public static double obterAlturaPredio(double tempo) {
        double velocidadeInicial = 0;
        final double ACELERACAO_PEDRA = 9.8;
        double alturaPredio;
        alturaPredio = velocidadeInicial + (ACELERACAO_PEDRA * tempo * tempo) / 2;
        return alturaPredio;
    }

    //Exercício 6

    public static double obterAlturaEdificio(double sombraEdificio, double sombraPessoa, double alturaPessoa) {
        double alturaEdificio;
        alturaEdificio = sombraEdificio * alturaPessoa / sombraPessoa;
        return alturaEdificio;
    }

    //Exercício 7

    public static double obterDistanciaZe() {
        double distanciaManuel = 42195;
        double tempoManuel = 4*60*60 + 2*60 + 10;
        double tempoZe = 1*60*60 + 5*60;
        double distanciaZe;
        distanciaZe = (distanciaManuel * tempoZe / tempoManuel) / 1000;
        return distanciaZe;
    }

    //Exercício 8

    public static double obterLadoC() {
        double ladoA = 60;
        double ladoB = 40;
        double anguloLambda = PI / 3;
        double ladoC;
        ladoC = sqrt( (ladoA * ladoA + ladoB * ladoB) - (2 * ladoA * ladoB * cos(anguloLambda)));
        return ladoC;
    }

    //Exercício 9

    public static double obterPerimetro (double ladoA, double ladoB){
        return 2 * ladoA + 2 * ladoB;
    }

    //Exercício 10

    public static double obterHipotenusa(double cateto1, double cateto2){
        return Math.sqrt(cateto1 * cateto1 + cateto2 * cateto2);
    }

    //Exercício 11
    public static double obterValorExpressao(double x){
        double y = (x * x) - (3 * x) + 1;
        return y;
    }

    //Exercício 12

    public static double obterTemperaturaFahrenheit(double celsius){
        return ((9 * celsius / 5) + 32);
    }

    //Exercício 13

    public static int obterHorasMinutos(int H, int M){
        return ((H * 60) + M);
      }
}






