package com.company.bloco1;

import java.util.Calendar;
import java.util.Scanner;

public class Bloco1_V1 {

    public static void main(String[]args) {
        //exercicio1();
        //exercicio2();
        //exercicio3();
        //exercicio4();
        //exercicio5();
        //exercicio6();
        //exercicio7();
        //exercicio8();
        //exercicio9();
        //exercicio10();
        //exercicio11();
        //exercicio12();
        //exercicio13();

    }

    public static void exercicio1(){

        //Dados
        int numRapazes, numRaparigas;
        double percentagemRapazes, percentagemRaparigas, numTotal;

        //Leitura de dados
        Scanner leitura = new Scanner(System.in);

        System.out.println("Introduza o número de rapazes");
        numRapazes = leitura.nextInt();
        System.out.println("Introduza o número de raparigas");
        numRaparigas = leitura.nextInt();

        //Processamento
        numTotal = numRaparigas + numRapazes;
        percentagemRapazes = numRapazes / numTotal * 100;
        percentagemRaparigas = numRaparigas / numTotal * 100;

        //Saída de dados
        System.out.println("A percentagem de rapazes é: " + percentagemRapazes + "%");
        System.out.println("A percentagem de raparigas é: " + percentagemRaparigas + "%");
    }

    public static void exercicio2() {

        //Dados
        int numTulipas, numRosas;
        double precoTulipa, precoRosa, precoTotal;

        //Leitura de dados
        Scanner ler = new Scanner(System.in);

        System.out.println("Introduza o número de túlipas");
        numTulipas = ler.nextInt();
        System.out.println("Introduza o número de rosas");
        numRosas = ler.nextInt();
        System.out.println("Introduza o preco de cada tulipa");
        precoTulipa = ler.nextDouble();
        System.out.println("Introduza o preco de cada rosa");
        precoRosa = ler.nextDouble();

        //Processamento
        precoTotal = (numTulipas * precoTulipa) + (numRosas * precoRosa);

        //Saída de dados
        System.out.println("O preço total do ramo é de " + precoTotal + "€");
    }

    public static void exercicio3() {

        //Dados
        double raioBase, alturaCilindro, volumeCilindro, capacidadeCilindro;

        //Leitura de dados
        Scanner ler = new Scanner(System.in);
        System.out.println("Indique, em metros, o raio da base");
        raioBase = ler.nextDouble();
        System.out.println("Indique, em metros, a altura do cilindro");
        alturaCilindro = ler.nextDouble();

        //Processamento
        volumeCilindro = Math.PI * raioBase * raioBase * alturaCilindro;
        capacidadeCilindro = volumeCilindro * 1000;

        //Saída de dados
        System.out.println("É possível armazenar " + String.format("%.2f",capacidadeCilindro) + "L no vasilhame.");

    }

    public static void exercicio4() {

        //Dados
        double instanteRelampago, instanteTrovao, velocidadeSom, distanciaTrovoada;

        //Leitura de dados
        Scanner ler = new Scanner(System.in);
        System.out.println("Defina o instante, em segundos, em que viu o relâmpago");
        instanteRelampago = ler.nextDouble();
        System.out.println("Defina o instante, em segundos, em que ouviu o trovão");
        instanteTrovao = ler.nextDouble();

        //Processamento
        velocidadeSom = 1224 * 1000 / 3600;
        distanciaTrovoada = velocidadeSom * (instanteRelampago - instanteTrovao);

        //Saída de dados
        System.out.println("A trovoada está a " + distanciaTrovoada + "m de distância.");

    }

    public static void exercicio5() {

        //Dados
        double alturaPredio, tempo;
        double velocidadeInicial = 0;
        double aceleracaoPedra = 9.8;

        //Leitura de dados
        Scanner ler = new Scanner(System.in);
        System.out.println("Indique o tempo, em segundos, que a pedra levou até atingir o chão");
        tempo = ler.nextDouble();


        //Processamento
        alturaPredio = velocidadeInicial + (aceleracaoPedra * tempo * tempo) / 2;

        //Saída de dados
        System.out.println("A altura do prédio é de " + alturaPredio + "metros.");

    }

    public static void exercicio6() {

        //Dados
        double sombraEdificio, sombraPessoa, alturaPessoa, alturaEdificio;

        //Leitura de dados
        Scanner ler = new Scanner(System.in);
        System.out.println("Indica, em metros, o comprimento da sombra do edifício");
        sombraEdificio = ler.nextDouble();
        System.out.println("Indica, em metros, o comprimento da tua sombra");
        sombraPessoa = ler.nextDouble();
        System.out.println("Indica, em metros, a tua altura");
        alturaPessoa = ler.nextDouble();

        //Processamento
        alturaEdificio =  sombraEdificio * alturaPessoa / sombraPessoa;;

        //Saída de dados
        System.out.println("A altura do edifício é de " + alturaEdificio + " metros.");

    }

    public static void exercicio7() {

        //Dados
        double distânciaManuel = 42195, distânciaZe;
        double tempoManuel = 4*60*60 + 2*60 + 10;
        double tempoZe = 1*60*60 + 5*60;

        //Processamento
        distânciaZe = (distânciaManuel * tempoZe / tempoManuel) / 1000;

        //Saída de dados
        System.out.println("O Zé correu " + String.format("%.3f", distânciaZe) + " Kms.");

    }

    public static void exercicio8() {

        //Dados
        double ladoC;
        double ladoA = 60;
        double ladoB = 40;
        double anguloLambda = Math.PI / 3;

        //Processamento
        ladoC = Math.sqrt( (ladoA * ladoA + ladoB * ladoB) - (2 * ladoA * ladoB * Math.cos(anguloLambda)));

        //Saúida de dados
        System.out.println("A distância entre os dois operários é de " + String.format("%.2f", ladoC) + " metros.");

    }

    public static void exercicio9() {

        //Dados
        double ladoA;
        double ladoB;
        double perimetro;

        //Leitura de dados
        Scanner ler = new Scanner(System.in);

        System.out.println("Introduza, em centímetros, o comprimento do retângulo");
        ladoB = ler.nextDouble();
        System.out.println("Introduza, em centímetros, a altura do retângulo");
        ladoA = ler.nextDouble();

        //Processamento
        perimetro = 2 * ladoA + 2 * ladoB;

        //Saída de dados
        System.out.println("O perímetro do retângulo é de " + perimetro + " metros quadrados.");
    }

    public static void exercicio10() {

        //Dados
        double cateto1;
        double cateto2;
        double hipotenusa;

        //Leitura de dados
        Scanner ler = new Scanner(System.in);

        System.out.println("Introduza o comprimento, em centímetros, do cateto 1");
        cateto1 = ler.nextDouble();
        System.out.println("Introduza o comprimento, em centímetros, do cateto 2");
        cateto2 = ler.nextDouble();

        //Processamento de dados
        hipotenusa = Math.sqrt(cateto1 * cateto1 + cateto2 * cateto2);

        //Saída de dados
        System.out.println("O comprimento da hipotenusa é de " + String.format("%.3f", hipotenusa) + " centímetros.");
    }

    public static void exercicio11(){

        //Dados
        double x;
        double y;

        //Leitura de dados
        Scanner ler = new Scanner(System.in);
        System.out.println("Introduza o valor de x");
        x = ler.nextDouble();

        //Processamento de dados
        y = (x * x) - (3 * x) + 1;

        //Saída de dados
        System.out.println("O valor de y é de " + y + ".");
    }

    public static void exercicio12(){

        //Dados
        double fahrenheit;
        double celsius;

        //Leitura de dados
        Scanner ler = new Scanner(System.in);

        System.out.println("Introduza a temperatura em graus Celsius");
        celsius = ler.nextDouble();

        //Processamento de dados
        fahrenheit =((9 * celsius / 5) + 32);

        //Saída de dados
        System.out.println("A temperatura em Fahrenheit será de " + fahrenheit + " graus.");
    }

    public static void exercicio13(){
       //Dados
        int H; //É possível restringir as horas de 0 a 23?
        int M; //É possível restringir os minutos de 0 a 59?
        int minutosPassados;

        //Leitura de dados
        Scanner ler = new Scanner(System.in);
        System.out.println("Introduza as horas");
        H = ler.nextInt();
        System.out.println("Introduza os minutos");
        M = ler.nextInt();

        //Processamento de dados
        minutosPassados = (H * 60) + M;

        //Saída de dados
        System.out.println("Passaram-se " + minutosPassados + " minutos desde as 0h.");
    }
}



