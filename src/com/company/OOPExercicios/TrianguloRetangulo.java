package com.company.OOPExercicios;

//Exercício 10
//public static double obterHipotenusa(double cateto1, double cateto2){return Math.sqrt(cateto1 * cateto1 + cateto2 * cateto2);}

public class TrianguloRetangulo {

    //Atributos
    private final double cateto1;
    private final double cateto2;

    //Construtor
    public TrianguloRetangulo(double cateto1, double cateto2) {
        this.cateto1 = cateto1;
        this.cateto2 = cateto2;
    }

    //Getters e setters

    public double getCateto1(){
        return cateto1;
    }

    public double getCateto2(){
        return cateto2;
    }

    //Métodos de negócio
    public boolean verificarDimensao(double cateto1, double cateto2) {
        return !(cateto1 <= 0) && !(cateto2 <= 0);
    }

    public double obterHipotenusa(double cateto1, double cateto2) {
        double hipotenusa = Math.sqrt(cateto1 * cateto1 + cateto2 * cateto2);
        if (verificarDimensao(cateto1, cateto2)) {
            return hipotenusa;
        } else {
            throw new IllegalArgumentException("The triangle's dimensions are not correct");
        }
    }
}

