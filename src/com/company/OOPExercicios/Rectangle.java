package com.company.OOPExercicios;

//Exercício 9, Bloco1
// public static double obterPerimetro (double ladoA, double ladoB){return 2 * ladoA + 2 * ladoB;}

public class Rectangle {

    //Atributos
    private double width;
    private double height;

    //Construtor
    public Rectangle(double width, double height) {
        //this.width = width;
        setWidth(width);
        //this.height = height;
        setHeight(height);
    }
    //Setters e getter

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    //Métodos de negócio
    public boolean verifyDimensions(double width, double height) {
        if (width <= 0) {
            throw new IllegalArgumentException("The rectangle's width is not correct");
        }
        if (height <= 0) {
            throw new IllegalArgumentException("The rectangle's height is not correct");
        }
        return true;
    }

    public double getPerimeter(double width, double height) {
        if (verifyDimensions(width, height)) {
            double perimeter = 2 * width + 2 * height;
            return perimeter;
        }
        return -1;
    }

}
