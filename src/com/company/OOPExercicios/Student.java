package com.company.OOPExercicios;

public class Student {

    //Atributos
    private final String name;
    private int number;
    private int grade;

    //Métodos construtores
    public Student(int number, String name) {        //Construtor - inicializa os atributos da classe
                                                    //Pelo menos um dos construtores tem de ser public
        if (!isNumberValid(number)) {                 //Quando se criar um estudante, este construtor está a dizer que só é possivel se ele tiver estes dois parâmetros
            throw new IllegalArgumentException("O número tem de ter sete dígitos e ser positivo");
        }
        if (!isNameValid(name)) {
            throw new IllegalArgumentException("O nome tem de ter mais de cinco caracteres e não pode ser vazio");
        }
        this.number = number;
        this.name = name;
        this.grade = -1;
    }

    //Métodos
    public static boolean isNumberValid(int number) {
        int digitCounter = 0;
        while (number > 0) {
            number = number / 10;
            digitCounter++;
        }
        return digitCounter == 7;
    }

    public int compareToByNumber(Student other) {
        if (this.number < other.number) {
            return -1; //lesser than the other
        }
        if (this.number > other.number) {
            return 1; //greater than the other
        }
        return 0; //both have the same value
    }

    public int compareToByGrade(Student other) {
        if (this.grade < other.grade) {
            return -1; //lesser than the other
        }
        if (this.grade > other.grade) {
            return 1; //greater than the other
        }
        return 0; //both have the same value
    }

    public void setGrade(int grade) {
        if (!this.isEvaluated() && this.isGradeValid(grade)) {
            this.grade = grade;
        }
    }

    public boolean isApproved(int grade) {
        return (grade >= 10);
    }

    public boolean isNameValid(String name) {
        return (name != null && name.trim().length() >= 5);
    }

    public boolean isGradeValid(int grade) {
        return grade <= 20 && grade >= 0;
    }

    public boolean isEvaluated() {
        return grade != -1;
    }


    public boolean equals(Object o){
        if(this == o){
            return true;
        }
        if(o == null || getClass() != o.getClass()){
            return false;
        }
        Student student = (Student) o;
        return this.number == student.number;
    }
}

