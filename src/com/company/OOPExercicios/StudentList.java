package com.company.OOPExercicios;

import java.util.ArrayList;
import java.util.Comparator;

public class StudentList {

    //Atributos
    private final ArrayList<Student> students;

    //Construtor
    public StudentList() {
        this.students = new ArrayList();
    }

    public StudentList(Student[] students) {
        if (students == null) {
            throw new IllegalArgumentException("O array de estudantes não deve ser nulo.");
        }
        this.students = new ArrayList();
        for (Student st : students) {
            this.add(st);
        }
    }

    //Métodos de negócio
    public boolean add(Student student) {
        if (student == null) {
            return false;
        }
        if (this.students.contains(student)) {
            return false;
        }
        return this.students.add(student);
    }

    public boolean remove(Student student) {
        if (student == null) {
            return false;
        }
        if (!this.students.contains(student)) {
            return false;
        }
        return this.students.remove(student);
    }

    public void sortByNumberAsc() {
        this.students.sort(new SortByNumber());
    }

    public void sortByGradeDesc() {
        this.students.sort(new SortByGradeDesc());
    }

    public Student[] toArray() {
        Student[] array = new Student[this.students.size()];
        return this.students.toArray(array);
    }

    /*public static boolean sortStudentsByNumberAsc(Student[] students){
        Student tempStudent = null;

        for (int index1 = 0; index1 < students.length; index1++){
            for (int index2 = index1+1; index2 < students.length; index2++){
                if(students[index1].compareToByNumber(students[index2]) > 0){
                    tempStudent = students[index1];
                    students[index1] = students[index2];
                    students[index2] = tempStudent;
                }
            }
        }
        return true;
    }*/

    /*public static boolean sortStudentsByGradesDesc(Student[] students){
        Student tempStudent = null;

        for (int index1 = 0; index1 < students.length; index1++){
            for (int index2 = index1+1; index2 < students.length; index2++){
                if(students[index1].compareToByGrade(students[index2]) < 0){
                    tempStudent = students[index1];
                    students[index1] = students[index2];
                    students[index2] = tempStudent;
                }
            }
        }
        return true;
    }*/

    private class SortByNumber implements Comparator<Student> {
        public int compare(Student st1, Student st2) {
            return st1.compareToByNumber(st2);
        }
    }

    private class SortByGradeDesc implements Comparator<Student> {
        public int compare(Student st1, Student st2) {
            return st1.compareToByGrade(st2) * (-1);
        }
    }

    /*private Student[] copyStudentsFromArray(Student[] students, int size){
        return copyStudentsFromArray(students, 0, size);
    }*/

    /*private Student[] copyStudentsFromArray(Student[] students, int startIndex, int size){
        Student[] copyArray = new Student[size];
        for(int count = startIndex; (count < size) && (count < students.length); count++){
            copyArray[count] = students[count];
        }
        return copyArray;
    }*/

    /*private Student[] join(Student[] students1, Student[] students2){
        int size = students1.length + students2.length;
        Student[] copyArray = new Student[size];

        for (int idx = 0; (idx < students1.length); idx++){
            copyArray[idx] = students1[idx];
        }

        for (int idx = 0; (idx < students2.length); idx++){
            copyArray[idx + students1.length] = students2[idx];
        }
        return copyArray;
    }*/
}


