package com.company.bloco5;

import java.util.Arrays;

import static com.company.bloco4.MethodsLibrary.verifyIfAllLinesHaveSameColumns;

public class Utilities {

    /**
     * O método addArrayElements vai somar todos os elementos de um array "numberArray"
     *
     * @param numberArray vetor de números inteiros
     * @return soma de todos os elementos do vetor
     */
    public static int addArrayElements(int[] numberArray) {

        int addArrayNumbers = 0;

        for (int i = 0; i < numberArray.length; i++) {
            addArrayNumbers = addArrayNumbers + numberArray[i];
        }

        return addArrayNumbers;
    }
//----------------------------------------------------------------------------------------------------------------------

    /**
     * O método faz a contagem dos múltiplos de um número inteiro "n" num dado vetor
     *
     * @param numberArray vetor que contém ou não os múltiplos de n
     * @param n           número inteiro que corresponde ao divisor
     * @return número inteiro que corresponde à contagem dos múltiplos de n
     */
    public static int countMultiplesOfN(int[] numberArray, int n) {
        int multiplesOfNCounter = 0;

        for (int i = 0; i < numberArray.length; i++) {
            if (isDivisibleByNumber(numberArray[i], n)) {
                multiplesOfNCounter++;
            }
        }
        return multiplesOfNCounter;
    }
//----------------------------------------------------------------------------------------------------------------------

    /**
     * O método verifica se o valor "value" é divisível por um número "n"
     *
     * @param value número que será dividido por "n"
     * @param n     possível divisor de "value"
     * @return booleano true, se "value" for divisível por "n", ou falso se não for
     */
    public static boolean isDivisibleByNumber(int value, int n) {
        return value % n == 0;
    }

//----------------------------------------------------------------------------------------------------------------------

    /**
     * O método copia os múltiplos de um número "n" de um dado vetor "number array" e retorna-os noutro vetor
     *
     * @param n                   número divisor
     * @param numberArray         vetor que contém os valores de onde serão copiados os múltiplos de "n"
     * @param multiplesOfNCounter contagem dos números múltiplos de "n", que será o tamanho do vetor de destino
     * @return array com os valores múltiplos de "n"
     */
    public static int[] getMultiplesOfNFromAnArray(int n, int[] numberArray, int multiplesOfNCounter) {
        int[] multiplesOfN = new int[multiplesOfNCounter];
        for (int i = 0, j = 0; i < numberArray.length; i++) {
            if (isDivisibleByNumber(numberArray[i], n)) {
                multiplesOfN[j] = numberArray[i];
                j++;
            }
        }
        return multiplesOfN;
    }
//----------------------------------------------------------------------------------------------------------------------

    /**
     * Obtém o número de dígitos da variável "number"
     *
     * @param number número inteiro
     * @return número de dígitos que compõe o número inteiro
     */
    public static int getDigitsCountFromNumber(int number) {
        int digitCounter = 0;

        if (number == 0) {
            return 1;
        }
        while (number != 0) {
            number = number / 10;
            digitCounter++;
        }
        return digitCounter;
    }
//----------------------------------------------------------------------------------------------------------------------

    /**
     * O método vai colocar os dígitos de um int "number" num array, ordenados da mesma forma
     *
     * @param number       número inteiro
     * @param digitCounter corresponde ao tamanho do array final
     * @return array digits
     */
    public static int[] placeNumberDigitsInArray(int number, int digitCounter) {
        int[] digits = new int[digitCounter];
        int digit;
        for (int i = (digitCounter - 1); i >= 0; i--) {
            digit = number % 10;
            digits[i] = digit;
            number = number / 10;
        }
        return digits;
    }
//----------------------------------------------------------------------------------------------------------------------

    /**
     * A função vai transformar um número positivo num array e comparar se esse array é igual am um array invertido
     *
     * @param number número inteiro positivo
     * @return booleano true, se os arrays forem iguais, ou false, se não o forem
     */
    public static boolean verifyIfPalindrome(int number) {
        //O método obtém a contagem de dígitos em "number" e o valor é guardado numa variável
        int digitCounter = getDigitsCountFromNumber(number);

        //O método cria um array com os dígitos de "number", ordenados da mesma forma
        int[] digits = placeNumberDigitsInArray(number, digitCounter);

        int[] reverseNumberArray = new int[digitCounter];
        int k = (digitCounter - 1);

        //O ciclo vai atribuir os valores do array dos dígitos do número, de forma inversa
        for (int i = 0; i < digitCounter; i++) {
            reverseNumberArray[i] = digits[k];
            k--;
        }
        return Arrays.equals(digits, reverseNumberArray);
    }
//----------------------------------------------------------------------------------------------------------------------

    /**
     * O método verifica se o valor de input é um número de armstrong
     *
     * @param originalNumber número inteiro
     * @return booleano true se o valor de input for um número de armstrong
     */
    public static boolean verifyArmstrongNumber(int originalNumber) {

        int sum = 0, remainder, digits = 0;

        int number = originalNumber;

        // Count number of digits
        while (number > 0) {
            digits++;
            number = number / 10;
        }

        number = originalNumber;

        while (number != 0) {
            remainder = number % 10;
            sum = (int) (sum + Math.pow(remainder, digits));
            number = number / 10;
        }
        return (originalNumber == sum);
    }
//----------------------------------------------------------------------------------------------------------------------

    /**
     * O método obtém o número de elementos de uma matriz
     *
     * @param numberMatrix matriz de números
     * @return número de elementos de uma matriz
     */
    public static int countNrElements(int[][] numberMatrix) {
        int elementCounter = 0;
        for (int i = 0; i < numberMatrix.length; i++) {
            //O ciclo percorre cada coluna para contar o número de elementos
            for (int j = 0; j < numberMatrix[i].length; j++) {
                elementCounter++;
            }
        }
        return elementCounter;
    }
//----------------------------------------------------------------------------------------------------------------------

    /**
     * A função determina de a matriz é ou não quadrada
     *
     * @param numberMatrix matriz de números reais
     * @return booleano verdadeiro se a matriz for quadrada
     */
    public static boolean verifyIfMatrixIsSquare(int[][] numberMatrix) {

        //Conta o número de linhas da matriz
        int numberOfLines = numberMatrix.length;
        //Verifica se todas as linhas têm o mesmo número de colunas e, se tiverem, retorna o número de colunas
        int numberOfColumns = getNrColumnsIfEveryMatrixRowIsTheSameLength(numberMatrix);
        //Se o número de colunas for igual ao número de linhas, a matriz é quadrada
        return numberOfColumns == numberOfLines;
    }
//----------------------------------------------------------------------------------------------------------------------

    /**
     * A função verifica se todas as linhas da matriz têm o mesmo número de colunas
     *
     * @param numberMatrix matriz de números reais
     * @return -1 se existir uma ou mais linhas com número de colunas diferente, ou o número de colunas, se as linhas
     * tiverem todas o mesmo número
     */
    public static int getNrColumnsIfEveryMatrixRowIsTheSameLength(int[][] numberMatrix) {

        int columnsNumber = numberMatrix[0].length;

        //O ciclo percorre cada linha da matriz e procura o número de colunas em cada uma, comparando-a à anterior.
        // Se o número de colunas da linha atual for diferente da anterior, retorna -1
        for (int i = 1; i < numberMatrix.length; i++) {
            columnsNumber = numberMatrix[i].length;
            if (columnsNumber != numberMatrix[i - 1].length) {
                return -1;
            }
        }
        return numberMatrix[0].length;
    }

//----------------------------------------------------------------------------------------------------------------------

    /**
     * O método verifica se todas as linhas da matriz têm o mesmo número de colunas e, se tiver, retorna a matriz transposta
     *
     * @param numberMatrix matriz de números
     * @return matriz transposta
     */
    public static TwoDimensionalArray getTransposedMatrix(TwoDimensionalArray numberMatrix) {

        //Verifica se todas as linhas da matriz têm o mesmo número de colunas
        if (getNrColumnsIfEveryMatrixRowIsTheSameLength(numberMatrix.toArray()) == -1) {
            return null;
        }

        int nrRows = numberMatrix.toArray()[0].length;
        int nrColumns = numberMatrix.toArray().length;
        int[][] transposedMatrixValues = new int[nrRows][nrColumns];

        //São copiados os valores da matriz original para a matriz transposta, invertendo as colunas e as linhas
        for (int i = 0; i < nrRows; i++) {
            for (int j = 0; j < nrColumns; j++) {
                transposedMatrixValues[i][j] = numberMatrix.toArray()[j][i];
            }
        }
        TwoDimensionalArray transposedMatrix = new TwoDimensionalArray(transposedMatrixValues);
        return transposedMatrix;
    }

//----------------------------------------------------------------------------------------------------------------------

    /**
     * O método conta o número de elementos da diagonal principal de uma matriz
     *
     * @param numberMatrix matriz de números
     * @return número de elementos da diagonal principal
     */
    public static int countNumberOfMainDiagonalElements(TwoDimensionalArray numberMatrix) {
        int countNumberElements = 0, verticalPosition = 0, horizontalPosition = 0;
        int nrRows = numberMatrix.toArray().length;
        int nrColumns = numberMatrix.toArray()[0].length;
        while (horizontalPosition < nrColumns && verticalPosition < nrRows) {
            verticalPosition++;
            horizontalPosition++;
            countNumberElements++;
        }
        return countNumberElements;
    }
//----------------------------------------------------------------------------------------------------------------------

    /**
     * O método obtém a diagonal principal de uma matriz, verificando se esta é quadrada ou retangular
     *
     * @param numberMatrix matriz de números
     * @return array com os valores da diagonal principal
     */
    public static Array getMatrixMainDiagonal(TwoDimensionalArray numberMatrix) {
        //Verifica se todas as linhas da matriz têm o mesmo número de colunas
        if (verifyIfAllLinesHaveSameColumns(numberMatrix.toArray()) == -1) {
            return null;
        }
        //O ciclo vai contar o número de elementos da diagonal
        int countNumberElements = countNumberOfMainDiagonalElements(numberMatrix);

        int verticalPosition = 0, horizontalPosition = 0, indexArray = 0;
        int nrRows = numberMatrix.toArray().length;
        int nrColumns = numberMatrix.toArray()[0].length;
        int[] mainDiagonalValues = new int[countNumberElements];

        //O ciclo vai copiar os elementos da diagonal principal da matriz para o novo array
        while (horizontalPosition < nrColumns && verticalPosition < nrRows) {
            mainDiagonalValues[indexArray] = numberMatrix.toArray()[horizontalPosition][verticalPosition];
            indexArray++;
            verticalPosition++;
            horizontalPosition++;
        }
        Array mainDiagonal = new Array(mainDiagonalValues);
        return mainDiagonal;
    }

//----------------------------------------------------------------------------------------------------------------------

    /**
     * O método obtém a diagonal secundária de uma matriz, verificando se esta é quadrada ou retangular primeiro
     *
     * @param numberMatrix matriz de números
     * @return array com os valores da diagonal secundária
     */
    public static Array getMatrixSecondaryDiagonal(TwoDimensionalArray numberMatrix) {
        //Verifica se todas as linhas da matriz têm o mesmo número de colunas
        if (verifyIfAllLinesHaveSameColumns(numberMatrix.toArray()) == -1) {
            return null;
        }

        int countNumberElements = 0, verticalPosition = 0, horizontalPosition = (numberMatrix.toArray()[0].length - 1), indexArray = 0;
        int nrRows = numberMatrix.toArray().length;
        //O ciclo vai contar o número de elementos da diagonal secundária
        while (horizontalPosition >= 0 && verticalPosition < nrRows) {
            verticalPosition++;
            horizontalPosition--;
            countNumberElements++;
        }

        int[] secondaryDiagonalValues = new int[countNumberElements];
        verticalPosition = 0;
        horizontalPosition = (numberMatrix.toArray()[0].length - 1);

        //O ciclo vai copiar os elementos da diagonal secundária da matriz para o novo array
        while (horizontalPosition >= 0 && verticalPosition < nrRows) {
            secondaryDiagonalValues[indexArray] = numberMatrix.toArray()[verticalPosition][horizontalPosition];
            indexArray++;
            verticalPosition++;
            horizontalPosition--;
        }

        Array secondaryDiagonal = new Array(secondaryDiagonalValues);
        return secondaryDiagonal;
    }

//----------------------------------------------------------------------------------------------------------------------

    /**
     * O método imprime na consola uma matriz quadrada com o formato do jogo sopa de letras
     *
     * @param gameSize     Tamanho da matriz quadrada a imprimir
     * @param letterMatrix Matriz a imprimir
     */
    public static void printMatrixWordSearchGame(int gameSize, char[][] letterMatrix) {
        for (int i = 0; i < gameSize; i++) {
            for (int j = 0; j < gameSize; j++) {
                System.out.print("| " + letterMatrix[i][j] + " |");
            }
            System.out.println();
        }
    }
//----------------------------------------------------------------------------------------------------------------------

}
