package com.company.bloco5.WordSearchGame;

import com.company.bloco5.Utilities;

import java.util.Scanner;

public class WordSearch {

    private final char[][] grid;
    private final Word[] wordList;
    private final Progress progress;

    public WordSearch() {
        grid = new char[][]{
                {'Q', 'V', 'N', 'X', 'L', 'Z', 'A', 'F', 'A', 'I'},
                {'F', 'B', 'V', 'A', 'H', 'G', 'N', 'H', 'N', 'F'},
                {'B', 'D', 'Y', 'Q', 'I', 'Y', 'U', 'G', 'G', 'Z'},
                {'Q', 'M', 'L', 'A', 'J', 'S', 'Q', 'X', 'O', 'K'},
                {'I', 'E', 'A', 'D', 'A', 'B', 'S', 'T', 'L', 'R'},
                {'Z', 'T', 'T', 'A', 'P', 'I', 'U', 'U', 'A', 'P'},
                {'L', 'A', 'I', 'N', 'A', 'X', 'R', 'X', 'R', 'Z'},
                {'Q', 'X', 'J', 'A', 'N', 'L', 'E', 'X', 'C', 'V'},
                {'R', 'I', 'F', 'C', 'G', 'Q', 'P', 'G', 'C', 'C'},
                {'U', 'N', 'O', 'N', 'A', 'B', 'E', 'L', 'M', 'M'}};

        wordList = new Word[]{
                new Word("PERU"),
                new Word("ITALY"),
                new Word("ANGOLA"),
                new Word("JAPAN"),
                new Word("CANADA"),
                new Word("RUSSIA"),
                new Word("LEBANON")};

        progress = new Progress(10);

    }

    public void renderGrid() {
        Utilities.printMatrixWordSearchGame(10, this.grid);
    }

    public void printEndGameInfo() {
        System.out.println("Total plays: " + this.progress.checkNumberOfPlays());
        System.out.println("Total valid plays: " + this.progress.checkNumberOfValidPlays());
        System.out.println("Total invalid plays: " + this.progress.checkNumberOfInvalidPlays());
        System.out.println("Missed words: " + this.retrieveMissedWords());
    }

    public void readNextWord() {
        System.out.println("=====================");
        System.out.println("Please insert a word");

        Scanner read = new Scanner(System.in);
        String word = read.next();


        while (validatePlay(word.toUpperCase())) {
            System.out.println("Please insert a word");
            word = read.next();

        }

    }

    public boolean validatePlay(String word) {
        boolean isValidWord = checkIfWordExists(word);
        this.progress.addNewPlay(word, isValidWord);

        if (isValidWord) {
            System.out.println("Valid");
        } else {
            System.out.println("Invalid");
        }
        if (retrieveMissedWords().equals("")) {
            return false;
        }
        return progress.checkIfRemainingPlays();
    }


    private boolean checkIfWordExists(String word) {
        for (int i = 0; i < wordList.length; i++) {
            if (wordList[i].toString().equals(word)) {
                wordList[i].markAsFound();
                return true;
            }
        }
        return false;
    }

    public String retrieveMissedWords() {
        String missedWords = "";
        for (int i = 0; i < wordList.length; i++) {
            if (!wordList[i].verifyIfFound()) {
                missedWords = missedWords + " " + wordList[i].toString();
            }
        }
        return missedWords;
    }
}
