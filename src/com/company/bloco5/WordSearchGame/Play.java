package com.company.bloco5.WordSearchGame;

public class Play {

    private String insertedWord;
    private final boolean isValid;

    //Constructor
    public Play (String insertedWord, boolean isValid){
        this.insertedWord = insertedWord;
        this.isValid = isValid;
    }

    public boolean IsValidPlay()
    {
        return this.isValid;
    }

}
