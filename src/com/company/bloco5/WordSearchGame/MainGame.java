package com.company.bloco5.WordSearchGame;

import com.company.bloco4.MethodsLibrary;

public class MainGame {
    public static void main(String[] args){
        WordSearch game1 = new WordSearch();
        game1.renderGrid();
        game1.readNextWord();
        game1.printEndGameInfo();

    }
}
