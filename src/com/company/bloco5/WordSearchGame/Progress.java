package com.company.bloco5.WordSearchGame;

public class Progress {

    private final Play[] plays;
    private int maxNumberOfPlays;
    private int playNumber;

    public Progress(int maxNumberOfPlays){
        this.maxNumberOfPlays = maxNumberOfPlays;
        this.plays = new Play[maxNumberOfPlays];
    }

    public void addNewPlay(String word, boolean isValid){
        this.plays[playNumber] = new Play(word, isValid);
        playNumber++;
        maxNumberOfPlays--;
    }

    public boolean checkIfRemainingPlays(){
        return maxNumberOfPlays > 0;
    }

    public int checkNumberOfPlays(){
        return  playNumber;
    }

    public int checkNumberOfValidPlays(){
        int totalValidPlays = 0;
        for (int i = 0; i < plays.length; i++){
            if(plays[i] != null && plays[i].IsValidPlay()) {
                totalValidPlays++;
            }
        }
        return totalValidPlays;
    }

    public int checkNumberOfInvalidPlays(){
        int totalInvalidPlays = 0;
        for (int i = 0; i < plays.length; i++){
            if(plays[i] != null && !plays[i].IsValidPlay()) {
                totalInvalidPlays++;
            }
        }
        return totalInvalidPlays;
    }
}
