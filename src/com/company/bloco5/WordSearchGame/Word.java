package com.company.bloco5.WordSearchGame;

public class Word {

    private final String word;
    private boolean isFound;

    public Word (String word){
        this.word = word.toUpperCase();
    }

    public boolean verifyIfFound(){
        return isFound;
    }

    public void markAsFound(){
        this.isFound = true;
    }

    @Override
    public String toString()
    {
        return this.word;
    }
}
