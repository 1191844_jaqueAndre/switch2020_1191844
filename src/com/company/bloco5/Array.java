package com.company.bloco5;

import java.util.Arrays;

public class Array {

    //Attributes
    private int[] array;

    //Constructor que permite ao vetor ser vazio
    public Array() {
        this.array = new int[0];
        if (isArrayNull()) {
            throw new NullPointerException("The array can't be null.");
        }
    }

    //Constructor que permite ao vetor ter alguns valores
    public Array(int[] array) {

        this.array = new int[array.length];

        for (int i = 0; i < array.length; i++) {
            this.array[i] = array[i];
        }
        if (isArrayNull()) {
            throw new NullPointerException("The array can't be null.");
        }

    }

    //Business methods
//----------------------------------------------------------------------------------------------------------------------

    /**
     * Cria uma cópia do objecto array, retornando num array com os mesmos valores
     *
     * @return cópia do array
     */
    public Array createCopy() {
        int[] copy = new int[this.array.length];

        for (int i = 0; i < this.array.length; i++) {
            copy[i] = this.array[i];
        }
        Array arrayCopy = new Array(copy);
        return arrayCopy;
    }
//----------------------------------------------------------------------------------------------------------------------

    /**
     * Método para converter um vetor da classe Array para um array
     *
     * @return retorna o objeto array
     */
    public int[] toArray() {
        int[] arrayCopy = new int[this.array.length];
        for (int i = 0; i < this.array.length; i++) {
            arrayCopy[i] = this.array[i];
        }
        return arrayCopy;
    }

//----------------------------------------------------------------------------------------------------------------------

    /**
     * Varifica se o array é nulo
     *
     * @return booleano true, se o array for nulo
     */
    public boolean isArrayNull() {
        return this.array == null;
    }

//----------------------------------------------------------------------------------------------------------------------

    /**
     * Verifica se o array é vazio
     *
     * @return booleano true, se for vazio
     */
    public boolean isArrayEmpty() {
        return (array == null || this.array.length == 0);
    }

    //----------------------------------------------------------------------------------------------------------------------

    /**
     * O método permite a comparação entre dois objetos (compara classes, tamanho e cada valor do objeto array)
     *
     * @param o
     * @return true, se os objetos forem iguais
     */
    @Override
    public boolean equals(Object o) {
        if (o == null) return false;
        if (!(o instanceof Array) || getClass() != o.getClass()) return false;
        Array that = (Array) o;

        if (this.array.length != that.array.length) {
            return false;
        } else {
            for (int i = 0; i < this.array.length; i++) {
                if (this.array[i] != that.array[i])
                    return false;
            }
        }
        return true;
    }
//----------------------------------------------------------------------------------------------------------------------

    /**
     * Adiciona um novo valor ao objeto array
     *
     * @param newValue número inteiro
     * @return bolleano true, se o valor for adicionado
     */
    public void addNewValueToArray(int newValue) {

        int size = this.array.length + 1;
        int[] tempArray = new int[size];

        this.array = createCopy().toArray();

        for (int i = 0; i < size - 1; i++) {
            tempArray[i] = this.array[i];
        }
        tempArray[size - 1] = newValue;
        this.array = tempArray;
    }

//----------------------------------------------------------------------------------------------------------------------

    /**
     * Remove o primeiro elemento com um determinado valor do array
     *
     * @param valueToRemove número inteiro cuja primeira ocorrência será removida
     * @return booleano true, se o valor for removido
     */
    public void removeFirstOccurrenceValueFromArray(int valueToRemove) {
        if (isArrayEmpty()) {
            throw new IllegalArgumentException("The array can't be empty.");
        }
        if (countOccurrencesOfAValue(valueToRemove) == 0) {
            throw new IllegalArgumentException("There is no such value in the array");
        }
        this.array = createCopy().toArray();

        int size = this.array.length - 1;
        int[] tempArray = new int[size];

        for (int i = 0, j = 0; i < this.array.length; i++) {
            if (this.array[i] != valueToRemove) {
                tempArray[j] = this.array[i];
                j++;
            } else if (this.array[i] == valueToRemove && i != j) {
                tempArray[j] = this.array[i];
                j++;
            }
        }
        this.array = tempArray;
    }

//----------------------------------------------------------------------------------------------------------------------

    /**
     * Conta quantas vezes um determinado elemento aparece num array
     *
     * @param valueToCount número inteiro
     * @return número de vezes que o elemento aparece no array
     */
    public int countOccurrencesOfAValue(int valueToCount) {

        int valueCounter = 0;
        for (int i = 0; i < this.array.length; i++) {
            if (this.array[i] == valueToCount) {
                valueCounter++;
            }
        }
        return valueCounter;
    }

//----------------------------------------------------------------------------------------------------------------------

    /**
     * Obtém o valor de um determinado elemento indicado pela sua posição
     *
     * @param index número inteiro que indica a posição do elemento
     * @return retorna o elemento na posição dada
     */
    public int getValueFromPosition(int index) {
        if (isArrayEmpty()) {
            throw new IllegalArgumentException("The array can't be empty.");
        }
        if (index > this.array.length - 1 || index < 0) {
            throw new IllegalArgumentException("Essa posição não existe no array");
        }
        return this.array[index];
    }

//----------------------------------------------------------------------------------------------------------------------

    /**
     * Conta o número de elementos no array
     *
     * @return número de elementos
     */
    public int countArrayElements() {

        int elementCounter = 0;
        for (int i = 0; i < this.array.length; i++) {
            elementCounter++;
        }
        return elementCounter;
    }

//----------------------------------------------------------------------------------------------------------------------

    /**
     * Obtém o elemento com maior valor do array
     *
     * @return o maior número do array
     */
    public int getBiggestValue() {
        if (isArrayEmpty()) {
            throw new IllegalArgumentException("The array can't be empty.");
        }
        int biggestElement = this.array[0];
        for (int i = 0; i < this.array.length; i++) {
            if (this.array[i] > biggestElement) {
                biggestElement = this.array[i];
            }
        }
        return biggestElement;
    }

//----------------------------------------------------------------------------------------------------------------------

    /**
     * Obtém o elemento de menor valor do array
     *
     * @return o menor número do array
     */
    public int getSmallestValue() {
        if (isArrayEmpty()) {
            throw new IllegalArgumentException("The array can't be empty.");
        }
        int smallestElement = this.array[0];
        for (int i = 0; i < this.array.length; i++) {
            if (this.array[i] < smallestElement) {
                smallestElement = this.array[i];
            }
        }
        return smallestElement;
    }

//----------------------------------------------------------------------------------------------------------------------

    /**
     * Obtém a média dos elementos do array
     *
     * @return Valor correspondente à média
     */
    public double getElementsMean() {
        if (isArrayEmpty()) {
            throw new IllegalArgumentException("The array can't be empty.");
        }
        double elementsSum = Utilities.addArrayElements(this.array);
        double elementsMean = elementsSum / this.array.length;
        return Math.round(elementsMean);
    }

//----------------------------------------------------------------------------------------------------------------------

    /**
     * Obtém a média dos elementos pares do array
     *
     * @return valor correspondente à media dos elementos pares
     */
    public double getEvenElementsMean() {
        if (isArrayEmpty()) {
            throw new IllegalArgumentException("The array can't be empty.");
        }

        double evenElementsSum = 0;
        int evenElementsCounter = 0;
        for (int i = 0; i < this.array.length; i++) {
            if (this.array[i] % 2 == 0) {
                evenElementsSum = evenElementsSum + this.array[i];
                evenElementsCounter++;
            }
        }
        if (evenElementsCounter == 0) {
            throw new IllegalArgumentException("There are no even numbers in the array.");
        }
        return Math.round(evenElementsSum / evenElementsCounter);
    }

//----------------------------------------------------------------------------------------------------------------------

    /**
     * Obtém a média dos elementos ímpares do array
     *
     * @return valor correspondente à media dos elementos ímpares
     */
    public double getOddElementsMean() {
        if (isArrayEmpty()) {
            throw new IllegalArgumentException("The array can't be empty.");
        }

        double oddElementsSum = 0;
        int oddElementsCounter = 0;
        for (int i = 0; i < this.array.length; i++) {
            if (this.array[i] % 2 != 0) {
                oddElementsSum = oddElementsSum + this.array[i];
                oddElementsCounter++;
            }
        }
        if (oddElementsCounter == 0) {
            throw new IllegalArgumentException("There are no even numbers in the array.");
        }
        return Math.round(oddElementsSum / oddElementsCounter);
    }

//----------------------------------------------------------------------------------------------------------------------

    /**
     * Obtém a média dos elementos múltiplos de n
     *
     * @param n número inteiro
     * @return valor correspondente à média dos elementos múltiplos de n
     */
    public double getMultiplesOfANumberMean(int n) {
        if (isArrayEmpty()) {
            throw new IllegalArgumentException("The array can't be empty.");
        }
        int multiplesOfNCounter = Utilities.countMultiplesOfN(this.array, n);
        if (multiplesOfNCounter == 0) {
            throw new IllegalArgumentException("There are no multiples of " + n + " in this array.");
        }
        int[] multiplesOfN = Utilities.getMultiplesOfNFromAnArray(n, this.array, multiplesOfNCounter);
        double multiplesOfNSum = 0;

        for (int i = 0; i < multiplesOfN.length; i++) {
            multiplesOfNSum = multiplesOfNSum + multiplesOfN[i];
        }

        return Math.round(multiplesOfNSum / multiplesOfNCounter);
    }

//----------------------------------------------------------------------------------------------------------------------

    /**
     * Compara dois números inteiros
     *
     * @param number1
     * @param number2
     * @return valor inteiro 1, se o number1 for maior, -1 se o number 2 for maior, 0 se forem iguais
     */
    public int compareNumbers(int number1, int number2) {
        if (number1 > number2) {
            return 1;
        } else if (number2 > number1) {
            return -1;
        } else {
            return 0;
        }
    }

//----------------------------------------------------------------------------------------------------------------------

    /**
     * Ordena os valores do vetor por ordem ascendente
     *
     * @return true, se os valores forem organizados por ordem ascendente
     */
    public boolean sortArrayElementsAsc() {
        if (isArrayEmpty()) {
            throw new IllegalArgumentException("The array can't be empty.");
        }
        int temporaryVariable = 0;

        this.array = createCopy().toArray();

        for (int i = 0; i < this.array.length; i++) {
            for (int j = i + 1; j < this.array.length; j++) {
                if (compareNumbers(this.array[i], this.array[j]) > 0) {
                    temporaryVariable = this.array[i];
                    this.array[i] = this.array[j];
                    this.array[j] = temporaryVariable;
                }
            }
        }
        return true;
    }

//----------------------------------------------------------------------------------------------------------------------

    /**
     * Ordena os valores do vetor por ordem descendente
     *
     * @return true, se os valores forem organizados por ordem descendente
     */
    public boolean sortArrayElementsDesc() {
        if (isArrayEmpty()) {
            throw new IllegalArgumentException("The array can't be empty.");
        }
        int temporaryVariable = 0;

        this.array = createCopy().toArray();

        for (int i = 0; i < this.array.length; i++) {
            for (int j = i + 1; j < this.array.length; j++) {
                if (compareNumbers(this.array[i], this.array[j]) < 0) {
                    temporaryVariable = this.array[i];
                    this.array[i] = this.array[j];
                    this.array[j] = temporaryVariable;
                }
            }
        }
        return true;
    }

//----------------------------------------------------------------------------------------------------------------------

    /**
     * Verifica se o array contém apenas um elemento
     *
     * @return true, se o array só contiver um elemento
     */
    public boolean containsOneElementOnly() {
        return this.array.length == 1;
    }

//----------------------------------------------------------------------------------------------------------------------

    /**
     * Verifica se o array contém apenas elementos pares
     *
     * @return true, se só contiver elementos pares
     */
    public boolean containsOnlyEvenElements() {
        if (isArrayEmpty()) {
            throw new IllegalArgumentException("The array can't be empty.");
        }
        int evenElementsCounter = 0;
        for (int i = 0; i < this.array.length; i++) {
            if (this.array[i] % 2 == 0) {
                evenElementsCounter++;
            }
        }
        return evenElementsCounter == this.array.length;
    }

//----------------------------------------------------------------------------------------------------------------------

    /**
     * Verifica se o array contém apenas elementos ímpares
     *
     * @return true, se só contiver elementos ímpares
     */
    public boolean containsOnlyOddElements() {
        if (isArrayEmpty()) {
            throw new IllegalArgumentException("The array can't be empty.");
        }
        int oddElementsCounter = 0;
        for (int i = 0; i < this.array.length; i++) {
            if (this.array[i] % 2 != 0) {
                oddElementsCounter++;
            }
        }
        return oddElementsCounter == this.array.length;
    }

//----------------------------------------------------------------------------------------------------------------------

    /**
     * Verifica se o array contém elementos duplicados
     *
     * @return true, se contiver elementos duplicados
     */
    public boolean containsRepeatingElements() {
        int[] tempArray = this.array;
        int nrOfEqualElements = 0;

        for (int i = 0; i < this.array.length; i++) {
            for (int j = 0; j < this.array.length; j++) {
                if (tempArray[i] == this.array[j]) {
                    nrOfEqualElements++;
                }
            }
        }
        return nrOfEqualElements > this.array.length;
    }

//----------------------------------------------------------------------------------------------------------------------

    /**
     * Retorna os elementos do vetor cujo número de algarismos é superior ao número médio de
     * algarismos de todos os elementos do vetor
     *
     * @return array com os elementos cujo número de algarismos é superior ao número médio
     */
    public Array getElementsWithMoreDigitsThanAverage() {
        if (isArrayEmpty()) {
            throw new IllegalArgumentException("The array is empty.");
        }
        int digitsCounter = 0;
        double sumNrOfAllDigits = 0;

        this.array = createCopy().toArray();

        for (int i = 0; i < this.array.length; i++) {
            digitsCounter = Utilities.getDigitsCountFromNumber(this.array[i]);
            sumNrOfAllDigits = sumNrOfAllDigits + digitsCounter;
        }
        double totalNrDigitsMean = sumNrOfAllDigits / this.array.length;

        int[] tempArray = new int[this.array.length];
        int countNrElementsWithNrOfDigitsGreaterThenAverage = 0;

        for (int i = 0, j = 0; i < this.array.length; i++) {
            if (Utilities.getDigitsCountFromNumber(this.array[i]) > totalNrDigitsMean) {
                tempArray[j] = this.array[i];
                countNrElementsWithNrOfDigitsGreaterThenAverage++;
                j++;
            }
        }
        Array elementsWithNrOfDigitsGreaterThanAverage = getTruncatedArray(tempArray, countNrElementsWithNrOfDigitsGreaterThenAverage);

        return elementsWithNrOfDigitsGreaterThanAverage;
    }

//----------------------------------------------------------------------------------------------------------------------

    /**
     * Retorna os elementos do vetor cuja percentagem de algarismos pares é superior à média da
     * percentagem de algarismos pares de todos os elementos do vetor
     *
     * @return array com os elementos cuja percentagem de algarismos pares é superior à média
     */
    public Array getElementsWithHigherPercentageOfEvenNumbersThanAverage() {
        if (isArrayEmpty()) {
            throw new IllegalArgumentException("The array is empty.");
        }
        int[] auxiliaryArray = this.array;

        double percentagesOfEvenDigitsInAllNumbersMean = getMeanOfPercentagesOfEvenDigits();
        this.array = createCopy().toArray();

        int[] auxiliaryArray2 = this.array;

        int countNrElementsWithPercentageOfEvenDigitsGreaterThenAverage = 0;

        for (int i = 0, j = 0; i < auxiliaryArray2.length; i++) {
            double percentageOfEvenDigitsInANumber = getPercentageOfEvenDigitsInANumber(auxiliaryArray[i]);
            if (percentageOfEvenDigitsInANumber > percentagesOfEvenDigitsInAllNumbersMean) {
                auxiliaryArray2[j] = this.array[i];
                countNrElementsWithPercentageOfEvenDigitsGreaterThenAverage++;
                j++;
            }
        }
        Array elementsWithPercentageOfEvenDigitsGreaterThanAverage = getTruncatedArray(auxiliaryArray2, countNrElementsWithPercentageOfEvenDigitsGreaterThenAverage);

        return elementsWithPercentageOfEvenDigitsGreaterThanAverage;
    }

//----------------------------------------------------------------------------------------------------------------------

    /**
     * Obtém a percentagem de dígitos pares num número
     *
     * @param number número inteiro
     * @return valor correspondente à percentagem de dígitos pares
     */
    public double getPercentageOfEvenDigitsInANumber(int number) {

        int evenDigitsCounter = countEvenDigits(number);
        double digitsCount = Utilities.getDigitsCountFromNumber(number);

        double percentageOfEvenDigits = evenDigitsCounter / digitsCount * 100;
        return percentageOfEvenDigits;
    }

//----------------------------------------------------------------------------------------------------------------------

    /**
     * Faz a contagem de dígitos pares num número
     *
     * @param number número inteiro
     * @return valor correspondente ao número de dígitos pares
     */
    private int countEvenDigits(int number) {
        int evenDigitsCounter = 0;
        if (number == 0) {
            return 1;
        }
        while (number != 0) {
            int digit = number % 10;
            if (digit % 2 == 0) {
                evenDigitsCounter++;
            }
            number = number / 10;
        }
        return evenDigitsCounter;
    }

//----------------------------------------------------------------------------------------------------------------------

    /**
     * Obtém a média das percentagens dos dígitos pares num array
     *
     * @return valor correspondente à média das percentagens dos dígitos pares
     */
    public double getMeanOfPercentagesOfEvenDigits() {
        double sumOfPercentages = 0;
        for (int i = 0; i < array.length; i++) {
            double percentageOfEvenNumbersInAnElement = getPercentageOfEvenDigitsInANumber(array[i]);
            sumOfPercentages = sumOfPercentages + percentageOfEvenNumbersInAnElement;
        }

        double percentagesOfEvenDigitsInAllNumbersMean = sumOfPercentages / array.length;

        return percentagesOfEvenDigitsInAllNumbersMean;
    }

//----------------------------------------------------------------------------------------------------------------------

    /**
     * Copia os valores de um array para outro array com tamanho menor
     *
     * @param initialArray
     * @param arraySize    tamanho do novo array (inferior ao do original)
     * @return array com os valores do original, com um tamanho inferior
     */
    public Array getTruncatedArray(int[] initialArray, int arraySize) {
        int[] truncatedArrayValues = new int[arraySize];

        for (int i = 0; i < arraySize; i++) {
            truncatedArrayValues[i] = initialArray[i];
        }
        Array truncatedArray = new Array(truncatedArrayValues);

        return truncatedArray;
    }

//----------------------------------------------------------------------------------------------------------------------

    /**
     * Retorna os elementos do vetor compostos exclusivamente por algarismos pares
     *
     * @return array com os elementos compostos por apenas algarismos pares
     */
    public Array getElementsWithOnlyEvenDigits() {
        int[] auxiliaryArray = new int[this.array.length];
        int elementsWhereAllDigitsAreEvenCount = 0;

        for (int i = 0, j = 0; i < this.array.length; i++) {
            int evenDigitsCounter = countEvenDigits(this.array[i]);
            int digitsCounter = Utilities.getDigitsCountFromNumber(this.array[i]);
            if (evenDigitsCounter == digitsCounter) {
                auxiliaryArray[j] = this.array[i];
                elementsWhereAllDigitsAreEvenCount++;
                j++;
            }
        }
        int[] evenDigitsElements = new int[elementsWhereAllDigitsAreEvenCount];
        for (int i = 0; i < elementsWhereAllDigitsAreEvenCount; i++) {
            evenDigitsElements[i] = auxiliaryArray[i];
        }
        Array evenDigits = new Array(evenDigitsElements);
        return evenDigits;
    }

//----------------------------------------------------------------------------------------------------------------------

    /**
     * Retorna os elementos que são sequências crescentes do vetor
     *
     * @return array com os elementos que são sequências crescentes
     */
    public Array getElementsWithDigitsInAscOrder() {
        int[] auxiliaryArray = new int[this.array.length];
        int nrElementsAscSequence = 0;

        for (int i = 0, j = 0; i < this.array.length; i++) {
            int nrDigits = Utilities.getDigitsCountFromNumber(this.array[i]);
            int[] elementDigits = Utilities.placeNumberDigitsInArray(this.array[i], nrDigits);
            int positiveComparisons = 0;

            if (elementDigits.length > 1) {
                for (int k = 1; k < nrDigits; k++) {
                    if (elementDigits[k - 1] < elementDigits[k]) {
                        positiveComparisons++;
                    }
                }
                if (positiveComparisons == (nrDigits - 1)) {
                    auxiliaryArray[j] = this.array[i];
                    nrElementsAscSequence++;
                    j++;
                }
            }
        }
        Array elementsWithDigitsInAscOrder = getTruncatedArray(auxiliaryArray, nrElementsAscSequence);
        return elementsWithDigitsInAscOrder;
    }

//----------------------------------------------------------------------------------------------------------------------

    /**
     * Retorna as capicuas existentes no vetor
     *
     * @return array com as capicuas existentes
     */
    public Array getPalindromes() {

        int[] auxiliaryArray = new int[this.array.length];
        int palindromeCounter = 0;

        for (int i = 0, j = 0; i < this.array.length; i++) {
            if (Utilities.verifyIfPalindrome(this.array[i])) {
                auxiliaryArray[j] = this.array[i];
                palindromeCounter++;
                j++;
            }
        }
        Array palindromes = getTruncatedArray(auxiliaryArray, palindromeCounter);
        return palindromes;
    }

//----------------------------------------------------------------------------------------------------------------------

    /**
     * Retorna os números existentes no vetor compostos exclusivamente por um mesmo algarismo
     *
     * @return array com os números existentes compostos exclusivamente por um mesmo algarismo
     */
    public Array getElementsComposedOfTheSameDigits() {

        int[] auxiliaryArray = new int[this.array.length];
        int nrValuesWithTheSameDigits = 0;

        for (int i = 0, j = 0; i < this.array.length; i++) {
            int nrDigits = Utilities.getDigitsCountFromNumber(this.array[i]);
            int[] elementDigits = Utilities.placeNumberDigitsInArray(this.array[i], nrDigits);
            int positiveComparisons = 0;

            if (elementDigits.length > 1) {
                for (int k = 1; k < elementDigits.length; k++) {
                    if (elementDigits[k - 1] == elementDigits[k]) {
                        positiveComparisons++;
                    }
                }
                if (positiveComparisons == (elementDigits.length - 1)) {
                    auxiliaryArray[j] = this.array[i];
                    nrValuesWithTheSameDigits++;
                    j++;
                }
            } else if (elementDigits.length == 1) {
                auxiliaryArray[j] = this.array[i];
                nrValuesWithTheSameDigits++;
                j++;
            }
        }
        Array elementsWithDigitsInAscOrder = getTruncatedArray(auxiliaryArray, nrValuesWithTheSameDigits);
        return elementsWithDigitsInAscOrder;
    }

//----------------------------------------------------------------------------------------------------------------------

    /**
     * Retorna os números existentes no vetor que não são de Armstrong
     *
     * @return array com os números existentes que não são de Armstrong
     */
    public Array getNonArmstrongElements() {
        int[] auxiliaryArray = new int[this.array.length];
        int size = 0;

        for (int i = 0, j = 0; i < this.array.length; i++) {
            if (!Utilities.verifyArmstrongNumber(this.array[i])) {
                auxiliaryArray[j] = this.array[i];
                size++;
                j++;
            }
        }
        Array nonArmstrongElements = getTruncatedArray(auxiliaryArray, size);
        return nonArmstrongElements;
    }

//----------------------------------------------------------------------------------------------------------------------

    /**
     * Retorna os elementos que contêm uma sequência crescente de pelo menos n algarismos
     *
     * @param n número de algarismos
     * @return array com os elementos que contêm uma sequência crescente de pelo menos n algarismos
     */
    public Array getElementsWithNOfItsDigitsInAscOrder(int n) {
        int[] auxiliaryArray = new int[this.array.length];
        int nrElementsWithNDigitsInAscSequence = 0;

        for (int i = 0, j = 0; i < this.array.length; i++) {
            int nrDigits = Utilities.getDigitsCountFromNumber(this.array[i]);
            int[] elementDigits = Utilities.placeNumberDigitsInArray(this.array[i], nrDigits);
            int positiveComparisons = 0;

            if (elementDigits.length > 1) {
                for (int k = 1; k < nrDigits; k++) {
                    if (elementDigits[k - 1] < elementDigits[k]) {
                        positiveComparisons++;
                    }
                }
                if (n >= 1) {
                    if (positiveComparisons >= (n - 1)) {
                        auxiliaryArray[j] = this.array[i];
                        nrElementsWithNDigitsInAscSequence++;
                        j++;
                    }
                }
            }
        }
        Array elementsWithNDigitsInAscOrder = getTruncatedArray(auxiliaryArray, nrElementsWithNDigitsInAscSequence);
        return elementsWithNDigitsInAscOrder;
    }

//----------------------------------------------------------------------------------------------------------------------

    /**
     * Compara o vetor da classe Array com outro vetor
     *
     * @param array
     * @return true, se os vetores forem iguais
     */
    public boolean isArrayTheSame(int[] array) {
        return Arrays.equals(this.array, array);
    }

}