package com.company.bloco5;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class TwoDimensionalArray {

    private final Array[] twoDimensionalArray;

    //Construtor em que o vetor bidimensional fica vazio
    public TwoDimensionalArray() {
        this.twoDimensionalArray = new Array[0];
    }

    //Construtor que permite inicializar o array com alguns vetores tipo int[][]
    public TwoDimensionalArray(int[][] twoDimArrayInput) {

        this.twoDimensionalArray = new Array[twoDimArrayInput.length];
        for (int i = 0; i < twoDimArrayInput.length; i++) {
            Array singleArray = new Array(twoDimArrayInput[i]);
            this.twoDimensionalArray[i] = singleArray;
        }

        if (isTwoDimArrayNull()) {
            throw new NullPointerException("O vetor bidimensional não pode ser nulo.");
        }
    }

    //Construtor que permite inicializar o array com alguns vetores tipo Array[]
    public TwoDimensionalArray(Array[] twoDimArrayInput) {

        this.twoDimensionalArray = new Array[twoDimArrayInput.length];
        for (int i = 0; i < twoDimArrayInput.length; i++) {
            this.twoDimensionalArray[i] = twoDimArrayInput[i];
        }

        if (isTwoDimArrayNull()) {
            throw new NullPointerException("O vetor bidimensional não pode ser nulo.");
        }
    }

//----------------------------------------------------------------------------------------------------------------------

    /**
     * O método cria uma cópia do vetor bidimensional
     *
     * @return vetor bidimensional da classe TwoDimensionalArray
     */
    public TwoDimensionalArray createCopy() {

        TwoDimensionalArray twoDimArrayResult = new TwoDimensionalArray(this.twoDimensionalArray);

        for (int i = 0; i < this.twoDimensionalArray.length; i++) {
            /*Array resultRow =  this.twoDimensionalArray[i];
            twoDimArrayResult.twoDimensionalArray[i] = resultRow.createCopy();
                    ou
            Array resultRow2 =   new Array(this.twoDimensionalArray[i].toArray());
            twoDimArrayResult.twoDimensionalArray[i] = resultRow2;
                    ou  */
            twoDimArrayResult.twoDimensionalArray[i] = new Array(this.twoDimensionalArray[i].toArray());
        }
        return twoDimArrayResult;
    }

    //----------------------------------------------------------------------------------------------------------------------

    /**
     * O método verifica se o vetor bidimensional é nulo
     *
     * @return treu, se for nulo, false se não for
     */
    public boolean isTwoDimArrayNull() {
        return this.twoDimensionalArray == null;
    }

    //----------------------------------------------------------------------------------------------------------------------

    /**
     * Verifica se alguma linha da matriz está vazia
     *
     * @return true, se uma ou mais linhas da matriz estiverem vazias
     */
    public boolean isMatrixRowEmpty() {

        for (int i = 0; i < this.twoDimensionalArray.length; i++) {
            Array singleArray = new Array(this.twoDimensionalArray[i].toArray());
            if (singleArray.isArrayEmpty()) {
                return true;
            }
        }
        return false;
    }

//----------------------------------------------------------------------------------------------------------------------

    /**
     * O método compara dois objetos (classes, tanhanhos e linhas)
     *
     * @param o Objeto a comparar
     * @return true, se os dois objectos foram iguais segundo os parâmetros acima
     */
    @Override
    public boolean equals(Object o) {

        if (o == null) return false;

        if (!(o instanceof TwoDimensionalArray) || this.getClass() != o.getClass()) return false;

        TwoDimensionalArray that = (TwoDimensionalArray) o;

        if (this.twoDimensionalArray.length != that.twoDimensionalArray.length) {
            return false;
        } else {
            for (int i = 0; i < this.twoDimensionalArray.length; i++) {
                if (!this.twoDimensionalArray[i].equals(that.twoDimensionalArray[i]))
                    return false;
            }
        }
        return true;
    }


//----------------------------------------------------------------------------------------------------------------------

    /**
     * O método converte um array do tipo TwoDimensionalArray para um array bidimensional
     *
     * @return array bidimensional
     */
    public int[][] toArray() {
        int[][] twoDimensionalArray = new int[this.twoDimensionalArray.length][];
        for (int i = 0; i < this.twoDimensionalArray.length; i++) {
            Array row = new Array(this.twoDimensionalArray[i].toArray());
            twoDimensionalArray[i] = row.toArray();
        }
        return twoDimensionalArray;
    }

//----------------------------------------------------------------------------------------------------------------------

    /**
     * O método adiciona um novo elemento a uma linha desejada na matriz
     *
     * @param rowIndex Índice da linha à qual queremos adicionar o valor
     * @param newValue valor a adicionar
     * @return Array bidimensional que inclui a matriz original mais o valor adicionado
     */
    public TwoDimensionalArray addElementToRow(int rowIndex, int newValue) {
        TwoDimensionalArray twoDimArray = createCopy();

        if (rowIndex > twoDimArray.twoDimensionalArray.length - 1) {
            throw new ArrayIndexOutOfBoundsException("A linha indicada não existe");
        } else {
            Array row = new Array(twoDimArray.twoDimensionalArray[rowIndex].toArray());
            row.addNewValueToArray(newValue);
            twoDimArray.twoDimensionalArray[rowIndex] = row;
            return twoDimArray;
        }
    }
//----------------------------------------------------------------------------------------------------------------------

    /**
     * O método remove a primeira ocorrência de um valor específico da matriz
     *
     * @param valueToRemove Valor cuja primeira ocorrência será removida
     * @return Array bidimensional que inclui a matriz original menos o valor removido
     */
    public TwoDimensionalArray removeFirstOccurrenceOfAValue(int valueToRemove) {
        TwoDimensionalArray twoDimArray = createCopy();
        boolean valueIsRemoved = false;

        for (int i = 0; i < twoDimArray.twoDimensionalArray.length; i++) {
            Array row = new Array(twoDimArray.twoDimensionalArray[i].toArray());
            if (!valueIsRemoved && row.countOccurrencesOfAValue(valueToRemove) > 0) {
                row.removeFirstOccurrenceValueFromArray(valueToRemove);
                valueIsRemoved = true;
                twoDimArray.twoDimensionalArray[i] = row;
            }
        }
        if (!valueIsRemoved) throw new IllegalArgumentException("There is no such value in the array");

        return twoDimArray;
    }

//----------------------------------------------------------------------------------------------------------------------

    /**
     * O método verifica se a matriz está vazia
     *
     * @return True, se a matriz estiver vazia, false se não estiver.
     */
    public boolean isMatrixEmpty() {
        int emptyRowsCounter = 0;
        for (int i = 0; i < this.twoDimensionalArray.length; i++) {
            Array row = new Array(this.twoDimensionalArray[i].toArray());
            if (row.isArrayEmpty()) {
                emptyRowsCounter++;
            }
        }
        return (emptyRowsCounter == this.twoDimensionalArray.length);
    }
//----------------------------------------------------------------------------------------------------------------------

    /**
     * O método obtém o elemento de maior valor da matriz
     *
     * @return Elemento de maior valor da matriz
     */
    public int getBiggestValueFromMatrix() {
        int biggestValue = 0;
        int biggestValueInARow = 0;
        for (int i = 0; i < this.twoDimensionalArray.length; i++) {
            Array row = new Array(this.twoDimensionalArray[i].toArray());
            if (!row.isArrayEmpty()) {
                biggestValueInARow = row.getBiggestValue();
                if (biggestValue < biggestValueInARow) {
                    biggestValue = biggestValueInARow;
                }
            }
        }
        return biggestValue;
    }
//----------------------------------------------------------------------------------------------------------------------

    /**
     * O método obtém o elemento de menor valor da matriz
     *
     * @return Elemento de menor valor da matriz
     */
    public int getSmallestValueFromMatrix() {
        int smallestValue = 0;
        int smallestValueInARow = 0;

        Array row = new Array(this.twoDimensionalArray[0].toArray());
        if(!row.isArrayEmpty()) {
            smallestValue = row.getSmallestValue();
        }

        for (int i = 1; i < this.twoDimensionalArray.length; i++) {
            row = new Array(this.twoDimensionalArray[i].toArray());
            if (!row.isArrayEmpty()) {
                smallestValueInARow = row.getSmallestValue();
                if (smallestValue > smallestValueInARow) {
                    smallestValue = smallestValueInARow;
                }
            }
        }
        return smallestValue;
    }

//----------------------------------------------------------------------------------------------------------------------

    /**
     * O método obtém a média dos elementos da matriz
     *
     * @return Média dos elementos da matriz
     */
    public double getMatrixElementsMean() {
        if (this.isMatrixEmpty()) {
            throw new IllegalArgumentException("The Two Dimensional Array can't be empty.");
        }
        double elementSum = 0;

        for (int i = 0; i < this.twoDimensionalArray.length; i++) {
            Array row = new Array(this.twoDimensionalArray[i].toArray());
            elementSum = elementSum + Utilities.addArrayElements(row.toArray());
        }
        double elementsMean = elementSum / this.countNrElements();
        BigDecimal bd = new BigDecimal(Double.toString(elementsMean));
        bd = bd.setScale(2, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

//----------------------------------------------------------------------------------------------------------------------
    /**
     * O método obtém o número de elementos da matriz
     *
     * @return número de elementos de uma matriz
     */
    private int countNrElements() {
        int elementCounter = 0;
        for (int i = 0; i < this.twoDimensionalArray.length; i++) {
            for (int j = 0; j < this.twoDimensionalArray[i].toArray().length; j++) {
                elementCounter++;
            }
        }
        return elementCounter;
    }
//----------------------------------------------------------------------------------------------------------------------

    /**
     * O método obtém a soma dos valores de cada linha da matriz
     *
     * @return Array com os valores das somas dos elementos de cada linha da matriz
     */
    public Array getSumOfEachMatrixRow() {
        int[] sumOfEachMatrixRowValues = new int[this.twoDimensionalArray.length];

        int sumOfRowElements = 0;

        for (int i = 0; i < this.twoDimensionalArray.length; i++) {
            Array row = new Array(this.twoDimensionalArray[i].toArray());
            sumOfRowElements = Utilities.addArrayElements(row.toArray());
            sumOfEachMatrixRowValues[i] = sumOfRowElements;
        }
        Array sumOfEachMatrixRow = new Array(sumOfEachMatrixRowValues);
        return sumOfEachMatrixRow;
    }

//----------------------------------------------------------------------------------------------------------------------

    /**
     * O método obtém a soma dos valores de cada coluna da matriz
     *
     * @return Array com os valores das somas de elementos de cada coluna da matriz
     */
    public Array getSumOfEachMatrixColumn() {

        //Método para descobrir a maior coluna
        int longestRowLength = getLengthOfLongestRow();
        int[] sumOfEachMatrixColumnValues = new int[longestRowLength];
        int nrOfRows = this.twoDimensionalArray.length;
        for (int i = 0; i < nrOfRows; i++) {
            int[] column = this.twoDimensionalArray[i].toArray();
            for (int j = 0; j < column.length; j++) {
                sumOfEachMatrixColumnValues[j] = sumOfEachMatrixColumnValues[j] + column[j];
            }
        }
        Array sumOfEachMatrixColumn = new Array(sumOfEachMatrixColumnValues);
        return sumOfEachMatrixColumn;
    }

//----------------------------------------------------------------------------------------------------------------------

    /**
     * O método obtém o comprimento da coluna com mais elementos da matriz
     *
     * @return Valor do comprimento da coluna com mais elementos da matriz
     */
    private int getLengthOfLongestRow() {
        int rowSize = 0;
        int longestRowLength = 0;
        for (int i = 0; i < this.twoDimensionalArray.length; i++) {
            rowSize = this.twoDimensionalArray[i].toArray().length;
            if (rowSize > longestRowLength) {
                longestRowLength = rowSize;
            }
        }
        return longestRowLength;
    }
//----------------------------------------------------------------------------------------------------------------------

    /**
     * O método obtém o índice da coluna com o maior valor da soma dos seus elementos
     *
     * @return Índice da coluna com o maior valor da soma dos seus elementos
     */
    public int getIndexOfRowWithHighestValue() {
        Array sumOfEachMatrixRow = getSumOfEachMatrixRow();
        int highestValue = sumOfEachMatrixRow.toArray()[0];
        int indexOfHighestValue = 0;
        int rowLength = sumOfEachMatrixRow.toArray().length;
        for (int i = 0; i < rowLength; i++) {
            if (sumOfEachMatrixRow.toArray()[i] > highestValue) {
                indexOfHighestValue = i;
                highestValue = sumOfEachMatrixRow.toArray()[i];
            }
        }
        return indexOfHighestValue;
    }

//----------------------------------------------------------------------------------------------------------------------

    /**
     * O método verifica se a matriz é quadrada
     *
     * @return True, se a matriz for quadrada, falso se não for
     */
    public boolean isMatrixSquare() {
        return Utilities.verifyIfMatrixIsSquare(this.toArray());
    }

//----------------------------------------------------------------------------------------------------------------------

    /**
     * O método verifica se a matriz é simétrica, ou seja, igual à sua matriz transposta
     *
     * @return True, se a matriz for simétrica, falso se não for
     */
    public boolean isMatrixSymmetric() {
        if (!this.isMatrixSquare()) {
            throw new IllegalArgumentException("A matriz tem de ser quadrada para poder ser simétrica.");
        }
        TwoDimensionalArray transposedMatrix = Utilities.getTransposedMatrix(this);
        return transposedMatrix.equals(this);
    }

//----------------------------------------------------------------------------------------------------------------------

    /**
     * O método conta o número de elementos da diagonal principal, se a matriz for quadrada
     *
     * @return -1 se a matriz não for quadrada ou o número de elementos da diagonal principal da matriz, se esta for quadrada
     */
    public int countNrMainDiagonalElements() {
        if (!isMatrixSquare()) {
            return -1;
        }
        // ou   int nrOfElementsMainDiagonal = Utilities.countNumberOfMainDiagonalElements(this);

        return this.twoDimensionalArray.length;
    }

//----------------------------------------------------------------------------------------------------------------------

    /**
     * O método verifica se as diagonais primária e secondária de uma matriz são iguais
     *
     * @return True, se forem iguais, false se não forem
     */
    public Boolean isMainDiagonalEqualToSecondary() {
        Array mainDiagonal = Utilities.getMatrixMainDiagonal(this);
        Array secondaryDiagonal = Utilities.getMatrixSecondaryDiagonal(this);
        return (mainDiagonal != null && mainDiagonal.equals(secondaryDiagonal));
    }
//----------------------------------------------------------------------------------------------------------------------

    /**
     * O método obtém os elementos da matriz que têm um maior número de dígitos que a média
     *
     * @return Array com os elementos da matriz que têm um maior número de dígitos que a média
     */
    public Array getElementsWithMoreDigitsThanAverage() {
        int nrOfElements = Utilities.countNrElements(this.toArray());

        double totalNrOfDigits = countDigitsOfAllElements();
        double totalNrDigitsMean = totalNrOfDigits / nrOfElements;

        int[] finalArrayValues = new int[nrOfElements];
        int countNrElementsWithNrOfDigitsGreaterThenAverage = 0;

        for (int i = 0; i < twoDimensionalArray.length; i++) {
            for (int k = 0; k < twoDimensionalArray[i].toArray().length; k++) {
                if (Utilities.getDigitsCountFromNumber(twoDimensionalArray[i].toArray()[k]) > totalNrDigitsMean) {
                    finalArrayValues[countNrElementsWithNrOfDigitsGreaterThenAverage] = twoDimensionalArray[i].toArray()[k];
                    countNrElementsWithNrOfDigitsGreaterThenAverage++;
                }
            }
        }
        int[] elementsWithNrOfDigitsGreaterThanAverageValues = new int[countNrElementsWithNrOfDigitsGreaterThenAverage];
        System.arraycopy(finalArrayValues, 0, elementsWithNrOfDigitsGreaterThanAverageValues, 0, countNrElementsWithNrOfDigitsGreaterThenAverage);

        return new Array(elementsWithNrOfDigitsGreaterThanAverageValues);
    }
//----------------------------------------------------------------------------------------------------------------------

    /**
     * O método obtém a soma total dos dígitos de todos os elementos da matriz
     *
     * @return Soma total dos dígitos de todos os elementos da matriz
     */
    private double countDigitsOfAllElements() {
        int digitsCounter = 0;
        double sumNrOfAllDigits = 0;
        TwoDimensionalArray twoDimArray = new TwoDimensionalArray(twoDimensionalArray);

        for (int i = 0; i < this.twoDimensionalArray.length; i++) {
            for (int j = 0; j < this.twoDimensionalArray[i].toArray().length; j++) {
                digitsCounter = Utilities.getDigitsCountFromNumber(twoDimArray.toArray()[i][j]);
                sumNrOfAllDigits = sumNrOfAllDigits + digitsCounter;
            }
        }
        return sumNrOfAllDigits;
    }
//----------------------------------------------------------------------------------------------------------------------

    /**
     * O método obtém os elementos que têm uma maior percentagem de dígitos pares do que a média
     *
     * @return Array com os elementos que têm uma maior percentagem de dígitos pares do que a média
     */
    public Array getElementsWithAHigherPercentageOfEvenDigitsThanAverage() {
        double meanOfPercentageOfEvenDigitsInAMatrix = getMeanOfPercentageOfEvenDigitsInAMatrix();
        int nrOfElementsInTheMatrix = Utilities.countNrElements(this.toArray());
        int[] auxiliaryArray = new int[nrOfElementsInTheMatrix];
        int auxArrayIndex = 0;
        for (int i = 0; i < this.twoDimensionalArray.length; i++) {
            Array row = new Array(this.twoDimensionalArray[i].toArray());
            for (int j = 0, k = 0; j < this.twoDimensionalArray[i].toArray().length; j++) {
                double percentageOfEvenDigitsInANumber = row.getPercentageOfEvenDigitsInANumber(this.twoDimensionalArray[i].toArray()[j]);
                if (percentageOfEvenDigitsInANumber > meanOfPercentageOfEvenDigitsInAMatrix) {
                    auxiliaryArray[auxArrayIndex] = this.twoDimensionalArray[i].toArray()[j];
                    auxArrayIndex++;
                }
            }
        }
        int[] elementsWithAHigherPercentageOfEvenDigitsThanAverageValues = new int[auxArrayIndex];
        System.arraycopy(auxiliaryArray, 0, elementsWithAHigherPercentageOfEvenDigitsThanAverageValues, 0, auxArrayIndex);
        return new Array(elementsWithAHigherPercentageOfEvenDigitsThanAverageValues);
    }

//----------------------------------------------------------------------------------------------------------------------

    /**
     * O método obtém a média das percentagens dos dígitos pares na matriz
     *
     * @return Valor da média das percentagens dos dígitos pares na matriz
     */
    private double getMeanOfPercentageOfEvenDigitsInAMatrix() {
        double meanOfPercentagesOfEvenDigitsInARow = 0;
        double meanOfPercentagesOfEvenDigitsTotal = 0;

        for (int i = 0; i < this.twoDimensionalArray.length; i++) {
            Array row = new Array(this.twoDimensionalArray[i].toArray());
            meanOfPercentagesOfEvenDigitsInARow = row.getMeanOfPercentagesOfEvenDigits();
            meanOfPercentagesOfEvenDigitsTotal = meanOfPercentagesOfEvenDigitsTotal + meanOfPercentagesOfEvenDigitsInARow;
        }
        return meanOfPercentagesOfEvenDigitsTotal = meanOfPercentagesOfEvenDigitsTotal / this.twoDimensionalArray.length;
    }

//----------------------------------------------------------------------------------------------------------------------

    /**
     * O método inverte a ordem dos valores de cada coluna de uma matriz
     *
     * @return Array bidimensional com os valores de cada coluna da matriz invertidos
     */
    public TwoDimensionalArray reverseColumnElements() {
        int[][] reversedColumnMatrixValues = new int[this.twoDimensionalArray.length][];

        for (int i = 0; i < this.twoDimensionalArray.length; i++) {
            reversedColumnMatrixValues[i] = new int[this.twoDimensionalArray[i].toArray().length];
            for (int j = (this.twoDimensionalArray[i].toArray().length - 1), k = 0; j >= 0; j--, k++) {
                reversedColumnMatrixValues[i][k] = this.twoDimensionalArray[i].toArray()[j];
            }
        }
        return new TwoDimensionalArray(reversedColumnMatrixValues);
    }

//----------------------------------------------------------------------------------------------------------------------

    /**
     * O método inverte a ordem dos valores de cada linha de uma matriz
     *
     * @return Array bidimensional com os valores de cada linha da matriz invertidos
     */
    public TwoDimensionalArray reverseRowElements() {
        int[][] reversedRowsMatrixValues = new int[this.twoDimensionalArray.length][];

        for (int i = this.twoDimensionalArray.length - 1, k = 0; i >= 0; i--, k++) {
            reversedRowsMatrixValues[k] = new int[this.twoDimensionalArray[i].toArray().length];
            for (int j = 0; j < this.twoDimensionalArray[i].toArray().length; j++) {
                reversedRowsMatrixValues[k][j] = this.twoDimensionalArray[i].toArray()[j];
            }
        }
        return new TwoDimensionalArray(reversedRowsMatrixValues);
    }

//----------------------------------------------------------------------------------------------------------------------

    /**
     * O método roda uma matriz noventa graus (no sentido contrário ao dos ponteiros do relógio)
     *
     * @return Array bidimensional corespondente à matriz original, rodada noventa graus
     */
    public TwoDimensionalArray rotateMatrixNinetyDegrees() {
        if (Utilities.getNrColumnsIfEveryMatrixRowIsTheSameLength(this.toArray()) == -1) {
            throw new IllegalArgumentException("A matriz tem de ter o mesmo número de colunas em todas as linhas.");
        }
        int nrColumnsOriginalMatrix = this.twoDimensionalArray[0].toArray().length;
        int nrRowsOriginalMatrix = this.twoDimensionalArray.length;
        int[][] ninetyDegreesRotatedMatrixValues = new int[nrColumnsOriginalMatrix][nrRowsOriginalMatrix];

        for (int i = 0; i < nrRowsOriginalMatrix; i++) {
            for (int j = nrColumnsOriginalMatrix - 1, k = 0; j >= 0; j--, k++) {
                ninetyDegreesRotatedMatrixValues[k][i] = this.twoDimensionalArray[i].toArray()[j];
            }
        }
        return new TwoDimensionalArray(ninetyDegreesRotatedMatrixValues);
    }
//----------------------------------------------------------------------------------------------------------------------

    /**
     * O método roda uma matriz 180 graus
     *
     * @return Array bidimensional corespondente à matriz original, rodada 180 graus
     */
    public TwoDimensionalArray rotateMatrixAHundredAndEightyDegrees() {
        if (Utilities.getNrColumnsIfEveryMatrixRowIsTheSameLength(this.toArray()) == -1) {
            throw new IllegalArgumentException("A matriz tem de ter o mesmo número de colunas em todas as linhas.");
        }
        int nrColumnsOriginalMatrix = this.twoDimensionalArray[0].toArray().length;
        int nrRowsOriginalMatrix = this.twoDimensionalArray.length;
        int[][] aHundredAndEightyDegreesRotatedMatrixValues = new int[nrRowsOriginalMatrix][nrColumnsOriginalMatrix];

        for (int i = nrRowsOriginalMatrix - 1, l = 0; i >= 0; i--, l++) {
            for (int j = nrColumnsOriginalMatrix - 1, k = 0; j >= 0; j--, k++) {
                aHundredAndEightyDegreesRotatedMatrixValues[l][k] = this.twoDimensionalArray[i].toArray()[j];
            }
        }
        return new TwoDimensionalArray(aHundredAndEightyDegreesRotatedMatrixValues);
    }

//----------------------------------------------------------------------------------------------------------------------

    /**
     * O método roda uma matriz menos noventa graus (no sentido dos ponteiros do relógio)
     *
     * @return Array bidimensional corespondente à matriz original, rodada menos noventa graus
     */
    public TwoDimensionalArray rotateMatrixMinusNinetyDegrees() {
        if (Utilities.getNrColumnsIfEveryMatrixRowIsTheSameLength(this.toArray()) == -1) {
            throw new IllegalArgumentException("A matriz tem de ter o mesmo número de colunas em todas as linhas.");
        }
        int nrColumnsOriginalMatrix = this.twoDimensionalArray[0].toArray().length;
        int nrRowsOriginalMatrix = this.twoDimensionalArray.length;
        int[][] minusNinetyDegreesRotatedMatrixValues = new int[nrColumnsOriginalMatrix][nrRowsOriginalMatrix];

        for (int i = nrRowsOriginalMatrix - 1, k = 0; i >= 0; i--, k++) {
            for (int j = 0; j < nrColumnsOriginalMatrix; j++) {
                minusNinetyDegreesRotatedMatrixValues[j][k] = this.twoDimensionalArray[i].toArray()[j];
            }
        }
        return new TwoDimensionalArray(minusNinetyDegreesRotatedMatrixValues);
    }
}
