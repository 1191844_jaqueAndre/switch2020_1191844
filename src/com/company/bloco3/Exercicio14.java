package com.company.bloco3;

import java.util.Scanner;

public class Exercicio14 {

    public static void main(String[] args) {

        Scanner read = new Scanner(System.in);
        double amountEuros = 1;
        double exchangeRate = 0;

        while (amountEuros > 0) {

            System.out.println("Insira o valor em Euros");
            amountEuros = read.nextDouble();

            String exchangeOption = "";

            if (amountEuros > 0) {
                System.out.println("Insira a opção de câmbio: D (dólar), L (libra), I (iene), C (coroa sueca) ou F (franco suíço)");
                exchangeOption = read.next();

                while (!exchangeOption.equals("D") && !exchangeOption.equals("L") && !exchangeOption.equals("I") && !exchangeOption.equals("C") && !exchangeOption.equals("F")) {
                    System.out.println("Por favor, insira uma opção válida para o câmbio");
                    exchangeOption = read.next();
                }
            }

            switch (exchangeOption) {
                case "D":
                    exchangeRate = amountEuros * 1.534;
                    System.out.println(exchangeRate + " dólares");
                    break;
                case "L":
                    exchangeRate = amountEuros * 0.774;
                    System.out.println(exchangeRate + " libras");
                    break;
                case "I":
                    exchangeRate = amountEuros * 161.480;
                    System.out.println(exchangeRate + " ienes");
                    break;
                case "C":
                    exchangeRate = amountEuros * 9.593;
                    System.out.println(exchangeRate + " coroas suecas");
                    break;
                case "F":
                    exchangeRate = amountEuros * 1.601;
                    System.out.println(exchangeRate + " francos suíços");
                    break;
            }

        }
    }
}
