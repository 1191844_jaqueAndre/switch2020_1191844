package com.company.bloco3;

import java.util.Scanner;

public class Exercicio9 {

    public static void main(String[] args) {

        Scanner read = new Scanner(System.in);
        int extraHours = 0;
        double sumMonthSalaries = 0;
        int nrWorkers = 0;

        while (extraHours != -1) {

            System.out.println("Insira o número de horas extraordinárias do funcionário");
            extraHours = read.nextInt();
            while (extraHours < -2) {
                System.out.println("Por favor insira um valor válido");
                extraHours = read.nextInt();
            }
            if (extraHours != -1) {
                System.out.println("Insira o valor do salário de base do funcionário");
                double baseSalary = read.nextDouble();
                while (baseSalary < 0) {
                    System.out.println("Por favor insira um valor válido");
                    baseSalary = read.nextInt();
                }

                nrWorkers++;

                double monthlySalary = ((0.02 * baseSalary * extraHours) + baseSalary);
                System.out.println("O salário mensal do funcionário será " + monthlySalary);

                sumMonthSalaries = sumMonthSalaries + monthlySalary;
            }
        }
        double meanSalariesPerMonth = sumMonthSalaries / nrWorkers;
        System.out.println("A média dos salários dos funcionários no corrente mês é " + meanSalariesPerMonth);

    }

}

