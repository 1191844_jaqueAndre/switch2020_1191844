package com.company.bloco3;

import java.util.Scanner;

public class Exercicio13 {

    public static void main(String[] args) {

        Scanner read = new Scanner(System.in);
        int productCode = 1;

        while (productCode != 0) {
            productCode = read.nextInt();
            if (productCode == 1) {
                System.out.println("Alimento não perecível");
            } else if (productCode >= 2 && productCode <= 4) {
                System.out.println("Alimento perecível");
            } else if (productCode >= 5 && productCode <= 6) {
                System.out.println("Vestuário");
            } else if (productCode == 7) {
                System.out.println("Higiene pessoal");
            } else if (productCode >= 8 && productCode <= 15) {
                System.out.println("Higiene pessoal");
            } else {
                System.out.println("Código inválido");
            }
        }
    }

}
