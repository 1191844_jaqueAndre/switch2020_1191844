package com.company.bloco3;

import javax.jnlp.SingleInstanceListener;
import java.util.ArrayList;

public class Exercicio4 {

    public static void main(String[] args) {

        int inicioIntervalo = 5;
        int fimIntervalo = 10;
        /*4 a)*/
        obterNrsMultiplosDeTres(inicioIntervalo, fimIntervalo);
        /*
        /*4 b)*/
        int nrInteiro = 0;
        obterMultiplosDeUmDadoNrInteiro(inicioIntervalo, fimIntervalo, nrInteiro);

        /*4 c)*/
        obterNrsMultiplosDeTresECinco(inicioIntervalo, fimIntervalo);

        /*4 d)*/
        int nrInteiro1 = 0;
        int nrInteiro2 = 0;
        obterMultiplosDeDoisNrsInteiro(inicioIntervalo, fimIntervalo, nrInteiro1, nrInteiro2);

        /*4 e)*/
        obterSomaDosMultiplosDeDoisNrsInteiro(inicioIntervalo, fimIntervalo, nrInteiro1, nrInteiro2);
    }

    public static String obterNrsMultiplosDeTres(int inicioIntervalo, int fimIntervalo) {
        ArrayList<Integer> seqNumerosA = new ArrayList<>();

        if(fimIntervalo < inicioIntervalo){
            return "O fim do intervalo tem de ser maior do que o início.";
        }else {

            for (int i = inicioIntervalo; i < fimIntervalo; i++) {
                if (i % 3 == 0) {
                    seqNumerosA.add(i);
                }
            }
            if (seqNumerosA.size() == 0) {
                return "Nenhum dos números da sequência é múltiplo de três";
            } else {
                return "No intervalo dado há " + seqNumerosA.size() + " múltiplo(s) de 3";
            }
        }
    }

   public static String obterMultiplosDeUmDadoNrInteiro(int inicioIntervalo, int fimIntervalo, int nrInteiro) {
        ArrayList<Integer> seqNumerosB = new ArrayList<>();

       if(fimIntervalo < inicioIntervalo){
           return "O fim do intervalo tem de ser maior do que o início.";
       }else {
           for (int i = inicioIntervalo; i < fimIntervalo; i++) {
               if (i % nrInteiro == 0) {
                   seqNumerosB.add(i);
               }
           }

           if (seqNumerosB.size() == 0) {
               return "Nenhum dos números da sequência é múltiplo de " + nrInteiro;
           } else {
               return "No intervalo dado há " + seqNumerosB.size() + " múltiplo(s) de " + nrInteiro;
           }
       }
    }

    public static String obterNrsMultiplosDeTresECinco(int inicioIntervalo, int fimIntervalo) {
        ArrayList<Integer> seqNumerosC = new ArrayList<>();

        if (fimIntervalo < inicioIntervalo) {
            return "O fim do intervalo tem de ser maior do que o início.";
        } else {
            for (int i = inicioIntervalo; i < fimIntervalo; i++) {
                if (i % 3 == 0 && i % 5 == 0) {
                    seqNumerosC.add(i);
                }
            }

            if (seqNumerosC.size() == 0) {
                return "Nenhum dos números da sequência é múltiplo de três e cinco";
            } else {
                return "No intervalo dado há " + seqNumerosC.size() + " múltiplo(s) de três e cinco";
            }
        }
    }

    public static String obterMultiplosDeDoisNrsInteiro(int inicioIntervalo, int fimIntervalo, int nrInteiro1, int nrInteiro2) {
        ArrayList<Integer> seqNumerosD = new ArrayList<>();

        if (fimIntervalo < inicioIntervalo) {
            return "O fim do intervalo tem de ser maior do que o início.";
        } else {
            for (int i = inicioIntervalo; i < fimIntervalo; i++) {
                if (i % nrInteiro1 == 0 && i % nrInteiro2 == 0) {
                    seqNumerosD.add(i);
                }
            }

            if (seqNumerosD.size() == 0) {
                return "Nenhum dos números da sequência é múltiplo de " + nrInteiro1 + " e de " + nrInteiro2;
            } else {
                return "No intervalo dado há " + seqNumerosD.size() + " múltiplo(s) de " + nrInteiro1 + " e de " + nrInteiro2;
            }
        }
    }

    public static String obterSomaDosMultiplosDeDoisNrsInteiro(int inicioIntervalo, int fimIntervalo, int nrInteiro1, int nrInteiro2) {
        ArrayList<Integer> seqNumerosE = new ArrayList<>();
        int somaDosMultiplos = 0;

        if (fimIntervalo < inicioIntervalo) {
            return "O fim do intervalo tem de ser maior do que o início.";
        } else {
            for (int i = inicioIntervalo; i < fimIntervalo; i++) {
                if (i % nrInteiro1 == 0 && i % nrInteiro2 == 0) {
                    seqNumerosE.add(i);
                    somaDosMultiplos = somaDosMultiplos + i;
                }
            }

            if (seqNumerosE.size() == 0) {
                return "Nenhum dos números da sequência é múltiplo de " + nrInteiro1 + " e de " + nrInteiro2;
            } else {
                return "A soma dos múltiplos de " + nrInteiro1 + " e " + nrInteiro2 + " é " + somaDosMultiplos;
            }
        }
    }



}
