package com.company.bloco3;

public class Exercicio6 {

    public static void main(String[] args) {

        long num = 32432653L;
        NrDigitsLongNr(num);
        NrEvenDigitsLongNr(num);
        NrOddDigitsLongNr(num);
        AddDigitsLongNr(num);
        AddEvenDigitsLongNr(num);
        AddOddDigitsLongNr(num);
        getMeanDigitsLongNr(num);
        meanEvenDigitsLongNr(num);
        meanOddDigitsLongNr(num);
        long number = 32432653L;
        getReversedNumber(number);


    }

//--------------Exercício 6 a) -----------------------------------------------------------------------------------------

    public static int NrDigitsLongNr(long num) {
        int nrDig = 0;

        while (num != 0) {
            num = num / 10;
            nrDig++;
        }
        return nrDig;
    }

//--------------Exercício 6 b) -----------------------------------------------------------------------------------------

    public static int NrEvenDigitsLongNr(long num) {
        int nrDig = 0;

        while (num != 0) {
            if(num % 2 == 0) {
                nrDig++;
                num = num / 10;
            }else{
                num = num / 10;
            }
        }
        return nrDig;
    }

//--------------Exercício 6 c) -----------------------------------------------------------------------------------------

    public static int NrOddDigitsLongNr(long num) {
        int nrDig = 0;

        while (num != 0) {
            if(num % 2 != 0) {
                nrDig++;
                num = num / 10;
            }else{
                num = num / 10;
            }
        }
        return nrDig;
    }

//--------------Exercício 6 d) -----------------------------------------------------------------------------------------

    public static int AddDigitsLongNr(long num){

        long addDigits = 0;

        while (num != 0){
            addDigits = addDigits + (num % 10);
            num /= 10;
        }

        return (int) addDigits;
    }

//--------------Exercício 6 e) -----------------------------------------------------------------------------------------

    public static int AddEvenDigitsLongNr(long num){

        long digit = 0;
        long addEvenDigits = 0;

        while (num != 0){
            digit = num % 10;
            num /= 10;
            if(digit % 2 == 0){
                addEvenDigits = addEvenDigits + digit;
            }
        }

        return (int) addEvenDigits;
    }

//--------------Exercício 6 f) -----------------------------------------------------------------------------------------

    public static int AddOddDigitsLongNr (long num){

        long digit = 0;
        long addOddDigits = 0;

        while (num != 0){
            digit = num % 10;
            num /= 10;
            if(digit % 2 != 0){
                addOddDigits = addOddDigits + digit;
            }
         }

        return (int) addOddDigits;
    }

//--------------Exercício 6 g) -----------------------------------------------------------------------------------------

    public static String getMeanDigitsLongNr(long num){

        double addDigits = 0;
        double counter = 0;

        while (num != 0){
            addDigits = addDigits + (num % 10);
            counter++;
            num /= 10;
        }
        if (num == 0){
            return "0,00";
        }
        if (counter == 0){
            return "O número não pode ter zero dígitos";
        }

        return String.format("%.2f", addDigits / counter);
    }

//--------------Exercício 6 h) -----------------------------------------------------------------------------------------

    public static String meanEvenDigitsLongNr(long num){

        long digit = 0;
        double addEvenDigits = 0;
        double counter = 0;

        while (num != 0){
            digit = num % 10;
            num /= 10;
            if(digit % 2 == 0){
                counter ++;
                addEvenDigits = addEvenDigits + digit;
            }
        }


        if (counter == 0){
            return "O número tem zero dígitos ou não tem algarismos pares";
        }

        return String.format("%.2f", addEvenDigits / counter);
    }

//--------------Exercício 6 i) -----------------------------------------------------------------------------------------

    public static String meanOddDigitsLongNr (long num){

        long digit = 0;
        double addOddDigits = 0;
        double counter = 0;

        while (num != 0){
            digit = num % 10;
            num /= 10;
            if(digit % 2 != 0){
                counter ++;
                addOddDigits = addOddDigits + digit;
            }
        }


        if (counter == 0){
            return "O número tem zero dígitos ou não tem algarismos ímpares";
        }

        return String.format("%.2f", addOddDigits / counter);
    }

//--------------Exercício 6 i) -----------------------------------------------------------------------------------------

    public static long getReversedNumber (long number){
        long reverseNumber = 0;

        while (number != 0){
            reverseNumber = reverseNumber * 10;
            reverseNumber = reverseNumber + number%10;
            number = number/10;
        }
        return reverseNumber;
    }
}
