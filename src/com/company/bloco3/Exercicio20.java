package com.company.bloco3;

public class Exercicio20 {

    public static void main(String[] args) {

    }

    public static String classifyNumbers (int number){

        int divisorSum = 0;

        if(number < 0){
            return "O número não pode ser negativo";
        }

        for (int i = 1; i < number; i++){
            if (number % i == 0){
                divisorSum = divisorSum + i;
            }
        }

        if(divisorSum > number){
            return "O número é abundante";
        }else if (divisorSum == number){
            return "O número é perfeito";
        }else {
            return "O número é reduzido";
        }
    }
}
