package com.company.bloco3;

import java.util.Scanner;

public class Exercicio8 {

    public static void main(String[] args) {

        Scanner read = new Scanner(System.in);
        System.out.println("Introduza o valor ao qual deve ser inferior a soma dos valores positivos");
        int sumLimit = read.nextInt();
        int sum = 0;
        int smallestNumber = 0;
        int positiveNumber = 0;

        System.out.println("Introduza uma sequência de números positivos, premindo a ENTER entre eles");

        while (sum < sumLimit) {
            positiveNumber = read.nextInt();
                while (positiveNumber <=0){
                    System.out.println("Por favor insira apenas números positivos.");
                    positiveNumber = read.nextInt();
                }
            sum = sum + positiveNumber;

            if (smallestNumber == 0 || positiveNumber < smallestNumber) {
                smallestNumber = positiveNumber;
            }
        }
        System.out.println("O menor número é " + smallestNumber);

    }

}
