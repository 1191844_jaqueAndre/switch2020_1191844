package com.company.bloco3;

import java.util.Scanner;

 public class Exercicio2 {

    public static void main(String[] args) {

        Scanner ler = new Scanner(System.in);
        System.out.println("Introduza o número de alunos");
        int nrAlunos = ler.nextInt();

        if (nrAlunos < 0) {
            System.out.println("O número de alunos não pode ser negativo.");
        } else {

            int[] notasAlunos = new int[nrAlunos];

            /*Assumindo que as notas são introduzidas pelo utilizador

            System.out.println("Introduza as notas de cada aluno, premindo ENTER após cada uma");
            for (int i = 0; i < nrAlunos; i++) {
                notasAlunos[i] = ler.nextInt();
            } */

            //Assumindo que as notas são geradas aleatóriamente

            for (int i = 0; i < nrAlunos; i++) {
                notasAlunos[i] = (int) (Math.random() * (20 + 1));
            }


            //Para imprimir as notas do array
            System.out.println("Abaixo seguem as notas dos alunos na disciplina X");

            for (int i = 0; i < nrAlunos; i++) {
                System.out.println(notasAlunos[i]);
            }

            //Para calcular a porcentagem de notas positivas

            int nrNotasPositivas = 0;

            for (int i = 0; i < nrAlunos; i++) {
                if (notasAlunos[i] >= 10) {
                    nrNotasPositivas++;
                }
            }

            double porcentagemNotasPositivas = ((double) nrNotasPositivas / notasAlunos.length) * 100;

            System.out.println("A porcentagem de notas positivas é: " + String.format("%.2f", porcentagemNotasPositivas));

            //Para calcular a média das notas negativas

            double somaNotasNegativas = 0;
            int counterNotasNegativas = 0;

            for (int i = 0; i < nrAlunos; i++) {
                if (notasAlunos[i] < 10) {
                    counterNotasNegativas++;
                    somaNotasNegativas = somaNotasNegativas + notasAlunos[i];
                }
            }

            if (counterNotasNegativas == 0){
                System.out.println("Não há notas negativas");
            }else{
                double mediaNotasNegativas = somaNotasNegativas / counterNotasNegativas;
                System.out.println("A média das notas negativas é: " + String.format("%.2f", mediaNotasNegativas));
            }





        }
    }

    // b) Para ser possível realizar testes de regressão automática neste exercício, seria necessário que o valor da
    // variável nrAlunos pudesse ser introduzida pelo desenvolvedor e que o código estivesse estruturado em métodos.
}

