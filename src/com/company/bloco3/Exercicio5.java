package com.company.bloco3;

import java.util.Scanner;

public class Exercicio5 {

    public static void main(String[] args) {

        /*5 a)*/
        int inicioIntervalo = 5;
        int fimIntervalo = 12;
        obterSomaDeNrsPares(inicioIntervalo, fimIntervalo);

        /*5 b)*/
        obterQuantidadeDeNrsPares(inicioIntervalo, fimIntervalo);

        /*5 c)*/
        obterSomaDeNrsImpares(inicioIntervalo, fimIntervalo);

        /*5 d)*/
        obterQuantidadeDeNrsImpares(inicioIntervalo, fimIntervalo);

        /*5 e)*/
        int num = 9;
        obterSomaDeNrsMultiplos(inicioIntervalo, fimIntervalo, num);

        /*5 f)*/
        obterProdutoDeNrsMultiplos(inicioIntervalo, fimIntervalo, num);

        /*5 g)*/
        obterMediaDeNrsMultiplos(inicioIntervalo, fimIntervalo, num);

        /*5 h)*/
        int x = 3;
        int y = 5;
        obterMediaDeNrsMultiplosXY(inicioIntervalo, fimIntervalo, x, y);

    }

//--------------Exercício 5 a) -----------------------------------------------------------------------------------------

    public static int obterSomaDeNrsPares(int inicioIntervalo, int fimIntervalo) {
        int somaNrsPares = 0;
        if (inicioIntervalo > fimIntervalo) {
            return -1;
        } else {
            for (int i = inicioIntervalo; i < fimIntervalo; i++) {
                if (i % 2 == 0) {
                    somaNrsPares = somaNrsPares + i;
                }
            }
        }

        return somaNrsPares;
    }

//--------------Exercício 5 b) -----------------------------------------------------------------------------------------

    public static int obterQuantidadeDeNrsPares(int inicioIntervalo, int fimIntervalo) {
        int contadorNrsPares = 0;

        if (inicioIntervalo > fimIntervalo) {
            return -1;
        } else {
            for (int i = inicioIntervalo; i < fimIntervalo; i++) {
                if (i % 2 == 0) {
                    contadorNrsPares++;

                }
            }
        }

        return contadorNrsPares;
    }

//--------------Exercício 5 c) -----------------------------------------------------------------------------------------

    public static int obterSomaDeNrsImpares(int inicioIntervalo, int fimIntervalo) {
        int somaNrsImpares = 0;

        if (inicioIntervalo > fimIntervalo) {
            return -1;
        } else {
            for (int i = inicioIntervalo; i < fimIntervalo; i++) {
                if (i % 2 != 0) {
                    somaNrsImpares = somaNrsImpares + i;

                }
            }
        }

        return somaNrsImpares;
    }

//--------------Exercício 5 d) -----------------------------------------------------------------------------------------

    public static int obterQuantidadeDeNrsImpares(int inicioIntervalo, int fimIntervalo) {
        int contadorNrsImpares = 0;

        if (inicioIntervalo > fimIntervalo) {
            return -1;
        } else {
            for (int i = inicioIntervalo; i < fimIntervalo; i++) {
                if (i % 2 != 0) {
                    contadorNrsImpares++;

                }
            }
        }

        return contadorNrsImpares;
    }

//--------------Exercício 5 e) -----------------------------------------------------------------------------------------

    public static int obterSomaDeNrsMultiplos(int inicioIntervalo, int fimIntervalo, int num) {
        int somaDeMultiplos = 0;

        if (inicioIntervalo > fimIntervalo) {
            int temp = inicioIntervalo;
            inicioIntervalo = fimIntervalo;
            fimIntervalo = temp;
        }

        for (int i = inicioIntervalo; i < fimIntervalo; i++) {
            if (i % num == 0) {
                somaDeMultiplos = somaDeMultiplos + i;
            }
        }

        return somaDeMultiplos;
    }

//--------------Exercício 5 f) -----------------------------------------------------------------------------------------

    public static int obterProdutoDeNrsMultiplos(int inicioIntervalo, int fimIntervalo, int num) {
        int produtoDeMultiplos = 1;
        int contadorDeMultiplos = 0;

        if (inicioIntervalo > fimIntervalo) {
            return -1;
        } else {
            for (int i = inicioIntervalo; i < fimIntervalo; i++) {
                if (i % num == 0) {
                    contadorDeMultiplos++;
                    produtoDeMultiplos = produtoDeMultiplos * i;
                }
            }

        }
        if (contadorDeMultiplos == 0) {
            produtoDeMultiplos = 0;
        }

        return produtoDeMultiplos;
    }

//--------------Exercício 5 g) -----------------------------------------------------------------------------------------

    public static double obterMediaDeNrsMultiplos(int inicioIntervalo, int fimIntervalo, int num) {
        double somaDeMultiplos = 0;
        double contadorMultiplos = 0;

        if (inicioIntervalo > fimIntervalo) {
            return -1;
        } else {
            for (int i = inicioIntervalo; i < fimIntervalo; i++) {
                if (i % num == 0) {
                    contadorMultiplos++;
                    somaDeMultiplos = somaDeMultiplos + i;
                }
            }
        }
        if (contadorMultiplos == 0) {
            return -1;
        }

        return (somaDeMultiplos / contadorMultiplos);
    }

//--------------Exercício 5 h) -----------------------------------------------------------------------------------------

    public static double obterMediaDeNrsMultiplosXY(int inicioIntervalo, int fimIntervalo, int x, int y) {

        /*Scanner ler = new Scanner(System.in);
        System.out.println("Introduza o valor de X");
        x = ler.nextInt();
        System.out.println("Introduza o valor de Y");
        y = ler.nextInt();*/

        double somaDeMultiplos = 0;
        double contadorMultiplos = 0;

        if (inicioIntervalo > fimIntervalo) {
            return -1;
        } else {
            for (int i = inicioIntervalo; i < fimIntervalo; i++) {
                if (i % x == 0 || i % y == 0) {
                    contadorMultiplos++;
                    somaDeMultiplos = somaDeMultiplos + i;
                }
            }
        }
        if (contadorMultiplos == 0) {
            return -1;
        } else {
            System.out.println(somaDeMultiplos / contadorMultiplos);
            return (somaDeMultiplos / contadorMultiplos);
        }
    }

}



