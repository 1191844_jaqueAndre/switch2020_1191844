package com.company.bloco3;

public class Exercicio7 {

    public static void main(String[] args) {

        long number = 1234554321;
        verifyIfNumberIsPalindrome(number);
        int originalNumber = 32423;
        verifyArmstrongNumber(originalNumber);
        int rangeStart = 40;
        int rangeEnd = 50;
        getFirstPalindrome(rangeStart, rangeEnd);
        getBiggestPalindrome(rangeStart, rangeEnd);
        getNrPalindromes(rangeStart, rangeEnd);
        getFirstArmstrongNumber(rangeStart, rangeEnd);
        getAmountOfArmstrongNumbers(rangeStart, rangeEnd);


    }

//--------------Exercise 7 a) -----------------------------------------------------------------------------------------

    public static boolean verifyIfNumberIsPalindrome(long number) {
        long reverseNumber = 0L;
        long originalNumber = number;

        while (number != 0L) {
            reverseNumber = reverseNumber * 10;
            reverseNumber = reverseNumber + number % 10;
            number = number / 10;
        }
        return (reverseNumber == originalNumber);

    }

//--------------Exercise 7 b) -----------------------------------------------------------------------------------------

    public static boolean verifyArmstrongNumber(int originalNumber) {

        int sum = 0, remainder, digits = 0;

        int number = originalNumber;

        // Count number of digits
        while (number > 0) {
            digits++;
            number = number / 10;
        }

        number = originalNumber;

        while (number != 0) {
            remainder = number % 10;
            sum = (int) (sum + Math.pow(remainder, digits));
            number = number / 10;
        }

        return (originalNumber == sum);

    }

//--------------Exercise 7 c) -----------------------------------------------------------------------------------------

    public static String getFirstPalindrome(int rangeStart, int rangeEnd) {

        if (rangeStart < 10 && rangeEnd > 10) {
            rangeStart = 10;
        } else if (rangeStart < 10 && rangeEnd < 10) {
            return "Não há capicuas no intervalo dado.";
        }

        for (int i = rangeStart; i <= rangeEnd; i++) {

            int reverseNumber = 0;
            int number = i;

            while (number != 0) {
                reverseNumber = reverseNumber * 10;
                reverseNumber = reverseNumber + number % 10;
                number = number / 10;

                if (reverseNumber == i) {
                    return "A primeira capicua é: " + i;
                }
            }

        }

        return "Não há capicuas no intervalo dado.";
    }


//--------------Exercise 7 d) -----------------------------------------------------------------------------------------

    public static String getBiggestPalindrome(int rangeStart, int rangeEnd) {

        if (rangeStart < 10 && rangeEnd > 10) {
            rangeStart = 10;
        } else if (rangeStart < 10 && rangeEnd < 10) {
            return "Não há capicuas no intervalo dado.";
        }

        int palindrome = 0;

        for (int i = rangeStart; i <= rangeEnd; i++) {

            int reverseNumber = 0;
            int number = i;

            while (number != 0) {
                reverseNumber = reverseNumber * 10;
                reverseNumber = reverseNumber + number % 10;
                number = number / 10;

                if (reverseNumber == i) {
                    if (palindrome < reverseNumber) {
                        palindrome = reverseNumber;
                    }
                }
            }
        }
        if (palindrome == 0) {
            return "Não há capicuas no intervalo dado.";
        } else {
            return "A maior capicua é: " + palindrome;
        }
    }

//--------------Exercise 7 e) -----------------------------------------------------------------------------------------

    public static String getNrPalindromes(int rangeStart, int rangeEnd) {

        int palindrome = 0;
        int nrPalindromes = 0;

        for (int i = rangeStart; i <= rangeEnd; i++) {

            int reverseNumber = 0;
            int number = i;

            while (number != 0) {
                reverseNumber = reverseNumber * 10;
                reverseNumber = reverseNumber + number % 10;
                number = number / 10;

                if (reverseNumber == i) {
                    nrPalindromes++;
                }
            }
        }
        if (nrPalindromes == 0) {
            return "Não há capicuas no intervalo dado.";
        } else {
            return "O número de capicuas é: " + nrPalindromes;
        }
    }

//--------------Exercise 7 f) -----------------------------------------------------------------------------------------

    public static String getFirstArmstrongNumber(int rangeStart, int rangeEnd) {


        int i = 0;
        for (i = rangeStart; i <= rangeEnd; i++) {
            int number = i, remainder = 0, digits = 0, sum = 0;

            // Count number of digits
            while (number != 0) {
                digits++;
                number = number / 10;
            }

            number = i;

            while (number != 0) {
                remainder = number % 10;
                sum = (int)(sum + Math.pow(remainder, digits));
                number = number / 10;

                if (sum == i) {
                    return "O primeiro número de Armstrong é: " + i;
                }
            }
        }
            return "Não há números de Armstrong no intervalo dado";
    }

//--------------Exercise 7 g) -----------------------------------------------------------------------------------------

    public static int getAmountOfArmstrongNumbers(int rangeStart, int rangeEnd) {

        int i = 0;
        int armstrongNrCounter = 0;

        for (i = rangeStart; i <= rangeEnd; i++) {
            int number = i, remainder = 0, digits = 0, sum = 0;

            // Count number of digits
            while (number != 0) {
                digits++;
                number = number / 10;
            }

            number = i;

            while (number != 0) {
                remainder = number % 10;
                sum = (int)(sum + Math.pow(remainder, digits));
                number = number / 10;

                if (sum == i) {
                    armstrongNrCounter++;
                }
            }
        }
        if(armstrongNrCounter > 0){
            return armstrongNrCounter;
        }else {
            return -1;
        }
    }
}





