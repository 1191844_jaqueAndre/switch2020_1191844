package com.company.bloco3;

public class Exercicio12 {

    public static void main(String[] args) {

        double a = 0, b = 0, c = 0;
        getEquationSolution(a, b, c);

    }

    public static String getEquationSolution(double a, double b, double c) {

        double x = 0;
        double y = 0;
        double delta = (b * b - 4 * a * c);

        if (a == 0) {
            return "A equação não é de segundo grau";
        }
        if (delta < 0) {
            return "A equação não tem raízes reais";
        } else {
            x = (-b + Math.sqrt(delta)) / (2 * a);
            y = (-b - Math.sqrt(delta)) / (2 * a);

            if (x == y) {
                return "A equação tem uma raíz dupla: x = " + x;
            } else {
                return "A equação tem duas raízes reais: x = " + String.format("%.4f", x) + ", y = " + String.format("%.4f", y);
            }
        }
    }
}
