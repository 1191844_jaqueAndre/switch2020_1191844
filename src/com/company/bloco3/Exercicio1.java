package com.company.bloco3;

import java.util.Scanner;


public class Exercicio1 {

    // a) O objetivo do algoritmo é calcular o fatorial de um número.

    public static void main(String[] args) {

        //Entrada de dados
        Scanner ler = new Scanner(System.in);
        System.out.println("Introduza o valor da variável num");
        int num = ler.nextInt ();
        int res = 1;
        int x;

        //Processamento
        res = obterValorRes(num);

        //Saída de dados
        System.out.println("O resultado é: " + res);

    }

    public static int obterValorRes(int num){
        int res = 1;
        int x;

        for (x = num; x >= 1; x--){
            res = res * x;
        }

        return res;
    }
}
