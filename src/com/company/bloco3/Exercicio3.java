package com.company.bloco3;

import java.util.ArrayList;

public class Exercicio3 {

    public static void main(String[] args) {

        int[] arrayDeNumeros = {1, 2, 5, 9, 5, 324, -1, 87, 345, 23};
        obterSequenciaNumerosPositivos(arrayDeNumeros);
    }

    public static String obterSequenciaNumerosPositivos(int[] arrayDeNumeros) {
        ArrayList<Integer> seqNumeros = new ArrayList<>();
        int qtdNumerosSequencia = 0;
        int contadorNrsPares = 0;
        int contadorNrsImpares = 0;
        double somaNrsImpares = 0;

        for (int i = 0; i < arrayDeNumeros.length; i++) {
            if (arrayDeNumeros[i] > 0) {
                seqNumeros.add(arrayDeNumeros[i]);
                qtdNumerosSequencia++;
                if (arrayDeNumeros[i] % 2 == 0) {
                    contadorNrsPares++;
                } else {
                    somaNrsImpares += arrayDeNumeros[i];
                    contadorNrsImpares++;
                }
            } else if(arrayDeNumeros[0] <= 0){
                return "O primeiro número da sequência não é positivo.";
            }else {
                break;
            }
        }

        double porcentagemNrsPares = ((double) contadorNrsPares / qtdNumerosSequencia) * 100;
        double mediaNrsImpares = somaNrsImpares / contadorNrsImpares;

        if (contadorNrsImpares == 0){
            mediaNrsImpares = 0.00;
        }

        return"Para a sequência de números "+ seqNumeros +" temos a seguinte média dos números ímpares: "+String.format("%.2f",mediaNrsImpares)+", e a seguinte porcentagem de números pares: "+String.format("%.2f",porcentagemNrsPares)+"%.";

    }

    // b) É possível especificar testes de regressão automática para esta solução.

}



