package com.company.bloco3;

import java.util.Scanner;

public class Exercicio15 {

    public static void main(String[] args) {

        Scanner read = new Scanner(System.in);
        int grade = 0;

        while (grade >= 0){

            System.out.println("Introduza a nota do aluno");
            grade = read.nextInt();

            if (grade >= 0 && grade <= 4){
                System.out.println("Nota qualitativa: Mau");
            }else if (grade >= 5 && grade <= 9){
                System.out.println("Nota qualitativa: Medíocre");
            }else if (grade >= 10 && grade <= 13){
                System.out.println("Nota qualitativa: Suficiente");
            }else if (grade >= 14 && grade <= 17){
                System.out.println("Nota qualitativa: Bom");
            }else if (grade >= 18 && grade <= 20){
                System.out.println("Nota qualitativa: Muito Bom");
            }
        }
    }
}
