package com.company.bloco3;

public class Exercicio17 {

    public static void main(String[] args) {

        double dogWeight = 13;
        double foodWeight = 250;
        verifyDogFoodWeight(dogWeight, foodWeight);

    }

    public static String verifyDogFoodWeight (double dogWeight, double foodWeight) {

        while (dogWeight >= 0) {
            if (dogWeight <= 10 && foodWeight < 100) {
                return "A quantidade de comida não é suficiente para um cão de raça pequena";
            } else if (dogWeight <= 10 && foodWeight > 100) {
                return "A quantidade de comida é demasiada para um cão de raça pequena";
            } else if (dogWeight <= 10 && foodWeight == 100) {
                return "A quantidade de comida é adequada para um cão de raça pequena";
            }

            if (dogWeight > 10 && dogWeight <= 25 && foodWeight < 250) {
                return "A quantidade de comida não é suficiente para um cão de raça média";
            } else if (dogWeight > 10 && dogWeight <= 25 && foodWeight > 250) {
                return "A quantidade de comida é demasiada para um cão de raça média";
            } else if (dogWeight > 10 && dogWeight <= 25 && foodWeight == 250) {
                return "A quantidade de comida é adequada para um cão de raça média";
            }

            if (dogWeight > 25 && dogWeight <= 45 && foodWeight < 300) {
                return "A quantidade de comida não é suficiente para um cão de raça grande";
            } else if (dogWeight > 25 && dogWeight <= 45 && foodWeight > 300) {
                return "A quantidade de comida é demasiada para um cão de raça grande";
            } else if (dogWeight > 25 && dogWeight <= 45 && foodWeight == 300) {
                return "A quantidade de comida é adequada para um cão de raça grande";
            }

            if (dogWeight > 45 && foodWeight < 500) {
                return "A quantidade de comida não é suficiente para um cão de raça gigante";
            } else if (dogWeight > 25 && foodWeight > 500) {
                return "A quantidade de comida é demasiada para um cão de raça gigante";
            } else if (dogWeight > 25 && foodWeight == 500) {
                return "A quantidade de comida é adequada para um cão de raça gigante";
            }

        }
        return "";
    }
}
