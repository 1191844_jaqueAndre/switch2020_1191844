package com.company.bloco3;

public class Exercicio11 {

    public static void main(String[] args) {

        int number = 0;
        System.out.println(getCombinations(18));

    }

    public static String getCombinations(int number) {

        int rangeStart = 1;
        int rangeEnd = 20;

        if (number > rangeEnd || number < rangeStart) {
            return "O número tem de pertencer ao intervalo de " + rangeStart + " a " + rangeEnd;
        }

        String result = "";
        int combinations = 0;

        for (int n1 = 0; n1 <= 10; n1++) {

            int differenceResult = number - n1;

            if (differenceResult >= 0 && differenceResult <= 10) {

                String variationOfCombination1 = n1 + "+" + differenceResult + ";";
                String variationOfCombination2 = differenceResult + "+" + n1 + ";";

                if (result.contains(variationOfCombination1) == false && result.contains(variationOfCombination2) == false) {
                    result = result + variationOfCombination1;
                    combinations++;
                }

            }
        }
        return result + "O número de combinações é " + combinations;
    }

/* Opção2
        result = "";
        combinations = 0;
        int max = 10;

        for (int n1 = 0; n1 <= max; n1++) {

            int differenceResult = number - n1;

            if (differenceResult >= 0 && differenceResult <= 10) {

                max = differenceResult -1;

                String variationOfCombination1 = n1 + "+" + differenceResult + ";";

                    result = result + variationOfCombination1;
                    combinations++;

            }
        }

        return result + "O número de combinações é " + combinations;
    }*/
}


