package com.company.bloco3;

import java.util.Scanner;

public class Exercicio19 {

    public static void main(String[] args) {

        int n = 1;
        long evenNumbers = 0;
        long oddNumbers = 0;
        int evenNumberCounter = 0;
        long organizedNumbers = 0;

        Scanner read = new Scanner(System.in);
        System.out.println("Insira uma sequência de números positivos de 1 a 9, premindo ENTER entre cada um");

        while (n > 0) {

            n = read.nextInt();

            if (n > 0) {
                if (n % 2 == 0) {
                    evenNumbers *= 10;
                    evenNumbers = evenNumbers + n;
                    evenNumberCounter++;

                } else {
                    oddNumbers *= 10;
                    oddNumbers = oddNumbers + n;
                }
            }
        }

        organizedNumbers = (long) (oddNumbers * Math.pow(10, evenNumberCounter) + evenNumbers);
        System.out.println(organizedNumbers);
    }
}
