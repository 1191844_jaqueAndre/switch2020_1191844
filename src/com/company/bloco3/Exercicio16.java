package com.company.bloco3;

import java.util.Scanner;

public class Exercicio16 {

    public static void main(String[] args) {

        double salaryBeforeTax = 300;
        getSalaryAfterTax(salaryBeforeTax);
    }

    public static double getSalaryAfterTax (double salaryBeforeTax){

        double salaryAfterTax = 0;

        if (salaryBeforeTax < 0){
            return -1;
        }
        if (salaryBeforeTax <= 500){
            salaryAfterTax = salaryBeforeTax * 0.9;
        } else if (salaryBeforeTax <= 1000 && salaryBeforeTax > 500){
            salaryAfterTax = salaryBeforeTax * 0.85;
        } else if (salaryBeforeTax > 1000){
            salaryAfterTax = salaryBeforeTax * 0.8;
        }

        return salaryAfterTax;
    }
}
