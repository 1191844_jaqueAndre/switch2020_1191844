package com.company.bloco3;

import java.util.Scanner;

public class Exercicio17B {

    public static void main(String[] args) {

        double dogWeight = 0;
        double foodWeight = 0;

        while (dogWeight >= 0) {

            Scanner read = new Scanner(System.in);
            System.out.println("Introduza o peso do animal");
            dogWeight = read.nextDouble();

            if (dogWeight >=0) {

                System.out.println("Introduza a quantidade de comida");
                foodWeight = read.nextDouble();

                if (dogWeight <= 10 && foodWeight < 100) {
                    System.out.println("A quantidade de comida não é suficiente para um cão de raça pequena");
                } else if (dogWeight <= 10 && foodWeight > 100) {
                    System.out.println("A quantidade de comida é demasiada para um cão de raça pequena"); ;
                } else if (dogWeight <= 10 && foodWeight == 100) {
                    System.out.println("A quantidade de comida é adequada para um cão de raça pequena"); ;
                }

                if (dogWeight > 10 && dogWeight <= 25 && foodWeight < 250) {
                    System.out.println("A quantidade de comida não é suficiente para um cão de raça média"); ;
                } else if (dogWeight > 10 && dogWeight <= 25 && foodWeight > 250) {
                    System.out.println("A quantidade de comida é demasiada para um cão de raça média"); ;
                } else if (dogWeight > 10 && dogWeight <= 25 && foodWeight == 250) {
                    System.out.println("A quantidade de comida é adequada para um cão de raça média"); ;
                }

                if (dogWeight > 25 && dogWeight <= 45 && foodWeight < 300) {
                    System.out.println("A quantidade de comida não é suficiente para um cão de raça grande"); ;
                } else if (dogWeight > 25 && dogWeight <= 45 && foodWeight > 300) {
                    System.out.println("A quantidade de comida é demasiada para um cão de raça grande"); ;
                } else if (dogWeight > 25 && dogWeight <= 45 && foodWeight == 300) {
                    System.out.println("A quantidade de comida é adequada para um cão de raça grande"); ;
                }

                if (dogWeight > 45 && foodWeight < 500) {
                    System.out.println("A quantidade de comida não é suficiente para um cão de raça gigante"); ;
                } else if (dogWeight > 25 && foodWeight > 500) {
                    System.out.println("A quantidade de comida é demasiada para um cão de raça gigante"); ;
                } else if (dogWeight > 25 && foodWeight == 500) {
                    System.out.println("A quantidade de comida é adequada para um cão de raça gigante"); ;
                }

            }

        }
    }
}