package com.company.bloco3;

import java.util.Scanner;

public class Exercicio10 {

    public static void main(String[] args) {

        int positiveNr = 0;
        int productLimit = 0;
        int acumulatedProduct = 1;
        int biggestNumber = 0;

        Scanner read = new Scanner(System.in);

        System.out.println("Introduza o limite para o produto dos números positivos");
        productLimit = read.nextInt();

        do{
            System.out.println("Introduza um número positivo");
            positiveNr = read.nextInt();
            while (positiveNr <= 0){
                System.out.println("Por favor, introduza um número positivo");
                positiveNr = read.nextInt();
            }

            acumulatedProduct = acumulatedProduct * positiveNr;

            if (biggestNumber == 0 || positiveNr > biggestNumber) {
                biggestNumber = positiveNr;
            }

        } while (acumulatedProduct < productLimit);

        System.out.println("O maior número é " + biggestNumber);
    }
}
