package com.company.bloco3;

import java.util.Scanner;

public class Exercicio2_SemArrays {

    public static void main(String[] args) {

        int nrAlunos, nota, contaPositivas = 0, contaNegativas = 0, somaNegativas = 0;
        double mediaNegativas, porcentagemPositivas;

        Scanner ler = new Scanner(System.in);
        System.out.println("Introduza o número de alunos");
        nrAlunos = ler.nextInt();

        System.out.println("Introduza as notas de cada aluno, premindo ENTER após cada uma");
        for(int i= 1; i < nrAlunos; i++){
            nota = ler.nextInt();
            if (nota >= 10){
                contaPositivas += 1;
            } else {
                contaNegativas += 1;
                somaNegativas += nota;
            }
        }

        porcentagemPositivas = contaPositivas / nrAlunos;
        mediaNegativas = somaNegativas / contaNegativas;

        System.out.println("Porcentagem de notas positivas: " + porcentagemPositivas);
        System.out.println("Média de notas negativas: " + mediaNegativas);


    }

    //b) A resolução deste exercício feita da forma acima dificulta muito a separação do input do processamento da solução.
    // Assim sendo, dificulta também a realização de testes automáticos de regressão.
}
