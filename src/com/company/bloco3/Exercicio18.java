package com.company.bloco3;

import java.util.Scanner;

public class Exercicio18 {

    public static void main(String[] args) {

        Scanner read = new Scanner(System.in);
        System.out.println("Introduza o número do cartão de Cidadão");
        int idCardNumber = read.nextInt();

        while (idCardNumber > 99999999 || idCardNumber < 10000000) {
            System.out.println("Por favor, introduza um número válido");
            idCardNumber = read.nextInt();
        }

        System.out.println("Introduza o número de controlo");
        int idCardControlNumber = read.nextInt();

        while (idCardControlNumber > 9 || idCardControlNumber < 1) {
            System.out.println("Por favor, introduza um número válido");
            idCardControlNumber = read.nextInt();
        }

        verifyIdCardNumber(idCardNumber, idCardControlNumber);


    }

    public static String verifyIdCardNumber(int idCardNumber, int idCardControlNumber) {

        if (idCardNumber > 99999999 || idCardNumber < 10000000) {
            return "Por favor, introduza um número válido";
        }
        if (idCardControlNumber > 9 || idCardControlNumber < 1) {
            return "Por favor, introduza um número válido";
        }

        int newIdCardNumber = (idCardNumber * 10) + idCardControlNumber;
        int weightedSum = 0;
        int i = 0;

        for (i = 1; i <= 9; i++) {
            weightedSum = weightedSum + (newIdCardNumber % 10) * i;
            newIdCardNumber = newIdCardNumber / 10;
        }

        System.out.println("Verificação: " + (weightedSum % 11 == 0));
        return "Verificação: " + (weightedSum % 11 == 0);

    }

}
